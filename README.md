# README #

### What is this repository for? ###

* PHP Version of the BTI Admin Tools
* Version 1.0

### How do I get set up? ###

* Requires Win install of PHP/PEAR/AzureSDK
* Configuration - All config options are in __system__\includes\admin\conf\ADMINConfig.class.php
* Pear & Azure PHP SDK
* MS SQL Copy of BTILIVE DB

### Who do I talk to? ###

* Jay Rastrick (jay@rastrick.com)
