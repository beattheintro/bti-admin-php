<?php

	// ----------------------------------------------------------- >>>>>>>>>>
    // Filename : adminDashboard.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Users Admin page
	// ----------------------------------------------------------- >>>>>>>>>>

Class adminDashboard Extends GLOBALpdoWrapper
{
	function adminDashboardConnection()
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		// Connect to the dB and return a handler
		$handler = $this->db_connect("sqlsrv", $ADMINcfg->DEFAULT_DBHOST, $ADMINcfg->SYSTEM_DBNAME, $ADMINcfg->SYSTEM_DBUSER, $ADMINcfg->SYSTEM_DBPASSWORD);
		return $handler;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getDashboardData($handler)
	{
		global $ADMINcfg, $STDlib, $SITEsession;

		$DATA['TRACKS'] = $this->_getTrackDashboard($handler);
		$DATA['PACKS'] = $this->_getPacksDashboard($handler);
		$DATA['QUESTIONS'] = $this->_getQuestionsDashboard($handler);
		$DATA['THEMES'] = $this->_getThemesDashboard($handler);
		$DATA['PLAYERS'] = $this->_getPlayersDashboard($handler);
		
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// PRIVATE METHODS
	// ----------------------------------------------------------- >>>>>>>>>>
	
	function _getTrackDashboard($handler)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		
		$query = "SELECT COUNT(*) FROM dbo.Tracks";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getTrackDashboard_numTracks');
		$row = $this->db_fetch_row($result);
		$DATA["num"] = $row["0"];

		$query = "SELECT COUNT(*) FROM dbo.UserTrackLikes";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getTrackDashboard_numLikes');
		$row = $this->db_fetch_row($result);
		$DATA["numLikes"] = $row["0"];
		
		return $DATA;		
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	
	function _getPacksDashboard($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$query = "SELECT COUNT(*) FROM dbo.Packs";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getPacksDashboard');
		$row = $this->db_fetch_row($result);
		$DATA["num"] = $row["0"];

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function _getThemesDashboard($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$query = "SELECT COUNT(*) FROM dbo.Themes";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getThemesDashboard');
		$row = $this->db_fetch_row($result);
		$DATA["num"] = $row["0"];

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function _getQuestionsDashboard($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$query = "SELECT COUNT(*) FROM dbo.Questions";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getQuestionsDashboard');
		$row = $this->db_fetch_row($result);
		$DATA["num"] = $row["0"];

		return $DATA;
	}
	
	// ----------------------------------------------------------- >>>>>>>>>>

	function _getPlayersDashboard($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$query = "SELECT COUNT (*) FROM dbo.Users WHERE dbo.Users.IsAnonymous = '0'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getPlayersDashboard_REAL');
		$row = $this->db_fetch_row($result);
		$DATA["numReal"] = $row["0"];

		$query = "SELECT COUNT (*) FROM dbo.Users WHERE dbo.Users.IsAnonymous = '1'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getPlayersDashboard_ANON');
		$row = $this->db_fetch_row($result);
		$DATA["numAnon"] = $row["0"];

		$query = "SELECT COUNT (*) FROM dbo.Users WHERE dbo.Users.EmailValidated = '1'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getPlayersDashboard_VALIDATED');
		$row = $this->db_fetch_row($result);
		$DATA["numValidated"] = $row["0"];
		
		$query = "SELECT Sum(dbo.Users.Aggr_GamesWon) AS GamesWon FROM dbo.Users";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getPlayersDashboard_GamesWon');
		$row = $this->db_fetch_row($result);
		$DATA["numGamesWon"] = $row["0"];

		$query = "SELECT Sum(dbo.Users.Aggr_GamesLost) AS GamesLost FROM dbo.Users";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getPlayersDashboard_GamesLost');
		$row = $this->db_fetch_row($result);
		$DATA["numGamesLost"] = $row["0"];

		$query = "SELECT Sum(dbo.UserEarn.Discs) AS DiscsEarned FROM dbo.UserEarn";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getPlayersDashboard_DiscsEarned');
		$row = $this->db_fetch_row($result);
		$DATA["numDiscsEarned"] = $row["0"];

		$query = "SELECT Sum(dbo.UserSpend.Discs) AS DiscsSpent FROM dbo.UserSpend";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'DASH_getPlayersDashboard_DiscsSpent');
		$row = $this->db_fetch_row($result);
		$DATA["numDiscsSpent"] = $row["0"];
		
		return $DATA;
	}
	
	// ----------------------------------------------------------- >>>>>>>>>>

	function _safeVar($var)
	{
		global $ADMINcfg, $STDlib, $SITEsession;

		return $STDlib->makeSafe($var);

	}

	// ----------------------------------------------------------- >>>>>>>>>>
}

?>