<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminPacks.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Packs Methods
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $_SERVER["DOCUMENT_ROOT"].'/__system__/WindowsAzure/WindowsAzure.php';

use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Blob\Models;

Class adminPacks Extends GLOBALpdoWrapper
{
	function packsConnection()
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		// Connect to the dB and return a handler
		$handler = $this->db_connect("sqlsrv", $ADMINcfg->DEFAULT_DBHOST, $ADMINcfg->SYSTEM_DBNAME, $ADMINcfg->SYSTEM_DBUSER, $ADMINcfg->SYSTEM_DBPASSWORD);
		return $handler;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Pack Methods
	// ----------------------------------------------------------- >>>>>>>>>>

	function packsInfoBarData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$query = "SELECT COUNT(*) FROM dbo.Packs";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_packsInfoBarData');
		$row = $this->db_fetch_row($result);
		$DATA["numPacks"] = $row["0"];
        $_SESSION['PACKS']['numPacks'] = $DATA["numPacks"];

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function getPacks($handler, $startAt=0)
    {
        global $ADMINcfg, $SITEsession, $STDlib;

        $DATA = array();
        $query = '';
		       
        $query = "SELECT
								dbo.Packs.Id,
								dbo.Packs.Name,
								dbo.Packs.Created,
								dbo.Packs.Description,
								dbo.Packs.Active
							FROM
								dbo.Packs
							ORDER BY dbo.Packs.Created OFFSET ".$startAt." ROWS FETCH NEXT ".$ADMINcfg->MAX_DB_RESULTS." ROWS ONLY";
        
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_getPacks');
               
        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Description'] = $row['Description'];
                $DATA[$i]['Active'] = $row['Active'];
                $i++;
           }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;
    }
    
	// ----------------------------------------------------------- >>>>>>>>>>
    
    function getPacksBySearch($handler, $searchVars)
    {
        global $ADMINcfg, $SITEsession, $STDlib;

        $DATA = array();
        $query = '';
        
        $searchTerm = $searchVars['search_term'];
		$strLength = strlen($searchTerm);
        $DATA['search_term'] = $searchTerm;
        
        if(empty($searchTerm) || ($strLength < 2)) 
		{
            $DATA['error'] = 'No Data!';
            return $DATA;
		}

		// This is a search for a specific pack name
		$query = "SELECT
								dbo.Packs.Id,
								dbo.Packs.Name,
								dbo.Packs.Created,
								dbo.Packs.Description,
								dbo.Packs.CostToUnlock,
								dbo.Packs.Active,
								dbo.Packs.[Order],
								dbo.Packs.IsFreeToPlay
							FROM
								dbo.Packs
							WHERE
								dbo.Packs.Name LIKE '%".$this->_safeVar($searchTerm)."%' ";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_getPacksBySearch');

		if($result)
		{
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Description'] = $row['Description'];
                $DATA[$i]['CostToUnlock'] = $row['CostToUnlock'];
                $DATA[$i]['Active'] = $row['Active'];
                $DATA[$i]['Order'] = $row['Order'];
                $DATA[$i]['IsFreeToPlay'] = $row['IsFreeToPlay'];
                $i++;
           }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }

        return $DATA;
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>
    
	function getPackData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";

		if(!isset($_GET['Id']))
		{
			$DATA['error'] = 'No Data!';
			return $DATA;
		}

		// Get the pack
		$query = "SELECT
								dbo.Packs.Id,
								dbo.Packs.Name,
								dbo.Packs.Created,
								dbo.Packs.Description,
								dbo.Packs.CostToUnlock,
								dbo.Packs.Active,
								dbo.Packs.[Order],
								dbo.Packs.IsFreeToPlay
							FROM
								dbo.Packs
							WHERE
								dbo.Packs.Id = '".$this->_safeVar($_GET['Id'])."'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_getPackData_Pack');

		if($result)
		{
			$row = $this->db_fetch_array($result);
            $date = date_create($row['Created']);
            
            $DATA['PACK']['Id'] = $row['Id'];
            $DATA['PACK']['Created'] = date_format($date, 'd/m/Y');
            $DATA['PACK']['Name'] = $row['Name'];
            $DATA['PACK']['Description'] = $row['Description'];
            $DATA['PACK']['CostToUnlock'] = $row['CostToUnlock'];
            $DATA['PACK']['Active'] = $row['Active'];
            $DATA['PACK']['Order'] = $row['Order'];
            $DATA['PACK']['IsFreeToPlay'] = $row['IsFreeToPlay'];

			if(empty($DATA['PACK']['Id']))
			{
				$DATA['error'] = 'No Data!';
			} else {
                // Get the associated packs
                
                $packId = $DATA['PACK']['Id'];
                $DATA['QUESTIONS'] = $this->_getQuestionsForId($handler, $packId);
            }
		} else {
			$DATA['error'] = 'No Data!';
			return $DATA;
		}

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getPackEditData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		
		// Get the question data to edit
		$DATA = $this->getPackData($handler);
		
		return $DATA;		
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getPackAddData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		
		// Get the question data to add
		$DATA['QUESTIONS'] = $this->_getQuestions($handler);
		
		return $DATA;		
	}
	
	// ----------------------------------------------------------- >>>>>>>>>>

	function getAllPacksData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";

		// Get the track
		$query = "SELECT
						dbo.Packs.Id,
						dbo.Packs.Name,
						dbo.Packs.Description,
						dbo.Packs.Active
					FROM
						dbo.Packs
					ORDER BY
						dbo.Packs.Name ASC";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_getAllPacksData');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Description'] = $row['Description'];
                $DATA[$i]['Active'] = $row['Active'];
                $i++;
			}
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function addPackData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$packId = $STDlib->guid();
        $Name = $_POST['Name'];
        $Description = $_POST['Description'];
        $CostToUnlock = $_POST['CostToUnlock'];
        $Order = $_POST['Order'];
        $Active = $_POST['Active'];
        $IsFreeToPlay = $_POST['IsFreeToPlay'];
		
		$addQuestion = $_POST['addQuestion'];
        
        if($Active == 'on')
        {
            $Active = "1";
        }
		
		if(empty($Active)) 
		{
            $Active = "0";
        }

        if($IsFreeToPlay == 'on')
        {
            $IsFreeToPlay = "1";
        }
		
		if(empty($IsFreeToPlay))
		{
            $IsFreeToPlay = "0";
        }
		
        $gUID = strtolower($packId);
		
        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a thumbnail
            $pngTempFile = $_FILES['content_asset']['tmp_name']['thumb'];
            $fileName = 'mobile/'.$gUID.'.png';
            $imgTmp = imagecreatefrompng($pngTempFile);
			
			imageAlphaBlending($imgTmp, true);
			imageSaveAlpha($imgTmp, true);
            
            ob_start();
            imagepng($imgTmp, NULL, 0);
            $imageBlob = ob_get_contents();
            ob_end_clean();
            
            // Free up memory
            imagedestroy($imgTmp);
            
            $DATA['AZURE']['THUMB'] = $this->_azureSaveAssetToBlob($imageBlob, $fileName, 'packimages', "image/png");
        }

        // Run an insert query with Pack data!
        $query = "INSERT INTO dbo.Packs (
												 Id,
												 Name,
												 Created,
												 Description,
												 CostToUnlock,
												 Active,
												 [Order],
												 IsFreeToPlay												 
												)                         
											   VALUES (
												 '".$this->_safeVar($packId)."',
												 '".$this->_safeVar($Name)."',
												 GETDATE(),
												 '".$this->_safeVar($Description)."',
												 '".$this->_safeVar($CostToUnlock)."',
												 '".$this->_safeVar($Active)."',
												 '".$this->_safeVar($Order)."',
												 '".$this->_safeVar($IsFreeToPlay)."'
											   )";
		
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_addPackData_ADD_PACK');
		
		// Add the associated questions if needed
		if(is_array($addQuestion))
		{
			// Add the questions
			for($i=0; isset($addQuestion[$i]); $i++)
			{
				$thisGUID = $STDlib->guid();
				$QuestionId = $addQuestion[$i];
				
				$query = "INSERT INTO dbo.PackQuestions (
										Id,
										PackId,
										QuestionId,
										Created
										)
								 VALUES (
										'".$this->_safeVar($thisGUID)."',
										'".$this->_safeVar($packId)."',
										'".$this->_safeVar($QuestionId)."',
										GETDATE()
								)";
				$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_addPackData_ADD_QUESTION_'.$i);
			}
		}
        
        if($result)
        {
            $DATA['DB'] = 'Success!';
        } else {
            $DATA['error'] = 'Add Failed!';
        }
		
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
	function updatePackData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$packId = $_POST['Id'];
        $Name = $_POST['Name'];
        $Description = $_POST['Description'];
        $CostToUnlock = $_POST['CostToUnlock'];
        $Order = $_POST['Order'];
        $Active = $_POST['Active'];
        $IsFreeToPlay = $_POST['IsFreeToPlay'];
		
		$removeQuestion = $_POST['removeQuestion'];
        
        if($Active == 'on')
        {
            $Active = "1";
        }
		
		if(empty($Active)) 
		{
            $Active = "0";
        }

        if($IsFreeToPlay == 'on')
        {
            $IsFreeToPlay = "1";
        }
		
		if(empty($IsFreeToPlay))
		{
            $IsFreeToPlay = "0";
        }
		
        $gUID = strtolower($packId);
		
        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a new thumbnail
            $pngTempFile = $_FILES['content_asset']['tmp_name']['thumb'];
            $fileName = 'mobile/'.$gUID.'.png';
            $imgTmp = imagecreatefrompng($pngTempFile);
			
			imageAlphaBlending($imgTmp, true);
			imageSaveAlpha($imgTmp, true);
            
            ob_start();
            imagepng($imgTmp, NULL, 0);
            $imageBlob = ob_get_contents();
            ob_end_clean();
            
            // Free up memory
            imagedestroy($imgTmp);
            
            $DATA['AZURE']['THUMB'] = $this->_azureSaveAssetToBlob($imageBlob, $fileName, 'packimages', "image/png");
        }
		
		// Update the associated questions if needed
		if(is_array($removeQuestion))
		{
			// Clear out the questions
			for($i=0; isset($removeQuestion[$i]); $i++)
			{
				$gUID = $STDlib->guid();
				$QuestionId = $removeQuestion[$i];
				
				$query = "DELETE FROM 
									dbo.PackQuestions
								WHERE
									dbo.PackQuestions.QuestionId = '".$this->_safeVar($QuestionId)."'";
				$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_updatePackData_DELETE_QUESTION_'.$i);
			}
		}

        // Run an update query with Pack data!
        $query = "UPDATE dbo.Packs
										SET 
											dbo.Packs.Name = '".$this->_safeVar($Name)."',
											dbo.Packs.Description = '".$this->_safeVar($Description)."',
											dbo.Packs.CostToUnlock = '".$this->_safeVar($CostToUnlock)."',
											dbo.Packs.Active = '".$this->_safeVar($Active)."',
											dbo.Packs.[Order] = '".$this->_safeVar($Order)."',
											dbo.Packs.IsFreeToPlay = '".$this->_safeVar($IsFreeToPlay)."'
										WHERE
											dbo.Packs.Id = '".$packId."'";
		
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_updatePackData_UPDATE_PACK');
        
        if($result)
        {
            $DATA['DB'] = 'Success!';
        } else {
            $DATA['error'] = 'Update Failed!';
        }
		
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function deletePackData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        /** TO DO
         * Implement a deletion method based on flags in the db or actually deleting files.
         * 
         * 
         */


		//return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// PRIVATE METHODS
	// ----------------------------------------------------------- >>>>>>>>>>

    function _getQuestionsForId($handler, $packId)
    {
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";
        
        $query = "SELECT
								dbo.PackQuestions.Id,
								dbo.PackQuestions.QuestionId,
								CorrectTrack.Title AS CorrectTitle,
								CorrectTrack.Artist AS CorrectArtist,
								WrongTrack1.Title AS WrongTitle1,
								WrongTrack1.Artist AS WrongArtist1,
								WrongTrack2.Title AS WrongTitle2,
								WrongTrack2.Artist AS WrongArtist2
							FROM
								dbo.PackQuestions
							INNER JOIN dbo.Questions ON dbo.PackQuestions.QuestionId = dbo.Questions.Id
							INNER JOIN dbo.Tracks AS CorrectTrack ON dbo.Questions.CorrectTrackId = CorrectTrack.Id
							INNER JOIN dbo.Tracks AS WrongTrack1 ON dbo.Questions.WrongTrackId1 = WrongTrack1.Id
							INNER JOIN dbo.Tracks AS WrongTrack2 ON dbo.Questions.WrongTrackId2 = WrongTrack2.Id
							WHERE
								dbo.PackQuestions.PackId = '".$packId."'
							ORDER BY
								CorrectArtist ASC";
        
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_getQuestionsForId');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {

                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['QuestionId'] = $row['QuestionId'];
                $DATA[$i]['CorrectTitle'] = $row['CorrectTitle'];
                $DATA[$i]['CorrectArtist'] = $row['CorrectArtist'];
                $DATA[$i]['WrongTitle1'] = $row['WrongTitle1'];
                $DATA[$i]['WrongArtist1'] = $row['WrongArtist1'];
                $DATA[$i]['WrongTitle2'] = $row['WrongTitle2'];
                $DATA[$i]['WrongArtist2'] = $row['WrongArtist2'];
                $i++;
            }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;        
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>

    function _getQuestions($handler)
    {
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";
        
        $query = "SELECT
								dbo.PackQuestions.Id,
								dbo.PackQuestions.QuestionId,
								CorrectTrack.Title AS CorrectTitle,
								CorrectTrack.Artist AS CorrectArtist,
								WrongTrack1.Title AS WrongTitle1,
								WrongTrack1.Artist AS WrongArtist1,
								WrongTrack2.Title AS WrongTitle2,
								WrongTrack2.Artist AS WrongArtist2
							FROM
								dbo.PackQuestions
							INNER JOIN dbo.Questions ON dbo.PackQuestions.QuestionId = dbo.Questions.Id
							INNER JOIN dbo.Tracks AS CorrectTrack ON dbo.Questions.CorrectTrackId = CorrectTrack.Id
							INNER JOIN dbo.Tracks AS WrongTrack1 ON dbo.Questions.WrongTrackId1 = WrongTrack1.Id
							INNER JOIN dbo.Tracks AS WrongTrack2 ON dbo.Questions.WrongTrackId2 = WrongTrack2.Id
							ORDER BY
								CorrectArtist ASC";
        
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'PACKS_getQuestions');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {

                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['QuestionId'] = $row['QuestionId'];
                $DATA[$i]['CorrectTitle'] = $row['CorrectTitle'];
                $DATA[$i]['CorrectArtist'] = $row['CorrectArtist'];
                $DATA[$i]['WrongTitle1'] = $row['WrongTitle1'];
                $DATA[$i]['WrongArtist1'] = $row['WrongArtist1'];
                $DATA[$i]['WrongTitle2'] = $row['WrongTitle2'];
                $DATA[$i]['WrongArtist2'] = $row['WrongArtist2'];
                $i++;
            }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;        
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>
	
    function _azureSaveAssetToBlob($blobFileContents, $blobFileName, $blobContainer, $mimeType)
    {
        global $ADMINcfg, $SITEsession, $STDlib;
        
        // Create blob REST proxy.
        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($ADMINcfg->AZURE_CONNECTION_STR);

        $options = new Models\CreateBlobOptions();
        $options->setBlobContentType($mimeType);
		
        try {
            // Upload blob
            $blobRestProxy->createBlockBlob($blobContainer, $blobFileName, $blobFileContents, $options);
            $DATA = 'Success!';
        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here: 
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $errCode = $e->getCode();
            $errMessage = $e->getMessage();
            $DATA =  $errCode.": ".$errMessage;
        }
        
        return $DATA;
    }

	// ----------------------------------------------------------- >>>>>>>>>>
	
	function _safeVar($var)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		return $STDlib->makeQuerySafe($var);
	}

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>