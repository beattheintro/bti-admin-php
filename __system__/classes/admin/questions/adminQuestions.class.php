<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminQuestions.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Questions Methods
	// ----------------------------------------------------------- >>>>>>>>>>

Class adminQuestions Extends GLOBALpdoWrapper
{
	function questionsConnection()
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		// Connect to the dB and return a handler
		$handler = $this->db_connect("sqlsrv", $ADMINcfg->DEFAULT_DBHOST, $ADMINcfg->SYSTEM_DBNAME, $ADMINcfg->SYSTEM_DBUSER, $ADMINcfg->SYSTEM_DBPASSWORD);
		return $handler;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Question Methods
	// ----------------------------------------------------------- >>>>>>>>>>

	function questionsInfoBarData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$query = "SELECT COUNT(*) FROM dbo.Questions";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_questionsInfoBarData');
		$row = $this->db_fetch_row($result);
		$DATA["numQuestions"] = $row["0"];
        $_SESSION['QUESTIONS']['numQuestions'] = $DATA["numQuestions"];

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function getQuestions($handler, $startAt=0)
    {
        global $ADMINcfg, $SITEsession, $STDlib;

        $DATA = array();
        $query = '';
        
        $query = "SELECT
	                        dbo.Questions.Id,
	                        dbo.Questions.Created,
                            Tracks1.Id AS CorrectId,
	                        Tracks1.Title AS CorrectTitle,
	                        Tracks1.GotMP3 AS HasMP3,
	                        Tracks1.GotMP4 AS HasMP4,                            
                            Tracks2.Id AS WrongId1,
	                        Tracks2.Title AS WrongTitle1,
                            Tracks3.Id AS WrongId2,
	                        Tracks3.Title AS WrongTitle2
                        FROM
	                        dbo.Questions
                        INNER JOIN dbo.Tracks AS Tracks1 ON dbo.Questions.CorrectTrackId = Tracks1.Id
                        INNER JOIN dbo.Tracks AS Tracks2 ON dbo.Questions.WrongTrackId1 = Tracks2.Id
                        INNER JOIN dbo.Tracks AS Tracks3 ON dbo.Questions.WrongTrackId2 = Tracks3.Id
                        ORDER BY dbo.Questions.Created OFFSET ".$startAt." ROWS FETCH NEXT ".$ADMINcfg->MAX_DB_RESULTS." ROWS ONLY";
        
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_getQuestions');
               
        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['CorrectId'] = $row['CorrectId'];
                $DATA[$i]['CorrectTitle'] = $row['CorrectTitle'];
                $DATA[$i]['HasMP3'] = $row['HasMP3'];
                $DATA[$i]['HasMP3'] = $row['HasMP3'];
                $DATA[$i]['WrongId1'] = $row['WrongId1'];
                $DATA[$i]['WrongTitle1'] = $row['WrongTitle1'];
                $DATA[$i]['WrongId2'] = $row['WrongId2'];
                $DATA[$i]['WrongTitle2'] = $row['WrongTitle2'];
                $i++;
           }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;
    }
    
	// ----------------------------------------------------------- >>>>>>>>>>
    
    function getQuestionsBySearch($handler, $searchVars)
    {
        global $ADMINcfg, $SITEsession, $STDlib;

        $DATA = array();
        $query = '';
        
        $searchTerm = $searchVars['search_term'];
		$strLength = strlen($searchTerm);
        $DATA['search_term'] = $searchTerm;
        
        if(empty($searchTerm) || ($strLength < 2)) 
		{
            $DATA['error'] = 'No Data!';
            return $DATA;
		}
		
            // This is a search for a specific title or artist
            $query = "SELECT
								dbo.Questions.Id,
								dbo.Questions.Created,
								Tracks1.Id AS CorrectId,
								Tracks1.Title AS CorrectTitle,
								Tracks1.GotMP3 AS HasMP3,
								Tracks1.GotMP4 AS HasMP4,
								Tracks2.Id AS WrongId1,
								Tracks2.Title AS WrongTitle1,
								Tracks3.Id AS WrongId2,
								Tracks3.Title AS WrongTitle2
							FROM
								dbo.Questions
							INNER JOIN dbo.Tracks AS Tracks1 ON dbo.Questions.CorrectTrackId = Tracks1.Id
							INNER JOIN dbo.Tracks AS Tracks2 ON dbo.Questions.WrongTrackId1 = Tracks2.Id
							INNER JOIN dbo.Tracks AS Tracks3 ON dbo.Questions.WrongTrackId2 = Tracks3.Id
							WHERE
								Tracks1.Title LIKE '%".$this->_safeVar($searchTerm)."%' 
							OR
								Tracks2.Title LIKE '%".$this->_safeVar($searchTerm)."%' 
							OR
								Tracks3.Title LIKE '%".$this->_safeVar($searchTerm)."%' ";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_getQuestionsBySearch');
        
        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['CorrectId'] = $row['CorrectId'];
                $DATA[$i]['CorrectTitle'] = $row['CorrectTitle'];
                $DATA[$i]['HasMP3'] = $row['HasMP3'];
                $DATA[$i]['HasMP4'] = $row['HasMP4'];
                $DATA[$i]['WrongId1'] = $row['WrongId1'];
                $DATA[$i]['WrongTitle1'] = $row['WrongTitle1'];
                $DATA[$i]['WrongId2'] = $row['WrongId2'];
                $DATA[$i]['WrongTitle2'] = $row['WrongTitle2'];
                $i++;
           }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>
    
	function getQuestionData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";

		if(!isset($_GET['Id']))
		{
			$DATA['error'] = 'No Data!';
			return $DATA;
		}

		// Get the track
		$query = "SELECT
	                    dbo.Questions.Id,
	                    dbo.Questions.Created,
	                    Tracks1.Id AS CorrectId,
	                    Tracks1.Title AS CorrectTitle,
	                    Tracks1.Artist AS CorrectArtist,
	                    Tracks1.GotMP3 AS HasMP3,
	                    Tracks1.GotMP4 AS HasMP4,
	                    Tracks2.Id AS WrongId1,
	                    Tracks2.Title AS WrongTitle1,
	                    Tracks2.Artist AS WrongArtist1,
	                    Tracks3.Id AS WrongId2,
	                    Tracks3.Title AS WrongTitle2,
	                    Tracks3.Artist AS WrongArtist2
                    FROM
	                    dbo.Questions
                    INNER JOIN dbo.Tracks AS Tracks1 ON dbo.Questions.CorrectTrackId = Tracks1.Id
                    INNER JOIN dbo.Tracks AS Tracks2 ON dbo.Questions.WrongTrackId1 = Tracks2.Id
                    INNER JOIN dbo.Tracks AS Tracks3 ON dbo.Questions.WrongTrackId2 = Tracks3.Id
                    WHERE
	                    dbo.Questions.Id = '".$this->_safeVar($_GET['Id'])."'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_getQuestionData_Question');

		if($result)
		{
			$row = $this->db_fetch_array($result);
            $date = date_create($row['Created']);
            
            $DATA['QUESTION']['Id'] = $row['Id'];
            $DATA['QUESTION']['Created'] = date_format($date, 'd/m/Y');
            $DATA['QUESTION']['CorrectId'] = $row['CorrectId'];
            $DATA['QUESTION']['CorrectTitle'] = $row['CorrectTitle'];
            $DATA['QUESTION']['CorrectArtist'] = $row['CorrectArtist'];
            $DATA['QUESTION']['HasMP3'] = $row['HasMP3'];
            $DATA['QUESTION']['HasMP4'] = $row['HasMP4'];
            $DATA['QUESTION']['WrongId1'] = $row['WrongId1'];
            $DATA['QUESTION']['WrongTitle1'] = $row['WrongTitle1'];
            $DATA['QUESTION']['WrongArtist1'] = $row['WrongArtist1'];
            $DATA['QUESTION']['WrongId2'] = $row['WrongId2'];
            $DATA['QUESTION']['WrongTitle2'] = $row['WrongTitle2'];
            $DATA['QUESTION']['WrongArtist2'] = $row['WrongArtist2'];

			if(empty($DATA['QUESTION']['Id']))
			{
				$DATA['error'] = 'No Data!';
			} else {
                // Get the associated questions
                
                $questionId = $DATA['QUESTION']['Id'];                
                $DATA['PACKS'] = $this->_getPacksForId($handler, $questionId);
            }
		} else {
			$DATA['error'] = 'No Data!';
			return $DATA;
		}

		return $DATA;

	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getQuestionEditData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		
		// Get the question data to edit
		$DATA = $this->getQuestionData($handler);
		$DATA['TRACK_ARTISTS'] = $this->_getTrackArtistsForSelectBox($handler, TRUE);
		$DATA['TRACK_ARTISTS_ALL'] = $this->_getTrackArtistsForSelectBox($handler, FALSE);
		
		return $DATA;		
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getQuestionAddData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		
		// Get the question data to add
		//$DATA = $this->getQuestionData($handler);
		$DATA['TRACK_ARTISTS'] = $this->_getTrackArtistsForSelectBox($handler, TRUE);
		$DATA['TRACK_ARTISTS_ALL'] = $this->_getTrackArtistsForSelectBox($handler, FALSE);
		
		return $DATA;		
	}
	
	// ----------------------------------------------------------- >>>>>>>>>>

	function getAllPacksData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";

		// Get the track
		$query = "SELECT
						dbo.Packs.Id,
						dbo.Packs.Name,
						dbo.Packs.Description,
						dbo.Packs.Active
					FROM
						dbo.Packs
					ORDER BY
						dbo.Packs.Name ASC";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_getAllPacksData');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Description'] = $row['Description'];
                $DATA[$i]['Active'] = $row['Active'];
                $i++;
			}
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function addQuestionData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

        $questionId = $STDlib->guid();
        $correctTrackId = $_POST['trackCorrectId'];
        $wrongTrackId1 = $_POST['trackWrongId1'];
        $wrongTrackId2 = $_POST['trackWrongId2'];
        $selectedPacks = $_POST['packs'];

		// Generate a a rand() number for the Shuffler
		mt_srand();
		$Shuffler = mt_rand();

        // Run an insert query with all data!        
        $query = "INSERT INTO dbo.Questions (
												 Id,
												 Created,
												 CorrectTrackId,
												 WrongTrackId1,
												 WrongTrackId2,
												 Shuffler
												)                         
											   VALUES (
												 '".$this->_safeVar($questionId)."',
												 GETDATE(),
												 '".$this->_safeVar($correctTrackId)."',
												 '".$this->_safeVar($wrongTrackId1)."',
												 '".$this->_safeVar($wrongTrackId2)."',
												 '".$this->_safeVar($Shuffler)."'
											   )";
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_addQuestionData_ADD');

        if($result)
        {
            $DATA['DB'] = 'Success!';
            $DATA['Id'] = $questionId;
        } else {
            $DATA['error'] = 'Update Failed!';
        }
		
		// Add question to Packs - Foriegn Key Constraints in effect!
		if(is_array($selectedPacks) && isset($DATA['DB']))
		{
			for($i=0; isset($selectedPacks[$i]); $i++)
			{
				$thisGUID = $STDlib->guid();
				$query = "INSERT INTO dbo.PackQuestions (
															 Id,
															 PackId,
															 QuestionId,
															 Created
															)
														   VALUES (
															 '".$thisGUID."',
															 '".$this->_safeVar($selectedPacks[$i])."',
															 '".$this->_safeVar($questionId)."',
															 GETDATE()
														   )";
				$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_addQuestionData_ADD_PACKS_'.$thisGUID);
			}
		}
		                
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>   
    
	function updateQuestionData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

        $questionId = $_POST['Id'];
        $correctTrackId = $_POST['trackCorrectId'];
        $wrongTrackId1 = $_POST['trackWrongId1'];
        $wrongTrackId2 = $_POST['trackWrongId2'];
        $selectedPacks = $_POST['packs'];
        
		// Assume the packs have been changed so delete them & re-add them.
		$query = "DELETE
							FROM
								dbo.PackQuestions
							WHERE
								dbo.PackQuestions.QuestionId = '".$questionId."'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_updateQuestionData_CLEAN_PACKS');
		
		if(is_array($selectedPacks))
		{
			for($i=0; isset($selectedPacks[$i]); $i++)
			{
				$thisGUID = $STDlib->guid();
				$query = "INSERT INTO dbo.PackQuestions (
															 Id,
															 PackId,
															 QuestionId,
															 Created
															)
														   VALUES (
															 '".$thisGUID."',
															 '".$this->_safeVar($selectedPacks[$i])."',
															 '".$this->_safeVar($questionId)."',
															 GETDATE()
														   )";
				$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_updateQuestionData_ADD_PACKS_'.$thisGUID);
			}
		}
     
        // Run an update query        
        $query = "UPDATE dbo.Questions
						   SET 
							 dbo.Questions.CorrectTrackId='".$this->_safeVar($correctTrackId)."',
							 dbo.Questions.WrongTrackId1='".$this->_safeVar($wrongTrackId1)."',
							 dbo.Questions.WrongTrackId2='".$this->_safeVar($wrongTrackId2)."'
						   WHERE
							 dbo.Questions.Id='".$questionId."'";
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_updateQuestionData_UPDATE');
        
        if($result)
        {
            $DATA['DB'] = 'Success!';
        } else {
            $DATA['error'] = 'Update Failed!';
        }
        
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function deleteQuestionData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        /** TO DO
         * Implement a deletion method based on flags in the db or actually deleting files.
         * 
         * 
         */


		//return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// PRIVATE METHODS
	// ----------------------------------------------------------- >>>>>>>>>>

    function _getPacksForId($handler, $questionId)
    {
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";
        
        $query = "SELECT
							dbo.Packs.Id,
							dbo.Packs.Created,
							dbo.Packs.Name,
							dbo.Packs.Description
						FROM
							dbo.PackQuestions
						INNER JOIN dbo.Packs ON dbo.PackQuestions.PackId = dbo.Packs.Id
						WHERE
							dbo.PackQuestions.QuestionId = '".$questionId."'";
        
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_getPacksForId');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                $date = date_create($row['Created']);

                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Description'] = $row['Description'];
                $i++;
            }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;
        
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>

	function _getTrackArtistsForSelectBox($handler, $hasAssetCheck)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";

		// Get the track
		if($hasAssetCheck)
		{
			$query = "SELECT DISTINCT
									dbo.Tracks.Artist
								FROM
									dbo.Tracks
								WHERE
									dbo.Tracks.GotMP3 = '1'
								OR 
									dbo.Tracks.GotMP4 = '1'
								ORDER BY
									dbo.Tracks.Artist ASC";
		} else {
			$query = "SELECT DISTINCT
									dbo.Tracks.Artist
								FROM
									dbo.Tracks
								ORDER BY
									dbo.Tracks.Artist ASC";
		}
		
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_getTrackArtistsForSelectBox_'.$hasAssetCheck);

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $DATA[$i]['Artist'] = $row['Artist'];
                $i++;
			}
            
            if(!isset($DATA['0']['Artist']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function _getTrackTitlesForSelectBox($handler, $searchArtist, $hasAssetCheck)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";
		
		// Get the track
		if($hasAssetCheck)
		{
			$query = "SELECT
									dbo.Tracks.Id,
									dbo.Tracks.Title
								FROM
									dbo.Tracks
								WHERE
									dbo.Tracks.Artist = '".$this->_safeVar($searchArtist)."'
								AND (
									dbo.Tracks.GotMP3 = '1'
									OR dbo.Tracks.GotMP4 = '1'
								)
								ORDER BY
									dbo.Tracks.Title ASC";
		} else {
			$query = "SELECT
									dbo.Tracks.Id,
									dbo.Tracks.Title
								FROM
									dbo.Tracks
								WHERE
									dbo.Tracks.Artist = '".$this->_safeVar($searchArtist)."'
								ORDER BY
									dbo.Tracks.Title ASC";
		}
		
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'QUESTIONS_getTrackTitlesForSelectBox_'.$hasAssetCheck);

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Data'] = $row['Title'];
                $i++;
			}
            
            if(!isset($DATA['0']['Title']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	
	function _safeVar($var)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		return $STDlib->makeQuerySafe($var);
	}

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>