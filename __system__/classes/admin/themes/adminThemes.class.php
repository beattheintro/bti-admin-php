<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminThemes.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Themes Methods
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $_SERVER["DOCUMENT_ROOT"].'/__system__/WindowsAzure/WindowsAzure.php';

use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Blob\Models;

Class adminThemes Extends GLOBALpdoWrapper
{
	function themesConnection()
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		// Connect to the dB and return a handler
		$handler = $this->db_connect("sqlsrv", $ADMINcfg->DEFAULT_DBHOST, $ADMINcfg->SYSTEM_DBNAME, $ADMINcfg->SYSTEM_DBUSER, $ADMINcfg->SYSTEM_DBPASSWORD);
		return $handler;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Theme Methods
	// ----------------------------------------------------------- >>>>>>>>>>

	function themesInfoBarData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$query = "SELECT COUNT(*) FROM dbo.Themes";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_themesInfoBarData');
		$row = $this->db_fetch_row($result);
		$DATA["numThemes"] = $row["0"];
        $_SESSION['THEMES']['numThemes'] = $DATA["numThemes"];

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function getThemes($handler, $startAt=0)
    {
        global $ADMINcfg, $SITEsession, $STDlib;

        $DATA = array();
        $query = '';
		       
        $query = "SELECT
								dbo.Themes.Id,
								dbo.Themes.Name,
								dbo.Themes.Created,
								dbo.Themes.Shortname
							FROM
								dbo.Themes
							ORDER BY dbo.Themes.Created OFFSET ".$startAt." ROWS FETCH NEXT ".$ADMINcfg->MAX_DB_RESULTS." ROWS ONLY";
        
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_getThemes');
               
        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Shortname'] = $row['Shortname'];

				// Get the number of packs in this theme
				$subQuery = "SELECT
											COUNT (dbo.ThemePacks.PackId)
										FROM
											dbo.ThemePacks
										WHERE
											dbo.ThemePacks.ThemeId = '".$row['Id']."'";
				
				$subResult = $this->db_query($subQuery, FALSE, FALSE, FALSE, $handler, 'THEMES_getThemes_NUMPACKS');
				$subRow = $this->db_fetch_row($subResult);
				$DATA[$i]["numPacks"] = $subRow["0"];

                $i++;
           }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;
    }
    
	// ----------------------------------------------------------- >>>>>>>>>>
    
    function getThemesBySearch($handler, $searchVars)
    {
        global $ADMINcfg, $SITEsession, $STDlib;

        $DATA = array();
        $query = '';
        
        $searchTerm = $searchVars['search_term'];
		$strLength = strlen($searchTerm);
        $DATA['search_term'] = $searchTerm;
        
        if(empty($searchTerm) || ($strLength < 2)) 
		{
            $DATA['error'] = 'No Data!';
            return $DATA;
		}

		// This is a search for a specific pack name
		$query = "SELECT
								dbo.Themes.Id,
								dbo.Themes.Name,
								dbo.Themes.Created,
								dbo.Themes.Shortname
							FROM
								dbo.Themes
							WHERE
								dbo.Themes.Name LIKE '%".$this->_safeVar($searchTerm)."%' ";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_getThemesBySearch');

		if($result)
		{
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Shortname'] = $row['Shortname'];
                $i++;
           }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }

        return $DATA;
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>
    
	function getThemeData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";

		if(!isset($_GET['Id']))
		{
			$DATA['error'] = 'No Data!';
			return $DATA;
		}

		// Get the pack
		$query = "SELECT
								dbo.Themes.Id,
								dbo.Themes.Name,
								dbo.Themes.Created,
								dbo.Themes.Shortname
							FROM
								dbo.Themes
							WHERE
								dbo.Themes.Id = '".$this->_safeVar($_GET['Id'])."'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_getThemeData_Theme');

		if($result)
		{
			$row = $this->db_fetch_array($result);
            $date = date_create($row['Created']);
            
            $DATA['THEME']['Id'] = $row['Id'];
            $DATA['THEME']['Name'] = $row['Name'];
            $DATA['THEME']['Created'] = date_format($date, 'd/m/Y');
            $DATA['THEME']['Shortname'] = $row['Shortname'];

			if(empty($DATA['THEME']['Id']))
			{
				$DATA['error'] = 'No Data!';
			} else {
                // Get the associated themes
                
                $themeId = $DATA['THEME']['Id'];
                $DATA['PACKS'] = $this->_getPacksForId($handler, $themeId);
            }
		} else {
			$DATA['error'] = 'No Data!';
			return $DATA;
		}

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getThemeEditData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		
		// Get the question data to edit
		$DATA = $this->getThemeData($handler);
		$DATA['MISSING'] = $this->_getMissingPacksForId($handler, $_GET['Id']);
		
		return $DATA;		
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getThemeAddData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		
		// Get the question data to add
		$DATA['PACKS'] = $this->_getPacks($handler);
		
		return $DATA;		
	}
	
	// ----------------------------------------------------------- >>>>>>>>>>

	function getAllThemesData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";

		// Get the track
		$query = "SELECT
								dbo.Themes.Id,
								dbo.Themes.Name,
								dbo.Themes.Created,
								dbo.Themes.Shortname
					FROM
						dbo.Themes
					ORDER BY
						dbo.Themes.Name ASC";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_getAllThemesData');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Created'] = $row['Created'];
                $DATA[$i]['Shortname'] = $row['Shortname'];
                $i++;
			}
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function addThemeData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$themeId = $STDlib->guid();
        $Name = $_POST['Name'];
		$addPack = $_POST['addPack'];
		
		// Generate a Shortname for the blob storage. The original system used the Theme Name, but this is not unique enough.
		// We now generate a random string to be used as the blob storage container for this theme. This is written to the the db for
		// future reference so there is no need for it to be human readable.
		//$Shortname = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $Name));
		$Shortname = $STDlib->rndStr(10).'_'.strtolower(preg_replace("/[^A-Za-z0-9]/", "", $Name));
				
        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a new thumbnail
            $jpgTempFile = $_FILES['content_asset']['tmp_name']['thumb'];
            $imgInfo = getimagesize($jpgTempFile);
            $imgWidth = $imgInfo['0'];
            $imgHeight = $imgInfo['1'];

            // Check the image size and scale if necessary            
            if(($imgWidth > 1024) || ($imgHeight > 1024))
            {
                $res = imagecreatefromjpeg($jpgTempFile);
                $imgTmp = imagecreatetruecolor(1024,1024);
                imagecopyresampled($imgTmp,$res,0,0,0,0,1024,1024,$imgWidth,$imgHeight);            } else {
                $res = imagecreatefromjpeg($jpgTempFile);
                $imgTmp = imagecreatetruecolor($imgWidth,$imgHeight);
                imagecopyresampled($imgTmp,$res,0,0,0,0,$imgWidth,$imgHeight,$imgWidth,$imgHeight);           
            }
            
            ob_start();            imagejpeg($imgTmp, NULL, 100);            $imageBlob = ob_get_contents();            ob_end_clean();
            // Free up memory            imagedestroy($imgTmp);
            $fileName = $Shortname.'/bgmain.jpg';
            $DATA['AZURE']['THUMB'][] = $this->_azureSaveAssetToBlob($imageBlob, $fileName, 'themes', "image/jpeg");
        }
		
		// Copy the default images needed for a theme
		$btiIntroImg = $ADMINcfg->AZURE_BLOB_ADDR.'/themes/default/beat_the_intro.png'; 
		$bgCentralImg = $ADMINcfg->AZURE_BLOB_ADDR.'/themes/default/bgcentral.png';
		
		$btiIntroBlob = fopen($btiIntroImg, "r");
		$bgCentralBlob = fopen($bgCentralImg, "r");

		$fileName = $Shortname.'/beat_the_intro.png';
		$DATA['AZURE']['THUMB'][] = $this->_azureSaveAssetToBlob($btiIntroBlob, $fileName, 'themes', "image/png");

		$fileName = $Shortname.'/bgcentral.jpg';
		$DATA['AZURE']['THUMB'][] = $this->_azureSaveAssetToBlob($bgCentralBlob, $fileName, 'themes', "image/png");

        // Run an insert query with Theme data!
        $query = "INSERT INTO dbo.Themes (
												 Id,
												 Name,
												 Created,
												 Shortname
												)                         
											   VALUES (
												 '".$this->_safeVar($themeId)."',
												 '".$this->_safeVar($Name)."',
												 GETDATE(),
												 '".$this->_safeVar($Shortname)."'
											   )";
		
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_addThemeData_ADD_THEME');
		
		// Add the associated packs if needed
		if(is_array($addPack))
		{
			// Add the packs
			for($i=0; isset($addPack[$i]); $i++)
			{
				$thisGUID = $STDlib->guid();
				$PackId = $addPack[$i];
				
				$query = "INSERT INTO dbo.ThemePacks (
										Id,
										ThemeId,
										PackId,
										Created
										)
								 VALUES (
										'".$this->_safeVar($thisGUID)."',
										'".$this->_safeVar($themeId)."',
										'".$this->_safeVar($PackId)."',
										GETDATE()
								)";
				$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_addThemeData_ADD_PACK_'.$i);
			}
		}
        
        if($result)
        {
            $DATA['DB'] = 'Success!';
        } else {
            $DATA['error'] = 'Add Failed!';
        }
		
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
	function updateThemeData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$themeId = $_POST['Id'];
        $Name = $_POST['Name'];
        $Shortname = $_POST['Shortname'];
	
		$removePack = $_POST['removePack'];
		$addPack = $_POST['addPack'];
		
        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a new thumbnail
            $jpgTempFile = $_FILES['content_asset']['tmp_name']['thumb'];
            $imgInfo = getimagesize($jpgTempFile);
            $imgWidth = $imgInfo['0'];
            $imgHeight = $imgInfo['1'];

            // Check the image size and scale if necessary            
            if(($imgWidth > 1024) || ($imgHeight > 1024))
            {
                $res = imagecreatefromjpeg($jpgTempFile);
                $imgTmp = imagecreatetruecolor(1024,1024);
                imagecopyresampled($imgTmp,$res,0,0,0,0,1024,1024,$imgWidth,$imgHeight);            } else {
                $res = imagecreatefromjpeg($jpgTempFile);
                $imgTmp = imagecreatetruecolor($imgWidth,$imgHeight);
                imagecopyresampled($imgTmp,$res,0,0,0,0,$imgWidth,$imgHeight,$imgWidth,$imgHeight);           
            }
            
            ob_start();            imagejpeg($imgTmp, NULL, 100);            $imageBlob = ob_get_contents();            ob_end_clean();
            // Free up memory            imagedestroy($imgTmp);
            $fileName = $Shortname.'/bgmain.jpg';
            $DATA['AZURE']['THUMB'] = $this->_azureSaveAssetToBlob($imageBlob, $fileName, 'themes', "image/jpeg");
        }
		
		// Update the associated packs if needed
		if(is_array($removePack))
		{
			// Clear out the packs
			for($i=0; isset($removePack[$i]); $i++)
			{
				$packId = $removePack[$i];
				$query = "DELETE FROM 
									dbo.ThemePacks
								WHERE
									dbo.ThemePacks.PackId = '".$this->_safeVar($packId)."'";
				$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_updateThemeData_DELETE_PACK_'.$i);
			}
		}
		
		// Add new packs if needed
		if(is_array($addPack))
		{
			// Add the packs
			for($i=0; isset($addPack[$i]); $i++)
			{
				$thisGUID = $STDlib->guid();
				$PackId = $addPack[$i];
				
				$query = "INSERT INTO dbo.ThemePacks (
										Id,
										ThemeId,
										PackId,
										Created
										)
								 VALUES (
										'".$this->_safeVar($thisGUID)."',
										'".$this->_safeVar($themeId)."',
										'".$this->_safeVar($PackId)."',
										GETDATE()
								)";
				$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_updateThemeData_ADD_PACK_'.$i);
			}
		}
		
        // Run an update query with Theme data! We don't update the Shortname!
        $query = "UPDATE dbo.Themes
										SET 
											dbo.Themes.Name = '".$this->_safeVar($Name)."'
										WHERE
											dbo.Themes.Id = '".$themeId."'";
		
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_updateThemeData_UPDATE_THEME');
        
        if($result)
        {
            $DATA['DB'] = 'Success!';
        } else {
            $DATA['error'] = 'Update Failed!';
        }
		
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function deleteThemeData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        /** TO DO
         * Implement a deletion method based on flags in the db or actually deleting files.
         * 
         * 
         */


		//return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// PRIVATE METHODS
	// ----------------------------------------------------------- >>>>>>>>>>

    function _getPacksForId($handler, $themeId)
    {
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";
        
        $query = "SELECT
								dbo.Packs.Id,
								dbo.Packs.Name,
								dbo.Packs.Description,
								dbo.Packs.Active

						FROM
								dbo.ThemePacks
								INNER JOIN dbo.Packs ON dbo.ThemePacks.PackId = dbo.Packs.Id
						WHERE
								dbo.ThemePacks.ThemeId = '".$themeId."'
						ORDER BY
								dbo.Packs.Name ASC";
        
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_getPacksForId');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {

                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Description'] = $row['Description'];
                $DATA[$i]['Active'] = $row['Active'];
                $i++;
            }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;        
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>

	function _getMissingPacksForId($handler, $themeId)
    {
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";
        
        $query = "SELECT
							dbo.Packs.Id,
							dbo.Packs.Name,
							dbo.Packs.Description,
							dbo.Packs.Active
						FROM
							dbo.Packs
						LEFT OUTER JOIN dbo.ThemePacks ON dbo.Packs.Id = dbo.ThemePacks.PackId
						AND dbo.ThemePacks.ThemeId = '".$themeId."'
						WHERE
							dbo.ThemePacks.PackId IS NULL
						ORDER BY
								dbo.Packs.Active DESC";
        
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_getMissingPacksForId');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {

                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Name'] = $row['Name'];
                $DATA[$i]['Description'] = $row['Description'];
                $DATA[$i]['Active'] = $row['Active'];
                $i++;
            }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;        
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>

    function _getPacks($handler)
    {
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";
        
        $query = "SELECT
								dbo.Packs.Id,
								dbo.Packs.Name,
								dbo.Packs.Created,
								dbo.Packs.Description,
								dbo.Packs.Active
							FROM
								dbo.Packs
							ORDER BY dbo.Packs.Active DESC";
        
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'THEMES_getPacks');
		
        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Name'] = $row['Name'];
				$DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['Description'] = $row['Description'];
                $DATA[$i]['Active'] = $row['Active'];
                $i++;
			}
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;        
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>
	
    function _azureSaveAssetToBlob($blobFileContents, $blobFileName, $blobContainer, $mimeType)
    {
        global $ADMINcfg, $SITEsession, $STDlib;
        
        // Create blob REST proxy.
        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($ADMINcfg->AZURE_CONNECTION_STR);

        $options = new Models\CreateBlobOptions();
        $options->setBlobContentType($mimeType);
		
        try {
            // Upload blob
            $blobRestProxy->createBlockBlob($blobContainer, $blobFileName, $blobFileContents, $options);
            $DATA = 'Success!';
        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here: 
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $errCode = $e->getCode();
            $errMessage = $e->getMessage();
            $DATA =  $errCode.": ".$errMessage;
        }
        
        return $DATA;
    }

	// ----------------------------------------------------------- >>>>>>>>>>
	
	function _safeVar($var)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		return $STDlib->makeQuerySafe($var);
	}

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>