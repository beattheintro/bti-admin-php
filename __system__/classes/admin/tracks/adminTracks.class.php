<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminTracks.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Tracks Methods
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $_SERVER["DOCUMENT_ROOT"].'/__system__/WindowsAzure/WindowsAzure.php';

use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Blob\Models;

Class adminTracks Extends GLOBALpdoWrapper
{
	function tracksConnection()
	{
		global $ADMINcfg, $SITEsession, $STDlib;
		// Connect to the dB and return a handler
		$handler = $this->db_connect("sqlsrv", $ADMINcfg->DEFAULT_DBHOST, $ADMINcfg->SYSTEM_DBNAME, $ADMINcfg->SYSTEM_DBUSER, $ADMINcfg->SYSTEM_DBPASSWORD);
		return $handler;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Track Methods
	// ----------------------------------------------------------- >>>>>>>>>>

	function tracksInfoBarData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$query = "SELECT COUNT(*) FROM dbo.Tracks";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'TRACKS_tracksInfoBarData');
		$row = $this->db_fetch_row($result);
		$DATA["numTracks"] = $row["0"];
        $_SESSION['TRACKS']['numTracks'] = $DATA["numTracks"];

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function getTracks($handler, $startAt=0)
    {
        global $ADMINcfg, $SITEsession, $STDlib;

        $DATA = array();
        $query = '';
        
        //$query = "SELECT * FROM dbo.tracks LIMIT ".$startAt.",50;";
        $query = "SELECT * FROM dbo.Tracks ORDER BY dbo.Tracks.Created OFFSET ".$startAt." ROWS FETCH NEXT ".$ADMINcfg->MAX_DB_RESULTS." ROWS ONLY";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'TRACKS_getTracks');
               
        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['Title'] = $row['Title'];
                $DATA[$i]['Artist'] = $row['Artist'];
                $DATA[$i]['PurchaseURLiTunes'] = $row['PurchaseURLiTunes'];
                $DATA[$i]['PurchaseURLAmazon'] = $row['PurchaseURLAmazon'];
                $DATA[$i]['PurchaseURLGooglePlay'] = $row['PurchaseURLGooglePlay'];
                $DATA[$i]['Playable'] = $row['Playable'];
                $DATA[$i]['GotMP3'] = $row['GotMP3'];
                $DATA[$i]['GotOGG'] = $row['GotOGG'];
                $DATA[$i]['GotMP4'] = $row['GotMP4'];
                $DATA[$i]['GotThumb50'] = $row['GotThumb50'];
                $i++;
           }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;
    }
    
	// ----------------------------------------------------------- >>>>>>>>>>
    
    function getTracksBySearch($handler, $searchVars)
    {
        global $ADMINcfg, $SITEsession, $STDlib;

        $DATA = array();
        $query = '';
        
        $byArtist = $searchVars['by_artist'];
        $byTitle = $searchVars['by_title'];
        $searchTerm = $searchVars['search_term'];
        
        $DATA['by_artist'] = $byArtist;
        $DATA['by_title'] = $byTitle;
        $DATA['search_term'] = $searchTerm;
        
        if(($byArtist == TRUE) && !empty($searchTerm) && empty($byTitle)) {
            // This is a search for a specific artist       
            $query = "SELECT * 
                            FROM dbo.Tracks 
                            WHERE dbo.Tracks.Artist LIKE '%".$this->_safeVar($searchTerm)."%' 
                            ORDER BY dbo.Tracks.Artist ASC";
        } elseif(($byTitle == TRUE) && !empty($searchTerm) && empty($byArtist)) {
            // This is a search for a specific title
            $query = "SELECT * 
                            FROM dbo.Tracks 
                            WHERE dbo.Tracks.Title LIKE '%".$this->_safeVar($searchTerm)."%' 
                            ORDER BY dbo.Tracks.Artist ASC";
        } elseif(($byTitle == TRUE) && ($byArtist == TRUE) && !empty($searchTerm)) {
            // This is a search for a specific title or artist
            $query = "SELECT * 
                            FROM dbo.Tracks 
                            WHERE dbo.Tracks.Artist LIKE '%".$this->_safeVar($searchTerm)."%' 
                                OR
                                  dbo.Tracks.Title LIKE '%".$this->_safeVar($searchTerm)."%' 
                            ORDER BY dbo.Tracks.Created ASC";            
        } else {
            $DATA['error'] = 'No Data!';
            return $DATA;
        }
        
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'TRACKS_getTracksBySearch');
        
        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['Title'] = $row['Title'];
                $DATA[$i]['Artist'] = $row['Artist'];
                $DATA[$i]['PurchaseURLiTunes'] = $row['PurchaseURLiTunes'];
                $DATA[$i]['PurchaseURLAmazon'] = $row['PurchaseURLAmazon'];
                $DATA[$i]['PurchaseURLGooglePlay'] = $row['PurchaseURLGooglePlay'];
                $DATA[$i]['Playable'] = $row['Playable'];
                $DATA[$i]['GotMP3'] = $row['GotMP3'];
                $DATA[$i]['GotOGG'] = $row['GotOGG'];
                $DATA[$i]['GotMP4'] = $row['GotMP4'];
                $DATA[$i]['GotThumb50'] = $row['GotThumb50'];
                $i++;
           }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>
    
	function getTrackData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";

		if(!isset($_GET['Id']))
		{
			$DATA['error'] = 'No Data!';
			return $DATA;
		}

		// Get the track
		$query = "SELECT *							
						FROM
							dbo.Tracks
						WHERE
							dbo.Tracks.Id='".$this->_safeVar($_GET['Id'])."'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'TRACKS_getTrackData_Track');

		if($result)
		{
			$row = $this->db_fetch_array($result);
            $date = date_create($row['Created']);
            
            $DATA['TRACK']['Id'] = $row['Id'];
            $DATA['TRACK']['Created'] = date_format($date, 'd/m/Y');
            $DATA['TRACK']['Title'] = $row['Title'];
            $DATA['TRACK']['Artist'] = $row['Artist'];
            $DATA['TRACK']['PurchaseURLiTunes'] = $row['PurchaseURLiTunes'];
            $DATA['TRACK']['PurchaseURLAmazon'] = $row['PurchaseURLAmazon'];
            $DATA['TRACK']['PurchaseURLGooglePlay'] = $row['PurchaseURLGooglePlay'];
            $DATA['TRACK']['Playable'] = $row['Playable'];
            $DATA['TRACK']['GotMP3'] = $row['GotMP3'];
            $DATA['TRACK']['GotOGG'] = $row['GotOGG'];
            $DATA['TRACK']['GotMP4'] = $row['GotMP4'];
            $DATA['TRACK']['GotThumb50'] = $row['GotThumb50'];

			if(empty($DATA['TRACK']['Id']))
			{
				$DATA['error'] = 'No Data!';
			} else {
                // Get the associated questions
                
                $trackId = $DATA['TRACK']['Id'];                
                $DATA['QUESTION'] = $this->_getQuestionsForId($handler, $trackId);
            }
		} else {
			$DATA['error'] = 'No Data!';
			return $DATA;
		}

		return $DATA;

	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function addTrackData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

        $Artist = $_POST['Artist'];
        $Title = $_POST['Title'];
        $PurchaseURLAmazon = $_POST['PurchaseURLAmazon'];
        $PurchaseURLGooglePlay = $_POST['PurchaseURLGooglePlay'];
        $PurchaseURLiTunes = $_POST['PurchaseURLiTunes'];
        $Playable = $_POST['Playable'];
        $CreateOGG = $_POST['CreateOGG']; // TO DO
        
        if($Playable == 'on')
        {
            $Playable = 1;
        } elseif(empty($Playable)) {
            $Playable = 0;
        }
        
        $gUID = $STDlib->guid();
        $_gUID = strtolower($gUID);
        
        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a thumbnail
            $jpgTempFile = $_FILES['content_asset']['tmp_name']['thumb'];
            $imgInfo = getimagesize($jpgTempFile);
            
            $imgWidth = $imgInfo['0'];
            $imgHeight = $imgInfo['1'];

            // Check the image size and scale if necessary            
            if(($imgWidth > 50) || ($imgHeight > 50))
            {
                $res = imagecreatefromjpeg($jpgTempFile);
                $imgTmp = imagecreatetruecolor(50,50);
                
                imagecopyresampled($imgTmp,$res,0,0,0,0,50,50,$imgWidth,$imgHeight);
            } else {
                $res = imagecreatefromjpeg($jpgTempFile);
                $imgTmp = imagecreatetruecolor($imgWidth,$imgHeight);
                
                imagecopyresampled($imgTmp,$res,0,0,0,0,$imgWidth,$imgHeight,$imgWidth,$imgHeight);           
            }
            
            ob_start();
            imagejpeg($imgTmp, NULL, 100);
            $imageBlob = ob_get_contents();
            ob_end_clean();
            
            // Free up memory
            imagedestroy($imgTmp);
            
            $fileName = $_gUID.'_50.jpg';
            
            $DATA['AZURE']['THUMB'] = $this->_azureSaveAssetToBlob($imageBlob, $fileName, 'tracks', "image/jpeg");
            
            if($DATA['AZURE']['THUMB'] == "Success!")
            {
                $GotThumb50 = 1;
            } else {
                $GotThumb50 = 0;
            }
        } else {
            $GotThumb50 = 0;
        }
        
        if($_FILES['content_asset']['error']['mp3'] == 0)
        {
            // We have a MP3
            $audioBlob = file_get_contents($_FILES['content_asset']['tmp_name']['mp3']);
            $fileName = $_gUID.'.mp3';
            $DATA['AZURE']['MP3'] = $this->_azureSaveAssetToBlob($audioBlob, $fileName, 'tracks', "audio/mp3");

            if($DATA['AZURE']['MP3'] == "Success!")
            {
                $GotMP3 = 1;
            } else {
                $GotMP3 = 0;
            }
            
            // Do we need to generate an OGG from the MP3?
            if($CreateOGG)
            {
                $mp3Filename = $_FILES['content_asset']['tmp_name']['mp3'];
                $oggFilename = $_gUID.'.ogg';
                
                // Create a temp OGG file on local
                $sysTemp = sys_get_temp_dir();
                $tmpOGGFile = $sysTemp.'\\'.$oggFilename;
                
                $command = '"'.$ADMINcfg->FFMPEG_EXE.'" -i "'.$mp3Filename.'" -vn -acodec libvorbis -y "'.$tmpOGGFile.'"';
                $exeStr = exec($command, $exeOut, $reVar);
                
                // Get the processed OGG
                $oggBlob = file_get_contents($tmpOGGFile);
                
                $DATA['AZURE']['OGG'] = $this->_azureSaveAssetToBlob($oggBlob, $oggFilename, 'tracks', "audio/ogg");

                if($DATA['AZURE']['OGG'] == "Success!")
                {
                    $GotOGG = 1;
                    $GenOGG = 1;
                } else {
                    $GotOGG = 0;
                }
                
                // Clean up
                unlink($tmpOGGFile);
            }
        } else {
            $GotMP3 = 0;
        }

        if(($_FILES['content_asset']['error']['ogg'] == 0) && !isset($GenOGG))
        {
            // We have a OGG
            $audioBlob = file_get_contents($_FILES['content_asset']['tmp_name']['ogg']);
            $fileName = $_gUID.'.ogg';
            $DATA['AZURE']['OGG'] = $this->_azureSaveAssetToBlob($audioBlob, $fileName, 'tracks', "audio/ogg");

            if($DATA['AZURE']['OGG'] == "Success!")
            {
                $GotOGG = 1;
            } else {
                $GotOGG = 0;
            }  
        } elseif(!isset($GotOGG)) {
            $GotOGG = 0;
        }
 
        if($_FILES['content_asset']['error']['mp4'] == 0)
        {
            // We have a new MP4
            $audioBlob = file_get_contents($_FILES['content_asset']['tmp_name']['mp4']);
            $fileName = $_gUID.'.mp4';
            $DATA['AZURE']['MP4'] = $this->_azureSaveAssetToBlob($audioBlob, $fileName, 'tracks', "audio/mp4");
            
            if($DATA['AZURE']['MP4'] == "Success!")
            {
                $GotMP4 = 1;
            } else {
                $GotMP4 = 0;
            }              
        } else {
            $GotMP4 = 0;
        }
        
        // Run an update query with all data!
        
        $query = "INSERT INTO dbo.Tracks (
												 Id,
												 Created,
												 Title,
												 Artist,
												 PurchaseURLiTunes,
												 PurchaseURLAmazon,
												 PurchaseURLGooglePlay,
												 Playable,
												 GotMP3,
												 GotOGG,
												 GotThumb50,
												 GotMP4
												)                         
											   VALUES (
												 '".$this->_safeVar($gUID)."',
												 GETDATE(),
												 '".$this->_safeVar($Title)."',
												 '".$this->_safeVar($Artist)."',
												 '".$this->_safeVar($PurchaseURLiTunes)."',
												 '".$this->_safeVar($PurchaseURLAmazon)."',
												 '".$this->_safeVar($PurchaseURLGooglePlay)."',
												 '".$this->_safeVar($Playable)."',
												 '".$this->_safeVar($GotMP3)."',
												 '".$this->_safeVar($GotOGG)."',
												 '".$this->_safeVar($GotThumb50)."',
												 '".$this->_safeVar($GotMP4)."'
											   )";
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'TRACKS_addTrackData');
        
        if($result)
        {
            $DATA['DB'] = 'Success!';
            $DATA['Id'] = $gUID;
        } else {
            $DATA['error'] = 'Update Failed!';
        }
        
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>   
    
	function updateTrackData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$Id = $_POST['Id'];
        $Artist = $_POST['Artist'];
        $Title = $_POST['Title'];
        $PurchaseURLAmazon = $_POST['PurchaseURLAmazon'];
        $PurchaseURLGooglePlay = $_POST['PurchaseURLGooglePlay'];
        $PurchaseURLiTunes = $_POST['PurchaseURLiTunes'];
        $Playable = $_POST['Playable'];
        $CreateOGG = $_POST['CreateOGG']; // TO DO
        $GotMP3 = $_POST['GotMP3'];
        $GotOGG = $_POST['GotOGG'];
        $GotMP4 = $_POST['GotMP4']; // TO SAVE
        $GotThumb50 = $_POST['GotThumb50'];
        
        if($Playable == 'on')
        {
            $Playable = 1;
        } elseif(empty($Playable)) {
            $Playable = 0;
        }
        
        $gUID = strtolower($Id);
                
        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a new thumbnail
            $jpgTempFile = $_FILES['content_asset']['tmp_name']['thumb'];
            $imgInfo = getimagesize($jpgTempFile);
            
            $imgWidth = $imgInfo['0'];
            $imgHeight = $imgInfo['1'];

            // Check the image size and scale if necessary            
            if(($imgWidth > 50) || ($imgHeight > 50))
            {
                $res = imagecreatefromjpeg($jpgTempFile);
                $imgTmp = imagecreatetruecolor(50,50);
                
                imagecopyresampled($imgTmp,$res,0,0,0,0,50,50,$imgWidth,$imgHeight);
            } else {
                $res = imagecreatefromjpeg($jpgTempFile);
                $imgTmp = imagecreatetruecolor($imgWidth,$imgHeight);
                
                imagecopyresampled($imgTmp,$res,0,0,0,0,$imgWidth,$imgHeight,$imgWidth,$imgHeight);           
            }
            
            ob_start();
            imagejpeg($imgTmp, NULL, 100);
            $imageBlob = ob_get_contents();
            ob_end_clean();
            
            // Free up memory
            imagedestroy($imgTmp);
            
            $fileName = $gUID.'_50.jpg';
            
            $DATA['AZURE']['THUMB'] = $this->_azureSaveAssetToBlob($imageBlob, $fileName, 'tracks', "image/jpeg");
            
            if($DATA['AZURE']['THUMB'] == "Success!")
            {
                $GotThumb50 = 1;
            } else {
                $GotThumb50 = 0;
            }
        }
        
        if($_FILES['content_asset']['error']['mp3'] == 0)
        {
            // We have a new MP3
            $audioBlob = file_get_contents($_FILES['content_asset']['tmp_name']['mp3']);
            $fileName = $gUID.'.mp3';
            $DATA['AZURE']['MP3'] = $this->_azureSaveAssetToBlob($audioBlob, $fileName, 'tracks', "audio/mp3");

            if($DATA['AZURE']['MP3'] == "Success!")
            {
                $GotMP3 = 1;
            } else {
                $GotMP3 = 0;
            }
            
            // Do we need to generate an OGG from the MP3?
            if($CreateOGG)
            {
                $mp3Filename = $_FILES['content_asset']['tmp_name']['mp3'];
                $oggFilename = $gUID.'.ogg';
                
                $sysTemp = sys_get_temp_dir();
                $tmpOGGFile = $sysTemp.'\\'.$oggFilename;
                
                // Convert the MP3
                $command = '"'.$ADMINcfg->FFMPEG_EXE.'" -i "'.$mp3Filename.'" -vn -acodec libvorbis -y "'.$tmpOGGFile.'"';
                $exeStr = exec($command, $exeOut, $reVar);
       
                // Get the processed OGG
                $oggBlob = file_get_contents($tmpOGGFile);
                
                $DATA['AZURE']['OGG'] = $this->_azureSaveAssetToBlob($oggBlob, $oggFilename, 'tracks', "audio/ogg");

                if($DATA['AZURE']['OGG'] == "Success!")
                {
                    $GotOGG = 1;
                    $GenOGG = 1;
                } else {
                    $GotOGG = 0;
                }
                
                unlink($tmpOGGFile);
            }
        }        

        if(($_FILES['content_asset']['error']['ogg'] == 0) && !isset($GenOGG))
        {
            // We have a new OGG
            $audioBlob = file_get_contents($_FILES['content_asset']['tmp_name']['ogg']);
            $fileName = $gUID.'.ogg';
            $DATA['AZURE']['OGG'] = $this->_azureSaveAssetToBlob($audioBlob, $fileName, 'tracks', "audio/ogg");

            if($DATA['AZURE']['OGG'] == "Success!")
            {
                $GotOGG = 1;
            } else {
                $GotOGG = 0;
            }  
        }

        if($_FILES['content_asset']['error']['mp4'] == 0)
        {
            // We have a new MP4
            $audioBlob = file_get_contents($_FILES['content_asset']['tmp_name']['mp4']);
            $fileName = $gUID.'.mp4';
            $DATA['AZURE']['MP4'] = $this->_azureSaveAssetToBlob($audioBlob, $fileName, 'tracks', "audio/mp4");
            
            if($DATA['AZURE']['MP4'] == "Success!")
            {
                $GotMP4 = 1;
            } else {
                $GotMP4 = 0;
            }              
        }
     
        // Run an update query with all data!
        $query = "UPDATE dbo.Tracks 
                       SET 
                         dbo.Tracks.Title='".$this->_safeVar($Title)."',
                         dbo.Tracks.Artist='".$this->_safeVar($Artist)."',
                         dbo.Tracks.PurchaseURLiTunes='".$this->_safeVar($PurchaseURLiTunes)."',
                         dbo.Tracks.PurchaseURLAmazon='".$this->_safeVar($PurchaseURLAmazon)."',
                         dbo.Tracks.PurchaseURLGooglePlay='".$this->_safeVar($PurchaseURLGooglePlay)."',
                         dbo.Tracks.Playable='".$this->_safeVar($Playable)."',
                         dbo.Tracks.GotMP3='".$this->_safeVar($GotMP3)."',
                         dbo.Tracks.GotOGG='".$this->_safeVar($GotOGG)."',
                         dbo.Tracks.GotThumb50='".$this->_safeVar($GotThumb50)."',
						 dbo.Tracks.GotMP4='".$this->_safeVar($GotMP4)."'
                       WHERE
                         dbo.Tracks.Id='".$Id."'";
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'TRACKS_updateTrackData');
        
        if($result)
        {
            $DATA['DB'] = 'Success!';
        } else {
            $DATA['error'] = 'Update Failed!';
        }
        
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function deleteTrackData($handler)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        /** TO DO
         * Implement a deletion method based on flags in the db or actually deleting files.
         * 
         * 
         */


		//return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// PRIVATE METHODS
	// ----------------------------------------------------------- >>>>>>>>>>

    function _getQuestionsForId($handler, $trackId)
    {
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $DATA = array();
        $query = "";
        
        $query = "SELECT
	                        dbo.Questions.Id,
	                        dbo.Questions.Created,
                            Tracks1.Id AS CorrectId,
	                        Tracks1.Title AS CorrectTitle,
                            Tracks2.Id AS WrongId1,
	                        Tracks2.Title AS WrongTitle1,
                            Tracks3.Id AS WrongId2,
	                        Tracks3.Title AS WrongTitle2
                        FROM
	                        dbo.Questions
                        INNER JOIN dbo.Tracks AS Tracks1 ON dbo.Questions.CorrectTrackId = Tracks1.Id
                        INNER JOIN dbo.Tracks AS Tracks2 ON dbo.Questions.WrongTrackId1 = Tracks2.Id
                        INNER JOIN dbo.Tracks AS Tracks3 ON dbo.Questions.WrongTrackId2 = Tracks3.Id
                        WHERE
	                        dbo.Questions.CorrectTrackId = '".$trackId."'
                           OR 
                            dbo.Questions.WrongTrackId1 = '".$trackId."'
                           OR 
                            dbo.Questions.WrongTrackId2 = '".$trackId."'";
        
        $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'TRACKS_getQuestionsForId');

        if($result)
        {
            $i=0;
            while ($row = $this->db_fetch_array($result)) {
                
                $date = date_create($row['Created']);
                
                $DATA[$i]['Id'] = $row['Id'];
                $DATA[$i]['Created'] = date_format($date, 'd/m/Y');
                $DATA[$i]['CorrectId'] = $row['CorrectId'];
                $DATA[$i]['CorrectTitle'] = $row['CorrectTitle'];
                $DATA[$i]['WrongId1'] = $row['WrongId1'];
                $DATA[$i]['WrongTitle1'] = $row['WrongTitle1'];
                $DATA[$i]['WrongId2'] = $row['WrongId2'];
                $DATA[$i]['WrongTitle2'] = $row['WrongTitle2'];
                $i++;
            }
            
            if(!isset($DATA['0']['Id']))
            {
                $DATA['error'] = 'No Data!';
            }
        } else {
            $DATA['error'] = 'No Data!';
        }
        
        return $DATA;
        
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>
    
    function _azureSaveAssetToBlob($blobFileContents, $blobFileName, $blobContainer, $mimeType)
    {
        global $ADMINcfg, $SITEsession, $STDlib;
        
        // Create blob REST proxy.
        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($ADMINcfg->AZURE_CONNECTION_STR);

        $options = new Models\CreateBlobOptions();
        $options->setBlobContentType($mimeType);
       
        try {
            // Upload blob
            $blobRestProxy->createBlockBlob($blobContainer, $blobFileName, $blobFileContents, $options);
            $DATA = 'Success!';
        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here: 
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $errCode = $e->getCode();
            $errMessage = $e->getMessage();
            $DATA =  $errCode.": ".$errMessage;
        }
        
        return $DATA;
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>
    
	function _safeVar($var)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		return $STDlib->makeQuerySafe($var);
	}

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>