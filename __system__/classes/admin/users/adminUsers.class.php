<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminUsers.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Users Admin page
	// ----------------------------------------------------------- >>>>>>>>>>

Class adminUsers Extends GLOBALpdoWrapper
{
	function adminUsersConnection()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		// Connect to the dB and return a handler
		$handler = $this->db_connect("sqlsrv", $ADMINcfg->DEFAULT_DBHOST, $ADMINcfg->SYSTEM_DBNAME, $ADMINcfg->SYSTEM_DBUSER, $ADMINcfg->SYSTEM_DBPASSWORD);
		return $handler;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getUser($handler, $Id="")
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		if(empty($Id))
		{
			$query = "SELECT * FROM dbo.__AdminUsers WHERE u_name='".$this->_safeVar($_POST["u_name"])."' AND u_password='".$this->_safeVar($_POST["u_password"])."'";
		} else {
			$query = "SELECT * FROM dbo.__AdminUsers WHERE Id='".$this->_safeVar($Id)."'";
		}

		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'USERS_getUser');

		// Fetch the user
		$row = $this->db_fetch_row($result);

		if($row['0'] && empty($Id))
		{
			$DATA = array();
			$DATA["Id"] = $row['0'];
			$DATA["u_name"] = $row['1'];
			$DATA["u_password"] = $row['2'];
			$DATA["u_firstname"] = $row['3'];
			$DATA["u_lastname"] = $row['4'];
			$DATA["admin_1"] = $row['5'];
			$DATA["admin_new"] = $row['6'];
			$DATA["admin_edit"] = $row['7'];
			$DATA["admin_delete"] = $row['8'];
			$DATA["status"] = $row['9'];

			$_SESSION['_LOGIN_'] = $DATA;

		} elseif($row['0'] && !empty($Id)) {

			$DATA = array();
			$DATA["Id"] = $row['0'];
			$DATA["u_name"] = $row['1'];
			$DATA["u_password"] = $row['2'];
			$DATA["u_firstname"] = $row['3'];
			$DATA["u_lastname"] = $row['4'];
			$DATA["admin_1"] = $row['5'];
			$DATA["admin_new"] = $row['6'];
			$DATA["admin_edit"] = $row['7'];
			$DATA["admin_delete"] = $row['8'];
			$DATA["status"] = $row['9'];

            /*
			$SITEsession->add_session_data("1Id", $row['0']);
			$SITEsession->add_session_data("1u_name", $row['1']);
			$SITEsession->add_session_data("1u_password", $row['2']);
			$SITEsession->add_session_data("1u_firstname", $row['3']);
			$SITEsession->add_session_data("1u_lastname", $row['4']);
			$SITEsession->add_session_data("1admin_1", $row['5']);
			$SITEsession->add_session_data("1admin_new", $row['6']);
			$SITEsession->add_session_data("1admin_edit", $row['7']);
			$SITEsession->add_session_data("1admin_delete", $row['8']);
			$SITEsession->add_session_data("status", $row['9']);
            */

		} else {
			$DATA = array();
			$DATA["error"] = TRUE;
			//$SITEsession->add_session_data("login_error", TRUE);
		}
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function getUsers($handler)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		$query = "SELECT * FROM dbo.__AdminUsers ORDER BY Id ASC";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'USERS_getUsers');

		$i=0;
		$DATA = array();

		// Fetch the users
		while($row = $this->db_fetch_array($result)) {

			$DATA[$i]["Id"] = $row["Id"];
			$DATA[$i]["u_name"] = $row["u_name"];
			$DATA[$i]["u_password"] = $row["u_password"];
			$DATA[$i]["u_firstname"] = $row["u_firstname"];
			$DATA[$i]["u_lastname"] = $row["u_lastname"];
			$DATA[$i]["admin_1"] = $row["admin_1"];
			$DATA[$i]["admin_new"] = $row["admin_new"];
			$DATA[$i]["admin_edit"] = $row["admin_edit"];
			$DATA[$i]["admin_delete"] = $row["admin_delete"];
			$DATA[$i]["status"] = $row["status"];

			$i++;
		} // End while

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function updateUser($handler)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		if(!empty($_POST["Id"]) && !empty($_POST["u_name"]) && !empty($_POST["u_password"]))
		{
			$query = "UPDATE dbo.__AdminUsers 
                                    SET u_name='".$this->_safeVar($_POST["u_name"])."', 
                                        u_password='".$this->_safeVar($_POST["u_password"])."', 
                                        u_firstname='".$this->_safeVar($_POST["u_firstname"])."', 
                                        u_lastname='".$this->_safeVar($_POST["u_lastname"])."', 
                                        admin_1='".$_POST["admin_1"]."', 
                                        admin_new='".$_POST["admin_new"]."', 
                                        admin_edit='".$_POST["admin_edit"]."', 
                                        admin_delete='".$_POST["admin_delete"]."', 
                                        status='LIVE' 
                                      WHERE 
                                        Id='".$_POST["Id"]."'";
            
			//echo "<!-- ".$query." -->\n\n";
            $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'USERS_updateUser');

			$DATA = array();
			$DATA["Id"] = $_POST["Id"];
			$DATA["u_name"] = $_POST["u_name"];
			$DATA["u_password"] = $_POST["u_password"];
			$DATA["u_firstname"] = $_POST["u_firstname"];
			$DATA["u_lastname"] = $_POST["u_lastname"];
			$DATA["admin_1"] = $_POST["admin_1"];
			$DATA["admin_new"] = $_POST["admin_new"];
			$DATA["admin_edit"] = $_POST["admin_edit"];
			$DATA["admin_delete"] = $_POST["admin_delete"];
			$DATA["status"] = "LIVE";
			$DATA["result"] = $result;
			return $DATA;
		} else {
			return false;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function insertUser($handler)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		if(!empty($_POST["u_name"]) && !empty($_POST["u_password"]))
		{
			$chk = $this->_checkUser($_POST["u_name"], $handler);

			//var_dump($chk);

			if($chk != false)
			{
                $query = "INSERT INTO dbo.__AdminUsers
                                       (u_name
                                       ,u_password
                                       ,u_firstname
                                       ,u_lastname
                                       ,admin_1
                                       ,admin_new
                                       ,admin_edit
                                       ,admin_delete
                                       ,status)
                                 VALUES
                                       ('".$this->_safeVar($_POST["u_name"])."'
                                       ,'".$this->_safeVar($_POST["u_password"])."'
                                       ,'".$this->_safeVar($_POST["u_firstname"])."'
                                       ,'".$this->_safeVar($_POST["u_lastname"])."'
                                       ,'".$_POST["admin_1"]."'
                                       ,'".$_POST["admin_new"]."'
                                       ,'".$_POST["admin_edit"]."'
                                       ,'".$_POST["admin_delete"]."'
                                       ,'LIVE')";
				//echo "<!-- ".$query." -->\n\n";
                $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'USERS_insertUser');

				$DATA = array();
				$DATA["Id"] = $_POST["Id"];
				$DATA["u_name"] = $_POST["u_name"];
				$DATA["u_password"] = $_POST["u_password"];
				$DATA["u_firstname"] = $_POST["u_firstname"];
				$DATA["u_lastname"] = $_POST["u_lastname"];
				$DATA["admin_1"] = $_POST["admin_1"];
				$DATA["admin_new"] = $_POST["admin_new"];
				$DATA["admin_edit"] = $_POST["admin_edit"];
				$DATA["admin_delete"] = $_POST["admin_delete"];
				$DATA["status"] = "LIVE";
				$DATA["result"] = $result;
				return $DATA;
			} else {
				$DATA["error"] = "exists";
				return $DATA;
			}
		} else {
			return false;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function deleteUser($handler)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		if(!empty($_POST["Id"]) && !empty($_POST["delete"]))
		{
			$query = "DELETE FROM dbo.__AdminUsers WHERE Id='".$this->_safeVar($_POST["Id"])."'";
            $result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'USERS_deleteUser');

            return $result;
		} else {
			return false;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// PRIVATE METHODS
	// ----------------------------------------------------------- >>>>>>>>>>

	function _safeVar($var)
	{
		global $ADMINcfg, $STDlib, $SITEsession;

		return $STDlib->makeSafe($var);

	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function _checkUser($name, $handler)
	{
		global $ADMINcfg, $STDlib, $SITEsession;

		$query = "SELECT u_name FROM dbo.__AdminUsers WHERE u_name='".$name."'";
		$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, 'USERS_checkUser');
		$row = $this->db_fetch_row($result);

		if($row[0] == $name)
		{
			return false;
		} else {
			return true;
		}

	}
	// ----------------------------------------------------------- >>>>>>>>>>
}

?>