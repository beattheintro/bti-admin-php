<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : GLOBALdbaWrapper.class.php
	// Author: Cactus Designs (UK) Ltd.
	// Date: 11/12/2008
	// Version: 1.0
	// Description: Global DBA Wrapper.
	//			 DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		 with out consent from the author.
	// Copyright (c) 2007, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

	Class GLOBALdbaWrapper
	{

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_connect($dbaFile, $dbaMode='w', $dbaType='db4', $persistent=FALSE)
		{
			/* Open/create a dba style database */

			global $ADMINcfg, $MOBIcfg, $WEBcfg;

			if(is_object($ADMINcfg))
			{
				$DBobj = $ADMINcfg;
			} elseif(is_object($MOBIcfg)) {
				$DBobj = $MOBIcfg;
			} elseif(is_object($WEBcfg)) {
				$DBobj = $WEBcfg;
			}

			$time = time();
			$now = date("[d-m-y@G:i:s]", $time);

			$connectLogFile = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".dba.connect.errors.log";

			if(!file_exists($DBobj->SYSTEM_DB4_PATH.$dbaFile))
			{
				if($persistent)
				{
					$dbh = dba_popen($DBobj->SYSTEM_DB4_PATH.$dbaFile, "n", $dbaType);
				} else {
					$dbh = dba_open($DBobj->SYSTEM_DB4_PATH.$dbaFile, "n", $dbaType);
				}
			} else {
				if($persistent)
				{
					$dbh = dba_popen($DBobj->SYSTEM_DB4_PATH.$dbaFile, $dbaMode, $dbaType);
				} else {
					$dbh = dba_open($DBobj->SYSTEM_DB4_PATH.$dbaFile, $dbaMode, $dbaType);
				}
			}

			if(!$dbh)
			{
				if ($DBobj->DB_DEBUG)
				{
					echo "<h2>Can't connect to $dbaFile</h2>";
				} else {
					$logData = "";
					$logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\n";
					$logData .= $now." - CONNECT ERROR> dbaFile = ".$dbaFile."\n";
					error_log($logData, 3, $connectLogFile);
				}

				if ($DBobj->DB_DIE_ON_FAIL)
				{
					echo "<p>This script cannot continue, terminating.</p>";
					die();
				}
			}

			return $dbh;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_fetch_keys($dbh)
		{
			/* fetch all the keys from the db */

			if($dbh)
			{
				for($key = dba_firstkey($dbh); $key !== false; $key = dba_nextkey($dbh))
				{
					$keyset[] = $key;
				}
				return $keyset;
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_fetch_array($dbh, $sort=FALSE)
		{
			/* fetch all the data from the db */

			if($dbh)
			{
				$key = dba_firstkey($dbh);

				while ($key != false) {
					$value = dba_fetch($key, $dbh);
					$arr[$key] .= $value;
					$key = dba_nextkey($dbh);
				}

				if($sort)
				{
					//ksort($arr);
				}

				return $arr;
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_fetch_data($key, $dbh)
		{
			/* Find the value for the key in the current database. */

			if(!empty($key) && !empty($dbh))
			{
				$value = dba_fetch($key, $dbh);
				return $value;
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_key_check($key, $dbh)
		{
			/* Check for the key in the current database. */

			if(!empty($key) && !empty($dbh))
			{
				return dba_exists($key, $dbh);
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_insert_data($key, $value, $dbh)
		{
			/* Insert data in the current database. */

			if(!empty($key) && !empty($value) && !empty($dbh))
			{
				return dba_insert($key, $value, $dbh);
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_replace_data($key, $value, $dbh)
		{
			/* Replace data by key/value in the current database. */

			if(!empty($key) && !empty($value) && !empty($dbh))
			{
				return dba_replace($key, $value, $dbh);
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_delete_data($key, $dbh)
		{
			/* Delete data by key in the current database. */

			if(!empty($key) && !empty($dbh))
			{
				return dba_delete($key, $dbh);
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_optimise_data($dbh)
		{
			/* Optimise the current database, usually after deletes. */

			if($dbh)
			{
				return dba_optimize($dbh);
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_sync_data($dbh)
		{
			/* Sync the current database between memory and disk. */

			if($dbh)
			{
				return dba_sync($dbh);
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function dba_close_connection($dbh)
		{
			/* Close the dba connection. */

			if($dbh)
			{
				return  dba_close($dbh);
			} else {
				return FALSE;
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>
		// PRIVATE METHODS
		// ----------------------------------------------------------- >>>>>>>>>>
	} // Close Class
?>