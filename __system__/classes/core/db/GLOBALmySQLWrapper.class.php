<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : GLOBALmySQLWrapper.class.php
	// Author: Cactus Designs (UK) Ltd.
	// Date: 06/12/2007
	// Version: 1.0
	// Description: Global MySQL DB Wrapper.
	//			 DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		 with out consent from the author.
	// Copyright (c) 2007, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

	Class GLOBALmySQLWrapper
	{
		function db_connect($dbhost, $dbname, $dbuser, $dbpass)
		{
		/* connect to the database $dbname on $dbhost with the user/password pair
		 * $dbuser and $dbpass. */

			global $ADMINcfg, $MOBIcfg, $WEBcfg, $SMScfg;

			if(is_object($ADMINcfg))
			{
				$DBobj = $ADMINcfg;
			} elseif(is_object($MOBIcfg)) {
				$DBobj = $MOBIcfg;
			} elseif(is_object($WEBcfg)) {
				$DBobj = $WEBcfg;
			} elseif(is_object($SMScfg)) {
				$DBobj = $SMScfg;
			}

			$time = time();
			$now = date("[d-m-y@G:i:s]", $time);

			$connectLogFile = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".connect.errors.log";
			$dbselectLogFile = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".selectdb.errors.log";

			$dbpass = base64_decode($dbpass);

			$dbh = mysql_connect($dbhost, $dbuser, $dbpass, TRUE);

			if(!$dbh) {
				if ($DBobj->DB_DEBUG) {
					echo "<h2>Can't connect to $dbhost as $dbuser</h2>";
					echo "<p><b>MySQL Error</b>: ". mysql_error()."</p>";
				} else {
					$logData = "";
					$logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\n";
					$logData .= $now." - CONNECT ERROR> dbHost = ".$dbhost." :: dbUser = ".$dbuser."\n";
					$logData .= $now." - MySQL> ".mysql_error()."\n\n";
					error_log($logData, 3, $connectLogFile);
				}

				if ($DBobj->DB_DIE_ON_FAIL) {
					echo "<p>This script cannot continue, terminating.</p>";
					die();
				}
			}

			if(!mysql_select_db($dbname, $dbh)) {
				if ($DBobj->DB_DEBUG) {
					echo "<h2>Can't select database $dbname</h2>";
					echo "<p><b>MySQL Error</b>: ". mysql_error()."</p>";
				} else {
					$logData = "";
					$logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\n";
					$logData .= $now." - SELECT DB ERROR> dbHost = ".$dbhost." :: dbUser = ".$dbuser." :: dbName = ".$dbname."\n";
					$logData .= $now." - MySQL> ".mysql_error()."\n\n";
					error_log($logData, 3, $dbselectLogFile);
				}

				if($DBobj->DB_DIE_ON_FAIL) {
					echo "<p>This script cannot continue, terminating.</p>";
					die();
				}
			}

			return $dbh;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_query($query, $debug=false, $die_on_debug=false, $silent=false, $dbh=false, $query_debug="")
		{
		/* run the query $query against the current database.  if $debug is true, then
		 * we will just display the query on screen.  if $die_on_debug is true, and
		 * $debug is true, then we will stop the script after printing he debug message,
		 * otherwise we will run the query.  if $silent is true then we will surpress
		 * all error messages, otherwise we will print out that a database error has
		 * occurred */

			global $ADMINcfg, $MOBIcfg, $WEBcfg, $SMScfg;

			if(is_object($ADMINcfg))
			{
				$DBobj = $ADMINcfg;
			} elseif(is_object($MOBIcfg)) {
				$DBobj = $MOBIcfg;
			} elseif(is_object($WEBcfg)) {
				$DBobj = $WEBcfg;
			} elseif(is_object($SMScfg)) {
				$DBobj = $SMScfg;
			}

			$time = time();
			$now = date("[d-m-y@G:i:s]", $time);

			$queryLogFile = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".query.errors.log";

			if($DBobj->SQL_DEBUG)
			{
				if(!empty($query_debug))
				{
					$_SESSION["SQL_DEBUG"][$query_debug] = $query;
					$queryLog = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".query.log";
					$logData = "";
					$logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\n";
					$logData .= $now." - NAME> ".$query_debug."\n";
					$logData .= $now." - QUERY> ".$query."\n";
					error_log($logData, 3, $queryLog);
				}
			} else {
				$_SESSION["SQL_DEBUG"] = array();
			}


			if($debug) {
				echo "<pre>" . htmlspecialchars($query) . "</pre>";
				if($die_on_debug) {
					echo "<p>Terminating script for debug.</p>";
					die();
				}
			}

			if(!empty($query))
			{
				if($dbh)
				{
					/*
					if($DBobj->SYSTEM_DBCHARSET_ENABLE == "TRUE")
					{
						$x = mysql_query("SET NAMES '".$DBobj->SYSTEM_DBCHARSET."'", $dbh);
					}
					*/
					$qid = mysql_query($query, $dbh);
				} else {
					/*
					if($DBobj->SYSTEM_DBCHARSET_ENABLE == "TRUE")
					{
						$x = mysql_query("SET NAMES '".$DBobj->SYSTEM_DBCHARSET."'");
					}
					*/
					$qid = mysql_query($query);
				}
			}

			if(!$qid && !$silent) {
				if ($DBobj->DB_DEBUG) {
					echo "<h2>Can't execute query</h2>";
					echo "<pre>" . htmlspecialchars($query) . "</pre>";
					echo "<p><b>MySQL Error</b>: ". mysql_error()."</p>";
				} else {
					$logData = "";
					$logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\n";
					$logData .= $now." - QUERY> ".$query."\n";
					$logData .= $now." - ERROR> ".mysql_error()."\n\n";
					error_log($logData, 3, $queryLogFile);
				}

				if($DBobj->DB_DIE_ON_FAIL) {
					echo "<p>This script cannot continue, terminating.</p>";
					die();
				}
			}

			return $qid;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_escape_string($str, $dbh, $html=FALSE)
		{
		/* Escapes special characters in a string for use in a SQL statement */

			// Reverse magic_quotes_gpc effects on var if ON.
			$str = get_magic_quotes_gpc() ? stripslashes($str) : $str;
			$str = $html ? strip_tags($str) : $str;
			return mysql_real_escape_string($str, $dbh);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_fetch_assoc_array($qid)
		{
		/* grab the next row from the query result identifier $qid, and return it
		 * as an associative array.  if there are no more results, return FALSE */

			return mysql_fetch_assoc($qid);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_fetch_array($qid)
		{
		/* grab the next row from the query result identifier $qid, and return it
		 * as an associative array.  if there are no more results, return FALSE */

			return @mysql_fetch_array($qid);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_fetch_row($qid)
		{
		/* grab the next row from the query result identifier $qid, and return it
		 * as an array.  if there are no more results, return FALSE */

			return mysql_fetch_row($qid);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_fetch_object($qid)
		{
		/* Returns an object with properties that correspond to the fetched row and
		 * moves the internal data pointer ahead. If there are no more results, return
		 * FALSE */

			return mysql_fetch_object($qid);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_insert_id($dbh)
		{
			/* return the id of the last insert

			if(isset($this->conn))
			{
				return mysql_insert_id($this->connection);
			} else {
				return mysql_insert_id($dbh);
			}
			*/
			return mysql_insert_id($dbh);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_mysql_affected_rows($dbh)
		{
			/* return number of affceted rows for the INSERT / UPDATE / REPLACE / DELETE action */

			return mysql_affected_rows($dbh);
		}

		// ----------------------------------------------------------- >>>>>>>>>>
		// int mysql_num_rows  (  resource $result  )

		function db_mysql_num_rows($qid)
		{
			/* return number of rows for the result */

			return mysql_num_rows($qid);
		}

		// ----------------------------------------------------------- >>>>>>>>>>
		// PRIVATE METHODS
		// ----------------------------------------------------------- >>>>>>>>>>

		// ----------------------------------------------------------- >>>>>>>>>>
		// Private functions that shouldn't really be in here but they are useful
		// to have when bulding HTML forms. Anyway, here they are and here
		// they'll stay!
		// ----------------------------------------------------------- >>>>>>>>>>

		function _dbQueryLoopHTML($query, $prefix, $suffix, $found_str, $default="", $handler="")
		{
		/* this is an internal function and normally isn't called by the user.  it
		 * loops through the results of a select query $query and prints HTML
		 * around it, for use by things like listboxes and radio selections */

			$output = "";
			//$result = $this->db_query($query);
			$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, FALSE);
			while (list($val, $label) = $this->db_fetch_row($result)) {
				if (is_array($default))
					$selected = empty($default[$val]) ? "" : $found_str;
				else
					$selected = $val == $default ? $found_str : "";

				$output .= $prefix .' value="'.$val.'" '.$selected.'>'.$label.$suffix;
			}

			return $output;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function _dbListBoxHTML($query, $default="", $suffix="\n")
		{
		/* generate the <option> statements for a <select> listbox, based on the
		 * results of a SELECT query ($query).  any results that match $default
		 * are pre-selected, $default can be a string or an array in the case of
		 * multi-select listboxes.  $suffix is printed at the end of each <option>
		 * statement, and normally is just a line break */

			return $this->_dbQueryLoopHTML($query, "<option", $suffix, "selected", $default);
		}

	} // Close Class
?>