<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : GLOBALpdoWrapper.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Global PDO DB Wrapper.
	// ----------------------------------------------------------- >>>>>>>>>>

Class GLOBALpdoWrapper
	{
		function db_connect($vendor, $dbhost, $dbname, $dbuser, $dbpass)
		{
		/* connect to the database $dbname on $dbhost with the user/password pair
		 * $dbuser and $dbpass. */

			global $ADMINcfg;

			$DBobj = $ADMINcfg;

			$time = time();
			$now = date("[d-m-y@G:i:s]", $time);

			$connectLogFile = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".connect.errors.log";

			$dbpass = base64_decode($dbpass);
            
            if($vendor == 'mysql')
            {
                $_dsn = 'mysql:host='.$dbhost.';dbname='.$dbname;
            } elseif($vendor == 'sqlite') {
                $_dsn = 'sqlite:'.$dbhost.$dbname;            
            } elseif($vendor == 'sqlsrv') {
                $_dsn = 'sqlsrv:Server='.$dbhost.';Database='.$dbname.'';            
            } else {
                $_dsn = 'mysql:host='.$dbhost.';dbname='.$dbname.'';            
            }

            try {
                $dbh = new PDO($_dsn, $dbuser, $dbpass);
            }
            catch (PDOException $e) {

                if ($DBobj->DB_DEBUG) {
					echo "<h2>Can't connect to $dbhost as $dbuser</h2>";
					echo "<p><b>".strtoupper($vendor)." Error</b>: ". $e->getMessage()."</p>";
				} else {
					$logData = "";
					$logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\n";
					$logData .= $now." - CONNECT ERROR> DSN = ".$_dsn." :: dbUser = ".$dbuser."\n";
					$logData .= $now." - ".strtoupper($vendor)."> ".$e->getMessage()."\n\n";
					error_log($logData, 3, $connectLogFile);
				}

			    die();                
            }
            
			return $dbh;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_query($query, $debug=false, $die_on_debug=false, $silent=false, $dbh, $query_debug="")
		{
		/* run the query $query against the current database.  if $debug is true, then
		 * we will just display the query on screen.  if $die_on_debug is true, and
		 * $debug is true, then we will stop the script after printing he debug message,
		 * otherwise we will run the query.  if $silent is true then we will surpress
		 * all error messages, otherwise we will print out that a database error has
		 * occurred */

			global $ADMINcfg;

			$DBobj = $ADMINcfg;

			$time = time();
			$now = date("[d-m-y@G:i:s]", $time);

			$queryLogFile = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".query.errors.log";

			if($DBobj->SQL_DEBUG)
			{
				if(!empty($query_debug))
				{
					$_SESSION["SQL_DEBUG"][$query_debug] = $query;
					$queryLog = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".query.log";
					$logData = "";
					$logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\r";
					$logData .= $now." - NAME> ".$query_debug."\r";
					$logData .= $now." - QUERY> ".$query."\r\r";
					error_log($logData, 3, $queryLog);
				}
			} else {
				$_SESSION["SQL_DEBUG"] = array();
			}


			if($debug) 
			{
				$_SESSION["SQL_DEBUG"][$query_debug] = $query;
				$queryLog = $DBobj->SITE_LOG_PATH."/db/".$DBobj->LOG_FILE_PREFIX.".query.log";
				$logData = "";
				$logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\r";
				$logData .= $now." - NAME> ".$query_debug."\r";
				$logData .= $now." - QUERY> ".$query."\r\r";
				error_log($logData, 3, $queryLog);
				
				if($die_on_debug) {
					die();
				}
			}

            try {
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $dbh->prepare($query);
                $stmt->execute();
            }
            catch (PDOException $e) {
                    $logData = "";
                    $logData .= $now." - PAGE> ".$_SERVER["SCRIPT_FILENAME"]."\n";
                    $logData .= $now." - QUERY> ".$query."\n";
                    $logData .= $now." - ERROR> ".$e->getMessage()."\n\n";
                    error_log($logData, 3, $queryLogFile);
					$stmt = FALSE;
            }

			return $stmt;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_fetch_array($stmt)
		{
		/* grab the next row from the query result identifier $qid, and return it
		 * as an associative array.  if there are no more results, return FALSE */

			return $stmt->fetch(PDO::FETCH_ASSOC);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_fetch_row($stmt)
		{
		/* grab the next row from the query result identifier $qid, and return it
		 * as an array.  if there are no more results, return FALSE */

			return $stmt->fetch(PDO::FETCH_NUM);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_fetch_object($stmt)
		{
		/* Returns an object with properties that correspond to the fetched row and
		 * moves the internal data pointer ahead. If there are no more results, return
		 * FALSE */

			return $stmt->fetch(PDO::FETCH_OBJ);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function db_insert_id($dbh)
		{
			/* return the id of the last insert	*/
			return $dbh->lastInsertId();
		}

		// ----------------------------------------------------------- >>>>>>>>>>
		// PRIVATE METHODS
		// ----------------------------------------------------------- >>>>>>>>>>

		// ----------------------------------------------------------- >>>>>>>>>>
		// Private functions that shouldn't really be in here but they are useful
		// to have when bulding HTML forms. Anyway, here they are and here
		// they'll stay!
		// ----------------------------------------------------------- >>>>>>>>>>

		function _dbQueryLoopHTML($query, $prefix, $suffix, $found_str, $default="", $handler="")
		{
		/* this is an internal function and normally isn't called by the user.  it
		 * loops through the results of a select query $query and prints HTML
		 * around it, for use by things like listboxes and radio selections */

			$output = "";
			//$result = $this->db_query($query);
			$result = $this->db_query($query, FALSE, FALSE, FALSE, $handler, FALSE);
			while (list($val, $label) = $this->db_fetch_row($result)) {
				if (is_array($default))
					$selected = empty($default[$val]) ? "" : $found_str;
				else
					$selected = $val == $default ? $found_str : "";

				$output .= $prefix .' value="'.$val.'" '.$selected.'>'.$label.$suffix;
			}

			return $output;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function _dbListBoxHTML($query, $default="", $suffix="\n")
		{
		/* generate the <option> statements for a <select> listbox, based on the
		 * results of a SELECT query ($query).  any results that match $default
		 * are pre-selected, $default can be a string or an array in the case of
		 * multi-select listboxes.  $suffix is printed at the end of each <option>
		 * statement, and normally is just a line break */

			return $this->_dbQueryLoopHTML($query, "<option", $suffix, "selected", $default);
		}

	} // Close Class
?>