<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : SITESession.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Global Session Class for MS Azure Sessions
	// ----------------------------------------------------------- >>>>>>>>>>

	Class SITESession
	{

		var $_session;

		// ----------------------------------------------------------- >>>>>>>>>>

		function session($start, $exp, $limter, $regen=FALSE)
		{
			if($start)
			{
				$this->_init_session($exp, $limter, $regen);
			}

			if(is_array($this->_session))
			{
				$userIPAddress = $_SERVER['REMOTE_ADDR'];
				$userAgent = $_SERVER['HTTP_USER_AGENT'];
				$salt = '33Oe0LAP5dVOCSum94jXaH0qi';
				$timeStamp = time();

				if(!isset($_SESSION['_SESSTIME_']))
				{
					$_SESSION['_SESSTIME_'] = $timeStamp;
				}

				if(isset($_SESSION['_SESSIP_']) && ($_SESSION['_SESSIP_'] != $userIPAddress))
				{
					$destroySession = TRUE;
				} else {
					$destroySession = FALSE;
					$_SESSION['_SESSIP_'] = $userIPAddress;
				}

				if(isset($_SESSION['_SESSSIGN_']) && ($_SESSION['_SESSSIGN_'] != sha1($userAgent.$salt)))
				{
					$destroySession = TRUE;
				} else {
					$destroySession = FALSE;
					$_SESSION['_SESSSIGN_'] = sha1($userAgent.$salt);
				}

				if($destroySession)
				{
					$sessName = session_name();
					$_SESSION = array();
					if(isset($_COOKIE[$sessName]))
					{
						setcookie($sessName, '', time()-42000, '/');
					}
					$this->destroy_session();
					$this->_init_session($exp, $limter, $path, TRUE);
					$_SESSION['_SESSIP_'] = $userIPAddress;
					$_SESSION['_SESSSIGN_'] = sha1($userAgent.$salt);
					$_SESSION['_SESSTIME_'] = $timeStamp;
				}
			}
            
            /*
			if(is_array($this->_session))
			{
				foreach(array_keys($this->_session) as $key)
				{
					session_register($key);
				}
			}
            */
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function unset_session()
		{
			session_unset();
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function destroy_session()
		{
			session_destroy();
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function get_session_name()
		{
			return session_name();
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function get_session_id()
		{
			return session_id();
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function add_session_data($key, $val)
		{
			session_register($key);
			$this->_session[$key] = $val;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function unreg_session_var($var)
		{
			session_unregister($var);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function get_session_val($key)
		{
			if(isset($this->_session[$key]) && $this->in_session($key))
			{
				return $this->_session[$key];
			}
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function in_session($key)
		{
			return session_is_registered($key);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function get_session_list()
		{
			return array_keys($this->_session);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function set_cache_limiter($limiter)
		{
			session_cache_limiter($limiter);
		}

		// ----------------------------------------------------------- >>>>>>>>>>

		function set_cache_expire($delay)
		{
			session_cache_expire($delay);
		}

		// ----------------------------------------------------------- >>>>>>>>>>
		// PRIVATE METHODS
		// ----------------------------------------------------------- >>>>>>>>>>

		function _init_session($exp, $limter, $regen)
		{
			$this->set_cache_limiter($limter);
			session_start();

			if($regen)
			{
				session_regenerate_id(TRUE);
			}

			$this->_session = &$_SESSION;
			return;
		}

		// ----------------------------------------------------------- >>>>>>>>>>

	} // Close Class
?>