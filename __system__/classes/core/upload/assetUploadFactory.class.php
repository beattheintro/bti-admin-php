<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : assetUploadFactory.class.php
	// Author: Cactus Designs (UK) Ltd.
	// Date: 20/05/2009
	// Version: 1.0
	// Description: Assset Upload Factory.
	// DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		  with out consent from the author.
	// Copyright (c) 2009, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

Class assetUploadFactory Extends GLOBALmySQLWrapper
{
	var $OBJ;

	// Constructor
	function assetUploadFactory()
	{
		global $MOBIcfg, $ADMINcfg, $WEBcfg;

		if(is_object($MOBIcfg))
		{
			$this->OBJ = $MOBIcfg;
		} elseif(is_object($ADMINcfg)) {
			$this->OBJ = $ADMINcfg;
		} elseif(is_object($WEBcfg)) {
			$this->OBJ = $WEBcfg;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function uploadConnection()
	{
		global $SITEsession, $STDlib, $wall;
		// Connect to the dB and return a handler
		$handler = $this->db_connect($this->OBJ->DEFAULT_DBHOST, $this->OBJ->SYSTEM_DBNAME, $this->OBJ->SYSTEM_DBUSER, $this->OBJ->SYSTEM_DBPASSWORD);
		return $handler;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function saveAssetVideo($arrKey)
	{
		global $SITEsession, $STDlib;

		$timet = time();

		// Last Check!
		if(($_FILES[$arrKey]['error']['video']['source'] == 0) && ($_FILES[$arrKey]['error']['video']['preview'] == 0))
		{
			$uniqID1 = uniqid(rand(), true);
			$uniqID2 = uniqid(rand(), true);
			$uniqID3 = uniqid(rand(), true);
			$uniqID = sha1($uniqID1.$uniqID2.$uniqID3.$uniqID1.$uniqID2.$uniqID3);

			// ---------------------------------- >>>>>
			// VIDEO SOURCE PROCESSING
			// ---------------------------------- >>>>>

			$_FILES_SOURCE['tmp_name'] = $_FILES[$arrKey]['tmp_name']['video']['source'];
			$_FILES_SOURCE['name'] = $_FILES[$arrKey]['name']['video']['source'];
			$_FILES_SOURCE['type'] = $_FILES[$arrKey]['type']['video']['source'];
			$_FILES_SOURCE['error'] = $_FILES[$arrKey]['error']['video']['source'];
			$_FILES_SOURCE['size'] = $_FILES[$arrKey]['size']['video']['source'];
			$_FILES_SOURCE['INFO'] = $this->_videoFileData($_FILES_SOURCE['name'], $_FILES_SOURCE['type']);

			// Check we have an uploaded temporary (PHP) file.
			if(is_uploaded_file($_FILES_SOURCE['tmp_name']))
			{
				$DATA['VIDEO']['SOURCE']['META_FILENAME'] = explode("_", $_FILES_SOURCE['name']);

				$uniqFilename = 'VIDEO_SOURCE_'.$uniqID.$_FILES_SOURCE['INFO']['ext'];
				$destination = $_FILES_SOURCE['INFO']['path'].'/'.$uniqFilename;

				// Lets move it to the correct upload temp directory
				if(!move_uploaded_file($_FILES_SOURCE['tmp_name'], $destination))
				{
					$DATA['error'] = "Unable to copy ".$_FILES_SOURCE['tmp_name']." to ".$destination." directory!";
					return $DATA;
				}

				// CHMOD Videos
				//chmod($destination, 0644);

				$DATA['VIDEO']['SOURCE']['UPLOAD_DATA'] = $_FILES_SOURCE;
				$DATA['VIDEO']['SOURCE']['UNIQ_FILENAME'] = $uniqFilename;
				$DATA['VIDEO']['SOURCE']['UNIQ_FILENAME_ID'] = $uniqID;
				$DATA['VIDEO']['SOURCE']['PATH'] = $_FILES_SOURCE['INFO']['path'];
				$DATA['VIDEO']['SOURCE']['MIME'] = $_FILES_SOURCE['INFO']['mime'];
				$DATA['VIDEO']['SOURCE']['TYPE'] = $_FILES_SOURCE['INFO']['type'];
			} else {
				$DATA['error'] = "File ".$_FILES_SOURCE['name']." was not uploaded correctly!";
				return $DATA;
			}

			// ---------------------------------- >>>>>
			// VIDEO THUMBNAIL PROCESSING
			// ---------------------------------- >>>>>

			$_FILES_THUMB['tmp_name'] = $_FILES[$arrKey]['tmp_name']['video']['thumb'];
			$_FILES_THUMB['name'] = $_FILES[$arrKey]['name']['video']['thumb'];
			$_FILES_THUMB['type'] = $_FILES[$arrKey]['type']['video']['thumb'];
			$_FILES_THUMB['error'] = $_FILES[$arrKey]['error']['video']['thumb'];
			$_FILES_THUMB['size'] = $_FILES[$arrKey]['size']['video']['thumb'];
			$_FILES_THUMB['INFO'] = $this->_pictureFileData($_FILES_THUMB['name'], $_FILES_THUMB['type']);

			// Check we have an uploaded temporary (PHP) file.
			if(is_uploaded_file($_FILES_THUMB['tmp_name']))
			{
				$DATA['VIDEO']['THUMB']['META_FILENAME'] = explode("_", $_FILES_THUMB['name']);

				$uniqFilename = 'VIDEO_THUMB_'.$uniqID.$_FILES_THUMB['INFO']['ext'];
				$destination = $_FILES_THUMB['INFO']['path'].'/'.$uniqFilename;

				// Lets move it to the correct upload temp directory
				if(!move_uploaded_file($_FILES_THUMB['tmp_name'], $destination))
				{
					$DATA['error'] = "Unable to copy ".$_FILES_THUMB['tmp_name']." to ".$destination." directory!";
					return $DATA;
				}

				// CHMOD Videos
				//chmod($destination, 0644);

				$DATA['VIDEO']['THUMB']['UPLOAD_DATA'] = $_FILES_THUMB;
				$DATA['VIDEO']['THUMB']['UNIQ_FILENAME'] = $uniqFilename;
				$DATA['VIDEO']['THUMB']['UNIQ_FILENAME_ID'] = $uniqID;
				$DATA['VIDEO']['THUMB']['PATH'] = $_FILES_THUMB['INFO']['path'];
				$DATA['VIDEO']['THUMB']['MIME'] = $_FILES_THUMB['INFO']['mime'];
				$DATA['VIDEO']['THUMB']['TYPE'] = $_FILES_THUMB['INFO']['type'];
			} else {
				$DATA['error'] = "File ".$_FILES_THUMB['name']." was not uploaded correctly!";
				return $DATA;
			}

			// ---------------------------------- >>>>>
			// VIDEO PROMO PROCESSING
			// ---------------------------------- >>>>>

			if($_FILES[$arrKey]['error']['video']['promo'] != 4)
			{
				$_FILES_PROMO['tmp_name'] = $_FILES[$arrKey]['tmp_name']['video']['promo'];
				$_FILES_PROMO['name'] = $_FILES[$arrKey]['name']['video']['promo'];
				$_FILES_PROMO['type'] = $_FILES[$arrKey]['type']['video']['promo'];
				$_FILES_PROMO['error'] = $_FILES[$arrKey]['error']['video']['promo'];
				$_FILES_PROMO['size'] = $_FILES[$arrKey]['size']['video']['promo'];
				$_FILES_PROMO['INFO'] = $this->_pictureFileData($_FILES_PROMO['name'], $_FILES_PROMO['type']);

				// Check we have an uploaded temporary (PHP) file.
				if(is_uploaded_file($_FILES_PROMO['tmp_name']))
				{
					$DATA['VIDEO']['PROMO']['META_FILENAME'] = explode("_", $_FILES_PROMO['name']);

					$uniqFilename = 'VIDEO_PROMO_'.$uniqID.$_FILES_PROMO['INFO']['ext'];
					$destination = $_FILES_PROMO['INFO']['path'].'/'.$uniqFilename;

					// Lets move it to the correct upload temp directory
					if(!move_uploaded_file($_FILES_PROMO['tmp_name'], $destination))
					{
						$DATA['error'] = "Unable to copy ".$_FILES_PROMO['tmp_name']." to ".$destination." directory!";
						return $DATA;
					}

					// CHMOD Videos
					//chmod($destination, 0644);

					$DATA['VIDEO']['PROMO']['UPLOAD_DATA'] = $_FILES_PROMO;
					$DATA['VIDEO']['PROMO']['UNIQ_FILENAME'] = $uniqFilename;
					$DATA['VIDEO']['PROMO']['UNIQ_FILENAME_ID'] = $uniqID;
					$DATA['VIDEO']['PROMO']['PATH'] = $_FILES_PROMO['INFO']['path'];
					$DATA['VIDEO']['PROMO']['MIME'] = $_FILES_PROMO['INFO']['mime'];
					$DATA['VIDEO']['PROMO']['TYPE'] = $_FILES_PROMO['INFO']['type'];
				} else {
					$DATA['error'] = "File ".$_FILES_PROMO['name']." was not uploaded correctly!";
					return $DATA;
				}
			}

			return $DATA;
		} else {
			$DATA['error'] = 'File Error';
			return $DATA;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function saveAssetPicture($arrKey)
	{
		global $SITEsession, $STDlib;

		$timet = time();

		// Last Check!
		if(($_FILES[$arrKey]['error']['picture']['portrait'] == 0) && ($_FILES[$arrKey]['error']['picture']['landscape'] == 0) && ($_FILES[$arrKey]['error']['picture']['square'] == 0))
		{
			$uniqID1 = uniqid(rand(), true);
			$uniqID2 = uniqid(rand(), true);
			$uniqID3 = uniqid(rand(), true);
			$uniqID = sha1($uniqID1.$uniqID2.$uniqID3.$uniqID1.$uniqID2.$uniqID3);

			// ---------------------------------- >>>>>
			// PICTURE PORTRAIT PROCESSING
			// ---------------------------------- >>>>>

			$_FILES_PORTRAIT['tmp_name'] = $_FILES[$arrKey]['tmp_name']['picture']['portrait'];
			$_FILES_PORTRAIT['name'] = $_FILES[$arrKey]['name']['picture']['portrait'];
			$_FILES_PORTRAIT['type'] = $_FILES[$arrKey]['type']['picture']['portrait'];
			$_FILES_PORTRAIT['error'] = $_FILES[$arrKey]['error']['picture']['portrait'];
			$_FILES_PORTRAIT['size'] = $_FILES[$arrKey]['size']['picture']['portrait'];
			$_FILES_PORTRAIT['INFO'] = $this->_pictureFileData($_FILES_PORTRAIT['name'], $_FILES_PORTRAIT['type']);

			// Check we have an uploaded temporary (PHP) file.
			if(is_uploaded_file($_FILES_PORTRAIT['tmp_name']))
			{
				$DATA['PICTURE']['PORTRAIT']['META_FILENAME'] = explode("_", $_FILES_PORTRAIT['name']);

				$uniqFilename = 'PICTURE_PORTRAIT_'.$uniqID.$_FILES_PORTRAIT['INFO']['ext'];
				$destination = $_FILES_PORTRAIT['INFO']['path'].'/'.$uniqFilename;

				// Lets move it to the correct upload temp directory
				if(!move_uploaded_file($_FILES_PORTRAIT['tmp_name'], $destination))
				{
					$DATA['error'] = "Unable to copy ".$_FILES_PORTRAIT['tmp_name']." to ".$destination." directory!";
					return $DATA;
				}

				// CHMOD Videos
				//chmod($destination, 0644);

				$DATA['PICTURE']['PORTRAIT']['UPLOAD_DATA'] = $_FILES_PORTRAIT;
				$DATA['PICTURE']['PORTRAIT']['UNIQ_FILENAME'] = $uniqFilename;
				$DATA['PICTURE']['PORTRAIT']['UNIQ_FILENAME_ID'] = $uniqID;
				$DATA['PICTURE']['PORTRAIT']['PATH'] = $_FILES_PORTRAIT['INFO']['path'];
				$DATA['PICTURE']['PORTRAIT']['MIME'] = $_FILES_PORTRAIT['INFO']['mime'];
				$DATA['PICTURE']['PORTRAIT']['TYPE'] = $_FILES_PORTRAIT['INFO']['type'];
			} else {
				$DATA['error'] = "PORTRAIT File ".$_FILES_PORTRAIT['name']." was not uploaded correctly!";
				return $DATA;
			}

			// ---------------------------------- >>>>>
			// PICTURE LANDSCAPE PROCESSING
			// ---------------------------------- >>>>>

			$_FILES_LANDSCAPE['tmp_name'] = $_FILES[$arrKey]['tmp_name']['picture']['landscape'];
			$_FILES_LANDSCAPE['name'] = $_FILES[$arrKey]['name']['picture']['landscape'];
			$_FILES_LANDSCAPE['type'] = $_FILES[$arrKey]['type']['picture']['landscape'];
			$_FILES_LANDSCAPE['error'] = $_FILES[$arrKey]['error']['picture']['landscape'];
			$_FILES_LANDSCAPE['size'] = $_FILES[$arrKey]['size']['picture']['landscape'];
			$_FILES_LANDSCAPE['INFO'] = $this->_pictureFileData($_FILES_LANDSCAPE['name'], $_FILES_LANDSCAPE['type']);

			// Check we have an uploaded temporary (PHP) file.
			if(is_uploaded_file($_FILES_LANDSCAPE['tmp_name']))
			{
				$DATA['PICTURE']['LANDSCAPE']['META_FILENAME'] = explode("_", $_FILES_LANDSCAPE['name']);

				$uniqFilename = 'PICTURE_LANDSCAPE_'.$uniqID.$_FILES_LANDSCAPE['INFO']['ext'];
				$destination = $_FILES_LANDSCAPE['INFO']['path'].'/'.$uniqFilename;

				// Lets move it to the correct upload temp directory
				if(!move_uploaded_file($_FILES_LANDSCAPE['tmp_name'], $destination))
				{
					$DATA['error'] = "Unable to copy ".$_FILES_LANDSCAPE['tmp_name']." to ".$destination." directory!";
					return $DATA;
				}

				// CHMOD Videos
				//chmod($destination, 0644);

				$DATA['PICTURE']['LANDSCAPE']['UPLOAD_DATA'] = $_FILES_LANDSCAPE;
				$DATA['PICTURE']['LANDSCAPE']['UNIQ_FILENAME'] = $uniqFilename;
				$DATA['PICTURE']['LANDSCAPE']['UNIQ_FILENAME_ID'] = $uniqID;
				$DATA['PICTURE']['LANDSCAPE']['PATH'] = $_FILES_LANDSCAPE['INFO']['path'];
				$DATA['PICTURE']['LANDSCAPE']['MIME'] = $_FILES_LANDSCAPE['INFO']['mime'];
				$DATA['PICTURE']['LANDSCAPE']['TYPE'] = $_FILES_LANDSCAPE['INFO']['type'];
			} else {
				$DATA['error'] = "LANDSCAPE File ".$_FILES_LANDSCAPE['name']." was not uploaded correctly!";
				return $DATA;
			}

			// ---------------------------------- >>>>>
			// PICTURE SQUARE PROCESSING
			// ---------------------------------- >>>>>

			$_FILES_SQUARE['tmp_name'] = $_FILES[$arrKey]['tmp_name']['picture']['square'];
			$_FILES_SQUARE['name'] = $_FILES[$arrKey]['name']['picture']['square'];
			$_FILES_SQUARE['type'] = $_FILES[$arrKey]['type']['picture']['square'];
			$_FILES_SQUARE['error'] = $_FILES[$arrKey]['error']['picture']['square'];
			$_FILES_SQUARE['size'] = $_FILES[$arrKey]['size']['picture']['square'];
			$_FILES_SQUARE['INFO'] = $this->_pictureFileData($_FILES_SQUARE['name'], $_FILES_SQUARE['type']);

			// Check we have an uploaded temporary (PHP) file.
			if(is_uploaded_file($_FILES_SQUARE['tmp_name']))
			{
				$DATA['PICTURE']['SQUARE']['META_FILENAME'] = explode("_", $_FILES_SQUARE['name']);

				$uniqFilename = 'PICTURE_SQUARE_'.$uniqID.$_FILES_SQUARE['INFO']['ext'];
				$destination = $_FILES_SQUARE['INFO']['path'].'/'.$uniqFilename;

				// Lets move it to the correct upload temp directory
				if(!move_uploaded_file($_FILES_SQUARE['tmp_name'], $destination))
				{
					$DATA['error'] = "Unable to copy ".$_FILES_SQUARE['tmp_name']." to ".$destination." directory!";
					return $DATA;
				}

				// CHMOD Videos
				//chmod($destination, 0644);

				$DATA['PICTURE']['SQUARE']['UPLOAD_DATA'] = $_FILES_SQUARE;
				$DATA['PICTURE']['SQUARE']['UNIQ_FILENAME'] = $uniqFilename;
				$DATA['PICTURE']['SQUARE']['UNIQ_FILENAME_ID'] = $uniqID;
				$DATA['PICTURE']['SQUARE']['PATH'] = $_FILES_SQUARE['INFO']['path'];
				$DATA['PICTURE']['SQUARE']['MIME'] = $_FILES_SQUARE['INFO']['mime'];
				$DATA['PICTURE']['SQUARE']['TYPE'] = $_FILES_SQUARE['INFO']['type'];
			} else {
				$DATA['error'] = "SQUARE File ".$_FILES_SQUARE['name']." was not uploaded correctly!";
				return $DATA;
			}

			return $DATA;

		} else {
			$DATA['error'] = 'File Error';
			return $DATA;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function saveAsset($arrKey)
	{
		global $SITEsession, $STDlib;

		$timet = time();

		// Last Check!
		if($_FILES[$arrKey]['error'] == 0)
		{
			$userTempName = $_FILES[$arrKey]['tmp_name'];
			$uniqID = uniqid(rand(), true);
			$fileName = $_FILES[$arrKey]['name'];
			$mime = $_FILES[$arrKey]['type'];

			if($mime == 'application/octet-stream')
			{
				// We are going to have to work out the mime from the extension!
				$ext = strtolower(strstr($fileName, '.'));

				if($ext == '.jpg')
				{
					$mime = 'image/jpeg';
				} elseif($ext == '.3gp') {
					$mime = 'video/3gpp';
				} elseif($ext == '.avi') {
					$mime = 'video/avi';
				} elseif($ext == '.mpg') {
					$mime = 'video/mpeg';
				} elseif($ext == '.mp4') {
					$mime = 'video/mp4';
				} elseif($ext == '.wmv') {
					$mime = 'video/x-ms-wmv';
				} elseif($ext == '.mov') {
					$mime = 'video/quicktime';
				}
			}

			if(($mime == 'image/jpeg') || ($mime == 'image/pjpeg'))
			{
				$ext = '.jpg';
				$mime = 'image/jpeg';
				$type = 'image';
				$path = $this->OBJ->SYSTEM_IMAGE_TMP_PATH;
			} elseif(($mime == 'video/3gpp')) {
				$ext = '.3gp';
				$mime = 'video/3gpp';
				$type = 'video';
				$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
			} elseif(($mime == 'video/avi')) {
				$ext = '.avi';
				$mime = 'video/avi';
				$type = 'video';
				$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
			} elseif(($mime == 'video/mp4')) {
				$ext = '.mp4';
				$mime = 'video/mp4';
				$type = 'video';
				$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
			} elseif(($mime == 'video/mpeg')) {
				$ext = '.mpg';
				$mime = 'video/mpeg';
				$type = 'video';
				$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
			} elseif(($mime == 'video/x-ms-wmv')) {
				$ext = '.wmv';
				$mime = 'video/x-ms-wmv';
				$type = 'video';
				$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
			} elseif(($mime == 'video/quicktime')) {
				$ext = '.mov';
				$mime = 'video/quicktime';
				$type = 'video';
				$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
			} else {
				$ext = strtolower(strstr($fileName, '.'));
				$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
			}

			// Check we have an uploaded temporary (PHP) file.
			if(is_uploaded_file($userTempName))
			{
				$DATA['META_FILENAME'] = explode("_", $_FILES[$arrKey]['name']);

				$uniqFilename = sha1($uniqID.$fileName.$uniqID.$fileName.$uniqID).$ext;
				$destination = $path.'/'.$uniqFilename;

				// Lets move it to the correct upload temp directory
				if(!move_uploaded_file($userTempName, $destination))
				{
					$DATA['error'] = "Unable to copy ".$uniqFilename." to destination directory!";
					return $DATA;
				}

				// CHMOD Videos
				//chmod($destination, 0644);

				$DATA['UPLOAD_DATA'] = $_FILES[$arrKey];
				$DATA['UNIQ_FILENAME'] = $uniqFilename;
				$DATA['PATH'] = $path;
				$DATA['MIME'] = $mime;
				$DATA['TYPE'] = $type;
				return $DATA;
			} else {
				$DATA['error'] = "File ".$userTempName." was not uploaded correctly!";
				return $DATA;
			}

		} else {
			$DATA['error'] = 'File Error';
			return $DATA;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function deleteAsset($fileInfo)
	{
		global $SITEsession, $STDlib;

		$filePath = $fileInfo['FILEPATH'];
		$fileName = $fileInfo['FILENAME'];

		if(!empty($filePath) && !empty($fileName))
		{
			if(unlink($filePath.'/'.$fileName))
			{
				return;
			} else {
				$DATA['error'] = 'File Error';
				return $DATA;
			}
		} else {
			$DATA['error'] = 'File Error';
			return $DATA;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// PRIVATE METHODS
	// ----------------------------------------------------------- >>>>>>>>>>

	function _videoFileData($fileName, $mime)
	{
		global $SITEsession, $STDlib;

		if(empty($fileName) || empty($mime))
		{
			return;
		}

		if($mime == 'application/octet-stream')
		{
			// We are going to have to work out the mime from the extension!
			$ext = strtolower(strstr($fileName, '.'));

			if($ext == '.3gp') {
				$mime = 'video/3gpp';
			} elseif($ext == '.avi') {
				$mime = 'video/avi';
			} elseif($ext == '.mpg') {
				$mime = 'video/mpeg';
			} elseif($ext == '.mp4') {
				$mime = 'video/mp4';
			} elseif($ext == '.wmv') {
				$mime = 'video/x-ms-wmv';
			} elseif($ext == '.mov') {
				$mime = 'video/quicktime';
			}
		}

		if(($mime == 'video/3gpp'))
		{
			$ext = '.3gp';
			$mime = 'video/3gpp';
			$type = 'video';
			$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
		} elseif(($mime == 'video/avi')) {
			$ext = '.avi';
			$mime = 'video/avi';
			$type = 'video';
			$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
		} elseif(($mime == 'video/mp4')) {
			$ext = '.mp4';
			$mime = 'video/mp4';
			$type = 'video';
			$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
		} elseif(($mime == 'video/mpeg')) {
			$ext = '.mpg';
			$mime = 'video/mpeg';
			$type = 'video';
			$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
		} elseif(($mime == 'video/x-ms-wmv')) {
			$ext = '.wmv';
			$mime = 'video/x-ms-wmv';
			$type = 'video';
			$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
		} elseif(($mime == 'video/quicktime')) {
			$ext = '.mov';
			$mime = 'video/quicktime';
			$type = 'video';
			$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
		} else {
			$ext = strtolower(strstr($fileName, '.'));
			$type = 'video';
			$path = $this->OBJ->SYSTEM_VIDEO_TMP_PATH;
		}

		$DATA['mime'] = $mime;
		$DATA['ext'] = $ext;
		$DATA['type'] = $type;
		$DATA['path'] = $path;

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function _pictureFileData($fileName, $mime)
	{
		global $SITEsession, $STDlib;

		if(empty($fileName) || empty($mime))
		{
			return;
		}

		if($mime == 'application/octet-stream')
		{
			// We are going to have to work out the mime from the extension!
			$ext = strtolower(strstr($fileName, '.'));

			if($ext == '.jpg') {
				$mime = 'image/jpeg';
			} elseif($ext == '.png') {
				$mime = 'image/png';
			} elseif($ext == '.gif') {
				$mime = 'image/gif';
			}
		}

		if(($mime == 'image/jpeg') || ($mime == 'image/pjpeg'))
		{
			$ext = '.jpg';
			$mime = 'image/jpeg';
			$type = 'image';
			$path = $this->OBJ->SYSTEM_IMAGE_TMP_PATH;
		} elseif(($mime == 'image/png')) {
			$ext = '.png';
			$mime = 'image/png';
			$type = 'image';
			$path = $this->OBJ->SYSTEM_IMAGE_TMP_PATH;
		} elseif(($mime == 'image/gif')) {
			$ext = '.gif';
			$mime = 'image/gif';
			$type = 'image';
			$path = $this->OBJ->SYSTEM_IMAGE_TMP_PATH;
		} else {
			$ext = strtolower(strstr($fileName, '.'));
			$type = 'image';
			$path = $this->OBJ->SYSTEM_IMAGE_TMP_PATH;
		}

		$DATA['mime'] = $mime;
		$DATA['ext'] = $ext;
		$DATA['type'] = $type;
		$DATA['path'] = $path;

		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function _safeVar($var, $dbh="", $html=FALSE)
	{
		global$SITEsession, $STDlib, $wall;
		if($dbh) {
			return $this->db_escape_string($var, $dbh, $html);
		} else {
			return $STDlib->makeSafe($var);
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>