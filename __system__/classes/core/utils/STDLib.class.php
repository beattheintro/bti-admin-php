<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : STDlib.class.php
	// Author: Cactus Designs (UK) Ltd.
	// Date: 06/12/2007
	// Version: 1.0
	// Description: Useful functions that are needed across both CMS and Site.
	//			 DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		 with out consent from the author.
	// Copyright (c) 2007, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

Class STDLib
{

	var $imageMimeTypes;
	var $videoMimeTypes;
	var $mimeTypesToExtensions;
	var $keywordTypes;
	var $typeCategories;
	var $rejectReasons;

	var $daysOfWeek;

	var $systemPortlets;
	var $videoLengths;

	function STDLib()
	{
		$this->imageMimeTypes = array('image/gif'=>'GIF - image/gif', 'image/jpeg'=>'JPG - image/jpeg', 'image/png'=>'PNG - image/png');
		$this->videoMimeTypes = array('video/3gpp'=>'3GP - video/3gpp', 'video/avi'=>'AVI - video/avi', 'video/mpeg'=>'MPG - video/mpeg', 'video/mp4'=>'MP4 - video/mp4', 'video/x-ms-wmv'=>'WMV - video/x-ms-wmv', 'video/quicktime'=>'MOV - video/quicktime');
		$this->mimeTypesToExtensions = array('video/3gpp'=>'.3gp', 'video/avi'=>'.avi', 'video/mpeg'=>'.mpg', 'video/mp4'=>'.mp4', 'video/x-ms-wmv'=>'.wmv', 'video/quicktime'=>'.mov', 'image/gif'=>'.gif', 'image/jpeg'=>'.jpg', 'image/png'=>'.png');
		$this->keywordTypes = array("STANDARD"=>"Standard","DESCRIPTIVE"=>"Descriptive");

		$this->typeCategories = array("PICTURE"=>"Picture","VIDEO"=>"Video","APPLICATION"=>"Application");

		$this->numbers = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

		$this->numbersAsWords = array("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "zero", "naught");

		$this->rejectReasons = array('1'=>'Use of music','2'=>'More than one person featured','3'=>'Content too explicit','4'=>'Subject appears to be under 18','5'=>'Poor technical quality','6'=>'Profile contains inappropriate language','7'=>'Content contains inappropriate language','8'=>'Contains 3rd party branding/advertising','10'=>'Contains personal or contact information','11'=>'Duplicate content item','12'=>'Considered 3rd party owned','13'=>'Content not appropriate','9'=>'Other');

		$this->contentStandards = array('1'=>'(1) Hunk', '3'=>'(3) Nude', '4'=>'(4) Erection', '5'=>'(5) Anal Insertion', '6'=>'(6) XXX', '7'=>'(7) Nude+1', '8'=>'(8) Erection+1', '9'=>'(9) XXX+1');

		$this->daysOfWeek = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
		$this->monthsOfYear = array("01"=>'January',"02"=>'February',"03"=>'March',"04"=>'April',"05"=>'May',"06"=>'June',"07"=>'July',"08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");

		$this->systemPortlets = array(
													array("NAME"=>"Scroller", "ID"=>"1"),
													array("NAME"=>"Genre", "ID"=>"2"),
													array("NAME"=>"Banner", "ID"=>"3"),
													array("NAME"=>"Twintych", "ID"=>"4"),
													array("NAME"=>"Triptych", "ID"=>"5"),
													array("NAME"=>"Quadtych", "ID"=>"6"),
													array("NAME"=>"Content", "ID"=>"7"),
													array("NAME"=>"Navigation", "ID"=>"8"),
													array("NAME"=>"Editorial", "ID"=>"9")
												);

		$this->videoLengths = array(
																array("NAME"=>"video10", "LENGTH"=>"10"),					// 10 Secs
																array("NAME"=>"video20", "LENGTH"=>"20"),					// 20 Secs
																array("NAME"=>"video30", "LENGTH"=>"30"),					// 30 Secs
																array("NAME"=>"video60", "LENGTH"=>"60"),					// 1 min
																array("NAME"=>"video90", "LENGTH"=>"90"),					// 1.5 Mins
																array("NAME"=>"video120", "LENGTH"=>"120"),				// 2 Mins
																array("NAME"=>"video150", "LENGTH"=>"150"),				// 2.5 Mins
																array("NAME"=>"video180", "LENGTH"=>"180"),				// 3 Mins
																array("NAME"=>"video210", "LENGTH"=>"210"),				// 3.5 Mins
																array("NAME"=>"video240", "LENGTH"=>"240"),				// 4 Mins
																array("NAME"=>"video270", "LENGTH"=>"270"),				// 4.5 Mins
																array("NAME"=>"video300", "LENGTH"=>"300"),				// 5 Mins
																array("NAME"=>"video450", "LENGTH"=>"450"),				// 7.5 Mins
																array("NAME"=>"video600", "LENGTH"=>"600"),				// 10 Mins
																array("NAME"=>"video1200", "LENGTH"=>"1200"),			// 20 Mins
																array("NAME"=>"video1800", "LENGTH"=>"1800"),			// 30 Mins
																array("NAME"=>"video2400", "LENGTH"=>"2400"),			// 40 Mins
																array("NAME"=>"video3000", "LENGTH"=>"3000"),			// 50 Mins
																array("NAME"=>"video3600", "LENGTH"=>"3600"),			// 60 Mins
																array("NAME"=>"video4500", "LENGTH"=>"4500"),			// 1 Hour 15 Mins
																array("NAME"=>"video5400", "LENGTH"=>"5400"),			// 1 Hour 30 Mins
																array("NAME"=>"video6300", "LENGTH"=>"6300"),			// 1 Hour 45 Mins
																array("NAME"=>"video7200", "LENGTH"=>"7200"),			// 2 Hours
																array("NAME"=>"video8100", "LENGTH"=>"8100"),			// 2 Hours 15 Mins
																array("NAME"=>"video9000", "LENGTH"=>"9000"),			// 2 Hours 30 Mins
																array("NAME"=>"video9900", "LENGTH"=>"9900"),			// 2 Hours 45 Mins
																array("NAME"=>"video10800", "LENGTH"=>"10800"),		// 3 Hours
																array("NAME"=>"video12600", "LENGTH"=>"12600"),		// 3 Hours 30 Mins
																array("NAME"=>"video14400", "LENGTH"=>"14400"),		// 4 Hours
																array("NAME"=>"video18000", "LENGTH"=>"18000"),		// 5 Hours
															);

	}
	// ----------------------------------------------------------- >>>>>>>>>>

	function v(&$var) {
	/* returns $var with the HTML characters (like "<", ">", etc.) properly quoted,
	 * or if $var is undefined, will return an empty string.  note this function
	 * must be called with a variable, for normal strings or functions use o() */

		return isset($var) ? htmlspecialchars(addslashes($var)) : "";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function ov(&$var) {
	/* returns $var with the HTML characters (like "<", ">", etc.) properly quoted,
	 * and slashes stripped or if $var is undefined, will return an empty string.
	 * note this function must be called with a variable, for normal strings or
	 * functions use o() */

		return isset($var) ? htmlspecialchars(stripslashes($var)) : "";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function pv(&$var) {
	/* prints $var with the HTML characters (like "<", ">", etc.) properly quoted,
	 * or if $var is undefined, will print an empty string.  note this function
	 * must be called with a variable, for normal strings or functions use p() */

		echo isset($var) ? htmlspecialchars(stripslashes($var)) : "";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function o($var) {
	/* returns $var with HTML characters (like "<", ">", etc.) properly quoted,
	 * or if $var is empty, will return an empty string. */

		return empty($var) ? "" : htmlspecialchars(stripslashes($var));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function p($var) {
	/* prints $var with HTML characters (like "<", ">", etc.) properly quoted,
	 * or if $var is empty, will print an empty string. */

		echo empty($var) ? "" : htmlspecialchars(stripslashes($var));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function formCheckedArray(&$var, $val, $true_value = "checked", $false_value = "") {
	/* Slightly different in that it returns an array with 2 keys, "checked" & "notchecked" */

		if ($var == $val) {
			$DATA["checked"] = "checked";
			$DATA["notchecked"] = "";
		} else {
			$DATA["checked"] = "";
			$DATA["notchecked"] = "checked";
		}

		return $DATA;
	}


	// ----------------------------------------------------------- >>>>>>>>>>

	function isChecked($var, $val) {
		if ($var == $val) {
			$checked = ' checked="checked"';
		} else {
			$checked = '';
		}
		return $checked;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function arrayValuesRecursive($ary)
	{
		$lst = array();

		foreach( array_keys($ary) as $k )
		{
			$v = $ary[$k];

			if (is_scalar($v))
			{
				$list[] = $v;
			} elseif (is_array($v)) {
				$list = array_merge( $list, $this->arrayValuesRecursive($v));
			}
		}
		return $list;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function decodeHtmlSpecialChars($str)
	{
		$trans = get_html_translation_table(HTML_ENTITIES);
		$trans = array_flip($trans);
		$str = strtr($str, $trans);
		$str = preg_replace('/\&\#([0-9]+)\;/me', "chr('\\1')", $str);
		return $str;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function str_Ireplace($needle, $replacement, $haystack)
	{
		$i = 0;
		while (($pos = strpos(strtolower($haystack), strtolower($needle), $i)) !== false)
		{
			$haystack = substr($haystack, 0, $pos) . $replacement. substr($haystack, $pos + strlen($needle));
			$i = $pos + strlen($replacement);
		}
		return $haystack;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function makeSafe($var) {
	/* returns $var with HTML removed, and quotes escaped
	 * or if $var is empty, will return an empty string. */

		return empty($var) ? "" : strip_tags(addslashes($var));

	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function makeQuerySafe($var) {
	/* returns $var with HTML removed, or if $var is empty, will return an
	 * empty string. */

		return empty($var) ? "" : str_replace("'", "''", strip_tags($var));

	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function arrayToSelect($option, $selected='', $firstText, $optgroup=NULL)
	{
		$html = '';

		if ($selected == '' && $firstText != '') {
			$html .= '<option value="" selected="selected">'.$firstText.'</option>';
		} elseif($selected != '' && $firstText != '') {
			$html .= '<option value="">'.$firstText.'</option>';
		}

		if (isset($optgroup)) {
			foreach ($optgroup as $optgroupKey => $optgroupValue) {
				$html .= '<optgroup label="' . $optgroupValue . '">';

				foreach ($option[$optgroupKey] as $optionKey => $optionValue) {
					if ($optionKey == $selected) {
						$html .= '<option selected="selected" value="' . $optionKey . '">' . $optionValue . '</option>';
					} else {
						$html .= '<option value="' . $optionKey . '">' . $optionValue . '</option>';
					}
				}

				$html .= '</optgroup>';
			}
		} else {
			if(!empty($option))
			{
				foreach ($option as $key => $value) {
					if ($key == $selected) {
						$html .= '<option selected="selected" value="' . $key . '">' . $value . '</option>';
					} else {
						$html .= '<option value="' . $key . '">' . $value . '</option>';
					}
				}
			}
		}

		return $html;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function arrayToCheckbox($option, $htmlId, $format, $selected='')
	{
		$html = '';

		if(!empty($option))
		{
			foreach ($option as $key => $value)
			{
				if($format == 'array')
				{
					$idFormat = '[]';
				} else {
					$idFormat = '_'.$key;
				}

				if(is_array($selected))
				{
					for($i=0; isset($selected[$i]); $i++)
					{
						if ($key == $selected[$i])
						{
							$checked = 'checked="checked"';
							break;
						} else {
							$checked = '';
						}
					}
					$html .= '<input type="checkbox" id="'.$htmlId.$idFormat.'" name="'.$htmlId.$idFormat.'" value="' . $key . '" '.$checked.' />&nbsp;' . $value . '&nbsp;<br/>';
				} else {
					$html .= '<input type="checkbox" id="'.$htmlId.$idFormat.'" name="'.$htmlId.$idFormat.'" value="' . $key . '" />&nbsp;' . $value . '&nbsp;<br/>';
				}
			}
		}

		return $html;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function ordinalSuffix($value, $sup=FALSE)
	{
		if(!is_numeric($value))
		{
			return;
		}

		if ($value % 100 > 10 && $value %100 < 14)
		{
			/*** check for 11, 12, 13 ***/
			$suffix = 'th';
		} elseif($value == 0) {
			/*** check if number is zero ***/
			$suffix = '';
		} else {
			/*** get the last digit ***/
			$last = substr($value, -1, 1);

			switch($last)
			{
				case "1":
				$suffix = 'st';
				break;

				case "2":
				$suffix = 'nd';
				break;

				case "3":
				$suffix = 'rd';
				break;

				default:
				$suffix = 'th';
			}
		}

		if($sup)
		{
			$suffix = "<sup>" . $suffix . "</sup>";
		}

		return $value.$suffix;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function url($url, $query)
	{
		global $STDlib, $SITEsession;

		if(session_id() && !strstr($query, session_id()))
		{
			$ssid = htmlspecialchars(session_name().'='.session_id()); //htmlspecialchars(SID);

			if(!empty($query) && !strstr($query, $ssid))
			{
				$query = $query .'&amp;'. $ssid;
			} else {
				$query = '?'.$ssid;
			}
		}

		return $url.$query;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function renderTemplate($tpl, $tplData, $path)
	{
		if (empty($tplData)) {
			return;
		}

		include_once($path.'/'.$tpl);

		// Assumes tpl ends in '.tpl.php'
		$tpl = basename($tpl);
		$tplFuntion = substr($tpl, 0, (strlen($tpl)-8));

		ob_start();
		$tplFuntion($tplData);
		$tplResult = ob_get_contents();
		ob_end_clean();

		return $tplResult;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function mt()
	{
		$mtime = explode(" ", microtime());
		return ($mtime[1] + $mtime[0]);
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function validateEmailAddress($address)
	{
		if(ereg('^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$', $address))
		{
			return true;
		} else {
			return false;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function makePassword($length)
	{

		$salt = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz";

		$len = strlen($salt);
		$pass="";
		//mt_srand(10000000*(double)microtime());

		for ($i = 0; $i < $length; $i++)
		{
			$pass .= $salt[mt_rand(0,$len - 1)];
		}
		return $pass;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function rndStr($length)
	{

		$salt = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz";

		$len = strlen($salt);
		$str="";

		for ($i = 0; $i < $length; $i++)
		{
			$str .= $salt[mt_rand(0,$len - 1)];
		}
		return $str;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function dateDiff($start, $end)
	{
		$start_ts = strtotime($start);
		$end_ts = strtotime($end);
		$diff = $end_ts - $start_ts;
		return round($diff / 86400);
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Date format (I/O): YYYY-MM-DD

	function createDateRangeArray($strDateFrom, $strDateTo)
	{
		$aryRange=array();

		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2), substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2), substr($strDateTo,8,2),substr($strDateTo,0,4));

		if($iDateTo>=$iDateFrom)
		{
			array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry

			while ($iDateFrom<$iDateTo) {
				$iDateFrom+=86400; // add 24 hours
				array_push($aryRange,date('Y-m-d',$iDateFrom));
			}
		}
		return $aryRange;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Count from '0104' because January 4th is always in week 1 (according to ISO 8601).

	function getDaysInWeek($weekNumber, $year)
	{
		$time = strtotime($year . '0104 +' . ($weekNumber - 1). ' weeks');
		// Get the time of the first day of the week
		$mondayTime = strtotime('-' . (date('w', $time) - 1) . ' days',$time);
		// Get the times of days 0 -> 6
		$dayTimes = array();
		for ($i = 0; $i < 7; ++$i)
		{
			$dayTimes[] = strtotime('+' . $i . ' days', $mondayTime);
		}
		// Return timestamps for mon-sun.
		return $dayTimes;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function unlinkRecursive($dir, $deleteRootToo)
	{
		global $SITEsession, $STDlib;

		if(!$dh = @opendir($dir))
		{
			return;
		}

		while (false !== ($file = readdir($dh))) {
			if($file == '.' || $file == '..')
			{
				continue;
			}

			if (!@unlink($dir . '/' . $file))
			{
				$this->unlinkRecursive($dir.'/'.$file, true);
			}
		}

		closedir($dh);

		if ($deleteRootToo)
		{
			@rmdir($dir);
		}

		return;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function chmodRecursive($path, $octalMode)
	{
		global $SITEsession, $STDlib;

		// If the path is a file, just chmod the file and exit
		if(!is_dir($path))
		{
			return chmod($path, $octalMode);
		}

		if(!$dh = @opendir($path))
		{
			return;
		}

		// Loop through the directory and chmod the files, checking for symlinks etc.
		while (($file = readdir($dh)) !== false) {
			if($file == '.' || $file == '..')
			{
				continue;
			}

			$fullPath = $path.'/'.$file;
			if(is_link($fullPath))
			{
				return FALSE;
			} elseif(!is_dir($fullPath) && !chmod($fullPath, $octalMode)) {
				return FALSE;
			} elseif(!$this->chmodRecursive($fullPath, $octalMode)) {
				return FALSE;
			}
		}

		closedir($dh);

		// Now chmod the directory
		if(chmod($path, $octalMode))
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function strstrb($h, $n)
	{
		global $SITEsession, $STDlib;

		//$h = haystack, $n = needle
		return array_shift(explode($n,$h,2));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function arraySortBy($arr, $sortBy)
	{
		foreach($arr as $k=>$v)
		{
			$b[$k] = strtolower($v[$sortBy]);
		}

		asort($b);

		foreach($b as $key=>$val)
		{
			$c[] = $arr[$key];
		}

		return $c;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Array search for multidimensional arrays

	function multi_array_search($needle, $haystack)
	{
		foreach($haystack as $key=>$value)
		{
			$currentKey = $key;

			if($needle == $value)
			{
				return $value;
			}

			if($this->multi_array_search($needle, $value) == true)
			{
				return $currentKey;
			}
		}
		return false;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function generateQRImage($qrMessage, $qrWidth, $qrHeight)
	{
		$googleChartAPI = 'http://chart.apis.google.com/chart?cht=qr';
		$qrDimensions1 = '&chs=500x500';
		$qrDimensions2 = '&chs='.$qrWidth.'x'.$qrHeight;
		$qrData = '&chl='.urlencode($qrMessage);

		$qrCodeURL = $googleChartAPI.$qrDimensions1.$qrData;
		$qrCodeImage = $googleChartAPI.$qrDimensions2.$qrData;

		$qrImageMarkup = '<a href="'.$qrCodeURL.'" title="Click for larger version"><img src="'.$qrCodeImage.'" width="'.$qrWidth.'" height="'.$qrHeight.'" alt="Click for larger version" border="0" /></a>';

		return $qrImageMarkup;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	
	function guid() 
	{
		// VALID RFC 4211 COMPLIANT Universally Unique IDentifiers (UUID) version 4
		// Version 4 UUIDs are pseudo-random.
		
		mt_srand((double)microtime()*10000);
		
		$uuid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

			// 16 bits for "time_mid"
			mt_rand( 0, 0xffff ),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand( 0, 0x0fff ) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand( 0, 0x3fff ) | 0x8000,

			// 48 bits for "node"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
		
		return strtoupper($uuid);
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	
    function guid_old()
    {
        if (function_exists('com_create_guid'))
        {
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = substr($charid, 0, 8).$hyphen
                    .substr($charid, 8, 4).$hyphen
                    .substr($charid,12, 4).$hyphen
                    .substr($charid,16, 4).$hyphen
                    .substr($charid,20,12);
            // chr(123)// "{".chr(125);// "}"
            return $uuid;
        }
    }
    
    // ----------------------------------------------------------- >>>>>>>>>>

	function varDump($args)
	{
		echo "<pre>";
		print_r($args);
		echo "</pre>";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showDebugData($VarDump="", $includes="", $classes="", $mixedParam="", $functions="")
	{
		/* Dumps out some debug info to the browser, great for checking vars */

		global $ADMINcfg, $WEBcfg, $MOBIcfg, $SMScfg, $SITEsession, $STDlib;

		if(is_object($ADMINcfg))
		{
			$obj = $ADMINcfg;
		} elseif(is_object($MOBIcfg)) {
			$obj = $MOBIcfg;
		} elseif(is_object($WEBcfg)) {
			$obj = $WEBcfg;
		} elseif(is_object($SMScfg)) {
			$obj = $SMScfg;
		}

		if(!$obj->GLOBAL_DEBUG || ($obj->SERVICE == 'LIVE'))
		{
			return false;
		}

		echo ' <div class="clear"></div>'."\n";

		echo '<div class="debug">'."\n";

		if(!empty($VarDump))
		{
			//echo "<hr>\n";
			echo "<hr>\n";
			echo '<b style="font-family:verdana; color:black; background-color:white">Post Vars :</b><br>'."\n";
			echo "<pre>\n";
			ksort($_POST);
			reset($_POST);
			print_r($_POST);
			echo "</pre>\n";
			echo "<hr>\n";
			echo '<b style="font-family:verdana; color:black; background-color:white">Get Vars :</b><br>'."\n";
			echo "<pre>\n";
			ksort($_GET);
			reset($_GET);
			print_r($_GET);
			echo "</pre>\n";
			echo "<hr>\n";
			echo '<b style="font-family:verdana; color:black; background-color:white">Session Vars :</b><br>'."\n";
			if(is_array($_SESSION))
			{
				echo "<pre>\n";
				ksort($_SESSION);
				reset($_SESSION);
				print_r($_SESSION);
				echo "</pre>\n";
			}
			echo "<hr>\n";
			echo '<b style="font-family:verdana; color:black; background-color:white">Cookie Vars :</b><br>'."\n";
			echo "<pre>\n";
			ksort($_COOKIE);
			reset($_COOKIE);
			print_r($_COOKIE);
			echo "</pre>\n";
			echo "<hr>\n";
			echo '<b style="font-family:verdana; color:black; background-color:white">File Vars :</b><br>'."\n";
			echo "<pre>\n";
			ksort($_FILES);
			reset($_FILES);
			print_r($_FILES);
			echo "</pre>\n";
			echo "<hr>\n";
			//echo '<b style="font-family:verdana; color:black; background-color:white">Global Vars :</b><br>'."\n";
			//echo "<pre>\n";
			//ksort($GLOBALS);
			//reset($GLOBALS);
			//print_r($GLOBALS);
			//echo "</pre>\n";
			//echo "<hr>\n";
		}

		if($mixedParam)
		{
			if(is_array($mixedParam))
			{
				echo '<b style="font-family:verdana; color:black; background-color:white">User Defined Array :</b><br><br>'."\n";
				echo "<pre>\n";
				print_r($mixedParam);
				echo "</pre>\n";
				echo "<hr>\n";
			} elseif(is_object($mixedParam)) {
				echo '<b style="font-family:verdana; color:black; background-color:white">Object Vars :</b><br><br>'."\n";
				$objVars = get_object_vars($mixedParam);
				echo "<pre>\n";
				ksort($objVars);
				reset($objVars);
				print_r($objVars);
				echo "</pre>\n";
				echo "<hr>\n";
			} else {
				echo '<b style="font-family:verdana; color:black; background-color:white">User Defined Vars :</b><br><br>'."\n";
				echo "<pre>\n";
				echo "$mixedParam<br>\n";
				echo "</pre>\n";
				echo "<hr>\n";
			}
		}

		if(!empty($includes))
		{
			$included_files = get_included_files();

			$i = 1;

			echo '<b style="font-family:verdana; color:black; background-color:white">Requires and Includes :</b><br><br>'."\n";
			foreach($included_files as $filename)
			{
				if($i < 10)
				{
					$prefix = '0';
				} else {
					$prefix = '';
				}

				echo "$prefix$i - $filename<br>\n";
				$i++;
			}
			echo "<hr>\n";
		}

		if(!empty($classes))
		{
			$class_files = get_declared_classes();
            
            //$class_files = asort($_class_files);

			echo '<b style="font-family:verdana; color:black; background-color:white">Class Files :</b><br><br>'."\n";
			foreach($class_files as $filename)
			{
				echo "$filename<br>\n";
			}
			echo "<hr>\n";
            
			$class_files = spl_classes();
            
            //$class_files = asort($_class_files);

			echo '<b style="font-family:verdana; color:black; background-color:white">SPL Class Files :</b><br><br>'."\n";
			foreach($class_files as $filename)
			{
				echo "$filename<br>\n";
			}
			echo "<hr>\n";            
		}

		if(!empty($functions))
		{
			$defined = get_defined_functions();

			echo '<b style="font-family:verdana; color:black; background-color:white">Defined Functions :</b><br><br>'."\n";
			echo '<b style="font-family:verdana; color:black; background-color:white">User :</b><br>'."\n";
			echo "<pre>\n";
			ksort($defined);
			reset($defined);
			print_r($defined["user"]);
			echo "</pre>\n";
			echo "<pre>\n";
			echo '<b style="font-family:verdana; color:black; background-color:white">Internal :</b><br>'."\n";
			$manref = "http://www.php.net/manual/en/function";
			$arr = get_defined_functions();
			while (list($type,$list) = each($arr)) {
				   if ($type == "internal" && is_array($list)) {
						   sort($list);
						   foreach ($list as $func) {
								   if ($func == "_")
								   {
										   $func2 = "gettext";
								   echo "<a href=\"$manref.$func2.php\" target=\"_balnk\">$func2</a><br>\n";

								  } else {
										   $func2 = preg_replace("/_/", "-", $func);
								   echo "<a href=\"$manref.$func2.php\" target=\"_balnk\">$func</a><br>\n";
								  }
						   }
				   }
			}

			echo '<img src="' . $_SERVER['PHP_SELF'] .'?=' . zend_logo_guid() . '" alt="Zend Logo !" />';
			echo '<img src="' . $_SERVER['PHP_SELF'] . '?=' . php_logo_guid() . '" alt="PHP Logo !" />';
			echo "</pre>\n";
			echo "<hr>\n";
		}

		echo '</div>'."\n";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function logfileWriter($var, $method, $logFile)
	{
		global $ADMINcfg, $WEBcfg, $MOBIcfg, $FLASHcfg, $SMScfg, $SITEsession, $STDlib, $wall;

		if(is_object($ADMINcfg))
		{
			$obj = $ADMINcfg;
		} elseif(is_object($MOBIcfg)) {
			$obj = $MOBIcfg;
		} elseif(is_object($WEBcfg)) {
			$obj = $WEBcfg;
		} elseif(is_object($FLASHcfg)) {
			$obj = $FLASHcfg;
		} elseif(is_object($SMScfg)) {
			$obj = $SMScfg;
		}

		$c = $method.":\n";
		ob_start();
		print_r($var);
		$c .= ob_get_contents();
		ob_end_clean();
		$c .= "\n".'-------------------------------------'."\n";
		error_log($c, 3, $obj->SITE_LOG_PATH."/debug/".$logFile);
	}

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>