<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : siteUtils.class.php
	// Author: Cactus Designs (UK) Ltd.
	// Date: 21/12/2007
	// Version: 1.0
	// Description: Useful functions that are needed across both CMS and Site.
	//			 DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		 with out consent from the author.
	// Copyright (c) 2007, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

Class siteUtils Extends GLOBALmySQLWrapper
{
	function utilsConnection()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		// Connect to the dB and return a handler
		$handler = $this->db_connect($ADMINcfg->DEFAULT_DBHOST, $ADMINcfg->SYSTEM_DBNAME, $ADMINcfg->SYSTEM_DBUSER, $ADMINcfg->SYSTEM_DBPASSWORD);
		return $handler;
	}	

	// ----------------------------------------------------------- >>>>>>>>>>

	function getListBox($handler, $column, $table, $default)
	{
		global $ADMINcfg, $STDlib, $SITEsession;

		$query = 'SELECT DISTINCT `'.$column.'` AS k, `'.$column.'` AS v FROM `'.$table.'` ORDER BY `'.$column.'` ASC';

		//echo "<!-- ".$query." -->\n\n";
		$result = $this->_dbListBoxHTML($query, $default, "</option>");
		return $result;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// PRIVATE METHODS
	// ----------------------------------------------------------- >>>>>>>>>>

	function _safeVar($var)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		return $STDlib->makeSafe($var);
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function _dbListBoxHTML($query, $default="", $suffix="\n") 
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		return $this->_dbQueryLoopHTML($query, "<option", $suffix, "selected", $default);
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	
} // Close Class

?>