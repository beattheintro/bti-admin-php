<?php

    // ----------------------------------------------------------- >>>>>>>>>>
    // Filename : adminDashboard.api.inc.php
    // Author: Jason Rastrick
    // Date: 13/03/2015
    // Version: 1.0
    // Description: API layer for the Dashboard Tools
    // ----------------------------------------------------------- >>>>>>>>>>

	// ----------------------------------------------------------- >>>>>>>>>>
	// Dashboard API's
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getDashboardData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/dashboard/adminDashboard.class.php";
		$adminDashboard = new adminDashboard();
		$dbh = $adminDashboard->adminDashboardConnection();
		$DATA = $adminDashboard->getDashboardData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
?>