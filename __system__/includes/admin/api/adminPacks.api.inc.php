<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminPacks.api.inc.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: API layer for the Pack Tools
	// ----------------------------------------------------------- >>>>>>>>>>

	// ----------------------------------------------------------- >>>>>>>>>>
    // Pack Tools API's
	// ----------------------------------------------------------- >>>>>>>>>>

    function API_packsInfoBarData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->packsInfoBarData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function API_getPacks($template, $startAt)
    {
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->getPacks($dbh, $startAt);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));    
    }

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function API_getPacksBySearch($template, $searchVars)
    {
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->getPacksBySearch($dbh, $searchVars);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));    
    }
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getPackData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->getPackData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getPackEditData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->getPackEditData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getPackAddData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->getPackAddData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}
	
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getAllPacksData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->getAllPacksData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	
	function API_getTrackTitlesForSelectBox($template, $searchArtist, $hasAssetCheck, $selected)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->_getTrackTitlesForSelectBox($dbh, $searchArtist, $hasAssetCheck);
		$DATA['selected'] = $selected;
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Data API's
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_addPackData()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->addPackData($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_updatePackData()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->updatePackData($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_deletePackData($delete=TRUE)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/packs/adminPacks.class.php";
		$adminPacks = new adminPacks();
		$dbh = $adminPacks->packsConnection();
		$DATA = $adminPacks->deletePackData($dbh, $delete);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
?>