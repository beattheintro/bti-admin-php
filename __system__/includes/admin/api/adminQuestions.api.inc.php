<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminQuestions.api.inc.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: API layer for the Question Tools
	// ----------------------------------------------------------- >>>>>>>>>>

	// ----------------------------------------------------------- >>>>>>>>>>
    // Question Tools API's
	// ----------------------------------------------------------- >>>>>>>>>>

    function API_questionsInfoBarData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->questionsInfoBarData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function API_getQuestions($template, $startAt)
    {
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->getQuestions($dbh, $startAt);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));    
    }

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function API_getQuestionsBySearch($template, $searchVars)
    {
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->getQuestionsBySearch($dbh, $searchVars);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));    
    }
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getQuestionData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->getQuestionData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getQuestionEditData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->getQuestionEditData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getQuestionAddData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->getQuestionAddData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}
	
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getAllPacksData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->getAllPacksData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	
	function API_getTrackTitlesForSelectBox($template, $searchArtist, $hasAssetCheck, $selected)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->_getTrackTitlesForSelectBox($dbh, $searchArtist, $hasAssetCheck);
		$DATA['selected'] = $selected;
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Data API's
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_addQuestionData()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->addQuestionData($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_updateQuestionData()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->updateQuestionData($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_deleteQuestionData($delete=TRUE)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/questions/adminQuestions.class.php";
		$adminQuestions = new adminQuestions();
		$dbh = $adminQuestions->questionsConnection();
		$DATA = $adminQuestions->deleteQuestionData($dbh, $delete);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
?>