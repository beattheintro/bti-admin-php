<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminThemes.api.inc.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: API layer for the Theme Tools
	// ----------------------------------------------------------- >>>>>>>>>>

	// ----------------------------------------------------------- >>>>>>>>>>
    // Theme Tools API's
	// ----------------------------------------------------------- >>>>>>>>>>

    function API_themesInfoBarData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->themesInfoBarData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function API_getThemes($template, $startAt)
    {
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->getThemes($dbh, $startAt);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));    
    }

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function API_getThemesBySearch($template, $searchVars)
    {
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->getThemesBySearch($dbh, $searchVars);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));    
    }
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getThemeData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->getThemeData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getThemeEditData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->getThemeEditData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getThemeAddData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->getThemeAddData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}
	
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getAllThemesData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->getAllThemesData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	
	function API_getTrackTitlesForSelectBox($template, $searchArtist, $hasAssetCheck, $selected)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->_getTrackTitlesForSelectBox($dbh, $searchArtist, $hasAssetCheck);
		$DATA['selected'] = $selected;
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// Data API's
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_addThemeData()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->addThemeData($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_updateThemeData()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->updateThemeData($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_deleteThemeData($delete=TRUE)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/themes/adminThemes.class.php";
		$adminThemes = new adminThemes();
		$dbh = $adminThemes->themesConnection();
		$DATA = $adminThemes->deleteThemeData($dbh, $delete);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
?>