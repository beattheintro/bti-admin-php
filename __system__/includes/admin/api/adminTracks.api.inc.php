<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminTracks.api.inc.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: API layer for the Track Tools
	// ----------------------------------------------------------- >>>>>>>>>>

	// ----------------------------------------------------------- >>>>>>>>>>
	// Track Tools API's
	// ----------------------------------------------------------- >>>>>>>>>>

    function API_tracksInfoBarData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/tracks/adminTracks.class.php";
		$adminTracks = new adminTracks();
		$dbh = $adminTracks->tracksConnection();
		$DATA = $adminTracks->tracksInfoBarData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function API_getTracks($template, $startAt)
    {
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/tracks/adminTracks.class.php";
		$adminTracks = new adminTracks();
		$dbh = $adminTracks->tracksConnection();
		$DATA = $adminTracks->getTracks($dbh, $startAt);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));    
    }

	// ----------------------------------------------------------- >>>>>>>>>>
    
    function API_getTracksBySearch($template, $searchVars)
    {
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/tracks/adminTracks.class.php";
		$adminTracks = new adminTracks();
		$dbh = $adminTracks->tracksConnection();
		$DATA = $adminTracks->getTracksBySearch($dbh, $searchVars);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));    
    }
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_getTrackData($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/tracks/adminTracks.class.php";
		$adminTracks = new adminTracks();
		$dbh = $adminTracks->tracksConnection();
		$DATA = $adminTracks->getTrackData($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	// ----------------------------------------------------------- >>>>>>>>>>
	// Data API's
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_addTrackData()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/tracks/adminTracks.class.php";
		$adminTracks = new adminTracks();
		$dbh = $adminTracks->tracksConnection();
		$DATA = $adminTracks->addTrackData($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_updateTrackData()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/tracks/adminTracks.class.php";
		$adminTracks = new adminTracks();
		$dbh = $adminTracks->tracksConnection();
		$DATA = $adminTracks->updateTrackData($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_deleteTrackData($delete=TRUE)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/tracks/adminTracks.class.php";
		$adminTracks = new adminTracks();
		$dbh = $adminTracks->tracksConnection();
		$DATA = $adminTracks->deleteTrackData($dbh, $delete);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
?>