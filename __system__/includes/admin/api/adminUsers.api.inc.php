<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminUsers.api.inc.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: API layer for the Users Manager.
	// ----------------------------------------------------------- >>>>>>>>>>

	// ----------------------------------------------------------- >>>>>>>>>>
	// User API's
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_doUserLogin()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/users/adminUsers.class.php";
		$adminUsers = new adminUsers();
		$dbh = $adminUsers->adminUsersConnection();
		$DATA = $adminUsers->getUser($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_viewCMSUsers($template)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/users/adminUsers.class.php";
		$adminUsers = new adminUsers();
		$dbh = $adminUsers->adminUsersConnection();
		$DATA = $adminUsers->getUsers($dbh);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_viewCMSUser($template, $uid="")
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/users/adminUsers.class.php";
		$adminUsers = new adminUsers();
		$dbh = $adminUsers->adminUsersConnection();
		$DATA = $adminUsers->getUser($dbh, $uid);
		$DATA["update"] = TRUE;
		//var_dump($DATA);
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_insertCMSUser()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/users/adminUsers.class.php";
		$adminUsers = new adminUsers();
		$dbh = $adminUsers->adminUsersConnection();
		$DATA = $adminUsers->insertUser($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_updateCMSUser()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/users/adminUsers.class.php";
		$adminUsers = new adminUsers();
		$dbh = $adminUsers->adminUsersConnection();
		$DATA = $adminUsers->updateUser($dbh);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_deleteCMSUser()
	{
		global $ADMINcfg, $STDlib, $SITEsession;
		include_once $ADMINcfg->CORE_CLASS_PATH."/db/GLOBALpdoWrapper.class.php";
		include_once $ADMINcfg->SYSTEM_CLASS_PATH."/users/adminUsers.class.php";
		$adminUsers = new adminUsers();
		$dbh = $adminUsers->adminUsersConnection();
		$DATA = $adminUsers->deleteUser($dbh);
		$str = "User Name: ".$_POST["u_name"]."\n"."Admin: ".$_POST["admin_1"]."\n"."Create: ".$_POST["admin_new"]."\n"."Edit: ".$_POST["admin_edit"]."\n"."Delete: ".$_POST["admin_delete"];
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
?>