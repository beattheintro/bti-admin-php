<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : genericMarkup.api.inc.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: API layer for rendering Markup & Text elements.
	// ----------------------------------------------------------- >>>>>>>>>>

	// ----------------------------------------------------------- >>>>>>>>>>
	// Markup API's
	// ----------------------------------------------------------- >>>>>>>>>>

	function API_viewAnyMarkup($template, $extra="")
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        $DATA = array();
        
		if(is_array($extra) || !empty($extra))
		{
			$DATA["extra"] = $extra;
		}
                
		$DATA["view"] = TRUE;
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_viewLoginForm($template, $errors="")
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        $DATA = array();
        
		empty($errors) ? "" : $DATA["errors"] = $errors;
        
		$DATA["view"] = TRUE;        
		echo ($output = $STDlib->renderTemplate($template, $DATA, $ADMINcfg->SYSTEM_COMPONENT_TPL_PATH));
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function API_genPassword($length)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
        $DATA = array();
        
		$DATA = $STDlib->makePassword($length);
		return $DATA;
	}

	// ----------------------------------------------------------- >>>>>>>>>>
?>