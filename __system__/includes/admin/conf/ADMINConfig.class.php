<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : ADMINConfig.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Admin Conf file.
	// ----------------------------------------------------------- >>>>>>>>>>

	Class ADMINConfig
	{
		// Generic db host
		var $DEFAULT_DBHOST;
		var $MASTER_DBHOST;

		// Details for DB's
		var $SYSTEM_DBNAME;
		var $SYSTEM_DBUSER;
		var $SYSTEM_DBPASSWORD;

		var $MAX_DB_RESULTS;
		
		// Path to the installation of the site
		var $HOME_PATH;

		// Public roots
		var $SYSTEM_WWW_ROOT;

		// Domain names of the site.
		var $SITE_DOMAIN;

		// Is the LIVE or STAGING service?
		var $SERVICE;

		var $SERVER_IP;

		var $EMAIL_NOREPLY;
		var $EMAIL_SUPPORT;

		// Verbose debug, will output the messages to screen.
		var $GLOBAL_DEBUG;
		var $DB_DEBUG;
		var $DB_DIE_ON_FAIL;

		// Log queries to $_SESSION['SQL_DEBUG'] array.
		var $SQL_DEBUG;
		var $SQL_DEBUG_CLEAR;

		var $SYSTEM_DBCHARSET_ENABLE;
		var $SYSTEM_DBCHARSET;

		var $SYSTEM_PATH;
		var $CORE_CLASS_PATH;

		var $SYSTEM_CLASS_PATH;
		var $SYSTEM_INC_PATH;
		var $SYSTEM_STATIC_PATH;
		var $SYSTEM_PAGE_TPL_PATH;
		var $SYSTEM_COMPONENT_TPL_PATH;
		var $SYSTEM_SESSION_PATH;
        
        var $AZURE_ACC_NAME;
        var $AZURE_ACC_KEY;
        var $AZURE_CONNECTION_STR;
        
        var $AZURE_BLOB_ADDR;
        var $AZURE_BLOB_ADDR_DEV;

		var $SITE_LOG_PATH;
		var $SITE_WWW_ROOT;
		var $SITE_IMG_ROOT;
		var $SYSTEM_ICONS_PATH;
		
		var $MAX_UPLOAD_SIZE;
		var $LOG_FILE_PREFIX;
        
        var $FFMPEG_EXE;

		var $VALID_IMAGE_MIMES;
		var $VALID_AUDIO_MIMES;
        var $AUDIO_FORMATS;
        
		var $THIS_MONTH;
		var $THIS_MONTH_DATE;
		
		var $UNLOCK_VALUES;
		var $ORDER_VALUES;

		var $SYSTEM_VERSION;

		// Constructor
		function ADMINConfig() {

			// ----------------------------------------------------------- >>>>>>>>>>
			// Change the variables below to match system
			// ----------------------------------------------------------- >>>>>>>>>>

			// Details for DB's
			$this->SYSTEM_DBNAME = 'btidb';
			
			$_SERVER["COMPUTERNAME"] = getenv('COMPUTERNAME');

            if($_SERVER["COMPUTERNAME"] == 'EVICERATE')
            {
                // Generic db host
                $this->DEFAULT_DBHOST = 'localhost,1433';
                $this->SYSTEM_DBUSER = FALSE;
			    $this->SYSTEM_DBPASSWORD = FALSE;

                // Azure details
                $this->AZURE_ACC_NAME = 'devstoreaccount1';
                $this->AZURE_ACC_KEY = 'Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==';
                $this->AZURE_CONNECTION_STR = 'UseDevelopmentStorage=true';
                
                $this->AZURE_BLOB_ADDR_DEV = 'http://127.0.0.1:10000/devstoreaccount1';
                $this->AZURE_BLOB_ADDR = 'https://btistorage.blob.core.windows.net';

                // Domain name of the site.
                $this->SITE_DOMAIN = 'localhost';
                $_SERVER['SERVER_NAME'] = $this->SITE_DOMAIN;
                
            } else {
                // Generic db host
                $this->DEFAULT_DBHOST = 'oot2zvmj09.database.windows.net';
			    $this->SYSTEM_DBUSER = 'btiuser';
			    $this->SYSTEM_DBPASSWORD = 'YnRpcmVmMjIh';
                
                // Azure details
                $this->AZURE_ACC_NAME = 'btistorage';
                $this->AZURE_ACC_KEY = 'NG5tcwwS1M51/Ej8FgHUfrdOfe3hs/UhQ54CUGs6I5RFcpX/6m20s4XZdFP1SmoIqTsyp/XelFAYt8esosBURQ==';
                $this->AZURE_CONNECTION_STR = 'DefaultEndpointsProtocol=https;AccountName='.$this->AZURE_ACC_NAME.';AccountKey='.$this->AZURE_ACC_KEY;
				
				// DefaultEndpointsProtocol=https;AccountName=btistorage;AccountKey=NG5tcwwS1M51/Ej8FgHUfrdOfe3hs/UhQ54CUGs6I5RFcpX/6m20s4XZdFP1SmoIqTsyp/XelFAYt8esosBURQ==
                
                $this->AZURE_BLOB_ADDR = 'https://btistorage.blob.core.windows.net';

                // Domain name of the site.
                $this->SITE_DOMAIN = 'bti-admin.cloudapp.net';
                $_SERVER['SERVER_NAME'] = $this->SITE_DOMAIN;
            }
			
			$this->MAX_DB_RESULTS = 50;
            
            // Path to the installation of the site
			$this->HOME_PATH = $_SERVER["DOCUMENT_ROOT"].'/';

			// Public roots
			$this->SYSTEM_WWW_ROOT = '/';

			$this->SERVICE = 'STAGING';

			$this->SERVER_IP = '';

			$this->EMAIL_NOREPLY = 'noreply@rastrick.com';
			$this->EMAIL_SUPPORT = 'support@rastrick.com';

            if($_SERVER["COMPUTERNAME"] == 'EVICERATE')
            {
				// For very verbose debug, will output the messages to screen.
				// Set to TRUE to enable. Should not be enabled on a live site!
				$this->GLOBAL_DEBUG = TRUE;
				$this->DB_DEBUG = FALSE;
				$this->DB_DIE_ON_FAIL = FALSE;

				// Log queries to $_SESSION['SQL_DEBUG'] array.
				// Set to TRUE to enable. Should not be enabled on a live site!
				// To clear the array set SQL_DEBUG_CLEAR to TRUE and refresh the page.
				// NOTE: If both are set to TRUE you will be able to see the queries run on a single page.
				$this->SQL_DEBUG = TRUE;
				$this->SQL_DEBUG_CLEAR = TRUE;
			} else {
				// For very verbose debug, will output the messages to screen.
				// Set to TRUE to enable. Should not be enabled on a live site!
				$this->GLOBAL_DEBUG = FALSE;
				$this->DB_DEBUG = FALSE;
				$this->DB_DIE_ON_FAIL = FALSE;

				// Log queries to $_SESSION['SQL_DEBUG'] array.
				// Set to TRUE to enable. Should not be enabled on a live site!
				// To clear the array set SQL_DEBUG_CLEAR to TRUE and refresh the page.
				// NOTE: If both are set to TRUE you will be able to see the queries run on a single page.
				$this->SQL_DEBUG = FALSE;
				$this->SQL_DEBUG_CLEAR = FALSE;			
			}

			// ----------------------------------------------------------- >>>>>>>>>>
			// You should not have to edit the variables below. If you need to make
			// changes please contact Cactus Designs <support@cactusdesigns.net>
			// before doing so to ensure stability of your system! Cactus Designs cannot
			// accept resposiblity for any changes you make.
			// ----------------------------------------------------------- >>>>>>>>>>

			$this->SYSTEM_DBCHARSET_ENABLE = 'TRUE';
			$this->SYSTEM_DBCHARSET = 'uft8';

			$this->SYSTEM_SOAP_ENCODING = 'ISO-8859-1';
			$this->SYSTEM_SOAP_VERSION = '1.1';
			$this->SYSTEM_SOAP_TRACE = FALSE;

			$this->SYSTEM_PATH = $this->HOME_PATH.'__system__/';

			$this->CORE_APPS_PATH = $this->SYSTEM_PATH.'apps';
			$this->CORE_CLASS_PATH = $this->SYSTEM_PATH.'classes/core';

			$this->SYSTEM_CLASS_PATH = $this->SYSTEM_PATH.'classes/admin';
			$this->SYSTEM_INC_PATH = $this->SYSTEM_PATH.'includes/admin';
			$this->SYSTEM_STATIC_PATH = $this->SYSTEM_PATH.'static/admin';
			$this->SYSTEM_PAGE_TPL_PATH = $this->SYSTEM_PATH.'templates/admin/pages';
			$this->SYSTEM_COMPONENT_TPL_PATH = $this->SYSTEM_PATH.'templates/admin/components';
			$this->SYSTEM_SESSION_PATH = $this->NFS_PATH.'cache/sessions/admin';

			$this->SITE_LOG_PATH = $this->SYSTEM_PATH.'logs';
			$this->SITE_WWW_ROOT = $this->SYSTEM_WWW_ROOT;

			$this->SITE_IMG_ROOT = $this->SITE_WWW_ROOT.'/z/img';
			$this->SITE_IMG_ROOT_HTML = '/z/img';

			$this->SYSTEM_ICONS_PATH = $this->SITE_IMG_ROOT_HTML.'/icons';

			$this->MAX_UPLOAD_SIZE = '10000000';
			$this->LOG_FILE_PREFIX = 'admin';
            
            $this->FFMPEG_EXE = $this->SYSTEM_PATH.'binary/ffmpeg/ffmpeg';

			$this->VALID_IMAGE_MIMES = array('image/jpeg','image/png'); //,'image/gif','', 'image/pjpeg', 'image/x-png'
			$this->VALID_AUDIO_MIMES = array('audio/mp3', 'audio/mp4', 'audio/ogg');

			$this->AUDIO_FORMATS = array(
											array('FORMAT'=>'MP3', 'MIME'=>'audio/mp3'),
											array('FORMAT'=>'MP4', 'MIME'=>'audio/mp4'),
											array('FORMAT'=>'OGG', 'MIME'=>'audio/ogg')
										);
            
			$this->THIS_MONTH = strtolower(date('F'));//'february';
			$this->THIS_MONTH_DATE = date('Y-m', time());//'2009-02';
			
			$this->UNLOCK_VALUES['0'] = '0 Discs';
			$this->UNLOCK_VALUES['10000'] = '10,000 Discs';
			$this->UNLOCK_VALUES['20000'] = '20,000 Discs';
			$this->UNLOCK_VALUES['30000'] = '30,000 Discs';
			$this->UNLOCK_VALUES['40000'] = '40,000 Discs';
			$this->UNLOCK_VALUES['50000'] = '50,000 Discs';
			
			$this->ORDER_VALUES['0'] = '0';
			$this->ORDER_VALUES['10'] = '10';
			            
			// ----------------------------------------------------------- >>>>>>>>>>

			$this->SYSTEM_VERSION = '1.0.0';
		}
	} // Close Class
?>