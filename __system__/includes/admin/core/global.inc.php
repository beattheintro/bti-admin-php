<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : global.inc.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Admin global include file.
	// ----------------------------------------------------------- >>>>>>>>>>

	error_reporting(E_ALL ^ E_NOTICE);																																				// Set error reporting level

	ini_set('arg_separator.output', '&amp;');

	require_once $_SERVER["DOCUMENT_ROOT"].'/__system__/includes/admin/conf/ADMINConfig.class.php';	// Base config file
	$ADMINcfg = new ADMINConfig();
    

    require_once $ADMINcfg->CORE_CLASS_PATH .'/session/SITESession.class.php';	// Session system
	$SITEsession = new SITESession();
	$sess = $SITEsession->session(TRUE, '30', 'no-cache, must-revalidate');		// Init session

    /*
	require_once $ADMINcfg->SYSTEM_PATH .'/WindowsAzure/SessionHandler/SessionHandler.php';	// Session system
	$SITEsession = new SiteSessions();
    session_set_save_handler($SITEsession, TRUE);
    session_start();                                                                        // Init session
    */

	require_once $ADMINcfg->CORE_CLASS_PATH .'/utils/STDLib.class.php';						// Standard library functions
	$STDlib = new STDLib();

	$ADMINcfg->WWW_SECTION = $section;
	$ADMINcfg->WWW_PAGE = $page;

	if($ADMINcfg->SQL_DEBUG_CLEAR)
	{
		unset($_SESSION['SQL_DEBUG']);
	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// API's For Site
	// ----------------------------------------------------------- >>>>>>>>>>

	require_once $ADMINcfg->SYSTEM_INC_PATH.'/api/genericMarkup.api.inc.php';

    if($ADMINcfg->WWW_SECTION == 'dashboard')
	{
		require_once $ADMINcfg->SYSTEM_INC_PATH.'/api/adminDashboard.api.inc.php';
	}
	
    if($ADMINcfg->WWW_SECTION == 'login')
	{
		require_once $ADMINcfg->SYSTEM_INC_PATH.'/api/adminUsers.api.inc.php';
	}

	if($ADMINcfg->WWW_SECTION == 'tracks')
	{
		require_once $ADMINcfg->SYSTEM_INC_PATH.'/api/adminTracks.api.inc.php';
	}

    if($ADMINcfg->WWW_SECTION == 'questions')
	{
		require_once $ADMINcfg->SYSTEM_INC_PATH.'/api/adminQuestions.api.inc.php';
	}

    if($ADMINcfg->WWW_SECTION == 'packs')
	{
		require_once $ADMINcfg->SYSTEM_INC_PATH.'/api/adminPacks.api.inc.php';
	}

	if($ADMINcfg->WWW_SECTION == 'themes')
	{
		require_once $ADMINcfg->SYSTEM_INC_PATH.'/api/adminThemes.api.inc.php';
	}

	if($ADMINcfg->WWW_SECTION == 'admin')
	{
		require_once $ADMINcfg->SYSTEM_INC_PATH.'/api/adminUsers.api.inc.php';
	}
?>