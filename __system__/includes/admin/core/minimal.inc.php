<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : minimal.inc.php
    // Author: Jason Rastrick
    // Date: 13/03/2015
    // Version: 1.0
	// Description: Admin minimal include file.
	// ----------------------------------------------------------- >>>>>>>>>>

	error_reporting(E_ALL ^ E_NOTICE);																																				// Set error reporting level

	ini_set('arg_separator.output', '&amp;');

	require_once $_SERVER["DOCUMENT_ROOT"].'/__system__/includes/admin/conf/ADMINConfig.class.php';													// Base config file
	$ADMINcfg = new ADMINConfig();

	require_once $ADMINcfg->CORE_CLASS_PATH .'/session/SITESession.class.php';									// Session system
	$SITEsession = new SITESession();
	$sess = $SITEsession->session(TRUE, '30', 'no-cache, must-revalidate', $ADMINcfg->SYSTEM_SESSION_PATH);		// Init session

	require_once $ADMINcfg->CORE_CLASS_PATH .'/utils/STDLib.class.php';											// Standard library functions
	$STDlib = new STDLib();

	$ADMINcfg->WWW_SECTION = $section;
	$ADMINcfg->WWW_PAGE = $page;

	if($ADMINcfg->SQL_DEBUG_CLEAR)
	{
		$SITEsession->unreg_session_var('SQL_DEBUG');
	}
?>