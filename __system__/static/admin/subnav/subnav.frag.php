<?
		$uID = $_SESSION['_LOGIN_']["Id"];
		$uN = $_SESSION['_LOGIN_']["u_name"];
		$admin = $_SESSION['_LOGIN_']["admin_1"];
        $adminNew = $_SESSION['_LOGIN_']['admin_new'];
?>
<? if($ADMINcfg->WWW_SECTION == "dashboard") { ?>
	<table cellpadding="0" cellspacing="0" border="0" class="subnavtable">
	<tr>
		<td align="left">&nbsp;</td>
		<td align="left">&nbsp;</td>
	</tr>
	</table>
<? } elseif($ADMINcfg->WWW_SECTION == "tracks") { ?>
	<table cellpadding="0" cellspacing="0" border="0" class="subnavtable">
	<tr>
		<td align="left">&nbsp;</td>
		<td align="left">&nbsp;</td>
    <? if(($ADMINcfg->WWW_PAGE == "home") || ($ADMINcfg->WWW_PAGE == "view-track") || ($ADMINcfg->WWW_PAGE == "edit-track") || ($ADMINcfg->WWW_PAGE == "delete-track")) { ?>
		<td align="left"><a href="/p/tracks/" class="selected-subnav" title="Track List">&nbsp;Tracks&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/tracks/" class="sub-pagenav" title="Track List">&nbsp;Tracks&nbsp;</a></td>
    <? } ?>
    <? if(($ADMINcfg->WWW_PAGE == "add-track") && ($adminNew == 1)) { ?>
		<td align="left"><a href="/p/tracks/add-track.php" class="selected-subnav" title="Add Track">&nbsp;Add Track&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/tracks/add-track.php" class="sub-pagenav" title="Add Track">&nbsp;Add Track&nbsp;</a></td>
    <? } ?>
    <? if(($ADMINcfg->WWW_PAGE == "search")) { ?>
		<td align="left"><a href="/p/tracks/search.php" class="selected-subnav" title="Search">&nbsp;Track Search&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/tracks/search.php" class="sub-pagenav" title="Search">&nbsp;Track Search&nbsp;</a></td>
    <? } ?>
<? } elseif($ADMINcfg->WWW_SECTION == "questions") { ?>
	<table cellpadding="0" cellspacing="0" border="0" class="subnavtable">
	<tr>
		<td align="left">&nbsp;</td>
		<td align="left">&nbsp;</td>
    <? if(($ADMINcfg->WWW_PAGE == "home") || ($ADMINcfg->WWW_PAGE == "view-question") || ($ADMINcfg->WWW_PAGE == "edit-question") || ($ADMINcfg->WWW_PAGE == "delete-question")) { ?>
		<td align="left"><a href="/p/questions/" class="selected-subnav" title="Question List">&nbsp;Questions&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/questions/" class="sub-pagenav" title="Question List">&nbsp;Questions&nbsp;</a></td>
    <? } ?>
    <? if(($ADMINcfg->WWW_PAGE == "add-question") && ($adminNew == 1)) { ?>
		<td align="left"><a href="/p/questions/add-question.php" class="selected-subnav" title="Add Question">&nbsp;Add Question&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/questions/add-question.php" class="sub-pagenav" title="Add Question">&nbsp;Add Question&nbsp;</a></td>
    <? } ?>
    <? if(($ADMINcfg->WWW_PAGE == "search")) { ?>
		<td align="left"><a href="/p/questions/search.php" class="selected-subnav" title="Search">&nbsp;Question Search&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/questions/search.php" class="sub-pagenav" title="Search">&nbsp;Question Search&nbsp;</a></td>
    <? } ?>
<? } elseif($ADMINcfg->WWW_SECTION == "packs") { ?>
	<table cellpadding="0" cellspacing="0" border="0" class="subnavtable">
	<tr>
		<td align="left">&nbsp;</td>
		<td align="left">&nbsp;</td>
    <? if(($ADMINcfg->WWW_PAGE == "home") || ($ADMINcfg->WWW_PAGE == "view-pack") || ($ADMINcfg->WWW_PAGE == "edit-pack") || ($ADMINcfg->WWW_PAGE == "delete-pack")) { ?>
		<td align="left"><a href="/p/packs/" class="selected-subnav" title="Pack List">&nbsp;Packs&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/packs/" class="sub-pagenav" title="Pack List">&nbsp;Packs&nbsp;</a></td>
    <? } ?>
    <? if(($ADMINcfg->WWW_PAGE == "add-pack") && ($adminNew == 1)) { ?>
		<td align="left"><a href="/p/packs/add-pack.php" class="selected-subnav" title="Add Pack">&nbsp;Add Pack&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/packs/add-pack.php" class="sub-pagenav" title="Add Pack">&nbsp;Add Pack&nbsp;</a></td>
    <? } ?>
    <? if(($ADMINcfg->WWW_PAGE == "search")) { ?>
		<td align="left"><a href="/p/packs/search.php" class="selected-subnav" title="Search">&nbsp;Pack Search&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/packs/search.php" class="sub-pagenav" title="Search">&nbsp;Pack Search&nbsp;</a></td>
    <? } ?>
<? } elseif($ADMINcfg->WWW_SECTION == "themes") { ?>
	<table cellpadding="0" cellspacing="0" border="0" class="subnavtable">
	<tr>
		<td align="left">&nbsp;</td>
		<td align="left">&nbsp;</td>
    <? if(($ADMINcfg->WWW_PAGE == "home") || ($ADMINcfg->WWW_PAGE == "view-theme") || ($ADMINcfg->WWW_PAGE == "edit-theme") || ($ADMINcfg->WWW_PAGE == "delete-theme")) { ?>
		<td align="left"><a href="/p/themes/" class="selected-subnav" title="Theme List">&nbsp;Themes&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/themes/" class="sub-pagenav" title="Theme List">&nbsp;Themes&nbsp;</a></td>
    <? } ?>
    <? if(($ADMINcfg->WWW_PAGE == "add-theme") && ($adminNew == 1)) { ?>
		<td align="left"><a href="/p/themes/add-theme.php" class="selected-subnav" title="Add Theme">&nbsp;Add Theme&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/themes/add-theme.php" class="sub-pagenav" title="Add Theme">&nbsp;Add Theme&nbsp;</a></td>
    <? } ?>
    <? if(($ADMINcfg->WWW_PAGE == "search")) { ?>
		<td align="left"><a href="/p/themes/search.php" class="selected-subnav" title="Search">&nbsp;Theme Search&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/themes/search.php" class="sub-pagenav" title="Search">&nbsp;Theme Search&nbsp;</a></td>
    <? } ?>
<? } elseif($ADMINcfg->WWW_SECTION == "admin") { ?>
	<table cellpadding="0" cellspacing="0" border="0" class="subnavtable">
	<tr>
		<td align="left">&nbsp;</td>
		<td align="left">&nbsp;</td>
    <? if(($ADMINcfg->WWW_PAGE == "users")) { ?>
		<td align="left"><a href="/p/users/" class="selected-subnav" title="Admin Users">&nbsp;Users&nbsp;</a></td>
    <? } else { ?>
		<td align="left"><a href="/p/users/" class="sub-pagenav" title="Admin Users">&nbsp;Users&nbsp;</a></td>
    <? } ?>
<? } ?>
	</tr>
	</table>