<?
	$uID = $_SESSION['_LOGIN_']["Id"];
	$uN = $_SESSION['_LOGIN_']["u_name"];
	$admin = $_SESSION['_LOGIN_']["admin_1"];

	//  ui-corner-tl ui-corner-tr
?>
	<div class="nav">
	<table cellpadding="0" cellspacing="0" border="0" class="navtable">
<? if($ADMINcfg->WWW_PAGE != "login") { ?>
	<tr>

<? if($ADMINcfg->WWW_SECTION == "dashboard") { ?>
		<td align="left">&nbsp;&nbsp;<a href="/p/dashboard/" class="selected-nav ui-corner-tl ui-corner-tr" title="Dashboard">Dashboard</a></td>
<? } else { ?>
		<td align="left">&nbsp;&nbsp;<a href="/p/dashboard/" class="pagenav ui-corner-tl ui-corner-tr" title="Dashboard">Dashboard</a></td>
<? } ?>

		<td align="left"><img src="/z/img/static/blank.png" width="5" height="10"></td>

<? if($ADMINcfg->WWW_SECTION == "tracks") { ?>
		<td align="left"><a href="/p/tracks/" class="selected-nav ui-corner-tl ui-corner-tr" title="Track Tools">Track Tools</a></td>
<? } else { ?>
		<td align="left"><a href="/p/tracks/" class="pagenav ui-corner-tl ui-corner-tr" title="Track Tools">Track Tools</a></td>
<? } ?>

		<td align="left"><img src="/z/img/static/blank.png" width="5" height="10"></td>

<? if($ADMINcfg->WWW_SECTION == "questions") { ?>
		<td align="left"><a href="/p/questions/" class="selected-nav ui-corner-tl ui-corner-tr" title="Question Tools">Question Tools</a></td>
<? } else { ?>
		<td align="left"><a href="/p/questions/" class="pagenav ui-corner-tl ui-corner-tr" title="Question Tools">Question Tools</a></td>
<? } ?>

		<td align="left"><img src="/z/img/static/blank.png" width="5" height="10"></td>

<? if($ADMINcfg->WWW_SECTION == "packs") { ?>
		<td align="left"><a href="/p/packs/" class="selected-nav ui-corner-tl ui-corner-tr" title="Pack Tools">Pack Tools</a></td>
<? } else { ?>
		<td align="left"><a href="/p/packs/" class="pagenav ui-corner-tl ui-corner-tr" title="Pack Tools">Pack Tools</a></td>
<? } ?>

		<td align="left"><img src="/z/img/static/blank.png" width="5" height="10"></td>

<? if(($ADMINcfg->WWW_SECTION == "themes")) { ?>
		<td align="left"><a href="/p/themes/" class="selected-nav ui-corner-tl ui-corner-tr" title="Theme Tools">Theme Tools</a></td>
<? } else { ?>
		<td align="left"><a href="/p/themes/" class="pagenav ui-corner-tl ui-corner-tr" title="Theme Tools">Theme Tools</a></td>
<? } ?>

		<td align="left"><img src="/z/img/static/blank.png" width="5" height="10"></td>
<? if(($ADMINcfg->WWW_SECTION == "competition")  && ($admin == 1)) { ?>
		<td align="left"><a href="/p/competition/" class="selected-nav ui-corner-tl ui-corner-tr" title="Competition Tools">Competition Tools</a></td>
<? } else { ?>
		<td align="left"><a href="/p/competition/" class="pagenav ui-corner-tl ui-corner-tr" title="Competition Tools">Competition Tools</a></td>
<? } ?>

		<td align="left"><img src="/z/img/static/blank.png" width="5" height="10"></td>

<? if(($ADMINcfg->WWW_SECTION == "players") && ($admin == 1)) { ?>
		<td align="left"><a href="/p/players/" class="selected-nav ui-corner-tl ui-corner-tr" title="Player Tools">Player Tools</a></td>
<? } else { ?>
		<td align="left"><a href="/p/players/" class="pagenav ui-corner-tl ui-corner-tr" title="Player Tools">Player Tools</a></td>
<? } ?>

		<td align="left"><img src="/z/img/static/blank.png" width="5" height="10"></td>

<? if(($ADMINcfg->WWW_SECTION == "admin") && ($admin == 1)) { ?>
		<td align="left"><a href="/p/users/" class="selected-nav ui-corner-tl ui-corner-tr" title="Administrative Tools">Admin Tools</a></td>
<? } else { ?>
		<td align="left"><a href="/p/users/" class="pagenav ui-corner-tl ui-corner-tr" title="Administrative Tools">Admin Tools</a></td>
<? } ?>

		<td align="left"><img src="/z/img/static/blank.png" width="5" height="10"></td>
	</tr>
<? } else { ?>
	<tr>
		<td align="left">&nbsp;&nbsp;</td>
	</tr>
<? } ?>
	</table>
	</div>
