<?

	function renderDashboard($DATA)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$new = $_SESSION['_LOGIN_']['admin_new'];
		$admin = $_SESSION['_LOGIN_']['admin_1'];

		//$STDlib->varDump($DATA);
?>
	<div style="margin:0px 0px 0px 0px;">
			<h3 class="ui-widget-header ui-corner-all" style="padding:10px 10px 10px 10px;font-size:18pt;">BTI Stats</h3>
	</div>
	<div>&nbsp;</div>
	<table cellpadding="0" cellspacing="0" border="0" align="center">
		<tr>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Players (Real)</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numReal"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Players (Anon)</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numAnon"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Players (Total)</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numReal"]+$DATA['PLAYERS']["numAnon"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Players (Validated)</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numValidated"]);?></b></p>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Discs Earned</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numDiscsEarned"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Discs Spent</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numDiscsSpent"]);?></b></p>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Track Likes</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['TRACKS']["numLikes"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Games Played</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numGamesWon"]+$DATA['PLAYERS']["numGamesLost"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Games Won</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numGamesWon"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Games Lost</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PLAYERS']["numGamesLost"]);?></b></p>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Tracks</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['TRACKS']["num"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Questions</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['QUESTIONS']["num"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Packs</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['PACKS']["num"]);?></b></p>
					</div>
				</div>
			</td>
			<td>
				<div style="width:200px;margin:0px 10px 0px 10px;">
					<div id="effect" class="ui-widget-content ui-corner-all">
						<h3 class="ui-widget-header ui-corner-all">Themes</h3>
						<p style="font-size:15pt;text-align:center;"><b><?=number_format($DATA['THEMES']["num"]);?></b></p>
					</div>
				</div>
			</td>
		</tr>
	</table>
<?
	}

?>