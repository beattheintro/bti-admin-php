<?

	function renderDashboardHeadlineReport($DATA)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$new = $_SESSION['_LOGIN_']['admin_new'];
		$admin = $_SESSION['_LOGIN_']['admin_1'];

		//$STDlib->varDump($DATA);

		$reportMonthName = date("F", time());
		$reportMonthValue = date("m", time());
		$reportYear = date("Y", time());

		// Create a year array
		$firstYear = '2010';
		$thisYear = date("Y", time());
		$_years = range($firstYear, $thisYear);
		// Make the array keys be years too
		for($i=0; isset($_years[$i]); $i++)
		{
			$_yr = $_years[$i];
			$YEARS[$_yr] = $_yr;
		}

		$numRows = 1;
		setlocale(LC_MONETARY, 'en_GB');

		//$STDlib->varDump($YEARS);
?>
<? if($new == 1 || $admin == 1) { ?>
<script type="text/javascript">
$(document).ready(function(){
	$(function(){
	  $("#hideRawData").click(function(){
		$("#rawData").css("display","none");
		$("#showRawData").css("display","inline");
		$("#hideRawData").css("display","none");
	  });
	});
	$(function(){
	  $("#showRawData").click(function(){
		$("#rawData").css("display","inline");
		$("#showRawData").css("display","none");
		$("#hideRawData").css("display","inline");
	  });
	});
});
</script>
<div style="margin:0px 0px 0px 0px;">
		<h3 class="ui-widget-header ui-corner-all" style="padding:10px 10px 10px 10px;">BTI <?=$reportMonthName."/".$reportYear;?> Headlines</h3>
</div>
<div>&nbsp;</div>
<table cellspacing="2" cellpadding="6" border="0" align="center" width="100%">
	<tr>
		<td class="tbl-transcode-head ui-corner-all" align="center">
			<?
				// Google Charts
				$numCols = $DATA['numCalDays']+1;
				$chdTracks = '';

				$chartColour = '33CCFF';

				for($x=0; $x <= $DATA['numCalDays']; $x++)
				{
					if($x == 0)
					{
						if(!empty($DATA['PPD'][$portalName][$x]['vol_sold']))
						{
							$chdTracks .= $DATA['PPD'][$portalName][$x]['vol_sold'];
							$tracksMaxVal = $DATA['PPD'][$portalName][$x]['vol_sold'];
							$tracksMinVal = $DATA['PPD'][$portalName][$x]['vol_sold'];
						} else {
							$chdTracks .= '0';
							$tracksMaxVal = '0';
							$tracksMinVal = '0';
						}

					} else {
						if(!empty($DATA['PPD'][$portalName][$x]['vol_sold']))
						{
							if($DATA['PPD'][$portalName][$x]['vol_sold'] >= $tracksMaxVal)
							{
								$tracksMaxVal = $DATA['PPD'][$portalName][$x]['vol_sold'];
							}

							if($DATA['PPD'][$portalName][$x]['vol_sold'] <= $tracksMinVal)
							{
								$tracksMinVal = $DATA['PPD'][$portalName][$x]['vol_sold'];
							}
						}

						// -------------------------------------------------------------------------------------- >>>>

						if(!empty($DATA['PPD'][$portalName][$x]['vol_sold']))
						{
							$chdTracks .= ",".$DATA['PPD'][$portalName][$x]['vol_sold'];
							if($DATA['PPD'][$portalName][$x]['vol_sold'] >= $tracksMaxVal)
							{
								$tracksMaxVal = $DATA['PPD'][$portalName][$x]['vol_sold'];
							}

							if($DATA['PPD'][$portalName][$x]['vol_sold'] <= $tracksMinVal)
							{
								$tracksMinVal = $DATA['PPD'][$portalName][$x]['vol_sold'];
							}
						} else {
							$chdTracks .= ',0';
						}
					}
				}

				$maxVal = $tracksMaxVal+50;

				$cWidth = '950';
				$cHeight = '315';

				$gAPI		= 'http://chart.apis.google.com/chart?';

				$chxr		= 'chxr=0,0,'.$maxVal.'|1,1,'.$numCols;
				$chxs		= '&chxs=0,676767,11.5,0,lt,676767|1,676767,11.5,0,lt,676767';
				$chxt		= '&chxt=y,x';
				$chbh		= '&chbh=a';
				$chs		= '&chs='.$cWidth.'x'.$cHeight;
				$cht			= '&cht=bvs';
				$chco		= '&chco='.$chartColour;
				$chds		= '&chds=0,'.$maxVal.',0,'.$maxVal.',0,'.$maxVal.',0,'.$maxVal;
				$chd		= '&chd=t:'.$chdTracks;
				$chdl		= '&chdl=PPD|Streams|Subs.+New|Subs.+Repeat';
				$chdlp		= '&chdlp=b';
				//$chg		= '&chg=5,5,5,5';
				$chma		= '&chma=5,5,5,25';
				//$chm		= '&chm=D,000000,0,-1,1|D,000000,1,-1,1|D,000000,2,-1,1|D,000000,3,-1,1|N,000000,-1,,8,,e::10|N,000000,0,-1,7,,c|N,000000,1,-1,7,,c|N,000000,2,-1,7,,c|N,000000,3,-1,7,,c';
				$chm		= '&chm=N,000000,-1,,8,,e::10|N,000000,0,-1,7,,c|N,000000,1,-1,7,,c|N,000000,2,-1,7,,c|N,000000,3,-1,7,,c';
				$chtt		= '&chtt=BTI+Report+-+Daily+Tracks+Played+-+'.$reportMonthName.'+'.$reportYear;

				$gURL = $gAPI.$chxr.$chxs.$chxt.$chbh.	$chs.$cht.$chco.$chds.$chd.$chdl.$chdlp.$chg.$chma.$chm.$chtt;

				/*
					http://chart.apis.google.com/chart
					?chxr=0,0,1179|1,1,31
					&chxs=0,676767,11.5,0,lt,676767|1,676767,11.5,0,lt,676767
					&chxt=y,x&chbh=a&chs=950x315
					&cht=bvs
					&chco=E70B91,FA6BC0,FB9BD5,FDCCEA
					&chds=0,1179,0,1179,0,1179,0,1179
					&chd=t:1179,906,873,640,868,867,834,748,760,784,874,873,838,768,798,759,718,757,634,585,706,716,794,701,686,740,721,648,721,708,575|||
					&chdl=PPD|Streams|Subs+New|Subs.+Repeat.
					&chg=5,5,5,5
					&chma=5,5,5,25
					&chm=D,000000,0,-1,1|D,000000,1,-1,1|D,000000,2,-1,1|D,000000,3,-1,1|N*zs*,000000,-1,,12,,e::10|N*zs*,000000,0,-1,8,,c|N*zs*,000000,1,-1,8,,c|N*zs*,000000,2,-1,8,,c|N*zs*,000000,3,-1,8,,c
					&chtt=AdultZone+-+Daily+Volume+Sold+-+(January)

					http://chart.apis.google.com/chart
					?chxr=0,0,451|1,1,31
					&chxs=0,676767,11.5,0,lt,676767|1,676767,11.5,0,lt,676767
					&chxt=y,x
					&chbh=a
					&chs=950x315
					&cht=bvs
					&chco=E70B91,FA6BC0,FB9BD5,FDCCEA
					&chds=0,451,0,451,0,451,0,451
					&chd=t:256,387,422,381,375,290,366,341,412,346,394,330,334,385,384,377,451,394,329,315,343,433,361,333,265,335,427,425,396,354,377
								|0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
								|0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
								|0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
					&chdl=PPD|Streams|Subs+New|Subs.+Repeat.
					&chdlp=b
					&chma=5,5,5,25
					&chm=D,000000,0,-1,1|D,000000,1,-1,1|D,000000,2,-1,1|D,000000,3,-1,1|N,000000,-1,,8,,e::10|N,000000,0,-1,7,,c|N,000000,1,-1,7,,c|N,000000,2,-1,7,,c|N,000000,3,-1,7,,c
					&chtt=AdultZone+-+Daily+Volume+Sold+-+(January)
				*/

			?>
			<table cellpadding="2" cellspacing="2" border="0">
				<tr>
					<td><a href="<?=$gURL;?>" border="0" title="Daily Volume Sold - Click for larger version" alt="Daily Volume Sold - Click for larger version" target="_blank"><img src="<?=$gURL;?>" width="<?=$cWidth;?>" height="<?=$cHeight;?>" alt="Daily Volume Sold - Click for larger version" border="0" /></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="right" class="tbl-transcode-head ui-corner-all"><input type="button" id="showRawData" value="SHOW RAW DATA" class="buttonPositive ui-corner-all" />&nbsp;<input type="button" id="hideRawData" value="HIDE RAW DATA" class="buttonPositive ui-corner-all hide-item" /></td>
	</tr>
	<tr>
		<td class="tbl-transcode-head ui-corner-all" align="center">
			<div id="rawData" class="hide-item">
			<table cellspacing="2" cellpadding="2" border="0" class="tbl-transcode-head ui-corner-all" width="750">
				<tr>
					<td colspan="12" align="left" style="background-color:#e0e0e0;font-weight:bold;" class="ui-corner-all"><?=strtoupper($reportMonthName.' '.$reportYear);?>&nbsp;RAW DATA</td>
				</tr>
				<tr>
					<td rowspan="2" align="center" style="background-color:#e0e0e0;font-weight:bold;" class="ui-corner-all">DATE</td>
					<td colspan="2" align="center" style="background-color:#<?=$tracksColour;?>;font-weight:bold;" class="ui-corner-all">PPD</td>
					<td rowspan="2" align="center" style="background-color:#eeeeee;font-weight:bold;" class="ui-corner-all">&nbsp;</td>
					<td colspan="2" align="center" style="background-color:#ffff99;font-weight:bold;" class="ui-corner-all">Total</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#eeeeee;font-weight:bold;">Vol</td>
				</tr>
				<?
					$numCols = $DATA['numCalDays']+1;
					for($x=0; $x <= $DATA['numCalDays']; $x++)
					{
						$tracksVolSold = $DATA['PPD'][$portalName][$x]['vol_sold'];

						$tracksTotalVolSold = $tracksTotalVolSold+$tracksVolSold;

						$totalTotalVolSold = $totalTotalVolSold+$tracksTotalVolSold;
				?>
				<tr onMouseOver="this.className='highlight-on'" onMouseOut="this.className='highlight-off'">
					<td style="border-bottom:1px solid #eeeeee;border-right:1px solid #eeeeee;background-color:#eeeeee;">BTI</td>
					<td style="border-bottom:1px solid #eeeeee;border-right:1px solid #eeeeee;"><?=$tracksVolSold;?></td>
					<td style="background-color:#eeeeee;font-weight:bold;" class="ui-corner-all">&nbsp;</td>
					<td style="border-bottom:1px solid #eeeeee;border-right:1px solid #eeeeee;"><?=$totalVolSold;?></td>
				</tr>
				<? } ?>
			</table>
			<div>&nbsp;</div>
			<table cellspacing="2" cellpadding="2" border="0" class="tbl-transcode-head ui-corner-all" width="750">
				<tr>
					<td colspan="12" align="left" style="background-color:#e0e0e0;font-weight:bold;" class="ui-corner-all"><?=strtoupper($reportMonthName.' '.$reportYear);?>&nbsp;SUMMARY</td>
				</tr>
				<tr>
					<td rowspan="2" align="center" style="background-color:#e0e0e0;font-weight:bold;" class="ui-corner-all">&nbsp;</td>
					<td colspan="2" align="center" style="background-color:#<?=$tracksColour;?>;font-weight:bold;" class="ui-corner-all">BTI</td>
					<td rowspan="2" align="center" style="background-color:#eeeeee;font-weight:bold;" class="ui-corner-all">&nbsp;</td>
					<td colspan="2" align="center" style="background-color:#ffff99;font-weight:bold;" class="ui-corner-all">Total</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#eeeeee;font-weight:bold;">Vol</td>
					<td align="center" style="background-color:#eeeeee;font-weight:bold;">Vol</td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #eeeeee;border-right:1px solid #eeeeee;background-color:#eeeeee;font-weight:bold;" class="ui-corner-all">TOTALS</td>
					<td style="border-bottom:1px solid #eeeeee;border-right:1px solid #eeeeee;background-color:#fff;"><?=$tracksTotalVolSold;?></td>
					<td style="background-color:#eeeeee;font-weight:bold;" class="ui-corner-all">&nbsp;</td>
					<td style="border-bottom:1px solid #eeeeee;border-right:1px solid #eeeeee;background-color:#fff;"><?=$totalTotalVolSold;?></td>
				</tr>
			</table>
			</div>
		</td>
	</tr>
</table>
<? } ?>
<? } else { ?>
<div class="info-bar">
	<div class="ui-widget-content ui-corner-error">
		<p class="info-bar-content-center msg-error">You don't have the correct permissions to view this section, please speak to an administrator.</p>
	</div>
</div>
<? } ?>
<?
	}

?>