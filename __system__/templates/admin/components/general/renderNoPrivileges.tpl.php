<?
function renderNoPrivileges($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
?>
<div class="ui-widget-content ui-corner-all">
    <table style="width: 100%; padding:20px;">
        <tr>
             <td>You do not have the correct privileges to <?=$DATA['extra'];?> item!</td>
        </tr>
    </table>
</div>
<?
}
?>