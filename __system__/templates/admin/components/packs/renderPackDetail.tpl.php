<?
function renderPackDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
?>
<? if(!isset($DATA['error'])) { ?>
<? 
	$cross = '<img src="/z/img/static/cross.png" title="" alt=""/>';
	$tick = '<img src="/z/img/static/tick.png" title="" alt=""/>';

	if($DATA['PACK']['Active'] == 1)
	{
		$Active = $tick;
	} else {
		$Active = $cross; 
	}

	if($DATA['PACK']['IsFreeToPlay'] == 1)
	{
		$IsFreeToPlay = $tick;
	} else {
		$IsFreeToPlay = $cross; 
	} 

	if(!isset($DATA['QUESTIONS']['error']))
	{
		$numQuestions = count($DATA['QUESTIONS']).'&nbsp;';
	} else {
		$numQuestions = '';
	}

	$packImage = $ADMINcfg->AZURE_BLOB_ADDR.'/packimages/mobile/'.strtolower($DATA['PACK']['Id']).'.png'; 
	
	// Is this file in the blob?
	$fileHeaders = @get_headers($packImage);
	if($fileHeaders[0] == 'HTTP/1.1 404 The specified blob does not exist.')
	{
		$packImage = '/z/img/static/cross.png';
		$pWidth = '20';
		$pHeight = '20';
		$hasImg = '&nbsp;No Pack Image!';
	} else {
		$pWidth = '180';
		$pHeight = '110';
		$hasImg = '';
	}	
?>
<div class="ui-widget-content ui-corner-all" style="margin-bottom:2px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Pack Information</td>
	</tr>
</table>
</div>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%;" class="table-striped">
		<tr>
            <td style="font-weight:bold;text-align:right;width:150px;">Created&nbsp;&nbsp;</td>
            <td colspan="3"><?=$DATA['PACK']['Created'];?></td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Pack Name&nbsp;&nbsp;</td>
            <td colspan="3"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACK']['Name']);?></td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Pack Description&nbsp;&nbsp;</td>
            <td colspan="3"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACK']['Description']);?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Active?&nbsp;&nbsp;</td>
			<td style="font-weight:normal;text-align:left;"><?=$Active;?></td>
			<td style="font-weight:bold;text-align:right;">&nbsp;&nbsp;</td>
			<td rowspan="5" style="font-weight:bold;text-align:center;width:250px;"><img src="<?=$packImage;?>" width="<?=$pWidth;?>" height="<?=$pHeight;?>" /><?=$hasImg;?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Free To Play?&nbsp;&nbsp;</td>
			<td colspan="2" style="font-weight:normal;text-align:left;"><?=$IsFreeToPlay;?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Cost to Unlock&nbsp;&nbsp;</td>
			<td colspan="2" style="font-weight:normal;text-align:left;"><?=number_format($DATA['PACK']['CostToUnlock']);?>&nbsp;Discs</td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Order&nbsp;&nbsp;</td>
			<td colspan="2" style="font-weight:normal;text-align:left;"><?=$DATA['PACK']['Order'];?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Pack Toolbox&nbsp;&nbsp;</td>
			<td style="font-weight:bold;text-align:left;" colspan="2">
                <? if($adminEdit) { ?><a href="/p/packs/edit-pack.php?Id=<?=$DATA['PACK']['Id'];?>" title="Edit Pack"><img src="/z/img/static/p_edit.png" title="Edit Pack" alt="Edit Pack"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/packs/delete-pack.php?Id=<?=$DATA['PACK']['Id'];?>" title="Delete Pack"><img src="/z/img/static/p_delete.png" title="Delete Pack" alt="Delete Pack"/></a>&nbsp;<? } ?>
			</td>
		</tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all" style="margin:2px 0px 2px 0px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;"><?=$numQuestions;?>Questions in this Pack</td>
	</tr>
	</table>
</div>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['QUESTIONS']['error'])) { ?>
	<table style="width: 100%;" class="table-striped" border="0">
        <tr>
            <td style="font-weight:bold;text-align:left;">Correct Answer</td>
            <td style="font-weight:bold;text-align:left;">Wrong Answer 1</td>
            <td style="font-weight:bold;text-align:left;">Wrong Answer 2</td>
            <td style="font-weight:bold;text-align:center;width:88px;">Toolbox</td>
        </tr>
        <? for($i=0; isset($DATA['QUESTIONS'][$i]['Id']); $i++) { ?>
        <tr>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['CorrectArtist']);?> - <?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['CorrectTitle']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['WrongArtist1']);?>&nbsp;-&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['WrongTitle1']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['WrongArtist2']);?>&nbsp;-&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['WrongTitle2']);?></td>
            <td style="text-align:center">
				<a href="/p/questions/search.php?search_term=<?=urlencode($DATA['QUESTIONS'][$i]['CorrectTitle']);?>" title="Search Correct Answer"><img src="/z/img/static/magnifier.png" title="Search Correct Answer" alt="Search Correct Answer"/></a>&nbsp;
                <a href="/p/questions/view-question.php?Id=<?=$DATA['QUESTIONS'][$i]['QuestionId'];?>" title="View Question"><img src="/z/img/static/p_view.png" title="View Question" alt="View Question"/></a>&nbsp
                <? if($adminEdit) { ?><a href="/p/questions/edit-question.php?Id=<?=$DATA['QUESTIONS'][$i]['QuestionId'];?>" title="Edit Question"><img src="/z/img/static/p_edit.png" title="Edit Question" alt="Edit Question"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/questions/delete-question.php?Id=<?=$DATA['QUESTIONS'][$i]['QuestionId'];?>" title="Delete Question"><img src="/z/img/static/p_delete.png" title="Delete Question" alt="Delete Question"/></a><? } ?>
            </td>
        </tr>
        <? } ?>
    </table>
<? } else { ?>
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No questions found for this pack.</td>
	</tr>
	</table>
<? } ?>
</div>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No pack found.</td>
	</tr>
	</table>
</div>
<? } ?>
<?
}
?>