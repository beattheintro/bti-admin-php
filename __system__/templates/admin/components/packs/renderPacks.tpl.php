<?
function renderPacks($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
	//$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
?>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['error'])) { ?>
    <table style="width: 100%;" class="table-striped sortable">
        <tr>
			<th class="sorttable_nosort" ></th>
            <th style="font-weight:bold;font-size:larger;width:75px;">Created</th>
			<th style="font-weight:bold;font-size:larger;">Active?</th>
            <th style="font-weight:bold;font-size:larger;width:100px;">Pack Name</th>
            <th style="font-weight:bold;font-size:larger;">Pack Description</th>
            <th class="sorttable_nosort" style="font-weight:bold;font-size:larger;width:100px;text-align:center;">Toolbox</th>
        </tr>
<? for($i=0; isset($DATA[$i]['Id']); $i++) { ?>
<? $thumbnail = $ADMINcfg->AZURE_BLOB_ADDR.'/packimages/mobile/'.strtolower($DATA[$i]['Id']).'.png'; ?>
<? if($DATA[$i]['Active'] == 1) { $Active = 'tick'; $sorttable_customkey='1'; } else { $Active = 'cross'; $sorttable_customkey='0'; } ?>
        <tr>
			<td><img src="<?=$thumbnail;?>" title="" alt="" width="75" height="50"/></td>
            <td><?=$DATA[$i]['Created'];?></td>
            <td style="text-align:center" sorttable_customkey="<?=$sorttable_customkey;?>"><img src="/z/img/static/<?=$Active;?>.png" title="" alt=""/></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['Name']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['Description']);?></td>
            <td style="text-align:center">
                <a href="/p/packs/view-pack.php?Id=<?=$DATA[$i]['Id'];?>" title="View Pack"><img src="/z/img/static/p_view.png" title="View Pack" alt="View Pack"/></a>&nbsp;&nbsp;
                <? if($adminEdit) { ?><a href="/p/packs/edit-pack.php?Id=<?=$DATA[$i]['Id'];?>" title="Edit Pack"><img src="/z/img/static/p_edit.png" title="Edit Pack" alt="Edit Pack"/></a>&nbsp;&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/packs/delete-pack.php?Id=<?=$DATA[$i]['Id'];?>" title="Delete Pack"><img src="/z/img/static/p_delete.png" title="Delete Pack" alt="Delete Pack"/></a><? } ?>
             </td>
        </tr>
<? } ?> 
</table>
<? } else { ?>
<table style="width: 100%; padding:20px;">
<tr>
	<td>No packs found.</td>
</tr>
</table>
<? } ?>

</div>

<?
}
?>