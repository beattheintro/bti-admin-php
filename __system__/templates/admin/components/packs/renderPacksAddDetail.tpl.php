<?
function renderPacksAddDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
	 $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];

	if(!isset($DATA['error'])) 
	{ 
		if(!isset($DATA['QUESTIONS']['error']))
		{
			$numQuestions = count($DATA['QUESTIONS']).'&nbsp;';
		} else {
			$numQuestions = '';
		}
		
		if(is_array($_SESSION['_PACKS_ADD_VARS_']))
		{
			$DATA['PACK']['Name'] = $_SESSION['_PACKS_ADD_VARS_']['Name'];
			$DATA['PACK']['Description'] = $_SESSION['_PACKS_ADD_VARS_']['Description'];
			$DATA['PACK']['Active'] = $_SESSION['_PACKS_ADD_VARS_']['Active'];
			$DATA['PACK']['IsFreeToPlay'] = $_SESSION['_PACKS_ADD_VARS_']['IsFreeToPlay'];
			$DATA['PACK']['CostToUnlock'] = $_SESSION['_PACKS_ADD_VARS_']['CostToUnlock'];
			$DATA['PACK']['Order'] = $_SESSION['_PACKS_ADD_VARS_']['Order'];
		}    

		$_SESSION['PACKS']['numQuestions'] = $numQuestions;

        $packImage = '/z/img/static/cross.png';
		$pWidth = '25';
		$pHeight = '25';
        $requiredImage = 'required ';
		
		//$STDlib->varDump($unlockVals);
?>
<? if($_GET['err'] == 'None') { ?>
<div class="ui-widget-content ui-corner-all pack-edit-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Pack has been added!</td>
        </tr>
    </table>
</div>  
<? } ?>
<div class="ui-widget-content ui-corner-all" style="margin-bottom:2px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Add Pack Information</td>
	</tr>
</table>
</div>
<form method="post" action="/p/__process__/__pack-add.php" id="packAddForm" name="packAddForm" enctype="multipart/form-data">
<input type="hidden" id="Id" name="Id" value="<?=$DATA['PACK']['Id'];?>">
<div class="ui-widget-content ui-corner-all">
	<table style="width:100%">
		<tr>
			<td style="text-align:right;">
                <input id="saveAddPack" name="saveAddPack" type="submit" value="SAVE ALL CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetAddPack" name="resetAddPack" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
			</td>
		</tr>
	</table>
	<table style="width: 100%;" class="table-striped">
		<tr>
            <td style="font-weight:bold;text-align:right;">Pack Name&nbsp;&nbsp;</td>
            <td colspan="3"><input class="input-box-pack-edit ui-corner-all required" id="Name" name="Name" type="text" value="<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACK']['Name']);?>" /></td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Pack Description&nbsp;&nbsp;</td>
            <td colspan="3"><textarea class="text-textarea-box ui-corner-all required" id="Description" name="Description"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACK']['Description']);?></textarea></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Active?&nbsp;&nbsp;</td>
			<td style="font-weight:normal;text-align:left;">
			<? if($DATA['PACK']['Active'] == 'On') { ?>
				<input id="isActive" name="Active" type="checkbox"  checked />
			<? } else { ?>
				<input id="isActive" name="Active" type="checkbox" />
			<? } ?>
			</td>
			<td style="font-weight:bold;text-align:right;">&nbsp;&nbsp;</td>
			<td rowspan="4" style="font-weight:bold;text-align:center;width:250px;"><img src="<?=$packImage;?>" width="<?=$pWidth;?>" height="<?=$pHeight;?>" /></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Free To Play?&nbsp;&nbsp;</td>
			<td colspan="2" style="font-weight:normal;text-align:left;">
			<? if($DATA['PACK']['IsFreeToPlay'] == 'On') { ?>
				<input id="IsFreeToPlay" name="IsFreeToPlay" type="checkbox" checked/>
			<? } else { ?>
				<input id="IsFreeToPlay" name="IsFreeToPlay" type="checkbox" />
			<? } ?>
			</td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Cost to Unlock&nbsp;&nbsp;</td>
			<td colspan="2" style="font-weight:normal;text-align:left;">
				<select id="CostToUnlock" name="CostToUnlock" class="select-box-no-size ui-corner-all" style="width:150px;">
				<? 
				foreach ($ADMINcfg->UNLOCK_VALUES as $key => $value)
				{
					if($key == $DATA['PACK']['CostToUnlock']) {
				?>
						<option value="<?=$key;?>" selected="selected"><?=$value;?></option>
				<? } else { ?>
						<option value="<?=$key;?>"><?=$value;?></option>
				<?	 
					 }
				}
				?>
				 </select>
			</td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Order&nbsp;&nbsp;</td>
			<td colspan="2" style="font-weight:normal;text-align:left;">
				<select id="Order" name="Order" class="select-box-no-size ui-corner-all" style="width:150px;">
				<? 
				foreach ($ADMINcfg->ORDER_VALUES as $key => $value)
				{
					if($key == $DATA['PACK']['Order']) {
				?>
						<option value="<?=$key;?>" selected="selected"><?=$value;?></option>
				<? } else { ?>
						<option value="<?=$key;?>"><?=$value;?></option>
				<?	 
					 }
				}
				?>
				 </select>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="font-weight:bold;text-align:right;">Add Pack Image&nbsp;</td>
			<td><?=$_SESSION['_PACKS_ADD_VARS_']['err']['PNG_MIME'];?><input class="<?=$requiredImage;?>input-box-file ui-corner-all" id="content_asset[thumb]" name="content_asset[thumb]" type="file" /></td>
		</tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all" style="margin:2px 0px 2px 0px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;"><?=$numQuestions;?>Questions Available</td>
	</tr>
	</table>
</div>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['QUESTIONS']['error'])) { ?>
	<table style="width: 100%;" class="table-striped" border="0">
        <tr>
            <td style="font-weight:bold;text-align:left;">Correct Answer</td>
            <td style="font-weight:bold;text-align:left;">Wrong Answer 1</td>
            <td style="font-weight:bold;text-align:left;">Wrong Answer 2</td>
            <td style="font-weight:bold;text-align:center;width:110px;">Add?</td>
        </tr>
        <? for($i=0; isset($DATA['QUESTIONS'][$i]['Id']); $i++) { ?>
        <tr>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['CorrectArtist']);?> - <?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['CorrectTitle']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['WrongArtist1']);?>&nbsp;-&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['WrongTitle1']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['WrongArtist2']);?>&nbsp;-&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTIONS'][$i]['WrongTitle2']);?></td>
            <td style="text-align:center">
                <input id="addQuestion" name="addQuestion[]" type="checkbox" value="<?=$DATA['QUESTIONS'][$i]['QuestionId'];?>" />
            </td>
        </tr>
        <? } ?>
    </table>
	<table style="width:100%">
		<tr>
			<td style="text-align:right;">
                <input id="saveEditPack" name="saveEditPack" type="submit" value="SAVE ALL CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetEditPack" name="resetEditPack" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
			</td>
		</tr>
	</table>
<? } else { ?>
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No questions found for this pack.</td>
	</tr>
	</table>
<? } ?>
</div>
</form>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No pack found.</td>
	</tr>
	</table>
</div>
<? } ?>
<?
}
?>