<?php
function renderPacksPagination($DATA)
{
    global $ADMINcfg, $SITEsession, $STDlib;
	//$STDlib->varDump($DATA);
    
    $DATA['numPacks'] = $_SESSION['QUESTIONS']['numPacks'];
	$DATA['totalPages'] = round($DATA['numPacks']/$ADMINcfg->MAX_DB_RESULTS)+1;
	$DATA['lastPage'] = $DATA['totalPages'];
	$DATA['thisPage'] = $DATA['extra']['thisPage'];

	if($DATA['thisPage'] < 15)
	{
		$DATA['maxPages'] = 15;
		$s = 1;
	} else {
		$DATA['maxPages'] = $DATA['thisPage']+15;
		$s = $DATA['thisPage'];
	}

	if($DATA['maxPages'] > $DATA['totalPages'])
	{
		$DATA['maxPages'] = $DATA['totalPages'];
	}
	//$STDlib->varDump($DATA);
?>
<div class="info-bar" style="margin-top:4px;">
	<div class="ui-widget-content ui-corner-all" style="padding:8px 10px 30px 10px">
        <ul class="pagination" style="display:inline">
            <? if($DATA['thisPage'] >= 15) { ?>
        <li style="display:inline" class="PagedList-skipToLast"><a href="/p/packs/?page=1" title="First Page">&laquo;&laquo;</a></li>
        <li style="display:inline" class="PagedList-skipToNext"><a href="/p/packs/?page=<?=$DATA['thisPage']-1;?>" rel="prev" title="prev">&laquo;</a></li>
            <? } ?>
            <? 
				for($i=$s; $i <= $DATA['maxPages']; $i++)
				{
					if($i == $DATA['thisPage'])
					{
						$activeClass = 'class="active"';
					} else {
						$activeClass = '';
					}
			?>
            <li style="display:inline" <?=$activeClass;?>><a href="/p/packs/?page=<?=$i;?>"><?=$i;?></a></li>
            <? } ?>
            <? if($DATA['maxPages'] != $DATA['totalPages']) { ?>
            <li style="display:inline" class="disabled PagedList-ellipses"><a>...</a></li>
            <? if($i != $DATA['lastPage']) { ?>
            <li style="display:inline"><a href="/p/packs/?page=<?=$DATA['lastPage'];?>"><?=$DATA['lastPage'];?></a></li>
            <? } ?>
            <li style="display:inline" class="PagedList-skipToNext"><a href="/p/packs/?page=<?=$DATA['thisPage']+1;?>" rel="next" title="next">&raquo;</a></li>
            <li style="display:inline" class="PagedList-skipToLast"><a href="/p/packs/?page=<?=$DATA['lastPage'];?>" title="Last Page ">&raquo;&raquo;</a></li>
            <? } ?>
        </ul>
     </div>
</div>
<?
}
?>