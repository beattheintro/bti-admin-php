<?

	function renderMobileManagerPreviewer($DATA)
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$new = $_SESSION['_LOGIN_']['admin_new'];
		$admin = $_SESSION['_LOGIN_']['admin_1'];

		//$STDlib->varDump($ADMINcfg);
		$thisPortal = $_SESSION['_PORTAL_']['portal_name'];

		if($ADMINcfg->SERVICE == 'STAGING')
		{
			$thisService = 'DEV';
			$ADMINcfg->GLOBAL_DEBUG = FALSE;
		} else {
			$thisService = 'LIVE';
		}

		/*
		if(is_array($_SESSION['_PORTLET_DATA_']))
		{
			// Serialize and base64 Encode the data
			$portletData = base64_encode(serialize($_SESSION['_PORTLET_DATA_']));
		}
		*/

		$uniqfile = md5($STDlib->makePassword(7));
		$url = $ADMINcfg->PORTAL_SERVICES[$thisPortal]['DOMAINS'][$thisService].'/em/index.php?uf='.$uniqfile;
		//$url = 'mobile.ukreal.local';

		$_SESSION['_PREVIEW_DATA_CACHE_'][] .= $uniqfile;
?>
<script type="text/javascript">
	$(document).ready(function(){
		$.get("/portal/mm-ajax.php",{uniq_file:'<?=$uniqfile;?>',method:'savePreviewSession'});
		$("#mm_preview").css("width","340px");
		$("#mm_preview").attr("src", "http://<?=$url;?>&UA=<?=$ADMINcfg->USER_AGENTS['IPHONE'];?>&_pType=<?=$_GET['pType'];?>");

		$(function(){
			$(window).unload( function (){
				$.get("/portal/mm-ajax.php",{method:'deletePreviewSession'});
			});
		});
	});

	$(function(){
	  $("select#phone_selector").change(function(){
		if($(this).val() == 'IPHONE')
		{
			$.get("/portal/mm-ajax.php",{uniq_file:'<?=$uniqfile;?>',method:'savePreviewSession'});
			$("#mm_preview").css("width","340px");
			$("#mm_preview").attr("src", "http://<?=$url;?>&UA=<?=$ADMINcfg->USER_AGENTS['IPHONE'];?>&_pType=<?=$_GET['pType'];?>");
			$("#mm_preview").load();
		} else if($(this).val() == 'ANDROID') {
			$.get("/portal/mm-ajax.php",{uniq_file:'<?=$uniqfile;?>',method:'savePreviewSession'});
			$("#mm_preview").css("width","320px");
			$("#mm_preview").attr("src", "http://<?=$url;?>&UA=<?=$ADMINcfg->USER_AGENTS['ANDROID'];?>&_pType=<?=$_GET['pType'];?>");
			$("#mm_preview").load();
		} else if($(this).val() == 'NOKIA') {
			$.get("/portal/mm-ajax.php",{uniq_file:'<?=$uniqfile;?>',method:'savePreviewSession'});
			$("#mm_preview").css("width","340px");
			$("#mm_preview").attr("src", "http://<?=$url;?>&UA=<?=$ADMINcfg->USER_AGENTS['NOKIA'];?>&_pType=<?=$_GET['pType'];?>");
			$("#mm_preview").load();
		} else if($(this).val() == 'NOKIA_B5') {
			$.get("/portal/mm-ajax.php",{uniq_file:'<?=$uniqfile;?>',method:'savePreviewSession'});
			$("#mm_preview").css("width","250px");
			$("#mm_preview").attr("src", "http://<?=$url;?>&UA=<?=$ADMINcfg->USER_AGENTS['NOKIA_B5'];?>&_pType=<?=$_GET['pType'];?>");
			$("#mm_preview").load();
		} else if($(this).val() == 'BLACKBERRY') {
			$.get("/portal/mm-ajax.php",{uniq_file:'<?=$uniqfile;?>',method:'savePreviewSession'});
			$("#mm_preview").css("width","440px");
			$("#mm_preview").attr("src", "http://<?=$url;?>&UA=<?=$ADMINcfg->USER_AGENTS['BLACKBERRY'];?>&_pType=<?=$_GET['pType'];?>");
			$("#mm_preview").load();
		}
	  });
	});
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center" valign="top" width="100%">
			<select id="phone_selector" class="select-box-no-size ui-corner-all" style="width:380px;">
				<option value="IPHONE" selected="selected">iPhone 3GS Emulator</option>
				<option value="ANDROID">Android (HTC Hero) Emulator</option>
				<option value="NOKIA">Nokia Generic Emulator</option>
				<option value="NOKIA_B5">Nokia Bank 5 Emulator</option>
				<option value="BLACKBERRY">Blackberry (8900 Curve) Emulator</option>
			</select>
		</td>
	</tr>
</table>
<hr class="with-border ui-corner-all" style="width:99%;background-color:#9D9D9D;border:1px solid #868686;" />
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center" valign="top" width="100%">
		<iframe src ="http://mobile.ukreal.local/" width="377" height="495" frameborder="0" name="mm_preview" class="no-border ui-corner-bottom" style="background-color:#fff;" id="mm_preview"><p>Your browser does not support iframes.</p></iframe>
		</td>
	</tr>
</table>
<?
	}

?>