<?php
function renderAJAXSelectBox($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;

    //$STDlib->varDump($DATA);
	$selected = $DATA['selected'];
	
	for($i=0; isset($DATA[$i]['Id']); $i++)
	{
		if($DATA[$i]['Data'] == $selected)
		{
			echo '<option value="'.$DATA[$i]['Id'].'" selected="selected">'.$DATA[$i]['Data'].'</option>';
		} else {
			echo '<option value="'.$DATA[$i]['Id'].'">'.$DATA[$i]['Data'].'</option>';
		}
	}	
}
?>