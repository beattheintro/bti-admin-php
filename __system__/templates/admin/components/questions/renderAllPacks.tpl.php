<?
function renderAllPacks($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
	//$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    //$adminDelete = $_SESSION['_LOGIN_']['admin_delete'];

	//$cross = '<img src="/z/img/static/cross.png" title="" alt=""/>';
	//$tick = '<img src="/z/img/static/tick.png" title="" alt=""/>';
	
	$inPacks = $_SESSION['QUESTIONS']['InPackId'];
?>
<div class="ui-widget-content ui-corner-all" style="margin:2px 0px 2px 0px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Add This Question to Packs</td>
	</tr>
	</table>
</div>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['error'])) { ?>
    <table style="width: 100%;" class="table-striped">
        <tr>
            <td style="font-weight:bold;width:165px;">Pack Name</td>
            <td style="font-weight:bold;">Pack Description</td>
            <td style="font-weight:bold;text-align:center;width:100px;">Add/Remove From Pack</td>
        </tr>
<? for($i=0; isset($DATA[$i]['Id']); $i++) { ?>
<?
	$thisPackId = $DATA[$i]['Id'];
	if(is_array($inPacks))
	{
		if(in_array($thisPackId, $inPacks))
		{
			$isChecked = 'checked';
		} else {
			$isChecked = '';
		}
	}
?>
        <tr>
            <td style="text-align:left;"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['Name']);?></td>
            <td style="text-align:left;"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['Description']);?></td>
            <td style="text-align:center;"><? if($adminEdit) { ?><input type="checkbox" id="packs[]"  name="packs[]" value="<?=$DATA[$i]['Id'];?>" <?=$isChecked;?>/><? } ?>
             </td>
        </tr>
<? } ?>
		<tr>
			<td colspan="3" style="text-align:right;">
                <input id="saveEditQuestion" name="saveEditQuestion" type="submit" value="SAVE ALL CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetEditQuestion" name="resetEditQuestion" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
			</td>
		</tr>
    </table>
	</form>
<? } else { ?>
    <table style="width: 100%; padding:20px;">
        <tr>
             <td>No packs found.</td>
        </tr>
    </table>
<? } ?>
</div>
<?
}
?>