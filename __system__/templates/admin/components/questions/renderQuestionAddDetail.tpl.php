<?
function renderQuestionAddDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
    //$adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    //$adminDelete = $_SESSION['_LOGIN_']['admin_delete'];

	$trackArtistData = $DATA['TRACK_ARTISTS'];
	$trackAllArtistData = $DATA['TRACK_ARTISTS_ALL'];
	$_SESSION['QUESTIONS']['trackArtistData'] = $trackArtistData;
	$_SESSION['QUESTIONS']['trackAllArtistData'] = $trackAllArtistData;
    //$STDlib->varDump($trackArtistData);
?>
<? if(!isset($DATA['error'])) { ?>
<? if($_GET['err'] == 'None') { ?>
<div class="ui-widget-content ui-corner-all question-edit-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Question has been added!</td>
        </tr>
    </table>
</div>  
<? } ?>
<div class="ui-widget-content ui-corner-all" style="margin-bottom:2px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Add Question Information</td>
	</tr>
</table>
</div>
<form method="post" action="/p/__process__/__question-add.php" id="questionAddForm" name="questionAddForm">
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%;" class="table-striped">
		<tr>
			<td>&nbsp;</td>
			<td style="font-weight:bold;text-align:left;">Artist</td>
			<td style="font-weight:bold;text-align:left;">Track Title</td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Correct Answer&nbsp;&nbsp;</td>
            <td>
				<select id="trackCorrectArtist" name="trackCorrectArtist" style="width:400px;" class="required">
					<option value="" selected="selected">---- Choose an Artist ----</option>
				<? for($i=0; isset($trackArtistData[$i]['Artist']); $i++) { ?>
					<option value="<?=$i;?>"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackArtistData[$i]['Artist']);?></option>
				<? } ?>
				</select>
			</td>
			<td>
				<select id="trackCorrectTitle" name="trackCorrectId" style="width:400px;" class="required">
					<option value="" selected="selected">-------------------------------------</option>
				</select>
			</td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Wrong Answer 1&nbsp;&nbsp;</td>
            <td>
				<select id="trackWrongArtist1" name="trackWrongArtist1" style="width:400px;" class="required">
					<option value="" selected="selected">---- Choose an Artist ----</option>
				<? for($i=0; isset($trackAllArtistData[$i]['Artist']); $i++) { ?>
					<option value="<?=$i;?>"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackAllArtistData[$i]['Artist']);?></option>
				<? } ?>
				</select>
			</td>
			<td>
				<select id="trackWrongTitle1" name="trackWrongId1" style="width:400px;" class="required">
					<option value="" selected="selected">-------------------------------------</option>
				</select>
            </td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Wrong Answer 2&nbsp;&nbsp;</td>
            <td>
				<select id="trackWrongArtist2" name="trackWrongArtist2" style="width:400px;" class="required">
					<option value="" selected="selected">---- Choose an Artist ----</option>
				<? for($i=0; isset($trackAllArtistData[$i]['Artist']); $i++) { ?>
					<option value="<?=$i;?>"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackAllArtistData[$i]['Artist']);?></option>
				<? } ?>
				</select>
			</td>
			<td>
				<select id="trackWrongTitle2" name="trackWrongId2" style="width:400px;" class="required">
					<option value="" selected="selected">-------------------------------------</option>
				</select>
			</td>
        </tr>
		<tr>
			<td colspan="3" style="text-align:right;">
                <input id="saveAddQuestion" name="saveAddQuestion" type="submit" value="SAVE ALL CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetAddQuestion" name="resetAddQuestion" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
			</td>
		</tr>
    </table>
</div>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No questions found.</td>
	</tr>
	</table>
</div>
<? } ?>
<?
}
?>