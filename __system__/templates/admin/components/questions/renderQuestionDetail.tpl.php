<?
function renderQuestionDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
    
?>
<? if(!isset($DATA['error'])) { ?>
<? 
	$cross = '<img src="/z/img/static/cross.png" title="" alt=""/>';
	$tick = '<img src="/z/img/static/tick.png" title="" alt=""/>';

	if(($DATA['QUESTION']['HasMP3'] == 1) || ($DATA['QUESTION']['HasMP4'] == 1))
	{
		$HasAsset = $tick."&nbsp;(Correct Answer has an MP3 or MP4)";
	} else {
		$HasAsset = $cross."&nbsp;(Correct Answer does NOT have an MP3 or MP4, please edit the track and upload an asset!)";
	}
?>

<div class="ui-widget-content ui-corner-all" style="margin-bottom:2px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Question Information</td>
	</tr>
</table>
</div>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%;" class="table-striped">
		<tr>
            <td style="font-weight:bold;text-align:right;width:150px;">Created&nbsp;&nbsp;</td>
            <td><?=$DATA['QUESTION']['Created'];?></td>
			<td style="font-weight:bold;text-align:right;">Track Toolbox</td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Correct Answer&nbsp;&nbsp;</td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION']['CorrectArtist']);?>&nbsp;-&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION']['CorrectTitle']);?></td>
			<td style="font-weight:bold;text-align:right;">
                <a href="/p/tracks/search.php?search_term=<?=urlencode($DATA['QUESTION']['CorrectArtist']);?>" title="Search Artist"><img src="/z/img/static/magnifier.png" title="Search Artist" alt="Search Artist"/></a>&nbsp;
                <a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION']['CorrectId'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a>&nbsp;
                <? if($adminEdit) { ?><a href="/p/tracks/edit-track.php?Id=<?=$DATA['QUESTION']['CorrectId'];?>" title="Edit Track"><img src="/z/img/static/p_edit.png" title="Edit Track" alt="Edit Track"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/tracks/delete-track.php?Id=<?=$DATA['QUESTION']['CorrectId'];?>" title="Delete Track"><img src="/z/img/static/p_delete.png" title="Delete Track" alt="Delete Track"/></a>&nbsp;<? } ?>
			</td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Wrong Answer 1&nbsp;&nbsp;</td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION']['WrongArtist1']);?>&nbsp;-&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION']['WrongTitle1']);?></td>
			<td style="font-weight:bold;text-align:right;">
                <a href="/p/tracks/search.php?search_term=<?=urlencode($DATA['QUESTION']['WrongArtist1']);?>" title="Search Artist"><img src="/z/img/static/magnifier.png" title="Search Artist" alt="Search Artist"/></a>&nbsp;
                <a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION']['WrongId1'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a>&nbsp;
                <? if($adminEdit) { ?><a href="/p/tracks/edit-track.php?Id=<?=$DATA['QUESTION']['WrongId1'];?>" title="Edit Track"><img src="/z/img/static/p_edit.png" title="Edit Track" alt="Edit Track"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/tracks/delete-track.php?Id=<?=$DATA['QUESTION']['WrongId1'];?>" title="Delete Track"><img src="/z/img/static/p_delete.png" title="Delete Track" alt="Delete Track"/></a>&nbsp;<? } ?>
			</td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Wrong Answer 2&nbsp;&nbsp;</td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION']['WrongArtist2']);?>&nbsp;-&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION']['WrongTitle2']);?></td>
			<td style="font-weight:bold;text-align:right;">
                <a href="/p/tracks/search.php?search_term=<?=urlencode($DATA['QUESTION']['WrongArtist2']);?>" title="Search Artist"><img src="/z/img/static/magnifier.png" title="Search Artist" alt="Search Artist"/></a>&nbsp;
                <a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION']['WrongId2'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a>&nbsp;
                <? if($adminEdit) { ?><a href="/p/tracks/edit-track.php?Id=<?=$DATA['QUESTION']['WrongId2'];?>" title="Edit Track"><img src="/z/img/static/p_edit.png" title="Edit Track" alt="Edit Track"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/tracks/delete-track.php?Id=<?=$DATA['QUESTION']['WrongId2'];?>" title="Delete Track"><img src="/z/img/static/p_delete.png" title="Delete Track" alt="Delete Track"/></a>&nbsp;<? } ?>
			</td>
        </tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Has Asset?&nbsp;&nbsp;</td>
			<td style="font-weight:normal;text-align:left;" colspan="2"><?=$HasAsset;?></td>
		<tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Question Toolbox&nbsp;&nbsp;</td>
			<td style="font-weight:bold;text-align:left;" colspan="2">
                <? if($adminEdit) { ?><a href="/p/questions/edit-question.php?Id=<?=$DATA['QUESTION']['Id'];?>" title="Edit Question"><img src="/z/img/static/p_edit.png" title="Edit Question" alt="Edit Question"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/questions/delete-question.php?Id=<?=$DATA['QUESTION']['Id'];?>" title="Delete Question"><img src="/z/img/static/p_delete.png" title="Delete Question" alt="Delete Question"/></a>&nbsp;<? } ?>
			</td>
		<tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all" style="margin:2px 0px 2px 0px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Question in Packs</td>
	</tr>
	</table>
</div>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['PACKS']['error'])) { ?>
	<table style="width: 100%;" class="table-striped" border="0">
        <tr>
            <td style="font-weight:bold;text-align:left;width:100px;">Created&nbsp;&nbsp;</td>
            <td style="font-weight:bold;text-align:left;">Pack Name</td>
            <td style="font-weight:bold;text-align:left;">Pack Description</td>
            <td style="font-weight:bold;text-align:center;width:88px;">Toolbox</td>
        </tr>
        <? for($i=0; isset($DATA['PACKS'][$i]['Id']); $i++) { ?>
        <tr>
            <td><?=$DATA['PACKS'][$i]['Created'];?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACKS'][$i]['Name']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACKS'][$i]['Description']);?></td>
            <td style="text-align:center">
				<a href="/p/packs/search.php?search_term=<?=urlencode($DATA['PACKS'][$i]['Name']);?>" title="Search Pack Name"><img src="/z/img/static/magnifier.png" title="Search Pack Name" alt="Search Pack Name"/></a>&nbsp;
                <a href="/p/packs/view-packs.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="View Pack"><img src="/z/img/static/p_view.png" title="View Pack" alt="View Pack"/></a>&nbsp
                <? if($adminEdit) { ?><a href="/p/packs/edit-packs.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="Edit Pack"><img src="/z/img/static/p_edit.png" title="Edit Pack" alt="Edit Pack"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/packs/delete-packs.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="Delete Pack"><img src="/z/img/static/p_delete.png" title="Delete Pack" alt="Delete Pack"/></a><? } ?>
            </td>
        </tr>
        <? } ?>
    </table>
<? } else { ?>
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No packs found for this question.</td>
	</tr>
	</table>
<? } ?>
</div>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No questions found.</td>
	</tr>
	</table>
</div>
<? } ?>
<?
}
?>