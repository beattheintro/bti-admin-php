<?
function renderQuestionEditDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
    //$adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    //$adminDelete = $_SESSION['_LOGIN_']['admin_delete'];

	$trackArtistData = $DATA['TRACK_ARTISTS'];
	$trackAllArtistData = $DATA['TRACK_ARTISTS_ALL'];
	$_SESSION['QUESTIONS']['trackArtistData'] = $trackArtistData;
	$_SESSION['QUESTIONS']['trackAllArtistData'] = $trackAllArtistData;
    //$STDlib->varDump($trackArtistData);
?>
<? if(!isset($DATA['error'])) { ?>
<? 
	$cross = '<img src="/z/img/static/cross.png" title="" alt=""/>';
	$tick = '<img src="/z/img/static/tick.png" title="" alt=""/>';

	if(($DATA['QUESTION']['HasMP3'] == 1) || ($DATA['QUESTION']['HasMP4'] == 1))
	{
		$HasAsset = $tick."&nbsp;(Correct Answer has an MP3 or MP4)";
	} else {
		$HasAsset = $cross."&nbsp;(Correct Answer does NOT have an MP3 or MP4, please edit the track and upload an asset!)";
	}

	if(!isset($DATA['PACKS']['error']))
	{
		for($i=0; isset($DATA['PACKS'][$i]['Id']); $i++)
		{
			$_SESSION['QUESTIONS']['InPackId'][$i] = $DATA['PACKS'][$i]['Id'];
		}
	}
?>
<? if($_GET['err'] == 'None') { ?>
<div class="ui-widget-content ui-corner-all question-edit-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Question has been updated!</td>
        </tr>
    </table>
</div>  
<? } ?>
<div class="ui-widget-content ui-corner-all" style="margin-bottom:2px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Edit Question Information</td>
	</tr>
</table>
</div>
<form method="post" action="/p/__process__/__question-edit.php" id="questionEditForm" name="questionEditForm">
<input type="hidden" id="Id" name="Id" value="<?=$DATA['QUESTION']['Id'];?>">
<div class="ui-widget-content ui-corner-all">
	<table style="width:100%">
		<tr>
			<td style="text-align:right;">
                <input id="saveEditQuestion" name="saveEditQuestion" type="submit" value="SAVE ALL CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetEditQuestion" name="resetEditQuestion" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
			</td>
		</tr>
	</table>
	<table style="width: 100%;" class="table-striped">
		<tr>
			<td>&nbsp;</td>
			<td style="font-weight:bold;text-align:left;">Artist</td>
			<td style="font-weight:bold;text-align:left;">Track Title</td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Correct Answer&nbsp;&nbsp;</td>
            <td>
				<select id="trackCorrectArtist" name="trackCorrectArtist" style="width:400px;">
				<? for($i=0; isset($trackArtistData[$i]['Artist']); $i++) { ?>
				<? $correctTrack = $DATA['QUESTION']['CorrectArtist']; ?>
					<? if($trackArtistData[$i]['Artist'] == $correctTrack) { ?>
					<option value="<?=$i;?>" selected="selected"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackArtistData[$i]['Artist']);?></option>
					<? $cId = $i; ?>
					<? } else { ?>
					<option value="<?=$i;?>"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackArtistData[$i]['Artist']);?></option>
					<? } ?>
				<? } ?>
				</select>
				<? $_SESSION['QUESTIONS']['CorrectTitle'] = $DATA['QUESTION']['CorrectTitle']; ?>
				<script type="text/javascript">
					$(document).ready(function () {
						var dataString = 'trackCorrectArtist=' + <?=$cId;?>;
						$.ajax({
							type: "POST",
							url: "/p/__process__/__question-ajax.php",
							data: dataString,
							cache: false,
							success: function (html) {
								$("#trackCorrectTitle").html(html);
							}
						});
					});
				</script>
			</td>
			<td>
				<select id="trackCorrectTitle" name="trackCorrectId" style="width:400px;">
					<option value="" selected="selected">-------------------------------------</option>
				</select>
			</td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Wrong Answer 1&nbsp;&nbsp;</td>
            <td>
				<select id="trackWrongArtist1" name="trackWrongArtist1" style="width:400px;">
				<? for($i=0; isset($trackAllArtistData[$i]['Artist']); $i++) { ?>
				<? $wrongArtist1 = $DATA['QUESTION']['WrongArtist1']; ?>
					<? if($trackAllArtistData[$i]['Artist'] == $wrongArtist1) { ?>
					<option value="<?=$i;?>" selected="selected"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackAllArtistData[$i]['Artist']);?></option>
					<? $wId1 = $i; ?>
					<? } else { ?>
					<option value="<?=$i;?>"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackAllArtistData[$i]['Artist']);?></option>
					<? } ?>
				<? } ?>
				</select>
				<? $_SESSION['QUESTIONS']['WrongTitle1'] = $DATA['QUESTION']['WrongTitle1']; ?>
				<script type="text/javascript">
					$(document).ready(function () {
						var dataString = 'trackWrongArtist1=' + <?=$wId1;?>;
						$.ajax({
							type: "POST",
							url: "/p/__process__/__question-ajax.php",
							data: dataString,
							cache: false,
							success: function (html) {
								$("#trackWrongTitle1").html(html);
							}
						});
					});
				</script>
			</td>
			<td>
				<select id="trackWrongTitle1" name="trackWrongId1" style="width:400px;">
					<option value="" selected="selected">-------------------------------------</option>
				</select>
            </td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:right;">Wrong Answer 2&nbsp;&nbsp;</td>
            <td>
				<select id="trackWrongArtist2" name="trackWrongArtist2" style="width:400px;">
				<? for($i=0; isset($trackAllArtistData[$i]['Artist']); $i++) { ?>
				<? $wrongArtist2 = $DATA['QUESTION']['WrongArtist2']; ?>
					<? if($trackAllArtistData[$i]['Artist'] == $wrongArtist2) { ?>
					<option value="<?=$i;?>" selected="selected"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackAllArtistData[$i]['Artist']);?></option>
					<? $wId2 = $i; ?>
					<? } else { ?>
					<option value="<?=$i;?>"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$trackAllArtistData[$i]['Artist']);?></option>
					<? } ?>
				<? } ?>
				</select>
				<? $_SESSION['QUESTIONS']['WrongTitle2'] = $DATA['QUESTION']['WrongTitle2']; ?>
				<script type="text/javascript">
					$(document).ready(function () {
						var dataString = 'trackWrongArtist2=' + <?=$wId2?>;
						$.ajax({
							type: "POST",
							url: "/p/__process__/__question-ajax.php",
							data: dataString,
							cache: false,
							success: function (html) {
								$("#trackWrongTitle2").html(html);
							}
						});
					});
				</script>
			</td>
			<td>
				<select id="trackWrongTitle2" name="trackWrongId2" style="width:400px;">
					<option value="" selected="selected">-------------------------------------</option>
				</select>
			</td>
        </tr>
		<tr>
            <td style="font-weight:bold;text-align:right;width:150px;">Created&nbsp;&nbsp;</td>
            <td colspan="2"><?=$DATA['QUESTION']['Created'];?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:right;">Has Asset?&nbsp;&nbsp;</td>
			<td style="font-weight:normal;text-align:left;" colspan="2"><?=$HasAsset;?></td>
		<tr>
    </table>
</div>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No questions found.</td>
	</tr>
	</table>
</div>
<? } ?>
<?
}
?>