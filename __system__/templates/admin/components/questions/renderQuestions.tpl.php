<?
function renderQuestions($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	//$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
?>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['error'])) { ?>
    <table style="width: 100%;" class="table-striped">
        <tr>
            <td style="font-weight:bold;font-size:larger;width:75px;">Created</td>
            <td style="font-weight:bold;font-size:larger;">Correct Answer</td>
            <td style="font-weight:bold;font-size:larger;">Wrong Answer 1</td>
            <td style="font-weight:bold;font-size:larger;">Wrong Answer 2</td>
            <td style="font-weight:bold;font-size:larger;width:100px;">Toolbox</td>
        </tr>
<? for($i=0; isset($DATA[$i]['Id']); $i++) { ?>

<? if(($DATA[$i]['HasMP3'] == 1) || ($DATA[$i]['HasMP4'] == 1)) { $playable = ''; } else { $playable = ' style="background-color:#ffcece;"'; } ?>
        <tr>
            <td<?=$playable;?>><?=$DATA[$i]['Created'];?></td>
            <td<?=$playable;?>><a href="/p/tracks/view-track.php?Id=<?=$DATA[$i]['CorrectId'];?>" title="View Track (New window)" target="_blank"><img src="/z/img/static/p_view.png" title="View Track (New window)" alt="View Track (New window)"/></a>&nbsp;&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['CorrectTitle']);?></td>
            <td<?=$playable;?>><a href="/p/tracks/view-track.php?Id=<?=$DATA[$i]['WrongId1'];?>" title="View Track (New window)" target="_blank"><img src="/z/img/static/p_view.png" title="View Track (New window)" alt="View Track (New window)"/></a>&nbsp;&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['WrongTitle1']);?></td>
            <td<?=$playable;?>><a href="/p/tracks/view-track.php?Id=<?=$DATA[$i]['WrongId2'];?>" title="View Track (New window)" target="_blank"><img src="/z/img/static/p_view.png" title="View Track (New window)" alt="View Track (New window)"/></a>&nbsp;&nbsp;<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['WrongTitle2']);?></td>
            <td<?=$playable;?>>
                <a href="/p/questions/search.php?search_term=<?=urlencode($DATA[$i]['CorrectTitle']);?>" title="Search Title"><img src="/z/img/static/magnifier.png" title="Search Title" alt="Search Title"/></a>&nbsp;&nbsp;
                <a href="/p/questions/view-question.php?Id=<?=$DATA[$i]['Id'];?>" title="View Question"><img src="/z/img/static/p_view.png" title="View Question" alt="View Question"/></a>&nbsp;&nbsp;
                <? if($adminEdit) { ?><a href="/p/questions/edit-question.php?Id=<?=$DATA[$i]['Id'];?>" title="Edit Question"><img src="/z/img/static/p_edit.png" title="Edit Question" alt="Edit Question"/></a>&nbsp;&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/questions/delete-question.php?Id=<?=$DATA[$i]['Id'];?>" title="Delete Question"><img src="/z/img/static/p_delete.png" title="Delete Question" alt="Delete Question"/></a><? } ?>
             </td>
        </tr>
<? } ?>
 
</table>

<? } else { ?>
<table style="width: 100%; padding:20px;">
<tr>

	<td>No questions found.</td>

</tr>
</table>

<? } ?>

</div>

<?
}
?>