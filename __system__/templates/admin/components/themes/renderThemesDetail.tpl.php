<?
function renderThemesDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
?>
<? if(!isset($DATA['error'])) { ?>
<? 
	if(!isset($DATA['PACKS']['error']))
	{
		$numPacks = count($DATA['PACKS']).'&nbsp;';
	} else {
		$numPacks = '';
	}

	$themeImage = $ADMINcfg->AZURE_BLOB_ADDR.'/themes/'.$DATA['THEME']['Shortname'].'/bgmain.jpg'; 
	
	// Is this file in the blob?
	$fileHeaders = @get_headers($themeImage);
	if($fileHeaders[0] == 'HTTP/1.1 404 The specified blob does not exist.')
	{
		$themeImage = '/z/img/static/cross.png';
		$pWidth = '20';
		$pHeight = '20';
		$hasImg = '&nbsp;No Theme Image!';
	} else {
		$pWidth = '180';
		$pHeight = '110';
		$hasImg = '';
	}
?>
<div class="ui-widget-content ui-corner-all" style="margin-bottom:2px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Theme Information</td>
	</tr>
</table>
</div>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%;" class="table-striped" border="0">
		<tr>
			<td rowspan="4" style="font-weight:bold;text-align:center;width:250px;"><img src="<?=$themeImage;?>" width="<?=$pWidth;?>" height="<?=$pHeight;?>" /><?=$hasImg;?></td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:left;width:150px;">Created&nbsp;&nbsp;</td>
            <td><?=$DATA['THEME']['Created'];?></td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:left;">Theme Name&nbsp;&nbsp;</td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['THEME']['Name']);?></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:left;">Theme Toolbox&nbsp;&nbsp;</td>
			<td style="font-weight:bold;text-align:left;">
                <? if($adminEdit) { ?><a href="/p/themes/edit-theme.php?Id=<?=$DATA['THEME']['Id'];?>" title="Edit Theme"><img src="/z/img/static/p_edit.png" title="Edit Theme" alt="Edit Theme"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/themes/delete-theme.php?Id=<?=$DATA['THEME']['Id'];?>" title="Delete Theme"><img src="/z/img/static/p_delete.png" title="Delete Theme" alt="Delete Theme"/></a>&nbsp;<? } ?>
			</td>
		</tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all" style="margin:2px 0px 2px 0px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;"><?=$numPacks;?>Packs in this Theme</td>
	</tr>
	</table>
</div>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['PACKS']['error'])) { ?>
	<table style="width: 100%;" class="table-striped" border="0">
        <tr>
            <td></td>
            <td style="font-weight:bold;text-align:left;">Name</td>
            <td style="font-weight:bold;text-align:left;">Description</td>
            <td style="font-weight:bold;text-align:left;">Active?</td>
            <td style="font-weight:bold;text-align:center;width:88px;">Toolbox</td>
        </tr>
        <? for($i=0; isset($DATA['PACKS'][$i]['Id']); $i++) { ?>
	<? $packImage = $ADMINcfg->AZURE_BLOB_ADDR.'/packimages/mobile/'.strtolower($DATA['PACKS'][$i]['Id']).'.png'; ?>
	<? if($DATA['PACKS'][$i]['Active'] == 1) { $Active = 'tick'; } else { $Active = 'cross'; } ?>
        <tr>
			<td><img src="<?=$packImage;?>" title="" alt="" width="75" height="50"/></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACKS'][$i]['Name']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACKS'][$i]['Description']);?></td>
            <td style="text-align:center"><img src="/z/img/static/<?=$Active;?>.png" title="" alt=""/></td>
            <td style="text-align:center">
				<a href="/p/packs/search.php?search_term=<?=urlencode($DATA['PACKS'][$i]['Name']);?>" title="Search Name"><img src="/z/img/static/magnifier.png" title="Search Name" alt="Search Name"/></a>&nbsp;
                <a href="/p/packs/view-pack.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="View Pack"><img src="/z/img/static/p_view.png" title="View Pack" alt="View Pack"/></a>&nbsp
                <? if($adminEdit) { ?><a href="/p/packs/edit-pack.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="Edit Pack"><img src="/z/img/static/p_edit.png" title="Edit Pack" alt="Edit Pack"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/packs/delete-pack.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="Delete Pack"><img src="/z/img/static/p_delete.png" title="Delete Pack" alt="Delete Pack"/></a><? } ?>
            </td>
        </tr>
        <? } ?>
    </table>
<? } else { ?>
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No packs found for this theme.</td>
	</tr>
	</table>
<? } ?>
</div>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No theme found.</td>
	</tr>
	</table>
</div>
<? } ?>
<?
}
?>