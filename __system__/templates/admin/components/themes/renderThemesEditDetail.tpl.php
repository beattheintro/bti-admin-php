<?
function renderThemesEditDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
	 $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
	 $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];

	if(!isset($DATA['error'])) 
	{ 
		if(!isset($DATA['PACKS']['error']))
		{
			$numPacks = count($DATA['PACKS']).'&nbsp;';
		} else {
			$numPacks = '';
		}

		if(!isset($DATA['MISSING']['error']))
		{
			$numMissingPacks = count($DATA['MISSING']).'&nbsp;';
		} else {
			$numMissingPacks = '';
		}

		if(is_array($_SESSION['_THEMES_EDIT_VARS_']))
		{
			$DATA['THEME']['Name'] = $_SESSION['_THEMES_EDIT_VARS_']['Name'];
		}    

		$_SESSION['THEMES']['numPacks'] = $numPacks;
		$_SESSION['THEMES']['numMissingPacks'] = $numMissingPacks;

		$themeImage = $ADMINcfg->AZURE_BLOB_ADDR.'/themes/'.$DATA['THEME']['Shortname'].'/bgmain.jpg'; 
        
        // Is this file in the blob?
        $fileHeaders = @get_headers($themeImage);
        if($fileHeaders[0] == 'HTTP/1.1 404 The specified blob does not exist.')
        {
            $themeImage = '/z/img/static/cross.png';
			$pWidth = '25';
			$pHeight = '25';
            $requiredImage = 'required ';
        } else {
			$pWidth = '180';
			$pHeight = '110';
            $requiredImage = '';
		}
		
		//$STDlib->varDump($unlockVals);
?>
<? if($_GET['err'] == 'None') { ?>
<div class="ui-widget-content ui-corner-all theme-edit-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Theme has been updated!</td>
        </tr>
    </table>
</div>  
<? } ?>
<div class="ui-widget-content ui-corner-all" style="margin-bottom:2px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;">Edit Theme Information</td>
	</tr>
</table>
</div>
<form method="post" action="/p/__process__/__theme-edit.php" id="themeEditForm" name="themeEditForm" enctype="multipart/form-data">
<input type="hidden" id="Id" name="Id" value="<?=$DATA['THEME']['Id'];?>">
<input type="hidden" id="Shortname" name="Shortname" value="<?=$DATA['THEME']['Shortname'];?>">
<div class="ui-widget-content ui-corner-all">
	<table style="width:100%">
		<tr>
			<td style="text-align:right;">
                <input id="saveEditTheme" name="saveEditTheme" type="submit" value="SAVE ALL CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetEditTheme" name="resetEditTheme" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
			</td>
		</tr>
	</table>
	<table style="width: 100%;" class="table-striped" border="0">
		<tr>
			<td rowspan="4" style="font-weight:bold;text-align:center;width:200px;"><img src="<?=$themeImage;?>" width="<?=$pWidth;?>" height="<?=$pHeight;?>" /></td>
		</tr>
		<tr>
            <td style="font-weight:bold;text-align:left;width:200px;">Theme Name&nbsp;&nbsp;</td>
            <td><input class="input-box-theme-edit ui-corner-all required" id="Name" name="Name" type="text" value="<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['THEME']['Name']);?>" /></td>
		</tr>
		<tr>
			<td style="font-weight:bold;text-align:left;width:200px;">Change/Add Theme Image&nbsp;</td>
			<td><?=$_SESSION['_THEMES_EDIT_VARS_']['err']['JPEG_MIME'];?><input class="<?=$requiredImage;?>input-box-file ui-corner-all" id="content_asset[thumb]" name="content_asset[thumb]" type="file" /></td>
		</tr>
		<? if($adminDelete) { ?>
		<tr>
			<td style="font-weight:bold;text-align:left;width:200px;">Theme Toolbox&nbsp;&nbsp;</td>
			<td style="font-weight:bold;text-align:left;">
                <a href="/p/themes/delete-theme.php?Id=<?=$DATA['THEME']['Id'];?>" title="Delete Theme"><img src="/z/img/static/p_delete.png" title="Delete Theme" alt="Delete Theme"/></a>&nbsp;
			</td>
		</tr>
		<? } ?>
    </table>
</div>
<div class="ui-widget-content ui-corner-all" style="margin:2px 0px 2px 0px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;"><?=$numPacks;?>Packs in this Theme</td>
	</tr>
	</table>
</div>
<div class="ui-widget-content ui-corner-all">
	<? if(!isset($DATA['PACKS']['error'])) { ?>
	<table style="width: 100%;" class="table-striped" border="0">
        <tr>
            <td></td>
            <td style="font-weight:bold;text-align:left;">Name</td>
            <td style="font-weight:bold;text-align:left;">Description</td>
            <td style="font-weight:bold;text-align:left;">Active?</td>
            <td style="font-weight:bold;text-align:center;width:110px;">Remove?</td>
            <td style="font-weight:bold;text-align:center;width:88px;">Toolbox</td>
        </tr>
        <? for($i=0; isset($DATA['PACKS'][$i]['Id']); $i++) { ?>
	<? $packImage = $ADMINcfg->AZURE_BLOB_ADDR.'/packimages/mobile/'.strtolower($DATA['PACKS'][$i]['Id']).'.png'; ?>
	<? if($DATA['PACKS'][$i]['Active'] == 1) { $Active = 'tick'; } else { $Active = 'cross'; } ?>
        <tr>
			<td><img src="<?=$packImage;?>" title="" alt="" width="75" height="50"/></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACKS'][$i]['Name']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['PACKS'][$i]['Description']);?></td>
            <td style="text-align:center"><img src="/z/img/static/<?=$Active;?>.png" title="" alt=""/></td>
            <td style="text-align:center">
                <input id="removePack" name="removePack[]" type="checkbox" value="<?=$DATA['PACKS'][$i]['Id'];?>" />
            </td>
            <td style="text-align:center">
				<a href="/p/packs/search.php?search_term=<?=urlencode($DATA['PACKS'][$i]['Name']);?>" title="Search Name"><img src="/z/img/static/magnifier.png" title="Search Name" alt="Search Name"/></a>&nbsp;
                <a href="/p/packs/view-pack.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="View Pack"><img src="/z/img/static/p_view.png" title="View Pack" alt="View Pack"/></a>&nbsp
                <? if($adminEdit) { ?><a href="/p/packs/edit-pack.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="Edit Pack"><img src="/z/img/static/p_edit.png" title="Edit Pack" alt="Edit Pack"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/packs/delete-pack.php?Id=<?=$DATA['PACKS'][$i]['Id'];?>" title="Delete Pack"><img src="/z/img/static/p_delete.png" title="Delete Pack" alt="Delete Pack"/></a><? } ?>
            </td>
        </tr>
        <? } ?>
    </table>
<? } else { ?>
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No packs found for this theme.</td>
	</tr>
	</table>
<? } ?>
</div>
	<div class="ui-widget-content ui-corner-all" style="margin:2px 0px 2px 0px;">
	<table style="width: 100%;">
	<tr>
		  <td style="font-weight:bold;font-size:10pt;"><?=$numMissingPacks;?>Additional Packs Available</td>
	</tr>
	</table>
</div>
<div class="ui-widget-content ui-corner-all">
	<? if(!isset($DATA['MISSING']['error'])) { ?>
	<table style="width: 100%;" class="table-striped" border="0">
        <tr>
            <td></td>
            <td style="font-weight:bold;text-align:left;">Name</td>
            <td style="font-weight:bold;text-align:left;">Description</td>
            <td style="font-weight:bold;text-align:left;">Active?</td>
            <td style="font-weight:bold;text-align:center;width:110px;">Add?</td>
            <td style="font-weight:bold;text-align:center;width:88px;">Toolbox</td>
        </tr>
        <? for($i=0; isset($DATA['MISSING'][$i]['Id']); $i++) { ?>
	<? $packImage = $ADMINcfg->AZURE_BLOB_ADDR.'/packimages/mobile/'.strtolower($DATA['MISSING'][$i]['Id']).'.png'; ?>
	<? if($DATA['MISSING'][$i]['Active'] == 1) { $Active = 'tick'; } else { $Active = 'cross'; } ?>
        <tr>
			<td><img src="<?=$packImage;?>" title="" alt="" width="75" height="50"/></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['MISSING'][$i]['Name']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['MISSING'][$i]['Description']);?></td>
            <td style="text-align:center"><img src="/z/img/static/<?=$Active;?>.png" title="" alt=""/></td>
            <td style="text-align:center">
                <? if($DATA['MISSING'][$i]['Active'] == 1) { ?><input id="addPack" name="addPack[]" type="checkbox" value="<?=$DATA['MISSING'][$i]['Id'];?>" /><? } ?>
            </td>
            <td style="text-align:center">
				<a href="/p/packs/search.php?search_term=<?=urlencode($DATA['MISSING'][$i]['Name']);?>" title="Search Name"><img src="/z/img/static/magnifier.png" title="Search Name" alt="Search Name"/></a>&nbsp;
                <a href="/p/packs/view-pack.php?Id=<?=$DATA['MISSING'][$i]['Id'];?>" title="View Pack"><img src="/z/img/static/p_view.png" title="View Pack" alt="View Pack"/></a>&nbsp
                <? if($adminEdit) { ?><a href="/p/packs/edit-pack.php?Id=<?=$DATA['MISSING'][$i]['Id'];?>" title="Edit Pack"><img src="/z/img/static/p_edit.png" title="Edit Pack" alt="Edit Pack"/></a>&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/packs/delete-pack.php?Id=<?=$DATA['MISSING'][$i]['Id'];?>" title="Delete Pack"><img src="/z/img/static/p_delete.png" title="Delete Pack" alt="Delete Pack"/></a><? } ?>
            </td>
        </tr>
        <? } ?>
    </table>
	<table style="width:100%">
		<tr>
			<td style="text-align:right;">
                <input id="saveEditTheme" name="saveEditTheme" type="submit" value="SAVE ALL CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetEditTheme" name="resetEditTheme" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
			</td>
		</tr>
	</table>
<? } else { ?>
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No packs found.</td>
	</tr>
	</table>
<? } ?>
</div>
</form>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
	<table style="width: 100%; padding:20px;">
	<tr>
		<td>No theme found.</td>
	</tr>
	</table>
</div>
<? } ?>
<?
}
?>