<?
function renderTrackAddDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
    
    if(is_array($_SESSION['_TRACK_ADD_VARS_']))
    {
        $Artist = $_SESSION['_TRACK_ADD_VARS_']['Artist'];
        $Title = $_SESSION['_TRACK_ADD_VARS_']['Title'];
        $PurchaseURLiTunes = $_SESSION['_TRACK_ADD_VARS_']['PurchaseURLiTunes'];
        $PurchaseURLAmazon = $_SESSION['_TRACK_ADD_VARS_']['PurchaseURLAmazon'];
        $PurchaseURLGooglePlay = $_SESSION['_TRACK_ADD_VARS_']['PurchaseURLGooglePlay'];
        
        if(isset($_SESSION['_TRACK_ADD_VARS_']['Playable']))
        {
            $playable = ' checked';
        }
    }    
?>
<? if($_GET['err'] == 'None') { ?>
<div class="ui-widget-content ui-corner-all track-edit-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Track has been added!</td>
        </tr>
    </table>
</div>  
<? } ?>
<div class="ui-widget-content ui-corner-all" style="margin:8px 0px 2px 0px;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:10pt;">Add Track Information</td>
        </tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all">
<? 
        $cross = '<img src="/z/img/static/cross.png" title="" alt=""/>';
        $playMP3 = $cross;
        $playOGG = $cross;
        $playMP4 = $cross;
        $GotThumb50 = $cross;

?>
<form action="/p/__process__/__track-add.php" enctype="multipart/form-data" method="post" id="trackAddForm" name="trackAddForm">
    <table style="width: 100%;" class="table-striped">
        <tr>
            <td class="td-track-edit">Artist</td>
            <td colspan="3"><input class="required input-box-track-edit ui-corner-all" id="Artist" name="Artist" type="text" value="<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$Artist);?>"/></td>
        </tr>
        <tr>
            <td class="td-track-edit">Title</td>
            <td colspan="3"><input class="required input-box-track-edit ui-corner-all" id="Title" name="Title" type="text" value="<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$Title);?>"/></td>
        </tr>
        <tr>
            <td class="td-track-edit"><?=$_SESSION['_TRACK_ADD_VARS_']['err']['JPG_MIME'];?>Thumbnail</td>
            <td style="text-align:center"><?=$GotThumb50;?></td>
            <td colspan="2"><input class="required input-box-file ui-corner-all" id="content_asset[thumb]" name="content_asset[thumb]" type="file" /></td>
        </tr>
        <!--<tr>
            <td class="td-track-edit">Thumbnail From URL</td>
            <td colspan="3"><input class="input-box-track-edit ui-corner-all" id="CoverURL" name="CoverURL" type="text" /></td>
        </tr>-->
        <tr>
            <td class="td-track-edit">iTunes Purchase URL</td>
            <td colspan="3"><input class="input-box-track-edit ui-corner-all" id="PurchaseURLiTunes" name="PurchaseURLiTunes" type="text" value="<?=$PurchaseURLiTunes;?>" /></td>
        </tr>
        <tr>
            <td class="td-track-edit">Amazon Purchase URL</td>
            <td colspan="3"><input class="input-box-track-edit ui-corner-all" id="PurchaseURLAmazon" name="PurchaseURLAmazon" type="text" value="<?=$PurchaseURLAmazon;?>" /></td>
        </tr>
        <tr>
            <td class="td-track-edit">Google Purchase URL</td>
            <td colspan="3"><input class="input-box-track-edit ui-corner-all" id="PurchaseURLGooglePlay" name="PurchaseURLGooglePlay" type="text" value="<?=$PurchaseURLGooglePlay;?>" /></td>
        </tr>
        <tr>
            <td class="td-track-edit">Track is Playable?</td>
            <td colspan="3"><input id="Playable" name="Playable" type="checkbox" <?=$playable;?> /></td>
        </tr>
        <tr>
            <td class="td-track-edit"><?=$_SESSION['_TRACK_ADD_VARS_']['err']['MP3_MIME'];?>MP3 File</td>
            <td style="text-align:center"><?=$playMP3;?></td>
            <td style="text-align:left;width:250px"><input class="input-box-file ui-corner-all" id="content_asset[mp3]" name="content_asset[mp3]" type="file" /></td>
            <td class="td-track-edit" style="text-align:left;"><input id="CreateOGG" name="CreateOGG" type="checkbox" />&nbsp;Create OGG from MP3?</td>
        </tr>
        <tr>
            <td class="td-track-edit"><?=$_SESSION['_TRACK_ADD_VARS_']['err']['OGG_MIME'];?>OGG File</td>
            <td style="text-align:center"><?=$playOGG;?></td>
            <td style="text-align:left;width:250px" colspan="2"><span class="ogg"><input class="input-box-file ui-corner-all" id="content_asset[ogg]" name="content_asset[ogg]" type="file" /></span></td>
        </tr>
        <tr>
            <td class="td-track-edit"><?=$_SESSION['_TRACK_ADD_VARS_']['err']['MP4_MIME'];?>MP4 File</td>
            <td style="text-align:center"><?=$playMP4;?></td>
            <td style="text-align:left;width:250px" colspan="2"><input class="input-box-file ui-corner-all" id="content_asset[mp4]" name="content_asset[mp4]" type="file" /></td>
        </tr>
        <tr>
            <td colspan="4" class="td-track-edit">
                <input id="saveAddTrack" name="saveAddTrack" type="submit" value="SAVE CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetAddTrack" name="resetAddTrack" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
            </td>
        </tr>
    </table>
</form>
</div>
<?
    unset($_SESSION['_TRACK_ADD_VARS_']);
}
?>