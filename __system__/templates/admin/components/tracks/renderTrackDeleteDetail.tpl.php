<?
function renderTrackDeleteDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
    
?>
<? if(!isset($DATA['error'])) { ?>
    <? if($DATA['TRACK']['Playable'] == 1) { $playable = 'tick'; } else { $playable = 'cross'; } ?>
    <? 
       $trackId = strtolower($DATA['TRACK']['Id']);
       $cross = '<img src="/z/img/static/cross.png" title="" alt=""/>';
       
       if($DATA['TRACK']['GotMP3'] == 1) 
       {
           $playMP3 = '<ul class="graphic"><li><a href="'.$ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.$trackId.'.mp3"></a></li></ul>';
       } else {
           $playMP3 = $cross;
       }
        
       if($DATA['TRACK']['GotOGG'] == 1) 
       { 
           $playOGG = '<ul class="graphic"><li><a href="'.$ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.$trackId.'.ogg"></a></li></ul>';           
       } else { 
           $playOGG = $cross;
       }
        
       if($DATA['TRACK']['GotMP4'] == 1) 
       { 
           $playMP4 = '<ul class="graphic"><li><a href="'.$ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.$trackId.'.mp4"></a></li></ul>';
       } else { 
           $playMP4 = $cross;
       }
       
       if($DATA['TRACK']['GotThumb50'] == 1)
       {
           $GotThumb50 = $ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.$trackId.'_50.jpg';
           
           // Is this file in the blob?
           $fileHeaders = @get_headers($GotThumb50);
           if($fileHeaders[0] == 'HTTP/1.1 404 The specified blob does not exist.')
           {
               $GotThumb50 = '/z/img/static/cross.png';
           }
       } else {
           $GotThumb50 = '/z/img/static/cross.png'; 
       }
       
    ?>
<div class="ui-widget-content ui-corner-all" style="margin-bottom:2px;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:10pt;">Delete Track Information</td>
        </tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all">
    <table style="width: 100%;" class="table-striped">
        <tr>
            <td style="font-weight:bold;text-align:center"></td>
            <td style="font-weight:bold;">Created</td>
            <td style="font-weight:bold;">Artist</td>
            <td style="font-weight:bold;">Title</td>
            <td style="font-weight:bold;text-align:center">Playable</td>
            <td style="font-weight:bold;text-align:center">MP3</td>
            <td style="font-weight:bold;text-align:center">OGG</td>
            <td style="font-weight:bold;text-align:center">MP4</td>
            <td style="font-weight:bold;text-align:center">Purchase</td>
        </tr>
        <tr>
            <td style="text-align:center;width:55px;border-left:1px solid #ededed;"><img src="<?=$GotThumb50;?>" title="" alt="" width="30" height="30"/></td>
            <td style="border-left:1px solid #ededed;"><?=$DATA['TRACK']['Created'];?></td>
            <td style="border-left:1px solid #ededed;"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['TRACK']['Artist']);?></td>
            <td style="border-left:1px solid #ededed;"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['TRACK']['Title']);?></td>
            <td style="text-align:center;border-left:1px solid #ededed;"><img src="/z/img/static/<?=$playable;?>.png" title="" alt=""/></td>
            <td style="text-align:center;border-left:1px solid #ededed;"><?=$playMP3;?></td>
            <td style="text-align:center;border-left:1px solid #ededed;"><?=$playOGG;?></td>
            <td style="text-align:center;border-left:1px solid #ededed;"><?=$playMP4;?></td>
            <td style="text-align:center;border-left:1px solid #ededed;">
                <a href="<?=$DATA['TRACK']['PurchaseURLiTunes'];?>" target="_blank"><img src="/z/img/static/itunes-icon_128x128.png" title="iTunes Purchase" alt="iTunes Purchase" width="18" height="18"/></a>&nbsp;&nbsp;
                <a href="<?=$DATA['TRACK']['PurchaseURLAmazon'];?>" target="_blank"><img src="/z/img/static/amazon-icon_128x128.png" title="Amazon Purchase" alt="Amazon Purchase" width="18" height="18"/></a>&nbsp;&nbsp;
                <a href="<?=$DATA['TRACK']['PurchaseURLGooglePlay'];?>" target="_blank"><img src="/z/img/static/googleplay-icon_128x128.png" title="Google Play Purchase" alt="Google Play Purchase" width="18" height="18"/></a>
            </td>
       </tr>
       <tr>
           <td colspan="9" style="text-align:right;padding:5px 2px 5px 2px;">
               <form action="/p/__process__/__track-delete.php" method="post" id="trackDelete" name="trackDelete">
                   <input type="hidden" value="<?=$DATA['TRACK']['Id'];?>" name="Id" id="Id" />
                   <input id="saveDeleteTrack" name="saveDeleteTrack" type="submit" value="DELETE TRACK" class="buttonNegative ui-corner-all" />&nbsp;
               </form>

           </td>
       </tr>

    </table>
</div>
<div class="ui-widget-content ui-corner-all" style="margin:2px 0px 2px 0px;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:10pt;">Question Information</td>
        </tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['QUESTION']['error'])) { ?>
    <table style="width: 100%;" class="table-striped" border="0">
        <tr>
            <td style="font-weight:bold;">Created</td>
            <td colspan="2" style="font-weight:bold;">Correct Track</td>
            <td colspan="2" style="font-weight:bold;">Wrong Track 1</td>
            <td colspan="2" style="font-weight:bold;">Wrong Track 2</td>
            <td colspan="2" style="font-weight:bold;text-align:center">Toolbox</td>
        </tr>
        <? for($i=0; isset($DATA['QUESTION'][$i]['Id']); $i++) { ?>
        <tr>
            <td><?=$DATA['QUESTION'][$i]['Created'];?></td>
            <td style="width:10px"><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['CorrectId'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a></td>
            <td><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['CorrectId'];?>" title="View Track"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION'][$i]['CorrectTitle']);?></a></td>
            <td style="width:10px"><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['WrongId1'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a></td>
            <td><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['WrongId1'];?>" title="View Track"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION'][$i]['WrongTitle1']);?></a></td>
            <td style="width:10px"><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['WrongId2'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a></td>
            <td><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['WrongId2'];?>" title="View Track"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION'][$i]['WrongTitle2']);?></a></td>
            <td style="text-align:center">
                <a href="/p/question/view-question.php?Id=<?=$DATA['QUESTION'][$i]['Id'];?>" title="View Question"><img src="/z/img/static/p_view.png" title="View Question" alt="View Question"/></a>&nbsp;&nbsp;
                <? if($adminEdit) { ?><a href="/p/tracks/edit-question.php?Id=<?=$DATA['QUESTION'][$i]['Id'];?>" title="Edit Question"><img src="/z/img/static/p_edit.png" title="Edit Question" alt="Edit Question"/></a>&nbsp;&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/tracks/delete-question.php?Id=<?=$DATA['QUESTION'][$i]['Id'];?>" title="Delete Question"><img src="/z/img/static/p_delete.png" title="Delete Question" alt="Delete Question"/></a><? } ?>
            </td>
        </tr>
        <? } ?>
    </table>
<? } else { ?>
    <table style="width: 100%; padding:20px;">
        <tr>
             <td>No questions found for this track.</td>
        </tr>
    </table>
<? } ?>
</div>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
    <table style="width: 100%; padding:20px;">
        <tr>
             <td>No track found.</td>
        </tr>
    </table>
</div>
<? } ?>

<?
}
?>