<?
function renderTrackEditDetail($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	
    //$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
    
    if(is_array($_SESSION['_TRACK_EDIT_VARS_']))
    {
        $DATA['TRACK']['Artist'] = $_SESSION['_TRACK_EDIT_VARS_']['Artist'];
        $DATA['TRACK']['Title'] = $_SESSION['_TRACK_EDIT_VARS_']['Title'];
        $DATA['TRACK']['PurchaseURLiTunes'] = $_SESSION['_TRACK_EDIT_VARS_']['PurchaseURLiTunes'];
        $DATA['TRACK']['PurchaseURLAmazon'] = $_SESSION['_TRACK_EDIT_VARS_']['PurchaseURLAmazon'];
        $DATA['TRACK']['PurchaseURLGooglePlay'] = $_SESSION['_TRACK_EDIT_VARS_']['PurchaseURLGooglePlay'];
        $DATA['TRACK']['Playable'] = $_SESSION['_TRACK_EDIT_VARS_']['Playable'];
    }    
?>
<? if($_GET['err'] == 'None') { ?>
<div class="ui-widget-content ui-corner-all track-edit-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Track has been updated!</td>
        </tr>
    </table>
</div>  
<? } ?>
<div class="ui-widget-content ui-corner-all" style="margin:8px 0px 2px 0px;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:10pt;">Edit Track Information</td>
        </tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['error'])) { ?>
<? if($DATA['TRACK']['Playable']) { $playable = ' checked'; } else { $playable = ''; } ?>
<? 
    $trackId = strtolower($DATA['TRACK']['Id']);
    $requiredThumb = '';
    if($DATA['TRACK']['GotThumb50'] == 1)
    {
        $GotThumb50 = $ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.$trackId.'_50.jpg';
        $formGotThumb50 = '1';
        
        // Is this file in the blob?
        $fileHeaders = @get_headers($GotThumb50);
        if($fileHeaders[0] == 'HTTP/1.1 404 The specified blob does not exist.')
        {
            $GotThumb50 = '/z/img/static/cross.png';
            $requiredThumb = 'required ';
            $formGotThumb50 = '0';
        }
    } else {
        $GotThumb50 = '/z/img/static/cross.png';
        $formGotThumb50 = '0';
    } 

    $cross = '<img src="/z/img/static/cross.png" title="" alt=""/>';
       
    if($DATA['TRACK']['GotMP3'] == 1) 
    {
        $playMP3 = '<ul class="graphic"><li><a href="'.$ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.$trackId.'.mp3"></a></li></ul>';
        $formGotMP3 = '1';
    } else {
        $playMP3 = $cross;
        $formGotMP3 = '0';
    }
        
    if($DATA['TRACK']['GotOGG'] == 1) 
    { 
        $playOGG = '<ul class="graphic"><li><a href="'.$ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.$trackId.'.ogg"></a></li></ul>';
        $formGotOGG = '1';
    } else { 
        $playOGG = $cross;
        $formGotOGG = '0';
    }
        
    if($DATA['TRACK']['GotMP4'] == 1) 
    { 
        $playMP4 = '<ul class="graphic"><li><a href="'.$ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.$trackId.'.mp4"></a></li></ul>';
        $formGotMP4 = '1';
    } else { 
        $playMP4 = $cross;
        $formGotMP4 = '0';
    } 
?>
<form action="/p/__process__/__track-edit.php" enctype="multipart/form-data" method="post" id="trackEditForm" name="trackEditForm">
<input id="Id" name="Id" value="<?=$DATA['TRACK']['Id'];?>" type="hidden" />
<input id="GotThumb50" name="GotThumb50" value="<?=$formGotThumb50;?>" type="hidden" />
<input id="GotMP3" name="GotMP3" value="<?=$formGotMP3;?>" type="hidden" />
<input id="GotOGG" name="GotOGG" value="<?=$formGotOGG;?>" type="hidden" />
<input id="GotMP4" name="GotMP4" value="<?=$formGotMP4;?>" type="hidden" />
    <table style="width: 100%;" class="table-striped">
        <tr>
            <td class="td-track-edit">Artist</td>
            <td colspan="3"><input class="required input-box-track-edit ui-corner-all" id="Artist" name="Artist" type="text" value="<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['TRACK']['Artist']);?>"/></td>
        </tr>
        <tr>
            <td class="td-track-edit">Title</td>
            <td colspan="3"><input class="required input-box-track-edit ui-corner-all" id="Title" name="Title" type="text" value="<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['TRACK']['Title']);?>"/></td>
        </tr>
        <tr>
            <td class="td-track-edit"><?=$_SESSION['_TRACK_EDIT_VARS_']['err']['JPG_MIME'];?>Thumbnail</td>
            <td style="text-align:center"><img src="<?=$GotThumb50;?>" title="" alt="" width="30" height="30"/></td>
            <td colspan="2"><input class="<?=$requiredThumb;?>input-box-file ui-corner-all" id="content_asset[thumb]" name="content_asset[thumb]" type="file" /></td>
        </tr>
        <!--<tr>
            <td class="td-track-edit">Thumbnail From URL</td>
            <td colspan="3"><input class="input-box-track-edit ui-corner-all" id="CoverURL" name="CoverURL" type="text" /></td>
        </tr>-->
        <tr>
            <td class="td-track-edit">iTunes Purchase URL</td>
            <td colspan="3"><input class="input-box-track-edit ui-corner-all" id="PurchaseURLiTunes" name="PurchaseURLiTunes" type="text" value="<?=$DATA['TRACK']['PurchaseURLiTunes'];?>" /></td>
        </tr>
        <tr>
            <td class="td-track-edit">Amazon Purchase URL</td>
            <td colspan="3"><input class="input-box-track-edit ui-corner-all" id="PurchaseURLAmazon" name="PurchaseURLAmazon" type="text" value="<?=$DATA['TRACK']['PurchaseURLAmazon'];?>" /></td>
        </tr>
        <tr>
            <td class="td-track-edit">Google Purchase URL</td>
            <td colspan="3"><input class="input-box-track-edit ui-corner-all" id="PurchaseURLGooglePlay" name="PurchaseURLGooglePlay" type="text" value="<?=$DATA['TRACK']['PurchaseURLGooglePlay'];?>" /></td>
        </tr>
        <tr>
            <td class="td-track-edit">Track is Playable?</td>
            <td colspan="3"><input id="Playable" name="Playable" type="checkbox" <?=$playable;?> /></td>
        </tr>
        <tr>
            <td class="td-track-edit"><?=$_SESSION['_TRACK_EDIT_VARS_']['err']['MP3_MIME'];?>MP3 File</td>
            <td style="text-align:center"><?=$playMP3;?></td>
            <td style="text-align:left;width:250px"><input class="input-box-file ui-corner-all" id="content_asset[mp3]" name="content_asset[mp3]" type="file" /></td>
            <td class="td-track-edit" style="text-align:left;"><input id="CreateOGG" name="CreateOGG" type="checkbox" />&nbsp;Create OGG from MP3?</td>
        </tr>
        <tr>
            <td class="td-track-edit"><?=$_SESSION['_TRACK_EDIT_VARS_']['err']['OGG_MIME'];?>OGG File</td>
            <td style="text-align:center"><?=$playOGG;?></td>
            <td style="text-align:left;width:250px" colspan="2"><span class="ogg"><input class="input-box-file ui-corner-all" id="content_asset[ogg]" name="content_asset[ogg]" type="file" /></span></td>
        </tr>
        <tr>
            <td class="td-track-edit"><?=$_SESSION['_TRACK_EDIT_VARS_']['err']['MP4_MIME'];?>MP4 File</td>
            <td style="text-align:center"><?=$playMP4;?></td>
            <td style="text-align:left;width:250px" colspan="2"><input class="input-box-file ui-corner-all" id="content_asset[mp4]" name="content_asset[mp4]" type="file" /></td>
        </tr>
        <tr>
            <td colspan="4" class="td-track-edit">
                <input id="saveEditTrack" name="saveEditTrack" type="submit" value="SAVE CHANGES" class="buttonPositive ui-corner-all" />&nbsp;
                <input id="resetEditTrack" name="resetEditTrack" type="reset" value="RESET" class="buttonBlue ui-corner-all" />
            </td>
        </tr>
    </table>
</form>
</div>
<div class="ui-widget-content ui-corner-all" style="margin:6px 0px 2px 0px;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:10pt;">Question Information</td>
        </tr>
    </table>
</div>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['QUESTION']['error'])) { ?>
    <table style="width: 100%;" class="table-striped" border="0">
        <tr>
            <td style="font-weight:bold;">Created</td>
            <td colspan="2" style="font-weight:bold;">Correct Track</td>
            <td colspan="2" style="font-weight:bold;">Wrong Track 1</td>
            <td colspan="2" style="font-weight:bold;">Wrong Track 2</td>
            <td colspan="2" style="font-weight:bold;text-align:center">Toolbox</td>
        </tr>
        <? for($i=0; isset($DATA['QUESTION'][$i]['Id']); $i++) { ?>
        <tr>
            <td><?=$DATA['QUESTION'][$i]['Created'];?></td>
            <td style="width:10px"><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['CorrectId'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a></td>
            <td><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['CorrectId'];?>" title="View Track"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION'][$i]['CorrectTitle']);?></a></td>
            <td style="width:10px"><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['WrongId1'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a></td>
            <td><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['WrongId1'];?>" title="View Track"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION'][$i]['WrongTitle1']);?></a></td>
            <td style="width:10px"><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['WrongId2'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a></td>
            <td><a href="/p/tracks/view-track.php?Id=<?=$DATA['QUESTION'][$i]['WrongId2'];?>" title="View Track"><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA['QUESTION'][$i]['WrongTitle2']);?></a></td>
            <td style="text-align:center">
                <a href="/p/questions/view-question.php?Id=<?=$DATA['QUESTION'][$i]['Id'];?>" title="View Question"><img src="/z/img/static/p_view.png" title="View Question" alt="View Question"/></a>&nbsp;&nbsp;
                <? if($adminEdit) { ?><a href="/p/questions/edit-question.php?Id=<?=$DATA['QUESTION'][$i]['Id'];?>" title="Edit Question"><img src="/z/img/static/p_edit.png" title="Edit Question" alt="Edit Question"/></a>&nbsp;&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/questions/delete-question.php?Id=<?=$DATA['QUESTION'][$i]['Id'];?>" title="Delete Question"><img src="/z/img/static/p_delete.png" title="Delete Question" alt="Delete Question"/></a><? } ?>
            </td>
        </tr>
        <? } ?>
    </table>
<? } else { ?>
    <table style="width: 100%; padding:20px;">
        <tr>
             <td>No questions found for this track.</td>
        </tr>
    </table>
<? } ?>
</div>
<? } else { ?>
<div class="ui-widget-content ui-corner-all">
    <table style="width: 100%; padding:20px;">
        <tr>
             <td>No track found.</td>
        </tr>
    </table>
</div>
<? } ?>

<?
    unset($_SESSION['_TRACK_EDIT_VARS_']);
}
?>