<?
function renderTracks($DATA)
{
	global $ADMINcfg, $SITEsession, $STDlib;
	//$STDlib->varDump($DATA);
    $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
    $adminDelete = $_SESSION['_LOGIN_']['admin_delete'];
?>
<div class="ui-widget-content ui-corner-all">
<? if(!isset($DATA['error'])) { ?>
    <table style="width: 100%;" class="table-striped sortable">
        <tr>
            <th class="sorttable_nosort" style="font-weight:bold;font-size:larger;"></th>
            <th style="font-weight:bold;font-size:larger;">Created</th>
            <th style="font-weight:bold;font-size:larger;">Artist</th>
            <th style="font-weight:bold;font-size:larger;">Title</th>
            <th style="font-weight:bold;font-size:larger;text-align:center">Playable?</th>
            <th class="sorttable_nosort" style="font-weight:bold;font-size:larger;text-align:center">Toolbox</th>
        </tr>
<? for($i=0; isset($DATA[$i]['Id']); $i++) { ?>
<? if($DATA[$i]['Playable'] == 1) { $playable = 'tick'; $sorttable_customkey='1'; } else { $playable = 'cross'; $sorttable_customkey='0'; } ?>
<? $thumbnail = $ADMINcfg->AZURE_BLOB_ADDR.'/tracks/'.strtolower($DATA[$i]['Id']).'_50.jpg'; ?>
        <tr>
            <td style="text-align:center;width:55px;"><img src="<?=$thumbnail;?>" width="50" height="50"/></td>
            <td><?=$DATA[$i]['Created'];?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['Artist']);?></td>
            <td><?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$DATA[$i]['Title']);?></td>
            <td style="text-align:center" sorttable_customkey="<?=$sorttable_customkey;?>"><img src="/z/img/static/<?=$playable;?>.png" title="" alt=""/></td>
            <td style="text-align:center">
                <a href="/p/tracks/search.php?search_term=<?=urlencode($DATA[$i]['Artist']);?>" title="Search Artist"><img src="/z/img/static/magnifier.png" title="Search Artist" alt="Search Artist"/></a>&nbsp;&nbsp;
                <a href="/p/tracks/view-track.php?Id=<?=$DATA[$i]['Id'];?>" title="View Track"><img src="/z/img/static/p_view.png" title="View Track" alt="View Track"/></a>&nbsp;&nbsp;
                <? if($adminEdit) { ?><a href="/p/tracks/edit-track.php?Id=<?=$DATA[$i]['Id'];?>" title="Edit Track"><img src="/z/img/static/p_edit.png" title="Edit Track" alt="Edit Track"/></a>&nbsp;&nbsp;<? } ?>
                <? if($adminDelete) { ?><a href="/p/tracks/delete-track.php?Id=<?=$DATA[$i]['Id'];?>" title="Delete Track"><img src="/z/img/static/p_delete.png" title="Delete Track" alt="Delete Track"/></a><? } ?>
             </td>
        </tr>
<? } ?>
    </table>
<? } else { ?>
    <table style="width: 100%; padding:20px;">
        <tr>
             <td>No tracks found.</td>
        </tr>
    </table>
<? } ?>
</div>
<?
}
?>