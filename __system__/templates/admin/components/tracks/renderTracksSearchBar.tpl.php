<?

function renderTracksSearchBar($DATA)
{
		global $ADMINcfg, $SITEsession, $STDlib;
        
        $searchTerm = $DATA['extra']['search_term'];
        $byArtist = $DATA['extra']['by_artist'];
        $byTitle = $DATA['extra']['by_title'];
        
        $artistIsChecked = '';
        $titleIsChecked = '';
        
        if(!empty($byArtist))
        {
            $artistIsChecked = ' checked';
        }

        if(!empty($byTitle))
        {
            $titleIsChecked = ' checked';
        }
        
        if(empty($artistIsChecked) && empty($titleIsChecked))
        {
            $artistIsChecked = ' checked';
        }

		//$STDlib->varDump($DATA);
?>
<div class="info-bar-search ui-widget-content ui-corner-all">
    <form method="post" action="/p/tracks/search.php">
    <table style="width: 100%;" border="0">
        <tr>
            <td><input id="search_term" name="search_term" type="text" class="input-box-search" value="<?=$searchTerm;?>"/></td>
            <td><input id="Submit1" name="Submit1" type="submit" value="GO" class="buttonPositive ui-corner-all"/></td>
        </tr>
        <tr>
            <td style="text-align:right;" colspan="2">
                By Title:&nbsp;<input id="by_title" name="by_title" type="checkbox"<?=$titleIsChecked;?> />&nbsp;
                By Artist:&nbsp;<input id="by_artist" name="by_artist" type="checkbox"<?=$artistIsChecked;?> />&nbsp;&nbsp;
            </td>
        </tr>
    </table>            
	</form>
</div>
<?
}
?>