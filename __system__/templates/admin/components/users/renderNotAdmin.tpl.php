<?

	function renderNotAdmin($DATA)
	{
		global $ADMINcfg, $SITEsession, $STDlib;
?>
<div class="clear">&nbsp;</div>
<div class="ui-widget-content ui-corner-all" style="width:98%;left:8px;position:absolute;">
    <table style="width: 100%; padding:20px;">
        <tr>
             <td>You don't have sufficient privileges to access this section, please speak to an administrator.</td>
        </tr>
    </table>
</div>
<?
	}
?>