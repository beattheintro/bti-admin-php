<?
	function renderUserUpdateForm($DATA)
	{
		global $ADMINcfg, $STDlib, $SITEsession;

        $DATA['extra'] = $_SESSION['_ADMIN_USERS_']['extra'];
        
		if($DATA["u_password"])
		{
			$pw = $DATA["u_password"];
		} else {
			$pw = $DATA["extra"]["pw"];
		}

		if($DATA["u_name"])
		{
			$readonly = "readonly";
		}
		//var_dump($DATA);
?>
<form action="/p/__process__/__users.php" method="POST" name="useraddform" id="useraddform">
<input type="hidden" name="Id" value="<?=$DATA["Id"];?>">
<div class="ui-widget-content ui-corner-all">
<table style="width:100%" class="table-striped">
	<tr>
		<? if($DATA["update"] == TRUE) { ?>
		<td style="font-weight:bold;font-size:larger;width:60%;" colspan="2">Update User</td>
		<? } else { ?>
		<td style="font-weight:bold;font-size:larger;width:60%;" colspan="2">Add User</td>
		<? } ?>
		<td style="font-weight:bold;font-size:larger;" colspan="2">With Permissions</td>
	</tr>
	<tr>
	    <td>Username&nbsp;</td>
	    <td style="text-align:center"><input class="required ui-corner-all input-box-user" type="text" name="u_name" value="<?=$DATA["u_name"];?>" <?=$readonly;?>></td>
	    <td style="text-align:right">Administrator&nbsp;&nbsp;</td><? $checked = $STDlib->formCheckedArray($DATA["admin_1"], 1); ?>
	    <td style="text-align:center" >Yes&nbsp;<input type="radio" name="admin_1" value="1" <?=$checked["checked"]; ?>>&nbsp;No&nbsp;<input type="radio" name="admin_1" value="0" <?=$checked["notchecked"]; ?>></td>
    </tr>
    <tr>
	    <td>Password&nbsp;</td>
	    <td style="text-align:center"><input class="required ui-corner-all input-box-user" type="text" name="u_password" size="60" value="<?=$pw;?>"></td>
	    <td style="text-align:right">Add Data&nbsp;&nbsp;</td><? $checked = $STDlib->formCheckedArray($DATA["admin_new"], 1); ?>
	    <td style="text-align:center">Yes&nbsp;<input type="radio" name="admin_new" value="1"<?=$checked["checked"]; ?>>&nbsp;No&nbsp;<input type="radio" name="admin_new" value="0" <?=$checked["notchecked"]; ?>></td>
    </tr>
    <tr>
	    <td>First Name&nbsp;</td>
	    <td style="text-align:center"><input class="required ui-corner-all input-box-user" type="text" name="u_firstname" size="60" value="<?=$DATA["u_firstname"];?>"></td>
	    <td style="text-align:right">Edit Data&nbsp;&nbsp;</td><? $checked = $STDlib->formCheckedArray($DATA["admin_edit"], 1); ?>
	    <td style="text-align:center">Yes&nbsp;<input type="radio" name="admin_edit" value="1"<?=$checked["checked"]; ?>>&nbsp;No&nbsp;<input type="radio" name="admin_edit" value="0" <?=$checked["notchecked"]; ?>></td>
    </tr>
    <tr>
	    <td>Last Name&nbsp;</td>
	    <td style="text-align:center"><input class="required ui-corner-all input-box-user" type="text" name="u_lastname" size="60" value="<?=$DATA["u_lastname"];?>"></td>
	    <td style="text-align:right">Delete Data&nbsp;&nbsp;</td><? $checked = $STDlib->formCheckedArray($DATA["admin_delete"], 1); ?>
	    <td style="text-align:center">Yes&nbsp;<input type="radio" name="admin_delete" value="1"<?=$checked["checked"]; ?>>&nbsp;No&nbsp;<input type="radio" name="admin_delete" value="0" <?=$checked["notchecked"]; ?>></td>
    </tr>
    <? if($DATA["update"] == TRUE) { ?>
    <tr>
	    <td>Delete this user?&nbsp;</td>
	    <td style="text-align:left" colspan="2"><input type="checkbox" name="delete" value="1">&nbsp;<span class="warn">Caution, this cannot be undone!</span></td>
    </tr>
    <? } ?>
	<? if($DATA["update"] == TRUE) { ?>
	<tr>
		<td style="text-align:right" colspan="4"><input type="submit"  name="usersave" value="UPDATE USER" class="buttonPositive ui-corner-all" /></td>
	</tr>
	<? } else { ?>
	<tr>
		<td style="text-align:right" colspan="4"><input type="submit" name="useradd" value="ADD USER" class="buttonPositive ui-corner-all" /></td>
	</tr>
	<? } ?>
</table>
</div>
</form>
<?
	//var_dump($DATA);
	}
?>