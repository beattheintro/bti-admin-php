<?

	function checkOption($option)
	{
		if($option == 1)
		{
			$img = "icon_active_15x15.png";
			return $img;
		} else {
			$img = "icon_inactive_15x15.png";
			return $img;
		}
	}

	function renderUsers($DATA)
	{
		global $ADMINcfg, $STDlib, $SITEsession;
        
        if($_SESSION['_LOGIN_']['Id'] == 1)
        {
            $superAdmin = 1;
        } else {
            $superAdmin = 0;
        }
        
        $DATA['extra'] = $_SESSION['_ADMIN_USERS_']['extra'];
        
?>
<? if($DATA["extra"]["err"] == 1) { ?>
<div class="ui-widget-content ui-corner-all user-error-msg" style="margin:8px 0px 2px 0px; border:2px solid red;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:red;">Failed : Both a Username & Password are required!</td>
        </tr>
    </table>
</div>  
<? } elseif($DATA["extra"]["err"] == "exists") { ?>
<div class="ui-widget-content ui-corner-all user-error-msg" style="margin:8px 0px 2px 0px; border:2px solid red;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:red;">Failed : The username already exists in the database!</td>
        </tr>
    </table>
</div>  
<? } elseif(($DATA["extra"]["err"] == "0") && ($DATA["extra"]["user"] == "insert")) { ?>
<div class="ui-widget-content ui-corner-all user-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Success : User added!</td>
        </tr>
    </table>
</div>  
<? } elseif(($DATA["extra"]["err"] == "0") && ($DATA["extra"]["user"] == "update")) { ?>
<div class="ui-widget-content ui-corner-all user-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Success : User updated!</td>
        </tr>
    </table>
</div>  
<? } elseif(($DATA["extra"]["err"] == "0") && ($DATA["extra"]["user"] == "delete")) { ?>
<div class="ui-widget-content ui-corner-all user-error-msg" style="margin:8px 0px 2px 0px; border:2px solid green;">
    <table style="width: 100%;">
        <tr>
            <td style="font-weight:bold;font-size:15pt;text-align:center;color:green;">Success : User deleted!</td>
        </tr>
    </table>
</div>
<? } ?>
<div class="ui-widget-content ui-corner-all">
<table cellpadding="4" cellspacing="0" border="0" class="cat-table" width="100%">
<tr>
	<th align="right" class="clear-table" colspan="5">&nbsp;</th>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;" colspan="4">User Permissions</th>
	<th align="right" class="clear-table">&nbsp;</th>
</tr>
<tr>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">ID</th>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">Username</th>
	<? if($superAdmin == 1) { ?><th align="center" class="td-head" style="font-weight:bold;font-size:larger;">Password</th><? } ?>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">Firstname</th>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">Lastname</th>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">Administrator</th>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">Add Data</th>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">Edit Data</th>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">Delete Data</th>
	<th align="center" class="td-head" style="font-weight:bold;font-size:larger;">&nbsp;</th>
</tr>
<tr>
	<td align="center" colspan="10" class="clear-table">&nbsp;</td>
</tr>
<? for($i=0; isset($DATA[$i]['Id']); $i++) { ?>
<? if($DATA[$i]['Id'] == "1") { ?>
<form action="/p/__process__/users.php" method="POST" name="userform<?=$i;?>">
<input type="hidden" name="Id" value="<?=$DATA[$i]['Id'];?>">
<tr>
	<td align="center" class="td-data"><?=$DATA[$i]['Id'];?></td>
	<td align="center" class="td-data"><?=stripslashes($DATA[$i]['u_name']);?></td>
	<td align="center" class="td-data">******</td>
	<td align="center" class="td-data"><?=stripslashes($DATA[$i]['u_firstname']);?></td>
	<td align="center" class="td-data"><?=stripslashes($DATA[$i]['u_lastname']);?></td>
	<td align="center" class="td-data"><img src="/z/img/static/<?=checkOption($DATA[$i]['admin_1']);?>" alt="" border="0" /></td>
	<td align="center" class="td-data"><img src="/z/img/static/<?=checkOption($DATA[$i]['admin_new']);?>" alt="" border="0" /></td>
	<td align="center" class="td-data"><img src="/z/img/static/<?=checkOption($DATA[$i]['admin_edit']);?>" alt="" border="0" /></td>
	<td align="center" class="td-data"><img src="/z/img/static/<?=checkOption($DATA[$i]['admin_delete']);?>" alt="" border="0" /></td>
	<td align="center" class="td-data">&nbsp;</td>
</tr>
</form>
<? } else { ?>
<? 
    if($superAdmin == 1)
    {
        $mailtoLink = '<a href="mailto:'.stripslashes($DATA[$i]['u_name']).'?subject=Your%20BTI%20Admin%20Login&body=Hi%20'.$DATA[$i]['u_firstname'].'%2C%0A%0AHere%20are%20your%20login%20details%20for%20the%20Admin%20tool%3A%0A%0AServer%3A%20http%3A//bti-admin-php.azurewebsites.net/%0AUsername%3A%20'.$DATA[$i]['u_name'].'%0APassword%3A%20'.$DATA[$i]['u_password'].'%0A%0ACheers%2C%0A%0AJay">'.$DATA[$i]['u_name'].'</a>';
    } else {
        $mailtoLink = stripslashes($DATA[$i]['u_name']);
    }
?>
<form action="/p/__process__/__users.php" method="POST" name="userform<?=$i;?>">
<input type="hidden" name="Id" value="<?=$DATA[$i]['Id'];?>">
<tr>
	<td align="center" class="td-data"><?=$DATA[$i]['Id'];?></td>
	<td align="center" class="td-data"><a href="mailto:<?=stripslashes($DATA[$i]['u_name']);?>"><?=$mailtoLink;?></a></td>
	<? if($superAdmin == 1) { ?><td align="center" class="td-data"><?=stripslashes($DATA[$i]['u_password']);?></td><? } ?>
	<td align="center" class="td-data"><?=stripslashes($DATA[$i]['u_firstname']);?></td>
	<td align="center" class="td-data"><?=stripslashes($DATA[$i]['u_lastname']);?></td>
	<td align="center" class="td-data"><img src="/z/img/static/<?=checkOption($DATA[$i]['admin_1']);?>" alt="" border="0" /></td>
	<td align="center" class="td-data"><img src="/z/img/static/<?=checkOption($DATA[$i]['admin_new']);?>" alt="" border="0" /></td>
	<td align="center" class="td-data"><img src="/z/img/static/<?=checkOption($DATA[$i]['admin_edit']);?>" alt="" border="0" /></td>
	<td align="center" class="td-data"><img src="/z/img/static/<?=checkOption($DATA[$i]['admin_delete']);?>" alt="" border="0" /></td>
	<td align="center" class="td-data"><input type="submit" name="edit" value="EDIT" class="buttonPositive ui-corner-all" /></td>
</tr>
</form>
<? } ?>
<? } ?>
<? if($DATA["error"] == "No Data") { ?>
<tr>
	<td align="center" colspan="10" class="td-data"><span class="warn">No Users available.</span></td>
</tr>
<? } ?>
</table>
</div>
<div>&nbsp;</div>
<?
	}
?>