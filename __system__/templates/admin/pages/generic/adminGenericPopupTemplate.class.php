<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminGenericPopupTemplate.class.php
	// Author: Cactus Designs (UK) Ltd
	// Date: 23/04/2010
	// Version: 1.0
	// Description: Generic Popup Page Template for the Admin System
	//			 DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		 with out consent from the author.
	// Copyright (c) 2006, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

	Class adminGenericPopupTemplate {

	var $basic = "admin";
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;
	var $tinyMCE;
	var $cols = "1";
	var $Border = "0";
	var $siteCSS = "";
	var $bodyBGcolor = "";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $onload = "";

	// ----------------------------------------------------------- >>>>>>>>>>

	function adminGenericPopupTemplate()
	{
		//ini_set("display_errors", "1");
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = "ANM Platform : Web Manager";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showDocType()
	{
		// XML Declaration (Used for XHTML)
		echo '<?xml version="1.0" encoding="iso-8859-1"?>'."\n";

		// XHTML 1.0
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" '."\n".'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
		echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">'."\n";

	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showTitle()
	{
		$this->setPageInfo();

		echo '<title>ANM Platform : '.$this->docTitle.'</title>'."\n";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showMetaTags()
	{

		$this->setPageInfo();

		echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />'."\n";
		//echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'."\n";
		echo '<meta name="description" content="'.$this->metaDescription.'" />'."\n";
		echo '<meta name="keywords" content="'.$this->metaKeywords.'" />'."\n";
		echo '<meta name="author" content="Cactus Designs (UK) Ltd." />'."\n";
		echo '<meta name="generator" content="CACTUS(Visceral)" />'."\n";
		if(isset($this->metaRefresh) && ($this->metaRefresh != ""))
		{
			echo '<meta http-equiv="Refresh" content="'.$this->metaRefresh.'" />'."\n";
		}
		echo '<meta http-equiv="expires" content="mon , 01 jan 1990 00:00:01 gmt" />'."\n";
		echo '<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />'."\n";
		echo '<link rel="icon" href="/favicon.ico" type="image/x-icon" />'."\n";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showStyleSheets()
	{
		// External CSS Declarations go here.
		echo '<link rel="stylesheet" type="text/css" href="/z/css/'.$this->siteCSS.'?ttt='.time().'" />'."\n";
		echo '<link rel="stylesheet" type="text/css" href="/z/themes/base/ui.all.css?ttt='.time().'" />'."\n";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCustomStyleSheets()
	{
		// Custom CSS Declarations go here.
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showJavaScript()
	{
		// External JavaScript Declarations go here.
		echo '<script language="JavaScript" type="text/javascript" src="/z/js/misc.js"></script>'."\n";
?>
<script language="Javascript1.2">
  <!--
	  function printpage() {
	  window.print();
  }
  //-->
</script>
<?
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCustomJavaScript()
	{
		// jQuery or other custom JS calls.

	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showHTMLDeclarations()
	{
		$this->showDocType();
		echo '<!-- '.date("F j, Y, G:i").' -->'."\n";
		echo '<head>'."\n";
		$this->showTitle();
		$this->showMetaTags();
		$this->showStyleSheets();
		$this->showJavaScript();
		$this->showCustomJavaScript();
		$this->showCustomStyleSheets();
		echo '</head>'."\n";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showBanner()
	{
		global $ADMINcfg, $SITEsession;
		// Banner goes here

		$uF =$_SESSION['_LOGIN_']["u_firstname"];
		$uL = $_SESSION['_LOGIN_']["u_lastname"];
		$uN = $_SESSION['_LOGIN_']["u_name"];
		$u = $uF." ".$uL;

		if(empty($uN))
		{
			$msg = "You are not logged in!";
			$u = "None";
		} else {
			$msg = "You are logged in as: ".$u." [".$uN."]";
		}

?>
	<div>
	<table cellpadding="2" cellspacing="2" border="0" width="100%" style="background-color:black;height:56px;-moz-border-radius-topleft: 10px; -webkit-border-top-left-radius: 10px; -moz-border-radius-topright: 10px; -webkit-border-top-right-radius: 10px;border:1px solid black;">
		<tr>
			<td align="left" style="background:url(/z/img/static/anm-logo-bg-750x56.jpg) top center repeat-x #000;color:white;" valign="bottom"></td>
		</tr>
	</table>
	</div>
	<div>
	<table cellpadding="5" cellspacing="0" border="0" class="logotbl" width="100%">
		<tr>
			<td align="right" valign="bottom"><span class="cmsTitle"><?=$this->pageTitle;?></span></td>
		</tr>
	</table>
	</div>
<?
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showTopNavigation()
	{
		global $ADMINcfg, $SITEsession;
		// Navigation goes here

		//include_once $ADMINcfg->SYSTEM_STATIC_PATH."/topnav/topnav.frag.php";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showTopSubNavigation()
	{
		global $ADMINcfg, $SITEsession;
		// Top Sub Nav goes here

		//echo '<div class="subnav">'."\n";
		//include_once $ADMINcfg->SYSTEM_STATIC_PATH."/subnav/subnav.frag.php";
		//echo '</div>'."\n";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showLeftCol()
	{
		// Left Col Content goes here
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		// Center Col Content goes here
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showRightCol()
	{
		// Right Col Content goes here
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function doDebug()
	{
		// Init debug
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showFooter($t )
	{
		global $ADMINcfg, $SITEsession;

		$currentDate = date("j F, Y, G:i A");
		$year = date("Y");

		echo '<div id="footer-popup">';
		echo '<center>ANM Platform Tools (v'.$ADMINcfg->SYSTEM_VERSION.') <b>::</b> Build Time = '.$t.' secs</center>';
		echo '</div>'."\n";

	}

	// ----------------------------------------------------------- >>>>>>>>>>
	// BUILD THE PAGE
	// ----------------------------------------------------------- >>>>>>>>>>

	function makePage()
	{

		global $ADMINcfg, $STDlib;

		$t1 = $STDlib->mt(); // Start time

		$this->showHTMLDeclarations();

		echo '<body class="'.$this->basic.'" '.$this->onload.'>'."\n";

		echo '<div id="header-popup">'."\n";
		$this->showBanner();
		echo '</div>'."\n";
		$this->showTopNavigation();
		$this->showTopSubNavigation();

		if($this->cols == 1)
		{
			echo '<div id="centercontent-pupup">'."\n";
			$this->showCenterCol();
			echo '</div>'."\n";
		} elseif ($this->cols == 2) {
			echo '<div id="leftcontent">'."\n";
			$this->showLeftCol();
			echo '</div>'."\n";
			echo '<div id="rightcontent">'."\n";
			$this->showRightCol();
			echo '</div>'."\n";
		} elseif ($this->cols == 3) {
			echo '<div id="leftcontent">'."\n";
			$this->showLeftCol();
			echo '</div>'."\n";
			echo '<div id="centercontent">'."\n";
			$this->showCenterCol();
			echo '</div>'."\n";
			echo '<div id="rightcontent">'."\n";
			$this->showRightCol();
			echo '</div>'."\n";
		}

		$t2 = $STDlib->mt(); // End time

		$t = substr(($t2-$t1), 0, 8); // Time taken to build page

		$this->showFooter($t);

		// Trun off in ADMINConfig.class.php, set GLOBAL_DEBUG to FALSE.
		$STDlib->showDeBugData(1, 1, "", "");

		echo '</body>'."\n";
		echo '</html>'."\n";
	}

} // Close class
?>