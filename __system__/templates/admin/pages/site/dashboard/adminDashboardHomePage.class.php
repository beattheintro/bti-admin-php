<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminDashboardHomePage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Dashboard page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminDashboardHomePage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCustomStyleSheets()
	{
?>
	<link rel="stylesheet" type="text/css" href="/z/css/jquery.css?ttt=<?=time();?>" />
<?
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCustomJavaScript()
	{
?>
	<script type="text/javascript" src="/z/js/jquery/jquery-1.3.2.js"></script>
	<script type="text/javascript" src="/z/js/ui/ui.core.js"></script>
<?
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Dashboard';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Dashboard';
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		// Get the dashboard
?>
	<table cellpadding="2" cellspacing="2" border="0" width="100%">
		<tr>
			<td colspan="2"><h1>Welcome <?=$_SESSION['_LOGIN_']['u_firstname'];?>&nbsp;<?=$_SESSION['_LOGIN_']['u_lastname'];?></h1></td>
		<tr>
			<td valign="top" align="center"><? API_getDashboardData('dashboard/renderDashboard.tpl.php'); ?></td>
		</tr>
	</table>
<?
		//$STDlib->varDump($_POST);
		echo '</div>'."\n";

	} // End showLeftCol()

} // Close Class
?>