<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminPacksEditItemPage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Pack Edit page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminPacksEditItemPage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Edit Pack';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Edit Pack';
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
	function showCustomJavaScript()
	{
		// jQuery or other custom JS calls.
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/jquery-1.3.2.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/form.validation.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/ui/ui.core.js"></script>'."\n";
?>
	<script type="text/javascript">
	$(document).ready(function(){
	    $("#packEditForm").validate();
	});

	$(window).load(function () {
	    setTimeout(function () { $('.pack-edit-error-msg').fadeOut() }, 5000);
	});

	$(function () {
		cbChk();
		$("#IsFreeToPlay").click(cbChk);
	});

	function cbChk() {
		if (this.checked) {
			$("#CostToUnlock").attr("disabled", true);
			$("#CostToUnlock").val('0');
		} else {
			$("#CostToUnlock").removeAttr("disabled");
			$("#CostToUnlock").val('20000');
		}
	}
</script>

<?
	}
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_packsInfoBarData('packs/renderPacksInfoBar.tpl.php');
        
        $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
        
        if($adminEdit == 0)
        {
            $DATA = 'edit this';
            API_viewAnyMarkup('general/renderNoPrivileges.tpl.php', $DATA);
        } else {
            API_getPackEditData('packs/renderPackEditDetail.tpl.php');
			//API_getAllPacksData('packs/renderAllPacks.tpl.php');
        }

		echo '</div>'."\n";
        
		//unset($_SESSION['PACKS']);
		unset($_SESSION['_PACKS_EDIT_VARS_']);

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>