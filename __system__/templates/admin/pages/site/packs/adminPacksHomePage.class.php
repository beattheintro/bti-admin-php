<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminPacksHomePage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Packs Home page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminPacksHomePage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Pack Tools';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Pack Tools';
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_packsInfoBarData('packs/renderPacksInfoBar.tpl.php');
        
        if(isset($_GET['page']) && ($_GET['page'] > 1))
        {
           $thisPage = $_GET['page'];
           $startAt = ($thisPage-1)*$ADMINcfg->MAX_DB_RESULTS;
        } elseif($_GET['page'] == 1) {
            $startAt = 0;
            $thisPage = 1;
        } else {
            $startAt = 0;
            $thisPage = 1;
        }
        
        $paginationData['thisPage'] = $thisPage;
        
        API_viewAnyMarkup('packs/renderPacksPagination.tpl.php', $paginationData);
        API_getPacks('packs/renderPacks.tpl.php', $startAt);
        API_viewAnyMarkup('packs/renderPacksPagination.tpl.php', $paginationData);
		//$STDlib->varDump($_POST);
		echo '</div>'."\n";
        
        unset($_SESSION['PACKS']);
		unset($_SESSION['_PACKS_EDIT_VARS_']);

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>