<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminContentToolsPopupPage.class.php
	// Author: Cactus Designs (UK) Ltd.
	// Date: 18/02/2010
	// Version: 1.0
	// Description: Page Template for the Conent Tools Dashboard page
	//			DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		 with out consent from the author.
	// Copyright (c) 2010, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericPopupBlankTemplate.class.php';

Class adminContentToolsPopupPage extends adminGenericPopupBlankTemplate {

	var $basic = 'popup';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Content Tools : Help';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Content Tools Help';
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_viewAnyMarkup('popups/renderContentToolsHelp.tpl.php');

		//$STDlib->varDump($_POST);
		echo '</div>'."\n";

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>