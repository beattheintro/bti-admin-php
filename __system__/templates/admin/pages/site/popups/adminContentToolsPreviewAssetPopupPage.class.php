<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminContentToolsPreviewAssetPopupPage.class.php
	// Author: Cactus Designs (UK) Ltd.
	// Date: 19/03/2010
	// Version: 1.0
	// Description: Page Template for the Conent Tools Preview Asset page
	//			DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		 with out consent from the author.
	// Copyright (c) 2010, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericPopupBlankTemplate.class.php';

Class adminContentToolsPreviewAssetPopupPage extends adminGenericPopupBlankTemplate {

	var $basic = 'popup';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Content Tools : Preview';

		if(isset($_GET['type']))
		{
			$titleTxt = ' - '.ucfirst($_GET['type']);
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = 'Content Tools Preview'.$titleTxt;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		if(($_GET['type'] == 'video'))
		{
			API_getContentPreview('popups/renderContentToolsPreviewVideo.tpl.php', 'VIDEO', $_GET['source']);
		} elseif(($_GET['type'] == 'picture')) {
			API_getContentPreview('popups/renderContentToolsPreviewPicture.tpl.php', 'PICTURE', $_GET['source']);
		} else {
			echo '<p>No asset selected!</p>';
		}

		//$STDlib->varDump($_POST);

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>