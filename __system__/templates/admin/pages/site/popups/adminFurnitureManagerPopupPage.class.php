<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminFurnitureManagerPopupPage.class.php
	// Author: Cactus Designs (UK) Ltd.
	// Date: 23/04/2010
	// Version: 1.0
	// Description: Page Template for the Furtniture Manager Popup page
	//			DO NOT EDIT WITHOUT PRIOR PERMISSON FROM AUTHOR.
	// Notice: This software is not open source and should not be distributed or reused
	//		 with out consent from the author.
	// Copyright (c) 2010, Cactus Designs (UK) Ltd,  All rights reserved.
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericPopupTemplate.class.php';

Class adminFurnitureManagerPopupPage extends adminGenericPopupTemplate {

	var $basic = 'popup';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCustomStyleSheets()
	{
?>
	<link rel="stylesheet" type="text/css" href="/z/css/jquery.css?ttt=<?=time();?>" />
<?
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCustomJavaScript()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$errors = $_SESSION['FORM']['ERRORS'];
		$portletArr = base64_encode(serialize($ADMINcfg->PORTLETS));
?>
	<script type="text/javascript" src="/z/js/jquery/jquery-1.3.2.js"></script>
	<script type="text/javascript" src="/z/js/jquery/form.validation.js"></script>
	<script type="text/javascript" src="/z/js/ui/ui.core.js"></script>
	<script type="text/javascript" src="/z/js/ui/ui.draggable.js"></script>
	<script type="text/javascript" src="/z/js/ui/ui.resizable.js"></script>
	<script type="text/javascript" src="/z/js/ui/ui.dialog.js"></script>
	<script type="text/javascript" src="/z/js/ui/bgiframe/jquery.bgiframe.js"></script>
	<!--
	-->
	<script type="text/javascript" src="/z/js/ui/ui.accordion.js"></script>
	<script type="text/javascript">
	$.ui.dialog.defaults.bgiframe = true;
	$(document).ready(function(){
		$("#addFurnitureForm").validate();
	});

	$(function() {
		$("#accordion").accordion({
			autoHeight: false
		});
		<? if(!$_GET['e']) { ?>
		$("#accordion").accordion("activate", 1);
		<? } ?>
	});
	</script>
<?
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Furniture Manager';

		if(isset($_GET['type']))
		{
			$titleTxt = ' - '.ucfirst($_GET['type']);
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = 'Furniture Manager'.$titleTxt;
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";

		API_furnitureManagerControlPanelData('portal/renderFunitureManagerPopupControlPanel.tpl.php');

		//$STDlib->varDump($_SESSION['_PORTLET_DATA_']);
		echo '</div>'."\n";

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>