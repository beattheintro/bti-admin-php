<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminQuestionsAddItemPage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Question Edit page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminQuestionsAddItemPage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Add Question';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Add Question';
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
	function showCustomJavaScript()
	{
		// jQuery or other custom JS calls.
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/jquery-1.3.2.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/form.validation.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/ui/ui.core.js"></script>'."\n";
?>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#questionAddForm").validate();
	});

	$(window).load(function () {
	    setTimeout(function () { $('.question-edit-error-msg').fadeOut() }, 5000);
	});

	$(document).ready(function () {
		$("#trackCorrectArtist").change(function () {
			var id = $(this).val();
			var dataString = 'trackCorrectArtist=' + id;

			$.ajax({
				type: "POST",
				url: "/p/__process__/__question-ajax.php",
				data: dataString,
				cache: false,
				success: function (html) {
					$("#trackCorrectTitle").html(html);
				}
			});
		});
	});

	$(document).ready(function () {
		$("#trackWrongArtist1").change(function () {
			var id = $(this).val();
			var dataString = 'trackWrongArtist1=' + id;

			$.ajax({
				type: "POST",
				url: "/p/__process__/__question-ajax.php",
				data: dataString,
				cache: false,
				success: function (html) {
					$("#trackWrongTitle1").html(html);
				}
			});
		});
	});

	$(document).ready(function () {
		$("#trackWrongArtist2").change(function () {
			var id = $(this).val();
			var dataString = 'trackWrongArtist2=' + id;

			$.ajax({
				type: "POST",
				url: "/p/__process__/__question-ajax.php",
				data: dataString,
				cache: false,
				success: function (html) {
					$("#trackWrongTitle2").html(html);
				}
			});
		});
	});
</script>

<?
	}
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_questionsInfoBarData('questions/renderQuestionsInfoBar.tpl.php');
        
        $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
		unset($_SESSION['QUESTIONS']['InPackId']);
        
        if($adminEdit == 0)
        {
            $DATA = 'edit this';
            API_viewAnyMarkup('general/renderNoPrivileges.tpl.php', $DATA);
        } else {
            API_getQuestionAddData('questions/renderQuestionAddDetail.tpl.php');
			API_getAllPacksData('questions/renderAllPacks.tpl.php');
        }

		echo '</div>'."\n";
        
        //unset($_SESSION['QUESTIONS']);
		unset($_SESSION['_QUESTION_ADD_VARS_']);

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>