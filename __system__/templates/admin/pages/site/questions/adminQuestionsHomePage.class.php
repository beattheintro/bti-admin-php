<?php

	// ----------------------------------------------------------- >>>>>>>>>>
    // Filename : adminQuestionsHomePage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Questions Home page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminQuestionsHomePage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Question Tools';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Question Tools';
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_questionsInfoBarData('questions/renderQuestionsInfoBar.tpl.php');
        
        if(isset($_GET['page']) && ($_GET['page'] > 1))
        {
           $thisPage = $_GET['page'];
           $startAt = ($thisPage-1)*$ADMINcfg->MAX_DB_RESULTS;
        } elseif($_GET['page'] == 1) {
            $startAt = 0;
            $thisPage = 1;
        } else {
            $startAt = 0;
            $thisPage = 1;
        }
        
        $paginationData['thisPage'] = $thisPage;
        
        API_viewAnyMarkup('questions/renderQuestionsPagination.tpl.php', $paginationData);
        API_getQuestions('questions/renderQuestions.tpl.php', $startAt);
        API_viewAnyMarkup('questions/renderQuestionsPagination.tpl.php', $paginationData);
		//$STDlib->varDump($_POST);
		echo '</div>'."\n";
        
        unset($_SESSION['QUESTIONS']);

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>