<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminQuestionsSearchPage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Questions Search page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminQuestionsSearchPage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Question Search Tools';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Question Search Tools';
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_questionsInfoBarData('questions/renderQuestionsInfoBar.tpl.php');
        
        if(isset($_GET['search_term']))
        {
            $searchVars['search_term'] = $_GET['search_term'];
            $searchVars['by_artist'] = TRUE;
        } else {
            $searchVars = $_POST;
        }
        
        API_viewAnyMarkup('questions/renderQuestionsSearchBar.tpl.php', $searchVars);
        API_getQuestionsBySearch('questions/renderQuestions.tpl.php', $searchVars);
		//$STDlib->varDump($_POST);
		echo '</div>'."\n";
        
        unset($_SESSION['QUESTIONS']);
        

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>