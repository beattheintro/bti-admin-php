<?php

	// ----------------------------------------------------------- >>>>>>>>>>
    // Filename : adminThemesDeleteItemPage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Theme Delete page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminThemesDeleteItemPage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Delete Theme';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Delete Theme';
	}

    // ----------------------------------------------------------- >>>>>>>>>>

    function showCustomStyleSheets()
	{
		// Custom CSS Declarations go here.
        echo '<link rel="stylesheet" type="text/css" href="/z/css/inlineplayer.css?ttt='.time().'" />'."\n";
        echo '<link rel="stylesheet" type="text/css" href="/z/css/flashblock.css?ttt='.time().'" />'."\n";
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
	function showCustomJavaScript()
	{
		// jQuery or other custom JS calls.
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/soundmanager2.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/inlineplayer.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/jquery-1.3.2.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/form.validation.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/ui/ui.core.js"></script>'."\n";
?>
	<script type="text/javascript">
	$(document).ready(function(){
	    $("#themeEditForm").validate();
	});

	$(document).ready(function () {
	    $('input[type="checkbox"]').click(function () {
	        if ($(this).attr("id") == "CreateOGG") {
	            $(".ogg").toggle();
	        }
	    });
	});

	$(window).load(function () {
	    setTimeout(function () { $('.theme-edit-error-msg').fadeOut() }, 5000);
	});

	</script>
<?
	}
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		//API_themesInfoBarData('themes/renderTracksInfoBar.tpl.php');
        
        $adminNew = $_SESSION['_LOGIN_']['admin_new'];
        
        if($adminNew == 0)
        {
            $DATA = 'delete this';
            //API_viewAnyMarkup('general/renderNoPrivileges.tpl.php', $DATA);
        } else {
           //API_getTrackData('themes/renderTrackDeleteDetail.tpl.php');
        }

		echo '</div>'."\n";
        
        unset($_SESSION['THEMES']);

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>