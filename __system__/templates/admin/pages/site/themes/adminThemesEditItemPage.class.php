<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminThemesEditItemPage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Theme Edit page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminThemesEditItemPage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Edit Theme';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Edit Theme';
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
	function showCustomJavaScript()
	{
		// jQuery or other custom JS calls.
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/jquery-1.3.2.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/form.validation.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/ui/ui.core.js"></script>'."\n";
?>
	<script type="text/javascript">
	$(document).ready(function(){
	    $("#themeEditForm").validate();
	});

	$(window).load(function () {
	    setTimeout(function () { $('.theme-edit-error-msg').fadeOut() }, 5000);
	});
</script>

<?
	}
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_themesInfoBarData('themes/renderThemesInfoBar.tpl.php');
        
        $adminEdit = $_SESSION['_LOGIN_']['admin_edit'];
        
        if($adminEdit == 0)
        {
            $DATA = 'edit this';
           API_viewAnyMarkup('general/renderNoPrivileges.tpl.php', $DATA);
        } else {
            API_getThemeEditData('themes/renderThemesEditDetail.tpl.php');
			//API_getAllThemesData('themes/renderAllThemes.tpl.php');
        }

		echo '</div>'."\n";
        
		//unset($_SESSION['THEMES']);
		unset($_SESSION['_THEMES_EDIT_VARS_']);

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>