<?php

	// ----------------------------------------------------------- >>>>>>>>>>
    // Filename : adminTracksSearchPage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Tracks Search page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminTracksSearchPage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'Track Search Tools';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'Track Search Tools';
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_tracksInfoBarData('tracks/renderTracksInfoBar.tpl.php');
        
        if(isset($_GET['page']) && ($_GET['page'] > 1))
        {
           $thisPage = $_GET['page'];
           $startAt = ($thisPage-1)*25;
        } elseif($_GET['page'] == 1) {
            $startAt = 0;
            $thisPage = 1;
        } else {
            $startAt = 0;
            $thisPage = 1;
        }
        
        $paginationData['thisPage'] = $thisPage;
        
        if(isset($_GET['search_term']))
        {
            $searchVars['search_term'] = $_GET['search_term'];
            $searchVars['by_artist'] = TRUE;
        } else {
            $searchVars = $_POST;
        }
        
        $searchVars['startAt'] = $startAt;
        
        //API_viewAnyMarkup('tracks/renderTracksPagination.tpl.php', $paginationData);
        API_viewAnyMarkup('tracks/renderTracksSearchBar.tpl.php', $searchVars);
        API_getTracksBySearch('tracks/renderTracks.tpl.php', $searchVars);
        //API_viewAnyMarkup('tracks/renderTracksPagination.tpl.php', $paginationData);
		//$STDlib->varDump($_POST);
		echo '</div>'."\n";
        
        unset($_SESSION['TRACKS']);
        

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>