<?php

	// ----------------------------------------------------------- >>>>>>>>>>
    // Filename : adminTracksViewItemPage.class.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Page Template for the Track Item page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH.'/generic/adminGenericTemplate.class.php';

Class adminTracksViewItemPage extends adminGenericTemplate {

	var $basic = 'admin';
	var $siteCSS = 'admin.css';
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = 'View Track';

		if(isset($_POST['type']))
		{
			$titleTxt = ucfirst($_POST['type']).' - ';
		} else {
			$titleTxt = '';
		}
		$this->pageTitle = $titleTxt.'View Track';
	}

    // ----------------------------------------------------------- >>>>>>>>>>

    function showCustomStyleSheets()
	{
		// Custom CSS Declarations go here.
        echo '<link rel="stylesheet" type="text/css" href="/z/css/inlineplayer.css?ttt='.time().'" />'."\n";
        echo '<link rel="stylesheet" type="text/css" href="/z/css/flashblock.css?ttt='.time().'" />'."\n";
	}

	// ----------------------------------------------------------- >>>>>>>>>>
    
	function showCustomJavaScript()
	{
		// jQuery or other custom JS calls.
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/soundmanager2.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/inlineplayer.js"></script>'."\n";

	}
    
	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		echo '<div class="content">'."\n";
		API_tracksInfoBarData('tracks/renderTracksInfoBar.tpl.php');
        
        if(isset($_GET['page']) && ($_GET['page'] > 1))
        {
           $thisPage = $_GET['page'];
           $startAt = ($thisPage-1)*25;
        } elseif($_GET['page'] == 1) {
            $startAt = 0;
            $thisPage = 1;
        } else {
            $startAt = 0;
            $thisPage = 1;
        }
        
        $paginationData['thisPage'] = $thisPage;
        
        if(isset($_GET['search_term']))
        {
            $searchVars['search_term'] = $_GET['search_term'];
            $searchVars['by_artist'] = TRUE;
        } else {
            $searchVars = $_POST;
        }
        
        $searchVars['startAt'] = $startAt;
        
        //API_viewAnyMarkup('tracks/renderTracksPagination.tpl.php', $paginationData);
        //API_viewAnyMarkup('tracks/renderTracksSearchBar.tpl.php', $searchVars);
        API_getTrackData('tracks/renderTrackDetail.tpl.php');
        //API_viewAnyMarkup('tracks/renderTracksPagination.tpl.php', $paginationData);
		//$STDlib->varDump($_POST);
		echo '</div>'."\n";
        
        unset($_SESSION['TRACKS']);

	} // End showLeftCol()

	// ----------------------------------------------------------- >>>>>>>>>>

} // Close Class
?>