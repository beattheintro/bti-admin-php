<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminLoginHomePage.class.php
    // Author: Jason Rastrick
    // Date: 13/03/2015
    // Version: 1.0
	// Description: Page Template for the Login page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH."/generic/adminGenericTemplate.class.php";

Class adminLoginHomePage extends adminGenericTemplate {

	var $basic = "admin";
	var $siteCSS = "admin.css";
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCustomStyleSheets()
	{
?>
	<link rel="stylesheet" type="text/css" href="/z/css/jquery.css?ttt=<?=time();?>" />
<?
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCustomJavaScript()
	{
?>
	<script type="text/javascript" src="/z/js/jquery/jquery-1.3.2.js"></script>
	<script type="text/javascript" src="/z/js/jquery/form.validation.js"></script>
	<script type="text/javascript" src="/z/js/ui/ui.core.js"></script>
<?
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = "Management : Login";
		$this->pageTitle = "Login";
	}

	// ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		API_viewLoginForm("users/renderLoginForm.tpl.php");

	} // End showLeftCol()

} // Close Class
?>