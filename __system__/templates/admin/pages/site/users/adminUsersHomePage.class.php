<?php

	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : adminUsersHomePage.class.php
    // Author: Jason Rastrick
    // Date: 13/03/2015
    // Version: 1.0
	// Description: Page Template for the Users Admin page
	// ----------------------------------------------------------- >>>>>>>>>>

require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH."/generic/adminGenericTemplate.class.php";

Class adminUsersHomePage extends adminGenericTemplate {

	var $basic = "admin";
	var $siteCSS = "admin.css";
	var $Border = "0";
	var $xHTML = "0"; // Validates to XHTML (1.0)
	var $docTitle;
	var $pageTitle;
	var $metaDescription;
	var $metaKeywords;
	var $metaRefresh;

	function setPageInfo()
	{
		$this->metaDescription = "";
		$this->metaKeywords = "";
		$this->metaRefresh = "";
		$this->docTitle = "Users & Administrators";
		$this->pageTitle = "Users & Administrators";
	}

    // ----------------------------------------------------------- >>>>>>>>>>
    
	function showCustomJavaScript()
	{
		// jQuery or other custom JS calls.
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/jquery-1.3.2.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/jquery/form.validation.js"></script>'."\n";
        echo '<script language="JavaScript" type="text/javascript" src="/z/js/ui/ui.core.js"></script>'."\n";
?>
	<script type="text/javascript">
	$(document).ready(function(){
	    $("#userAddForm").validate();
	});

	$(window).load(function () {
	    setTimeout(function () { $('.user-error-msg').fadeOut() }, 5000);
	});

	</script>
<?
	}

    // ----------------------------------------------------------- >>>>>>>>>>

	function showCenterCol()
	{
		global $ADMINcfg, $SITEsession, $STDlib;

		$admin = $_SESSION['_LOGIN_']["admin_1"];

		if($admin != 0)
		{
			// Admin user
			echo '<div class="content">'."\n";

			if(($_GET["user"] == "edit") && isset($_GET["uid"]))
			{
				API_viewCMSUser("users/renderUserUpdateForm.tpl.php", $_GET["uid"]);
			} elseif($_GET["user"] == "update") {
				$_SESSION['_ADMIN_USERS_']['extra']["err"] = $_GET["err"];
				$_SESSION['_ADMIN_USERS_']['extra']["user"] = $_GET["user"];
				$_SESSION['_ADMIN_USERS_']['extra']["pw"] = API_genPassword(8);
				API_viewCMSUsers("users/renderUsers.tpl.php");
				API_viewAnyMarkup("users/renderUserUpdateForm.tpl.php", $extra);
			} elseif($_GET["user"] == "insert") {
				$_SESSION['_ADMIN_USERS_']['extra']["err"] = $_GET["err"];
				$_SESSION['_ADMIN_USERS_']['extra']["user"] = $_GET["user"];
				$_SESSION['_ADMIN_USERS_']['extra']["pw"] = API_genPassword(8);
				API_viewCMSUsers("users/renderUsers.tpl.php");
				API_viewAnyMarkup("users/renderUserUpdateForm.tpl.php", $extra);
			} else {
				$_SESSION['_ADMIN_USERS_']['extra']["err"] = $_GET["err"];
				$_SESSION['_ADMIN_USERS_']['extra']["user"] = $_GET["user"];
				$_SESSION['_ADMIN_USERS_']['extra']["pw"] = API_genPassword(8);
				API_viewCMSUsers("users/renderUsers.tpl.php");
				API_viewAnyMarkup("users/renderUserUpdateForm.tpl.php", $extra);
			}
			echo '</div>'."\n";
		} else {
			echo '<div class=content">'."\n";
			API_viewAnyMarkup("users/renderNotAdmin.tpl.php", "User Manager");
			echo '</div>'."\n";
		}
        
        unset($_SESSION['_ADMIN_USERS_']);

	} // End showLeftCol()

} // Close Class
?>