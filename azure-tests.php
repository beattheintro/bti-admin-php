<?
// ----------------------------------------------------------- >>>>>>>>>>
// Filename : azure-tests.php
// Author: Jason Rastrick
// Date: 13/03/2015
// Version: 1.0
// Description: File to show Data
// ----------------------------------------------------------- >>>>>>>>>>

header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
header("Pragma : no-cache");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

/* Dump (v1.0) */
$section ="dump";
$page = "dump";

require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";

phpinfo();
//$STDlib->showDeBugData(1, 1, "", $ADMINcfg);

// PackName - BTI Starter Pack
// BinData(3,"+Eo3xA+ArU+wb2G/kkMWdg==")
// C4374AF8-800F-4FAD-B06F-61BF92431676
// "PackId" : BinData(3, "M4mSrzjZiEOZXiTsHFC2QQ==")

// print every log message possible
/*
MongoLog::setLevel(MongoLog::ALL); // all log levels
MongoLog::setModule(MongoLog::ALL); // all parts of the driver
$m = new MongoClient(); // connect
$db = 'local';
$bti_resumegame = 'bti_resumegame';
$bti_partyplaygame = 'bti_partyplaygame';
$bti_userspecificdata = 'bti_userspecificdata';

$m->$db->setProfilingLevel(2);

$packNames = $m->$db->$bti_resumegame->distinct('PackName');
$STDlib->varDump($packNames);

for($i=0; isset($packNames[$i]); $i++)
{
	$packName = $packNames[$i];
	$query = array('PackName'=>$packName);
	$count = $m->$db->$bti_resumegame->find($query)->count();
	
	$GamePlayedRegular[$packName] = $count;
}

$STDlib->varDump($GamePlayedRegular);

$packNames = $m->$db->$bti_partyplaygame->distinct('PackName');
$STDlib->varDump($packNames);

for($i=0; isset($packNames[$i]); $i++)
{
	$packName = $packNames[$i];
	$query = array('PackName'=>$packName);
	$count = $m->$db->$bti_partyplaygame->find($query)->count();
	
	$GamePlayedPartyPlay[$packName] = $count;
}

$STDlib->varDump($GamePlayedPartyPlay);

$packIds = $m->$db->$bti_userspecificdata->distinct('SpendCollection.Items.PackId');
//$STDlib->varDump($packIds);

for($i=0; isset($packIds[$i]); $i++)
{
	$packId = $packIds[$i];
	$query = array('SpendCollection.Items.PackId'=>$packId);
	$count = $m->$db->$bti_userspecificdata->find($query)->count();
	
	$Unlocks[$i] = $count;
}

$STDlib->varDump($Unlocks);
*/
//$packId = $arr[]['PackId'];
//$query = array('PackId'=>$packId);
//$cursor = $m->$db->$collection->find($query)->count();
//$STDlib->varDump($cursor);

//$dbs = $m->listDBs();
//$STDlib->varDump($dbs);

//$collections = $m->$db->getCollectionNames();
//$STDlib->varDump($collections);

?>