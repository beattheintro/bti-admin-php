<?php
	// ----------------------------------------------------------- >>>>>>>>>>
    // Author: Jason Rastrick
    // Date: 13/03/2015
    // Version: 1.0
	// ----------------------------------------------------------- >>>>>>>>>>

	header("Cache-Control : \"no-cache, must-revalidate\"");
	header("Pragma : \"no-cache\"");
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* TOOL (v0.0) */
	$section ="";
	$page = "";

	require_once '/__system__/includes/admin/core/global.inc.php';

	$uid = $_SESSION['_LOGIN_']["uid"];

	if(!isset($uid))
	{
		header("Location: ./login.php");
	} else {
		header("Location: /p/dashboard/index.php");
	}

?>