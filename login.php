<?php
	// ----------------------------------------------------------- >>>>>>>>>>
    // Author: Jason Rastrick
    // Date: 13/03/2015
    // Version: 1.0
	// ----------------------------------------------------------- >>>>>>>>>>

	header("Cache-Control : \"no-cache, must-revalidate\"");
	header("Pragma : \"no-cache\"");
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* TOOL (v0.0) */
	$section ="login";
	$page = "login";
    
    //phpinfo();
    //exit;

	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";

	if($_GET["out"] == "1")
	{
        unset($_SESSION['_LOGIN_']);
        unset($_SESSION['TRACKS']);
        unset($_SESSION['_TRACK_ADD_VARS_']);
        unset($_SESSION['_TRACK_EDIT_VARS_']);
        unset($_SESSION['_TRACK_DELETE_VARS_']);
        unset($_SESSION['QUESTIONS']);
        unset($_SESSION['_QUESTION_EDIT_VARS_']);
        unset($_SESSION['_ADMIN_USERS_']);
        unset($_SESSION['SQL_DEBUG']);        
	}
    
    //var_dump($_POST);

	if(!empty($_POST["u_name"]) && !empty($_POST["u_password"]))
	{
		$errors = API_doUserLogin();
        
        //var_dump($errors);
        
		if($errors["error"] != TRUE)
		{
			//$eventMessage = 'Login success!';
			//API_addEvent('USER_LOGIN', $eventMessage, 1);
			header("Location: /p/dashboard/");
			exit;
		} else {
			require $ADMINcfg->SYSTEM_PAGE_TPL_PATH."/site/users/adminLoginHomePage.class.php";
			$adminLoginHomePage = new adminLoginHomePage();
			$adminLoginHomePage->makePage();
		}
	} else {
		require_once $ADMINcfg->SYSTEM_PAGE_TPL_PATH."/site/users/adminLoginHomePage.class.php";
		$adminLoginHomePage = new adminLoginHomePage();
		$adminLoginHomePage->makePage();
	}

?>