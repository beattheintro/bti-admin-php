<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : __pack-add.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Pack Add $_POST Data Processor
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Pack Add (v1.0) */
	$section ="packs";
	$page = "add";
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";
    
	unset($_SESSION['_PACKS_ADD_VARS_']);
	
	// Update Track
	if(isset($_POST["saveAddPack"]))
	{
	    $_SESSION['_PACKS_ADD_VARS_'] = $_POST;

        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a new thumbnail
            $thisMIME = $_FILES['content_asset']['type']['thumb'];
            if(!in_array($thisMIME, $ADMINcfg->VALID_IMAGE_MIMES) || ($thisMIME != 'image/png'))
            {
                // INVALID THUMB MIME
                $_SESSION['_PACKS_ADD_VARS_']['err']['PNG_MIME'] = '<span style="color:red;font-weight:bold;">&nbsp;(Only PNG is allowed)&nbsp;</span>';
            }
        }

        if(is_array($_SESSION['_PACKS_ADD_VARS_']['err']))
        {
			header("Location: /p/packs/add-pack.php?Id=".$_POST["Id"]."&pack=add&err=mime");
			exit;            
        }

		//$STDlib->varDump($_POST);
		//$STDlib->varDump($_FILES);
		//$STDlib->varDump($_SESSION);
		//$STDlib->varDump($DATA);
		//exit;
	
		$DATA = API_addPackData();
        
		if($DATA["error"])
		{
			header("Location: /p/packs/add-pack.php?Id=".$_POST["Id"]."&pack=add&err=".$DATA["error"]);
			exit;
        } elseif(!$DATA["error"]) {
            unset($_SESSION['_PACKS_ADD_VARS_']);
			header("Location: /p/packs/add-pack.php?Id=".$_POST["Id"]."&pack=add&err=None");
			exit;	
		} else {
			header("Location: /p/packs/add-pack.php?Id=".$_POST["Id"]."&pack=add&err=1");
			exit;	
		}
	}
?>