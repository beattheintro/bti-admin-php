<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : __question-add.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Question Add $_POST Data Processor
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Question Add (v1.0) */
	$section ="questions";
	$page = "add";
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";
    
	// Update Track
	if(isset($_POST["saveAddQuestion"]))
	{
        $_SESSION['_QUESTION_ADD_VARS_'] = $_POST;
                
		$DATA = API_addQuestionData();

        //$STDlib->varDump($_POST);
        //$STDlib->varDump($_FILES);
        //$STDlib->varDump($_SESSION);
        //exit;
        
		if($DATA["error"])
		{
			header("Location: /p/questions/add-question.php?Id=".$DATA["Id"]."&question=insert&err=".$DATA["error"]);
			exit;
        } elseif(!$DATA["error"]) {
            unset($_SESSION['_QUESTION_ADD_VARS_']);
			unset($_SESSION['QUESTIONS']['InPackId']);
			header("Location: /p/questions/add-question.php?Id=".$DATA["Id"]."&question=insert&err=None");
			exit;	
		} else {
			header("Location: /p/questions/add-question.php?Id=".$DATA["Id"]."&question=insert&err=1");
			exit;	
		}
	}
?>