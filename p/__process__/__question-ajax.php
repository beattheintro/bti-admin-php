<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : __question-ajax.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Questions AJAX Data Processor
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Questions AJAX (v1.0) */
	$section ="questions";
	$page = "ajax";
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";

	$trackArtistData = $_SESSION['QUESTIONS']['trackArtistData'];
	$trackAllArtistData = $_SESSION['QUESTIONS']['trackAllArtistData'];
	
	//$STDlib->varDump($_POST);	

	// http://localhost:49356/p/__process__/__question-ajax.php?trackCorrectArtist=570
		
	if(isset($_POST['trackCorrectArtist']))	{
		$postId = $_POST['trackCorrectArtist'];
		$searchTerm = $trackArtistData[$postId]['Artist'];
		$selected = $_SESSION['QUESTIONS']['CorrectTitle'];
		API_getTrackTitlesForSelectBox('questions/renderAJAXSelectBox.tpl.php', $searchTerm, TRUE, $selected);
	} elseif(isset($_POST['trackWrongArtist1'])) {
		$postId = $_POST['trackWrongArtist1'];
		$searchTerm = $trackAllArtistData[$postId]['Artist'];
		$selected = $_SESSION['QUESTIONS']['WrongTitle1'];
		API_getTrackTitlesForSelectBox('questions/renderAJAXSelectBox.tpl.php', $searchTerm, FALSE, $selected);
	} elseif(isset($_POST['trackWrongArtist2'])) {
		$postId = $_POST['trackWrongArtist2'];
		$searchTerm = $trackAllArtistData[$postId]['Artist'];
		$selected = $_SESSION['QUESTIONS']['WrongTitle2'];
		API_getTrackTitlesForSelectBox('questions/renderAJAXSelectBox.tpl.php', $searchTerm, FALSE, $selected);
	} else {
		echo '';
	}
?>