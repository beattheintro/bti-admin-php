<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : __question-edit.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Question Edit $_POST Data Processor
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Question Edit (v1.0) */
	$section ="questions";
	$page = "edit";
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";
    
	// Update Track
	if(isset($_POST["saveEditQuestion"]) && isset($_POST["Id"]))
	{
        $_SESSION['_QUESTION_EDIT_VARS_'] = $_POST;
                
		$DATA = API_updateQuestionData();

        //$STDlib->varDump($_POST);
        //$STDlib->varDump($_FILES);
        //$STDlib->varDump($_SESSION);
        //exit;
        
		if($DATA["error"])
		{
			header("Location: /p/questions/edit-question.php?Id=".$_POST["Id"]."&question=update&err=".$DATA["error"]);
			exit;
        } elseif(!$DATA["error"]) {
            unset($_SESSION['_QUESTION_EDIT_VARS_']);
			unset($_SESSION['QUESTIONS']['InPackId']);
			header("Location: /p/questions/edit-question.php?Id=".$_POST["Id"]."&question=update&err=None");
			exit;	
		} else {
			header("Location: /p/questions/edit-question.php?Id=".$_POST["Id"]."&question=update&err=1");
			exit;	
		}
	}
?>