<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : __theme-edit.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Theme Edit $_POST Data Processor
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Theme Edit (v1.0) */
	$section ="themes";
	$page = "edit";
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";
    
	unset($_SESSION['_THEMES_EDIT_VARS_']);
	
	// Update Track
	if(isset($_POST["saveEditTheme"]) && isset($_POST["Id"]))
	{
	    $_SESSION['_THEMES_EDIT_VARS_'] = $_POST;
		$numPacks = $_SESSION['THEMES']['numPacks'];

        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a new thumbnail
            $thisMIME = $_FILES['content_asset']['type']['thumb'];
            if(!in_array($thisMIME, $ADMINcfg->VALID_IMAGE_MIMES) || ($thisMIME != 'image/jpeg'))
            {
                // INVALID THUMB MIME
                $_SESSION['_THEMES_EDIT_VARS_']['err']['JPEG_MIME'] = '<span style="color:red;font-weight:bold;">&nbsp;(Only JPEG is allowed)&nbsp;</span>';
            }
        }

        if(is_array($_SESSION['_THEMES_EDIT_VARS_']['err']))
        {
			header("Location: /p/themes/edit-theme.php?Id=".$_POST["Id"]."&track=update&err=mime");
			exit;            
        }

		//$STDlib->varDump($_POST);
		//$STDlib->varDump($_FILES);
		//$STDlib->varDump($_SESSION);
		//$STDlib->varDump($DATA);
		//exit;
	
		$DATA = API_updateThemeData();
        
		if($DATA["error"])
		{
			header("Location: /p/themes/edit-theme.php?Id=".$_POST["Id"]."&theme=update&err=".$DATA["error"]);
			exit;
        } elseif(!$DATA["error"]) {
            unset($_SESSION['_THEMES_EDIT_VARS_']);
			header("Location: /p/themes/edit-theme.php?Id=".$_POST["Id"]."&theme=update&err=None");
			exit;	
		} else {
			header("Location: /p/themes/edit-theme.php?Id=".$_POST["Id"]."&theme=update&err=1");
			exit;	
		}
	}
?>