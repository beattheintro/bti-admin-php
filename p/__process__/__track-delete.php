<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : __track-delete.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Track Delete $_POST Data Processor
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Track Delete (v1.0) */
	$section ="tracks";
	$page = "Delete";
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";
    
	// Add Track
	if(isset($_POST["saveDeleteTrack"]))
	{
        $_SESSION['_TRACK_DELETE_VARS_'] = $_POST;        
        
		//$DATA = API_deleteTrackData();

        $STDlib->varDump($DATA);
        $STDlib->varDump($_POST);
        $STDlib->varDump($_FILES);
        $STDlib->varDump($_SESSION);
        exit;
        
		if($DATA["error"])
		{
			header("Location: /p/tracks/add-delete.php?Id=".$DATA["Id"]."&track=delete&err=".$DATA["error"]);
			exit;
        } elseif(!$DATA["error"]) {
            unset($_SESSION['_TRACK_DELETE_VARS_']);
			header("Location: /p/tracks/add-delete.php?Id=".$DATA["Id"]."&track=delete&err=None");
			exit;	
		} else {
			header("Location: /p/tracks/add-delete.php?Id=".$DATA["Id"]."&track=delete&err=1");
			exit;	
		}
	}
?>