<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : __track-edit.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Track Edit $_POST Data Processor
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Track Edit (v1.0) */
	$section ="tracks";
	$page = "edit";
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";
    
	// Update Track
	if(isset($_POST["saveEditTrack"]) && isset($_POST["Id"]))
	{
        $_SESSION['_TRACK_EDIT_VARS_'] = $_POST;
        
        if($_FILES['content_asset']['error']['thumb'] == 0)
        {
            // We have a new thumbnail
            $thisMIME = $_FILES['content_asset']['type']['thumb'];
            if(!in_array($thisMIME, $ADMINcfg->VALID_IMAGE_MIMES) || ($thisMIME != 'image/jpeg'))
            {
                // INVALID THUMB MIME
                $_SESSION['_TRACK_EDIT_VARS_']['err']['JPG_MIME'] = '<span class="msg-error">&nbsp;(Only JPG is allowed)</span>';
            }
        }
        
        if($_FILES['content_asset']['error']['mp3'] == 0)
        {
            // We have a new MP3
            $thisMIME = $_FILES['content_asset']['type']['mp3'];
            if(!in_array($thisMIME, $ADMINcfg->VALID_AUDIO_MIMES) || ($thisMIME != 'audio/mp3'))
            {
                // INVALID MP3 MIME
                $_SESSION['_TRACK_EDIT_VARS_']['err']['MP3_MIME'] = '<span class="msg-error">&nbsp;(Only MP3 is allowed)</span>';
            }            
        }

        if($_FILES['content_asset']['error']['ogg'] == 0)
        {
            // We have a new OGG
            $thisMIME = $_FILES['content_asset']['type']['ogg'];
            if(!in_array($thisMIME, $ADMINcfg->VALID_AUDIO_MIMES) || ($thisMIME != 'audio/ogg'))
            {
                // INVALID OGG MIME
                $_SESSION['_TRACK_EDIT_VARS_']['err']['OGG_MIME'] = '<span class="msg-error">&nbsp;(Only OGG is allowed)</span>';
            }
        }

        if($_FILES['content_asset']['error']['mp4'] == 0)
        {
            // We have a new MP4
            $thisMIME = $_FILES['content_asset']['type']['mp4'];
            if(!in_array($thisMIME, $ADMINcfg->VALID_AUDIO_MIMES) || ($thisMIME != 'audio/mp4'))
            {
                // INVALID MP4 MIME
                $_SESSION['_TRACK_EDIT_VARS_']['err']['MP4_MIME'] = '<span class="msg-error">&nbsp;(Only MP4 is allowed)</span>';
            }            
        }
        
        if(is_array($_SESSION['_TRACK_EDIT_VARS_']['err']))
        {
			header("Location: /p/tracks/edit-track.php?Id=".$_POST["Id"]."&track=update&err=mime");
			exit;            
        }
        
		$DATA = API_updateTrackData();

        //$STDlib->varDump($imgInfo);
        //$STDlib->varDump($_POST);
        //$STDlib->varDump($_FILES);
        //$STDlib->varDump($_SESSION);
        //exit;
        
		if($DATA["error"])
		{
			header("Location: /p/tracks/edit-track.php?Id=".$_POST["Id"]."&track=update&err=".$DATA["error"]);
			exit;
        } elseif(!$DATA["error"]) {
            unset($_SESSION['_TRACK_EDIT_VARS_']);
			header("Location: /p/tracks/edit-track.php?Id=".$_POST["Id"]."&track=update&err=None");
			exit;	
		} else {
			header("Location: /p/tracks/edit-track.php?Id=".$_POST["Id"]."&track=update&err=1");
			exit;	
		}
	}
?>