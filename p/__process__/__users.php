<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : users.php
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Users $_POST Data Processor
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Users (v1.0) */
	$section ="admin";
	$page = "users";
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";

	// View User
	if(isset($_POST["edit"]) && isset($_POST["Id"]))
	{
		header("Location: /p/users/index.php?user=edit&uid=".$_POST["Id"]);
		exit;
	}

	// Update User
	if(isset($_POST["usersave"]) && isset($_POST["Id"]))
	{
		if($_POST["delete"] == "1")
		{
			// DO DELETE
			$DATA = API_deleteCMSUser();
			header("Location: /p/users/index.php?user=delete&err=0");
			exit;	
		} 

		$DATA = API_updateCMSUser();
		if($DATA["error"])
		{
			header("Location: /p/users/index.php?user=update&err=".$DATA["error"]);
			exit;	
		} elseif(!$DATA["error"]) {
			header("Location: /p/users/index.php?user=update&err=0");
			exit;	
		} else {
			header("Location: /p/users/index.php?user=update&err=1&uid=".$_POST["Id"]);
			exit;	
		}
	}

	// Insert User
	if(isset($_POST["useradd"]) && !empty($_POST["u_name"]) && isset($_POST["u_password"]))
	{
		$DATA = API_insertCMSUser();
		//var_dump($DATA);

		if($DATA["error"])
		{
			header("Location: /p/users/index.php?user=insert&err=".$DATA["error"]);
			exit;	
		} elseif(!$DATA["error"]) {
			header("Location: /p/users/index.php?user=insert&err=0");
			exit;	
		} else {
			header("Location: /p/users/index.php?user=insert&err=1&uid=".$_POST["Id"]);
			exit;	
		}
	} else {
		header("Location: /p/users/index.php?user=insert&err=1");
		exit;	
	}
?>