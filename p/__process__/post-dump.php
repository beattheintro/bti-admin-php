<?
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : post-dump.php
    // Author: Jason Rastrick
    // Date: 13/03/2015
	// Version: 1.0
	// Description: File to show POST/Data
	// ----------------------------------------------------------- >>>>>>>>>>
	
	header("Cache-Control : no-cache, must-revalidate, no-store, pre-check=0, post-check=0, max-age=0");
	header("Pragma : no-cache");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Dump (v1.0) */
	$section ="dump";
	$page = "dump";
    
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";

	$STDlib->showDeBugData(1, 1, "", $ADMINcfg);
?>