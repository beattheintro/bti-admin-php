<?php
	// ----------------------------------------------------------- >>>>>>>>>>
	// Author: Jason Rastrick
	// Date: 21/12/2007
	// Version: 1.0
	// ----------------------------------------------------------- >>>>>>>>>>

	header("Cache-Control : \"no-cache, must-revalidate\"");
	header("Pragma : \"no-cache\"");
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	/* Delete (v1.0) */
	$section ="themes";
	$page = "delete-themes";
    
    //phpinfo();
    //exit;
    
	require_once $_SERVER["DOCUMENT_ROOT"]."/__system__/includes/admin/core/global.inc.php";

	$uid = $_SESSION['_LOGIN_']["Id"];
  
    //$STDlib->showDebugData($_SESSION,1,1,'','');
    //exit;

	if(!isset($uid))
	{
		header("Location: /login.php");
	} else {
		require $ADMINcfg->SYSTEM_PAGE_TPL_PATH."/site/tracks/adminThemesDeleteItemPage.class.php";
		$adminThemesDeleteItemPage = new adminThemesDeleteItemPage();
		$adminThemesDeleteItemPage->makePage();
	}

?>