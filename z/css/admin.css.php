/*
// ----------------------------------------------------------- >>>>>>>>>>
// Filename : admin.css
// Author: Jason Rastrick
// Date: 13/03/2015
// Version: 1.0
// Description: Global css file
// ----------------------------------------------------------- >>>>>>>>>>
*/
<?
	header('Content-type: text/css');

	$colour1 = '#0a6bcd';
	$colour2 = '#3399ff';
	$colour3 = '#bbddff';
?>
#header {
        position: top;
        background-color: #FFFFFF;
        border-left: 2px solid <?=$colour1;?>; border-right: 2px solid <?=$colour1;?>; border-top: 2px solid <?=$colour1;?>;
        margin: 2px auto 0px auto;
		padding:0px;
		width:1000px;
		-moz-border-radius-topleft: 10px/*{cornerRadius}*/;
		-webkit-border-top-left-radius: 10px/*{cornerRadius}*/;
		border-top-left-radius: 10px;
		-moz-border-radius-topright: 10px/*{cornerRadius}*/;
		-webkit-border-top-right-radius: 10px/*{cornerRadius}*/;
		border-top-right-radius: 10px;
}


/* #leftcontent {position: relative; top:20px; width:98%; float:left; padding:3px; margin:2px 2px 30px 6px; border: 0px solid red;} */
#centercontent {position: relative; width:1000px; margin:0px auto 0px auto; width:1000px; border-left: 2px solid <?=$colour2;?>; border-right: 2px solid <?=$colour2;?>;min-height:650px;}
/* #rightcontent {position: relative; top:0px; width:0px; float:left; padding:0px; margin:0px; border: 0px solid blue; } */

#footer {
	position:relative;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.9em;
	font-weight:normal;
	width:1000px;
    margin: 0px auto 0px auto;
    background-color: <?=$colour2;?>;
	padding:5px 0px 5px 0px;
    border-left: 2px solid <?=$colour2;?>; border-right: 2px solid <?=$colour2;?>;border-bottom: 2px solid <?=$colour2;?>;
	-moz-border-radius-bottomleft: 10px/*{cornerRadius}*/;
	-webkit-border-bottom-left-radius: 10px/*{cornerRadius}*/;
	border-bottom-left-radius: 10px;
	-moz-border-radius-bottomright: 10px/*{cornerRadius}*/;
	-webkit-border-bottom-right-radius: 10px/*{cornerRadius}*/;
	border-bottom-right-radius: 10px;
}

#centercontent-pupup {position: relative; width:750px; margin:0px auto 0px auto; width:750px; border-left: 2px solid <?=$colour2;?>; border-right: 2px solid <?=$colour2;?>;}

#header-popup {
        position: top;
        background-color: #FFFFFF;
        border-left: 2px solid <?=$colour1;?>; border-right: 2px solid <?=$colour1;?>; border-top: 2px solid <?=$colour1;?>;
        margin: 2px auto 0px auto;
		padding:0px;
		width:750px;
		-moz-border-radius-topleft: 10px/*{cornerRadius}*/;
		-webkit-border-top-left-radius: 10px/*{cornerRadius}*/;
		border-top-left-radius: 10px;
		-moz-border-radius-topright: 10px/*{cornerRadius}*/;
		-webkit-border-top-right-radius: 10px/*{cornerRadius}*/;
		border-top-right-radius: 10px;
}
#footer-popup {
	position:relative;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.9em;
	font-weight:normal;
	width:750px;
    margin: 0px auto 0px auto;
    background-color: <?=$colour2;?>;
	padding:5px 0px 5px 0px;
    border-left: 2px solid <?=$colour2;?>; border-right: 2px solid <?=$colour2;?>;border-bottom: 2px solid <?=$colour2;?>;
	-moz-border-radius-bottomleft: 10px/*{cornerRadius}*/;
	border-bottom-left-radius: 10px;
	-webkit-border-bottom-left-radius: 10px/*{cornerRadius}*/;
	-moz-border-radius-bottomright: 10px/*{cornerRadius}*/;
	border-radius-bottom-right: 10px;
	-webkit-border-bottom-right-radius: 10px/*{cornerRadius}*/;
}

.nav {
	background: <?=$colour1;?>;
	width: 1000px;
	margin:0px auto 0px auto;
    border-bottom: 1px solid #fff; 	border-left: 2px solid <?=$colour1;?>; border-right: 2px solid <?=$colour1;?>; border-top: 1px solid <?=$colour2;?>;
}

.subnav {
	background: <?=$colour2;?>;
	width: 1000px;
	margin:0px auto 0px auto;
	border:1px solid <?=$colour2;?>;
    border-bottom: 2px solid <?=$colour2;?>; 	border-left: 2px solid <?=$colour2;?>; border-right: 2px solid <?=$colour2;?>;
}

.tertiary {
	background: <?=$colour3;?>;
	width: 1000px;
	margin:0px auto 0px auto;
	padding: 2px 0px 0px 0px;
	border:1px solid <?=$colour3;?>;
	border-top: 1px solid #fff;
    /*border-bottom: 2px solid <?=$colour2;?>;*/
	border-left: 2px solid <?=$colour2;?>;
	border-right: 2px solid <?=$colour2;?>;
}

.navtable {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.9em;
    background-color: <?=$colour1;?>;
	font-weight:bold;
	height:22px;
	padding:0px;
}

.subnavtable {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.9em;
    background-color: <?=$colour2;?>;
	font-weight:bold;
	height:20px;
}

.tertiarynavtable {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.9em;
    background-color: <?=$colour3;?>;
	font-weight:bold;
	height:20px;
}

.logotbl {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.9em;
    background-color: <?=$colour1;?>;
	font-weight:bold;
}

.user {
        position: absolute;
		text-align:right;
		top:48px;
		float:right; right:0px;
		width:100%;
        margin: 0px;
        padding-right: 4px;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 0.8em;
		font-weight:normal;
}

BODY.admin {
	margin: 1px auto 0px auto;
	color : #333;
	background-color: #f5f5f5;
	 font: normal 76% Verdana, Arial, Helvetica, sans-serif;
}

BODY.admin-beta {
	margin: 1px auto 0px auto;
	color : #333;
	background-color: #FFF;
	 font: normal 76% Verdana, Arial, Helvetica, sans-serif;
	background:url(/z/img/static/beta-bg_500x120.jpg) repeat;
}

BODY.popup {
	margin: 1px auto 0px auto;
	color : #333;
	background-color: #FFF;
	 font: normal 76% Verdana, Arial, Helvetica, sans-serif;
}

BODY.popup-mm {
	margin: 1px auto 0px auto;
	padding:0px;
	color : #FFFFFF;
	background-color: #E4E4E4;
	font: normal 76% Verdana, Arial, Helvetica, sans-serif;
}

.homeAdmin-bg {
	width:735px;
	height:599px;
	position: relative;
	top:10px;
	left: 25px;
	background:url(/z/img/static/homepagebg-735x599.jpg) no-repeat;
	padding:5px 0px 15px 0px;
	margin:0px 0px 0px 0px;
}

.homeAdmin-head {
	width:730px;
	height:123px;
}

.homeAdmin-footer {
	width:730px;
	height:123px;
}

.homeAdmin-leftcol {
	width:180px;
	height:300px;
}

.homeAdmin-centercol {
	width:300px;
	height:300px;
}

.homeAdmin-rightcol {
	width:150px;
	height:300px;
}

.homeAdmin-leftcolTop {
	height:90px;
}

.homeAdmin-leftcolCenter {
	height:50px;
}

.homeAdmin-leftcolBottom {
	height:150px;
}

.homeAdmin-bottomLeftcol {
	width:244px;
	height:100px;
}

.homeAdmin-bottomCentercol {
	width:244px;
	height:100px;
}

.homeAdmin-bottomRightcol {
	width:244px;
	height:100px;
}

a {
	color: #6F6F6F;
	text-decoration: none;
}

a:hover {
	color : #330000;
	text-decoration : underline;
}

a:active {
	color : #6F6F6F;
	text-decoration : underline;
}

a.pagenav:hover {
	color : #330000;
	text-decoration : underline;
	background-color:<?=$colour2;?>;
	border-top: 1px solid #FFF;  border-bottom: 1px solid <?=$colour2;?>; border-left: 1px solid #FFF; border-right:1px solid #FFF;
}

a.sub-pagenav:hover {
	text-decoration : underline;
	background-color:#FFFFFF;
	color : #000;
    background-color: #FFF;
	font-size: 0.9em;
	font-weight: bold;
	text-decoration : none;
	padding:5px 5px 6px 5px;
	border-top: 1px solid #FFF;
	border-bottom: 1px solid #FFF;
}

a.tertiary-pagenav:hover {
	text-decoration : underline;
	background-color:#FFFFFF;
	color : #000;
    background-color: #FFF;
	font-size: 0.9em;
	font-weight: bold;
	text-decoration : none;
	padding:5px;
	border-top: 1px solid #FFF;
	border-bottom: 1px solid #FFF;
}

.selected-nav {
	color : #FFFFFF;
    background-color: <?=$colour2;?>;
	font-size: 0.9em;
	font-weight: bold;
	text-decoration : none;
	padding:5px;
	margin:0px;
	border-top: 1px solid #FFF;  
    border-bottom: 1px solid <?=$colour2;?>; 
    border-left: 1px solid #FFF; 
    border-right:1px solid #FFF;
}

.selected-subnav {
	color : #000;
    background-color: #f5f5f5;
	font-size: 0.9em;
	font-weight: bold;
	text-decoration : none;
	padding:3px 3px 4px 3px;
	border-top: 3px solid #f5f5f5;
	border-bottom: 3px solid #f5f5f5;

}

.pagenav {
	color : #FFF;
	font-size: 0.9em;
	font-weight: bold;
	text-decoration : none;
	padding:5px;
}

.sub-pagenav {
	color : #FFF;
	font-size: 0.9em;
	font-weight: bold;
	text-decoration : none;
	padding:5px 5px 6px 5px;
}

.tertiary-pagenav {
	color : #000;
	font-size: 0.9em;
	font-weight: bold;
	text-decoration : none;
	padding:5px;
}

.additional-nav {
	color:#949494;
	font-size:0.9em;
	font-weight:bold;
	text-decoration:none;
	padding:2px 2px 2px 2px;
	margin:0px 10px 4px 10px;
	background-color:#D9E3EA;
}

.content {
		font-size: 1.0em;
		/*padding:10px 25px 10px 25px;*/
		padding:10px 10px 10px 10px;
}

.content-mm {
		font-size: 1.0em;
		padding:10px 10px 10px 10px;
}

.dotted {
	width:725px;
	margin:0px 0px 10px 0px;
	border-bottom: 1px dotted #000;
}

INPUT, TEXTAREA, SELECT {
	color : #000;
	font-size: 0.9em;
}

.select-box {
	color : #000;
	font-size: 0.8em;
}

.button {
	border : solid 1px #cccccc;
	background: #E9ECEF;
	color : #838383;
	font-weight : bold;
	font-size: 0.9em;
	padding: 2px;
	margin:0px;
}

.error {
	color : #C40000;
	font-size: 1.0em;
	font-weight : bold;
}

.cmsTitle {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 2.0em;
	font-weight : normal;
	color : #ffffff;
	padding:0px;
	margin:0px;
}

td {
	font-size: 1.0em;
}

.tbl-output {
	margin:0px 0px 20px 0px;
	color : #000000;
	font-size: 0.8em;
	font-weight : bold;
	background: #FFF;
}

.tbl-add {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.8em;
	font-weight : normal;
	background: #FFF;
}

.tbl-add-asset {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.8em;
	font-weight : normal;
	background: #FFF;
	border:1px solid silver;
}

.tbl-add-asset-deleted {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FF0000;
	border:2px solid white;
}

.tbl-add-asset-status {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FFFFCC;
	border:2px solid white;
}

.tbl-add-asset-default {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.8em;
	font-weight : normal;
	background: #FFF;
	border:1px solid #FF00FF;
}

.tbl-add-asset-head {
	margin:0px 0px 2px 0px;
	color : #000000;
	font-size: 0.8em;
	font-weight : normal;
	background: #FFF;
	border:1px solid silver;
}

.tbl-add-asset-head-default {
	margin:0px 0px 2px 0px;
	color : #000000;
	font-size: 0.8em;
	font-weight : normal;
	background: #FFF;
	border:1px solid #FF00FF;
}

.tbl-search-net {
	margin:0px 0px 2px 0px;
	color : #000000;
	font-size: 1.0em;
	font-weight : normal;
	background: #FFF;
	border:1px solid silver;
}

.tbl-keyword-association {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 1.0em;
	font-weight : normal;
	background: #FFF;
	border:1px solid silver;
}

.tbl-transcode-head {
	margin:0px 0px 2px 0px;
	color : #000000;
	font-size: 1.0em;
	font-weight : normal;
	background: #FFF;
	border:1px solid silver;
}

.tbl-transcode {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FFF;
	border:0px solid silver;
}

.tbl-heading {
	margin:0px 0px 10px 0px;
	color : #000000;
	border:1px solid #000;
	font-size: 1.8em;
	font-weight : bold;
}

.tbl-output-declined {
	margin:0px 0px 20px 0px;
	color : #000000;
	font-size: 1.0em;
	font-weight : bold;
	background: #A4BB9D;
}

.td-question {
	color:#62ba2c;
	font:normal normal bold 1.2em/1.0em Arial, Helvetica, sans-serif;
}

.td-track-edit {
	color:#000;
	font:normal normal bold 1.1em/1.0em Arial, Helvetica, sans-serif;
    padding-right:5px;
    text-align:right;
}

.td-head {
	border-bottom:1px solid <?=$colour1;?>;
	color:#474747;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}

.td-data {
	color:#474747;
	font:normal normal normal 1.0em/1.0em Arial, Helvetica, sans-serif;
}

.td-assets-data {
	color:#000;
	background-color:#ecf8f9;
	font:normal normal normal 1.0em/1.0em Arial, Helvetica, sans-serif;
}

.td-add {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.85em;
	font-weight : normal;
	background: #FFF;
	border-bottom:0px solid #AAAAAA;
}

.td-add-deleted {
	margin:0px 0px 0px 0px;
	padding:4px;
	color : #FFF;
	font-size: 1.3em;
	font-weight : bold;
	background: #FF0000;
	border-bottom:1px solid red;
}

.td-add-status {
	margin:0px 0px 0px 0px;
	padding:4px;
	color : #000000;
	font-size: 1.1em;
	font-weight : bold;
	background: #FFFFCC;
	border-bottom:0px solid #fff;
}

.td-add-bordered {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.85em;
	font-weight : normal;
	background: #FFF;
	border:1px solid silver;
}

.td-transcode {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FFF;
	border-bottom:1px solid silver;
}

.td-add-cs-keyword {
	color:#000;
	background-color:#E4E4E4;
	font:normal normal normal 1.0em/1.0em Arial, Helvetica, sans-serif;
	font-weight : bold;
	border:1px solid silver;
}

.th-search-net {
	margin:0px 0px 0px 0px;
	padding:3px 3px 3px 5px;
	color : #000000;
	font-size: 0.85em;
	font-weight : bold;
	background: #E4E4E4;
	border-bottom:1px solid #E4E4E4;
}

.th-add {
	margin:0px 0px 0px 0px;
	padding:3px 3px 3px 5px;
	color : #000000;
	font-size: 0.85em;
	font-weight : bold;
	background: #E4E4E4;
	border-bottom:1px solid #E4E4E4;
}

.th-add-sorted {
	margin:0px 0px 0px 0px;
	padding:3px 3px 3px 5px;
	color : #000000;
	font-size: 0.85em;
	font-weight : bold;
	background: #99ff33;
	border-bottom:1px solid #E4E4E4;
}

.th-add-portlet {
	margin:0px 0px 0px 0px;
	padding:3px 3px 3px 5px;
	color : #000000;
	font-size: 0.7em;
	font-weight : bold;
	background: #E4E4E4;
	border-bottom:1px solid #E4E4E4;
}

.td-add-comment {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.8em;
	font-weight : normal;
	background: #FFF;
	border-bottom:0px solid #AAAAAA;
}

.td-toolbox {
	margin:0px 0px 0px 0px;
	padding:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.85em;
	font-weight : bold;
	background: #fff;
	border:0px solid #fff;
}

form {
	margin: 0px 0px 0px 0px;
	padding: 0px;
	display:inline;
}

.input-box {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.2em;
	font-weight : normal;
	width:300px;
}

.input-box-nosize {
	border : solid 0px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1em;
	font-weight : normal;
}

.input-box-50px {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.2em;
	font-weight : normal;
	width:50px;
}

.input-box-100px {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.2em;
	font-weight : normal;
	width:100px;
}

.input-box-150px {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.2em;
	font-weight : normal;
	width:150px;
}

.input-box-track-edit {
	font-family: Arial, Helvetica, sans-serif;
    display:block;
    width:775px;
    height:20px;
    font-size:14px;
    color:#000;
    background-color:#fff;
    border:1px solid #ccc;
}

.input-box-pack-edit {
	font-family: Arial, Helvetica, sans-serif;
    display:block;
    width:805px;
    height:20px;
    font-size:14px;
    color:#000;
    background-color:#fff;
    border:1px solid #ccc;
}

.input-box-theme-edit {
	font-family: Arial, Helvetica, sans-serif;
    display:block;
    width:500px;
    height:20px;
    font-size:14px;
    color:#000;
    background-color:#fff;
    border:1px solid #ccc;
}

.input-box-user {
	font-family: Arial, Helvetica, sans-serif;
    display:block;
    width:500px;
    height:20px;
    font-size:14px;
    color:#000;
    background-color:#fff;
    border:1px solid #ccc;
}

.input-box-search {
    display:block;
    width:885px;
    height:15px;
    font-size:14pt;
    font-weight:bold;
    line-height:1.42857143;
    color:#000;
    background-color:#fff;
    background-image:none;
    border:2px solid #ccc;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    padding:6px 12px;
}

.select-box-150px {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.0em;
	font-weight : normal;
	width:150px;
}

.select-box-no-size {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.0em;
	font-weight : normal;
}

.required {
	/*
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	width:300px;
	*/
}

.required-checkbox {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
}

.input-box-short {
	border : dotted 1px #000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	width:320px;
}

.input-box-file {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.1em;
	font-weight : normal;
}

.input-box-short2 {
	border : dotted 1px #000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	width:300px;
}

.input-box-alias {
	border : dotted 1px #000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	width:190px;
}

.asset-input-box {
	border : dotted 0px #000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.0em;
	font-weight : normal;
	width:400px;
}

.textarea-box-nosize {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
}

.textarea-box-100-50px {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	width:100px;
	height:50px;
}

.textarea-box-100-25px {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	width:100px;
	height:25px;
}

.textarea-box-25 {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	width:300px;
	height:25px;
}

.textarea-box-50 {
	border : solid 1px #AAAAAA;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	width:300px;
	height:50px;
}

.textarea-box-aboutme {
	border : dotted 1px #000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.0em;
	font-weight : normal;
	width:300px;
	height:100px;
}

.text-textarea-box {
    border:1px solid #ccc;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.0em;
	font-weight : normal;
	width:800px;
	height:50px;
}

.asset-filepath-box {
	border : dotted 1px #000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.0em;
	font-weight : normal;
	width:200px;
	height:15px;
}

.input-title {
	border : solid 1px #cccccc;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.0em;
	font-weight : bold;
	width:735px;
}

.input-error {
	border : solid 1px #cccccc;
	width:130px;
}

.msg {
	color : #330000;
	font-size: 1.0em;
	font-weight : bold;
	background: #FFFF99;
	border : dotted 1px #000;
	padding:5px;
}

.msg-warn {
	color : #fff;
	font-size: 1.0em;
	font-weight : bold;
	background: #FF0000;
	border : dotted 1px #000;
	padding:5px;
}

.warn {
	color : #C40000;
	font-size: 0.9em;
	font-weight : bold;
}

.debug {
	font-family: Arial, Helvetica, sans-serif;
	color : #000;
	font-size: 0.9em;
	font-weight : bold;
	background: #FFF;
	padding:5px;
	margin:5px;
}

pre  {
	font-family: Arial, Helvetica, sans-serif;
	color : #000;
	font-size: 1.0em;
	font-weight : normal;
	background: #FFF;
	padding:5px;
	margin:5px;
}

.left {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.0em;
	font-weight : bold;
	color : #000;
	display:block;
	position:relative;
	float:left;
}
.right {
	display:block;
	position:relative;
	float:right;
}
hr.clear {
	clear:both;
	display:block;
	visibility:hidden;
	width:100%;
	margin:0px;
	padding:0px;
}
.listBlock {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:0px 0px 4px 0px;
	padding:0px 0px 0px 0px;
}
/*
li {
	margin:0px 0px 0px 16px;
	padding:0px 0px 0px 0px;
	list-style:square outside url(/z/img/static/bulletSquare_7x11.gif);
	color:#62ba2c;
	font:normal normal normal 1.0em/1.4em Arial, Helvetica, sans-serif;
}
li.linkList {
	margin:0px 0px 0px 16px;
	padding:0px 0px 0px 0px;
	list-style:square outside url(/z/img/static/bulletLink_7x11.gif);
	color:#62ba2c;
	font:normal normal normal 1.0em/1.2em Arial, Helvetica, sans-serif;
}
li.numList {
	margin:0px 0px 0px 24px;
	padding:0px 0px 0px 0px;
	list-style:decimal-leading-zero outside;
	color:#62ba2c;
	font:normal normal normal 1.0em/1.2em Arial, Helvetica, sans-serif;
}
*/

h1 {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:0px 0px 0px 0px;
	padding:0px 0px 0px 0px;
	color:#747474;
	font:normal normal bold 2.0em Arial, Helvetica, sans-serif;
	border-bottom:1px solid silver;
	letter-spacing:0.02em;
	font-smooth:always;
}
h2 {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:0px 0px 4px 0px;
	padding:0px 0px 0px 0px;
	color:#FF0066;
	font:normal normal bold 1.4em/1.0em Arial, Helvetica, sans-serif;
	letter-spacing:0.02em;
}
/*
h3 {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:8px 0px 4px 0px;
	padding:0px 0px 0px 0px;
	color:#474747;
	font:normal normal bold 1.4em/1.0em Arial, Helvetica, sans-serif;
}
*/
h4 {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:8px 0px 0px 0px;
	padding:0px 0px 0px 0px;
	color:#474747;
	font:normal normal bold 1.2em/1.4em Arial, Helvetica, sans-serif;
}

/*
ul {
	margin:0px 0px 0px 0px;
	padding:0px 0px 0px 0px;
}
li {
	margin:0px 0px 0px 16px;
	padding:0px 0px 0px 0px;
	list-style:square outside url(/z/img/static/bulletSquare_7x11.gif);
	color:#62ba2c;
	font:normal normal normal 1.2em/1.4em Arial, Helvetica, sans-serif;
}
li.linkList {
	margin:0px 0px 0px 16px;
	padding:0px 0px 0px 0px;
	list-style:square outside url(/z/img/static/bulletLink_7x11.gif);
	color:#62ba2c;
	font:normal normal normal 1.2em/1.4em Arial, Helvetica, sans-serif;
}
li.linkListMore { /
	margin:0px 0px 0px 16px;
	padding:0px 0px 0px 0px;
	list-style:square outside url(/z/img/static/bulletLinkMore_7x11.gif);
	color:#62ba2c;
	font:normal normal bold 1.2em/1.4em Arial, Helvetica, sans-serif;
}
li.downloadList {
	margin:0px 0px 0px 16px;
	padding:0px 0px 0px 0px;
	list-style:square outside url(/z/img/static/bulletDownload_7x11.gif);
	color:#62ba2c;
	font:normal normal normal 1.2em/1.4em Arial, Helvetica, sans-serif;
}
*/

hr.clear {
	clear:both;
	display:block;
	visibility:hidden;
	width:100%;
	margin:0px;
	padding:0px;
}
.td-actions {
	padding:4px 4px 4px 4px;
	color:#949494;
	background-color:#ecf8f9;
	text-align:right;
}
.buttonPositive {
	background-color:#62ba2c;
	color:#ffffff;
	border:2px solid #ccc;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
    height:31px;
}
.buttonPositive-mm {
	background-color:#62ba2c;
	color:#ffffff;
	border:1px solid #575757;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}
.buttonAddPortlet {
	background-color:#939393;
	color:#ffffff;
	border:1px solid #575757;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}

.buttonBlue-mm {
	background-color:#0099FF;
	color:#000000;
	border:1px solid #575757;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}
.buttonSavePortlet-mm {
	background-color:#0099FF;
	color:#ffffff;
	border:1px solid #575757;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}
.buttonPicker-mm {
	background-color:#FF9900;
	color:#000000;
	border:1px solid #575757;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}
.buttonPortletCancel-mm {
	background-color:#CACA00;
	color:#000000;
	border:1px solid #575757;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}

.buttonNegative {
	background-color:#ff0000;
	color:#ffffff;
	border:2px solid #ccc;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
    height:31px;
}
.buttonBlue {
	background-color:#0099FF;
	color:#ffffff;
	border:2px solid #ccc;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
    height:31px;
}

.welcomeBlock {
	display:block;
	float:left;
	position:relative;
	width:135px;
	margin:0px 4px 4px 0px;
	padding:4px 4px 4px 4px;
	border-top:1px solid <?=$colour1;?>; border-right:1px solid <?=$colour1;?>; border-bottom:1px solid <?=$colour1;?>;
	background-color:#ecf8f9;
	text-align:left;
}
.yourPhoneBlock {
	display:block;
	float:left;
	position:relative;
	width:143px;
	margin:0px 4px 4px 0px;
	padding:0px 0px 0px 0px;
	border-top:1px solid #ebebeb; border-right:1px solid #ebebeb; border-bottom:1px solid #ebebeb;
	background-color:#ffffff;
	text-align:left;
}
.yourPhoneImage {
	display:block;
	float:left;
	position:relative;
	width:44px;
	margin:0px 0px 0px 0px;
	padding:0px 0px 0px 0px;
	text-align:left;
}
.yourPhoneText {
	display:block;
	float:left;
	position:relative;
	margin:0px 0px 0px 0px;
	padding:4px 0px 0px 0px;
	text-align:left;
}
.yourPhoneLinkBlock {
	display:block;
	position:relative;
	float:left;
	margin:0px 0px 0px 0px;
	padding:0px 0px 0px 0px;
}
.boxOutLeft {
	display:block;
	float:left;
	position:relative;
	width:135px;
	margin:0px 0px 4px 0px;
	padding:4px 4px 4px 4px;
	border-top:1px solid #ebebeb; border-right:1px solid #ebebeb; border-bottom:1px solid #ebebeb;
	text-align:left;
}
.boxOutRight {
	display:block;
	float:right;
	position:relative;
	width:135px;
	margin:0px 0px 4px 4px;
	padding:4px 4px 4px 4px;
	border-top:1px solid #ebebeb; border-bottom:1px solid #ebebeb; border-left:1px solid #ebebeb;
	text-align:left;
}
.mainPromo {
	display:block;
	float:left;
	position:relative;
	width:441px;
	margin:0px 0px 4px 0px;
	padding:0px 4px 0px 4px;
	text-align:left;
}
.skyScraper {
	display:block;
	float:left;
	position:relative;
	width:141px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	text-align:left;
}
.homeSubPromo {
        display:block;
        float:left;
        position:relative;
        width:241px;
        margin:0px 0px 0px 0px;
        padding:0px 4px 0px 4px;
        background:url(/z/img/static/bgSubPromo_249x117.jpg) no-repeat bottom left;
}
.homeSubPromo01 {
	display:block;
	float:left;
	position:relative;
	width:241px;
	height:117px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	border-right:1px solid <?=$colour1;?>;
	background:url(/z/img/content/homeSubPromo01_249x117.jpg) no-repeat bottom left;
}
.homeSubPromo02 {
	display:block;
	float:left;
	position:relative;
	width:241px;
	height:117px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	background:url(/z/img/content/homeSubPromo02_249x117.jpg) no-repeat bottom left;
}
.homeSubPromo03 {
	display:block;
	float:left;
	position:relative;
	width:241px;
	height:117px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	border-left:1px solid <?=$colour1;?>;
	background:url(/z/img/content/homeSubPromo03_249x117.jpg) no-repeat bottom left;
}
.subPromoTitle {
        display:block;
        position:relative;
        float:left;
        width:100%;
        margin:0px 0px 0px 0px;
        padding:2px 0px 4px 0px;
        background-color:#62ba2c;
        color:#ffffff;
        font:normal normal bold 1.2em/1.1em Arial, Helvetica, sans-serif;
        text-indent:4px;
}
.subPromoKicker {
        color:#474747;
}

.subPromoImage {
        display:block;
        position:relative;
        float:left;
        width:105px;
        margin:0px 0px 0px 0px;
        padding:0px 0px 0px 0px;
}

.subPromoText {
        display:block;
        position:relative;
        float:right;
        width:132px;
        padding:10px 4px 0px 0px;
        font:normal normal normal 1.2em/1.2em Arial, Helvetica, sans-serif;
        text-align:right;
        vertical-align:middle;
}

.inPageSingleCol {
	display:block;
	float:left;
	position:relative;
	width:141px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	text-align:left;
}
.inPageDoubleCol {
	display:block;
	float:left;
	position:relative;
	width:292px;
	margin:0px 0px 0px 0px;
	padding:0px 3px 0px 4px;
	text-align:left;
}
.inPageTripleCol {
	display:block;
	float:left;
	position:relative;
	width:441px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	text-align:left;
}
.inPageQuadCol {
	display:block;
	float:left;
	position:relative;
	width:591px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	text-align:left;
}
.inPageFullCol {
	display:block;
	float:left;
	position:relative;
	width:741px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	text-align:left;
}
.globalHomeFullCol {
	display:block;
	float:left;
	position:relative;
	width:743px;
	margin:0px 0px 0px 0px;
	padding:0px 0px 0px 6px;
	text-align:left;
}
.globalHomeMainFlash {
	display:block;
	float:left;
	position:relative;
	width:551px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 0px;
	text-align:left;
}
.globalHomeSubFlash {
	display:block;
	float:left;
	position:relative;
	width:171px;
	margin:0px 0px 4px 0px;
	padding:4px 4px 4px 4px;
	border:1px solid #ebebeb;
	text-align:left;
}
.globalHomeSubPromo {
	display:block;
	float:left;
	position:relative;
	width:171px;
	margin:0px 4px 0px 0px;
	padding:4px 4px 4px 4px;
	border:1px solid #ebebeb;
}
.thumbnailBlock {
	display:block;
	float:left;
	position:relative;
	width:131px;
	margin:0px 0px 2px 0px;
	padding:4px 4px 4px 4px;
	border:1px solid #ebebeb;
	text-align:left;
}
.thumbnailDetail {
	display:block;
	float:left;
	position:relative;
	width:131px;
	margin:0px 0px 8px 0px;
	padding:4px 4px 0px 4px;
	border:1px solid #ebebeb;
	text-align:left;
}
.imageBlock {
	display:block;
	float:left;
	position:relative;
	width:282px;
	margin:0px 0px 2px 0px;
	padding:4px 4px 4px 4px;
	border:1px solid #ebebeb;
	text-align:left;
}
.imageDetail {
	display:block;
	float:left;
	position:relative;
	width:282px;
	margin:0px 0px 8px 0px;
	padding:4px 4px 4px 4px;
	border:1px solid #ebebeb;
	text-align:left;
}
.faqItemsBlock {
	display:block;
	float:left;
	position:relative;
	width:282px;
	margin:0px 0px 2px 0px;
	padding:0px 4px 4px 4px;
	border:1px solid #ebebeb;
	text-align:left;
}
.contextPromo01 {
	display:block;
	float:left;
	position:relative;
	width:123px;
	margin:0px 0px 0px 0px;
	padding:0px 4px 0px 4px;
	background:url(/z/img/content/contextPromo01_131x116.jpg) no-repeat top left;
}
/* Other core components end here */
/* Form styles start here */
.formBlock {
	display:block;
	position:relative;
	float:left;
	width:441px;
	margin:8px 0px 8px 0px;
	padding:0px 0px 0px 0px;
	border-top:2px solid #474747; border-bottom:8px solid #474747;
}
.formRow {
	display:block;
	position:relative;
	float:left;
	width:441px;
	margin:0px 0px 0px 0px;
	padding:0px 0px 0px 0px;
	border-bottom:1px solid <?=$colour1;?>;
	background-color:#ebebeb;
}
.formSectionTitle {
	display:block;
	position:relative;
	float:left;
	width:437px;
	margin:4px 0px 0px 0px;
	padding:2px 0px 2px 4px;
	background-color:<?=$colour1;?>;
	font:normal normal bold 1.2em/1.0em Arial, Helvetica, sans-serif;
	color:#474747;
}
.formTitle {
	display:block;
	position:relative;
	float:left;
	width:141px;
	margin:0px 0px 0px 0px;
	padding:4px 4px 2px 4px;
	font:normal normal normal 1.2em/1.0em Arial, Helvetica, sans-serif;
}
.formTitleRequired {
	font-weight:bold;
}
.formExplanation {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:4px 0px 0px 0px;
	padding:2px 0px 4px 4px;
	background-color:#ffffff;
	font:normal normal normal 1.2em/1.0em Arial, Helvetica, sans-serif;
	color:#000000;
}
.formContent {
	display:block;
	position:relative;
	float:left;
	width:282px;
	margin:0px 0px 0px 0px;
	padding:2px 4px 2px 4px;
	background-color:#ffffff; /* was #ecf8f9 */
	font:normal normal normal 1.2em/1.0em Arial, Helvetica, sans-serif;
}
.formRowHighlight {
	display:block;
	position:relative;
	float:left;
	width:441px;
	margin:0px 0px 0px 0px;
	padding:6px 0px 4px 0px;
	border-bottom:1px solid <?=$colour1;?>;
	background-color:<?=$colour1;?>;
}
.formDropDown {
	background-color:#ecf8f9;
	font:normal normal normal 1.0em/1.0em Arial, Helvetica, sans-serif;
	color:#000000;
}
.formInputPos {
	padding:2px 2px 2px 2px;
	background-color:#62ba2c;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
	color:#ffffff;
}
.formNote {
	font:normal normal normal 0.9em/1.2em Arial, Helvetica, sans-serif;
	margin-top:2px;
}
.formFooter {
	display:block;
	position:relative;
	float:left;
	width:433px;
	margin:0px 0px 0px 0px;
	padding:4px 4px 4px 4px;
	font:normal normal bold 1.2em/1.0em Arial, Helvetica, sans-serif;
	color:#ffffff;
	text-align:right;
}
/* Form styles end here */
/* Other general styles start here */
.bodyText {
	font:normal normal normal 1.2em/1.2em Arial, Helvetica, sans-serif;
	margin-bottom:12px;
}
.normalText {
	font:normal normal normal 1.2em/1.2em Arial, Helvetica, sans-serif;
}
.globalCorpInfoPromoTitle {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:0px 0px 0px 0px;
	padding:2px 4px 4px 0px;
	color:#474747;
	font:normal normal bold 1.2em/1.1em Arial, Helvetica, sans-serif;
}
.globalCorpInfoPromoTitleStart {
	color:#474747;
}
.globalCorpInfoPromoText {
	display:block;
	position:relative;
	float:left;
	padding:0px 4px 0px 0px;
	font:normal normal normal 1.2em/1.2em Arial, Helvetica, sans-serif;
	text-align:left;
	vertical-align:middle;
}
.subPromoTitle {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:0px 0px 0px 0px;
	padding:2px 0px 4px 0px;
	background-color:#62ba2c;
	color:#ffffff;
	font:normal normal bold 1.2em/1.1em Arial, Helvetica, sans-serif;
	text-indent:4px;
}
.subPromoTitleStart {
	color:#474747;
}
.subPromoText {
	display:block;
	position:relative;
	float:right;
	padding:10px 4px 0px 0px;
	font:normal normal normal 1.2em/1.2em Arial, Helvetica, sans-serif;
	text-align:right;
	vertical-align:middle;
}
.contextPromoText {
	display:block;
	position:relative;
	float:right;
	padding:55px 4px 0px 0px;
	font:normal normal normal 1.2em/1.2em Arial, Helvetica, sans-serif;
	text-align:right;
	vertical-align:middle;
}
.listBlock {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:0px 0px 4px 0px;
	padding:0px 0px 0px 0px;
}
.listText {
	color:#474747;
}
.dateText {
	font:normal normal normal 1.0em/1.0em Arial, Helvetica, sans-serif;
	color:#474747;
}
.greenBullet {
	color:#478820;
	font-weight:bold;
}
.thumbnailTitle {
	font:normal normal bold 1.2em/1.2em Arial, Helvetica, sans-serif;
	margin-bottom:5px;
}
.thumbnailText {
	font:normal normal normal 1.2em/1.2em Arial, Helvetica, sans-serif;
	margin-bottom:5px;
}
tr.rowdata {
	background-color:#ebebeb;
}
td.header {
	background-color:#ebebeb;
}
.globalCorpInfoTitle {
	display:block;
	position:relative;
	float:left;
	width:100%;
	margin:8px 0px 0px 0px;
	padding:0px 0px 0px 0px;
	color:#474747;
	font:normal normal bold 1.2em/1.4em Arial, Helvetica, sans-serif;
}
/* Other general styles end here */
a.bullet, a.bullet:link, a.bullet:visited, a.bullet:active {
	color:#478820;
	text-decoration:underline;
}
a.bullet:hover {
	color:#000000;
	text-decoration:underline;
}

a.login-text, a.login-text:link, a.login-text:visited, a.login-text:active {
	color:#fff;
}
a.login-text:hover {
	color:#000;
	background-color:#fff;
}

/* jQuery UI */

#effect { height: 100px; padding: 0.4em; position: relative; }
#effect h3 { margin: 0; padding: 0.4em; text-align: center; }

#tooltip {
	position: absolute;
	z-index: 3000;
	border: 1px solid #FFA8B9;
	background-color: #FFFFCA;
	padding: 5px;
	opacity: 0.85;
	-moz-border-radius: 4px/*{cornerRadius}*/;
	-webkit-border-radius: 4px/*{cornerRadius}*/;
	border-radius: 4px;
}
#tooltip h3, #tooltip div { margin: 0; }

.tool-tip { }

span.hilite {}
span { cursor:pointer; }

input.error,textarea.error{
  border:1px solid #f00;
}

label.error{ color:#f00; }

.info-bar {
	margin:0px 0px 25px 0px;
	height:20px;
}

.info-bar-search {
	padding:5px 5px 8px 5px;
	margin:0px 0px 5px 0px;
	height:55px;
	font-family:Verdana,Arial,sans-serif;
	font:1.2em Verdana,Arial,Helvetica,sans-serif;
	border:1px solid #AAAAAA;
}

.info-bar-content-right {
	text-align:right;
	padding:0px 10px 0px 0px;
}

.info-bar-content-center {
	text-align:center;
	padding:0px 10px 0px 0px;
}

.msg-error {
	color:red;
	font-weight:bold;
    font-size:8pt;
    float:left;
}

.msg-success {
	color:#00CC00;
	font-weight:bold;
}

.my-header {
	cursor:pointer;
	margin-top:1px;
	position:relative;
	-moz-border-radius:4px 4px 4px 4px;
	border-radius:4px 4px 4px 4px;
	background:url("/z/themes/base/images/ui-bg_glass_75_e6e6e6_1x400.png") repeat-x scroll 50% 50% #E6E6E6;
	border:1px solid #D3D3D3;
	color:#555555;
	font-weight:normal;
	outline:medium none;
	font-size:100%;
	line-height:1.3;
	list-style:none outside none;
	margin:0;
	padding:7px;
	text-decoration:none;
	font-family:Verdana,Arial,sans-serif;
	font:76% Verdana,Arial,Helvetica,sans-serif;
}

.my-span-title {
	position:absolute;
	top: -11px;
	left:7px;
	color:#555555;
	font-weight:normal;
	margin:0;
	padding:0;
	text-decoration:none;
	font-family:Verdana,Arial,sans-serif;
	font:140% Verdana,Arial,Helvetica,sans-serif;
	padding:20px;
	margin-left:3px;
}

.my-span-cancel {
	position:absolute;
	top: 8px;
	right:10px;
	color:#555555;
	font-weight:normal;
	margin:0;
	padding:0;
	text-decoration:none;
	font-family:Verdana,Arial,sans-serif;
	font:120% Verdana,Arial,Helvetica,sans-serif;
	/*padding:20px;
	margin-left:660px;*/
}

.my-span {
	left:0.5em;
	margin-top:2px;
	position:relative;
	top:50%;
	background-image:url("/z/themes/base/images/ui-icons_888888_256x240.png");
	background-position:-32px -16px;
	/*background-image:url("/z/themes/base/images/ui-icons_222222_256x240.png");*/
	height:16px;
	width:16px;
	background-repeat:no-repeat;
	display:block;
	overflow:hidden;
	text-indent:-99999px;
}

.ac_results {
	padding: 0px;
	border: 1px solid black;
	background-color: white;
	overflow: hidden;
	z-index: 99999;
}

.ac_results ul {
	width: 100%;
	list-style-position: outside;
	list-style: none;
	padding: 0;
	margin: 0;
}

.ac_results li {
	margin: 0px;
	padding: 2px 5px;
	cursor: default;
	display: block;
	/*width: 100%;*/
	font: menu;
	font-size: 12px;
	line-height: 16px;
	overflow: hidden;
	font-family:Verdana,Arial,sans-serif;
	color: black;
}

.ac_loading {
	background: white url('indicator.gif') right center no-repeat;
}

.ac_odd {
	background-color: #EBEBEB;
}

.ac_over {
	background-color: #DDDDDD;
	color: white;
}

.no-border {
	border:0px solid #fff;
}

.with-border {
	border : solid 1px #AAAAAA;
}

.show-item {
	visibility:visible;
}

.hide-item {
	display:none;
}

.hide-div-item {
	display:none;
	padding:0px;
	margin:0px;
}

.show-div-item {
	display:inline; /* we don't do blocks! */
	padding:0px;
	margin:0px;
}

.td-severity-0 {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FFFFFF;
	border-bottom:1px solid silver;
}

.td-severity-1 {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FFFFCC;
	border-bottom:1px solid silver;
}

.td-severity-2 {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FFFF33;
	border-bottom:1px solid silver;
}

.td-severity-3 {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FF9933;
	border-bottom:1px solid silver;
}

.td-severity-4 {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.9em;
	font-weight : normal;
	background: #FF0000;
	border-bottom:1px solid silver;
}

.watchdog-msg-detail {
	border : solid 0px silver;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.9em;
	font-weight : normal;
	width:975px;
	height:200px;
	padding:2px 2px 2px 2px;
}

/* Row Highlighter */

.highlight-on {background-color: #ffffcc;}
.highlight-off {background-color: #fff;}

/* PORTLETS */

#editbox {
 color: #333;
 background: #fff;
 padding:0px 0px 10px 0px;
 margin:0px 0px 10px 0px;
 border: #000 solid 0px;
}

#components {
 padding:0px 0px 0px 0px;
 margin:0px 0px 0px 0px;
}

#components li {
	list-style: none;
	width: 245px;
	padding:1px;
	margin:0px 0px 1px 0px;
	background-color:#F0F0F0;
	border: #A3A3A3 solid 1px;
	-moz-border-radius: 4px/*{cornerRadius}*/;
	-webkit-border-radius: 4px/*{cornerRadius}*/;
	border-radius: 4px;
	color:#fff;
}

.buttonDeleteIcon {
	background:url('/z/img/static/portletDeleteIcon.png') no-repeat;
	color:#ffffff;
	border:0px solid #575757;
	width:22px;
	height:22px;
	margin:2px;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}
.buttonEditIcon {
	background:url('/z/img/static/portletEditIcon.png') no-repeat;
	color:#ffffff;
	border:0px solid #575757;
	width:22px;
	height:22px;
	margin:2px;
	font:normal normal bold 1.0em/1.0em Arial, Helvetica, sans-serif;
}
.buttonBox {
	position: relative;
	top:0px;
	padding:0px;
	margin:0px;
	border:0px solid #A3A3A3;
}
.buttonBoxTD {
	padding:0px;
	margin:0px;
	border:1px solid #A3A3A3;
}

.ul-portlets {
	padding:0px;
	margin:0px;
}

#debugOutput {

}

.portlet-field-group-a {
	border : solid 1px white;
	background-color:#EEEEEE;
	/*
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	*/
}

.portlet-field-group-b {
	border : solid 1px white;
	background-color:#E9E9E9;
	/*
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.3em;
	font-weight : normal;
	*/
}

/* Content Browser */

.display-asset-result {
	border-bottom:1px solid #727272;
}

.display-asset-result-alt {
	/*border-top:2px solid #33CC33;*/
	border-bottom:0px solid #33CC33;
	background-color:#33CC33;
}

.edit-content {
	/*display:none;
	width:770px;
	border:2px solid #33CC33;
	background-color:#006CD9;
	margin:2px;*/
}

.edit-content-border {
	border:2px solid #33CC33;
	padding:2px;
	background-color: #33CC33;
	/*display:none;
	width:770px;
	border:2px solid #33CC33;
	background-color:#006CD9;
	margin:2px;*/
}

.edit-content-panel {
	display:none;
	width:100%;
	/*border:2px solid #33CC33;
	background-color:#006CD9;
	margin:2px;*/
}

.tbl-add-asset-head-alt {
	margin:0px 0px 2px 0px;
	color : #000000;
	font-size: 0.8em;
	font-weight : normal;
	background-color: #33CC33;
	border-color:#33CC33;
}

.td-add-alt {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.85em;
	font-weight : normal;
	background-color: #33CC33;
	border-bottom:0px solid #AAAAAA;
}

.opaque1 {
	margin: 1px auto 0px auto;
	color : #333;
	font: normal 76% Verdana, Arial, Helvetica, sans-serif;
	filter:alpha(opacity=20);
	opacity: 0.2;
	-moz-opacity:0.2;
	background-color:#000000;
}

.darkenBackground {
	background-color: rgb(0, 0, 0);
	opacity: 0.7; /* Safari, Opera */
	-moz-opacity:0.70; /* FireFox */
	filter: alpha(opacity=70); /* IE */
	z-index: 20;
	height: 100%;
	width: 100%;
	background-repeat:repeat;
	position:fixed;
	top: 0px;
	left: 0px;
}

#fuzz{ position:absolute; top:0; left:0; width:100%; z-index:100; background: url('/z/img/static/fuzz.gif'); display:none; text-align:left; }
.fuzz-msgbox{ position:absolute; width:300px; height:200px; z-index:200; border:5px solid #222; background: #FFF; top: 50%; left: 50%; margin-top: -100px; margin-left: -150px; }
#dim{ position:absolute; top:0; left:0; width:100%; z-index:100; background: url('/z/img/static/dim.png'); display:none; text-align:left; }
.dim-msgbox{ position:absolute; width:200px; height:50px; z-index:200; border:5px solid #222; background: #FFF; top: 50%; left: 50%; margin-top: -100px; margin-left: -120px; text-align:center;vertical-align:middle; padding:10px }

/*** Furniture Browser ***/

.furnitureThumbnail{
position: relative;
z-index: 0;
}

.furnitureThumbnail:hover{
background-color: transparent;
z-index: 50;
}

.furnitureThumbnail span{ /*CSS for enlarged image*/
position: absolute;
background-color: lightyellow;
padding: 5px;
left: -1000px;
border: 1px dashed gray;
visibility: hidden;
color: black;
text-decoration: none;
}

.furnitureThumbnail span img{ /*CSS for enlarged image*/
border-width: 0;
padding: 2px;
}

.furnitureThumbnail:hover span{ /*CSS for enlarged image on hover*/
visibility: visible;
top: 0;
left: 60px; /*position where enlarged image should offset horizontally */
}

/* SMS Scheduler */
.td-sms-pending {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.7em;
	font-weight : normal;
	background-color: #ffffcc;
	border-bottom:1px solid #AAAAAA;
}
.td-sms-finished {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.7em;
	font-weight : normal;
	background-color: #ccffcc;
	border-bottom:1px solid #AAAAAA;
}
.td-sms-running {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.7em;
	font-weight : normal;
	background-color: #33ffff;
	border-bottom:1px solid #AAAAAA;
}

.td-sms-batch-failed {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.7em;
	font-weight : normal;
	background-color: #ff0000;
	border-bottom:1px solid #AAAAAA;
}
.td-sms-batch-pending {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.7em;
	font-weight : normal;
	background-color: #f9f9f9;
	border-bottom:1px solid #AAAAAA;
}
.td-sms-batch-completed {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.7em;
	font-weight : normal;
	background-color: #ccffcc;
	border-bottom:1px solid #AAAAAA;
}
.td-sms-batch-running {
	margin:0px 0px 0px 0px;
	color : #000000;
	font-size: 0.7em;
	font-weight : normal;
	background-color: #33ffff;
	border-bottom:1px solid #AAAAAA;
}
