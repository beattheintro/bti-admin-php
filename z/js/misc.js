/*
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : misc.js
	// Author: Jason Rastrick
	// Date: 13/03/2015
	// Version: 1.0
	// Description: Global JavaScript file for this client/site
	// ----------------------------------------------------------- >>>>>>>>>>
*/

function openAnySize (url, w, h, name) {
	var winopts = "toolbar=no,location=no,directories=no,status=no,";
	winopts = winopts + "menubar=no,scrollbars=yes,resizable=yes,";
	winopts = winopts + "width=" + w + ",height=" + h; remote = window.open(url,name,winopts);
}

function openAnySizeWithToolbar (url, w, h, name) {
	var winopts = "toolbar=yes,location=no,directories=no,status=no,";
	winopts = winopts + "menubar=no,scrollbars=yes,resizable=yes,";
	winopts = winopts + "width=" + w + ",height=" + h; remote = window.open(url,name,winopts);
}

function openAnySizeWithStatus (url, w, h, name) {
	var winopts = "toolbar=no,location=no,directories=no,status=yes,";
	winopts = winopts + "menubar=no,scrollbars=yes,resizable=yes,";
	winopts = winopts + "width=" + w + ",height=" + h; remote = window.open(url,name,winopts);
}

function findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function showHideLayers() {
	var i,p,v,obj,args=showHideLayers.arguments;
	for (i=0; i<(args.length-2); i+=3) if ((obj=findObj(args[i]))!=null) { v=args[i+2];
	if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
	obj.visibility=v; }
}

function startRemoteTextEditorPreview(url) {
  remoteWin = window.open("","PreviewerRemote",'height=600,width=810,location=no,scrollbars=yes,menubars=yes,toolbars=yes,resizable=yes,status=yes');
  if (remoteWin != null) {
    remoteWin.opener = self;
    remoteWin.location = url;
  }
}

function validateDeclineSelected ( )
{
    valid = true;

    if ( document.moditem.reject_reason.value == "NULL")
    {
        alert ( "Please select a reason for declining this item!" );
        valid = false;
    }

    return valid;
}

function validateStatusSelected()
{
    valid = true;

    if (document.profileitem.change_status.value == "NULL")
    {
        alert ("Please select a status for this item!");
        valid = false;
    }

    return valid;
}

/* Copy to clipboard */
function ClipBoard(mytext)
{
	if (window.clipboardData)
	{
		window.clipboardData.setData("Text", mytext);
	} else if (window.netscape)  { 
		netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
		if (!clip) return;

		var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
		if (!trans) return;   

		trans.addDataFlavor('text/unicode');

		var str = new Object();
		var len = new Object();
		var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
		var copytext=mytext;
		str.data=copytext;
		trans.setTransferData("text/unicode",str,copytext.length*2);
		var clipid=Components.interfaces.nsIClipboard;
		if (!clip) return false;
		clip.setData(trans,null,clipid.kGlobalClipboard);
	}
	/* alert("Following info was copied to your clipboard:\n\n" + mytext); */
	return false;
}

/* -------------------------------------------------------------------------------------------------------------- */

function confirmDelete(msg)
{
    return confirm(msg);
}
