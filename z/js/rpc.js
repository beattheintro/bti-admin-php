/*
	// ----------------------------------------------------------- >>>>>>>>>>
	// Filename : rpc.js
	// Author: Jason Rastrick
	// Date: 21/12/2007
	// Version: 1.0
	// Description: XML HTTP Remote Procedure Calls (AJAX)
	// ----------------------------------------------------------- >>>>>>>>>>
*/

var ajax = new httpreq();
var contentObj;
var tplObj;
var contentListObj;

function getImage(sel,id)
{
	var img = sel.options[sel.selectedIndex].value;
	contentObj = document.getElementById(id);
	ajax.requestFile = 'rpc/img.php?img='+img;	// Specifying which file to get
	ajax.onCompletion = viewImage;	// Specify function that will be executed after file has been found
	ajax.runAJAX();		// Execute AJAX function
}

function viewImage(id)
{
	var obj = document.getElementById(id);
	contentObj.innerHTML = ajax.response;	// Executing the response from Ajax as Javascript code
}

function getFile(fileName,divID)
{
	tplObj = document.getElementById(divID);
	ajax.requestFile = fileName;	// Specifying which file to get
	ajax.onCompletion = showContent;	// Specify function that will be executed after file has been found
	ajax.runAJAX();		// Execute AJAX function
}

function showContent(divID)	// Displaying content in the content <div>
{
	tplObj.innerHTML = ajax.response;	// ajax.response is a variable that contains the content of the external file	
}

function getCategoryList(sel)
{
	var cat_id = sel.options[sel.selectedIndex].value;
	document.getElementById('sub_cat_id').options.length = 0;	// Empty sub_cat_id select box
	if(cat_id.length>0){
		ajax.requestFile = 'rpc/sub.php?cat_id='+cat_id;	// Specifying which file to get
		ajax.onCompletion = getSubCategoryList;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		// Execute AJAX function
	}
}

function getSubCategoryList()
{
	var obj = document.getElementById('sub_cat_id');
	eval(ajax.response);	// Executing the response from Ajax as Javascript code	
}

function getContentListNoEdit(sel,divID)
{
	var sub_cat_id = sel.options[sel.selectedIndex].value;
	contentListObj = document.getElementById(divID);
	if(sub_cat_id.length>0){
		var postVar = "sub_cat_id=" +sub_cat_id;
		ajax.requestFile = 'rpc/contne.php';	// Specifying which file to get
		ajax.onCompletion = showContentList;	// Specify function that will be executed after file has been found
		ajax.method = 'POST';
		ajax.runAJAX(postVar);		// Execute AJAX function
	}
}

function getContentList(sel,divID)
{
	var sub_cat_id = sel.options[sel.selectedIndex].value;
	contentListObj = document.getElementById(divID);
	if(sub_cat_id.length>0){
		var postVar = "sub_cat_id=" +sub_cat_id;
		ajax.requestFile = 'rpc/cont.php';	// Specifying which file to get
		ajax.onCompletion = showContentList;	// Specify function that will be executed after file has been found
		ajax.method = 'POST';
		ajax.runAJAX(postVar);		// Execute AJAX function
	}
}

function showContentList(divID)	// Displaying content in the content <div>
{
	contentListObj.innerHTML = ajax.response;	// ajax.response is a variable that contains the content of the external file	
}

